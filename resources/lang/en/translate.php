<?php

return [
    'add_successfully'      => 'Item added successfully',
    'add_failed'            => 'Item not added',
    'updated_successfully'  => 'Item updated successfully',
    'update_failed'         => 'Item not updated',
    'deleted_successfully'  => 'Item deleted successfully',
    'delete_failed'         => 'Item not deleted',
    'failed'                => 'Operation Faild',
    'InternalServerErrorTryLater'=>'Server Error , Pelase Check',
    'cant_be_nok'           => 'This Nok has been assigned before'
];
