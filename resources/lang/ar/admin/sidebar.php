<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin SideBar Langs
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'dashboard'         => 'الرئيسية',
    'settings'          => 'اعدادات المنتجات',
    'categories'        => 'الاقسام',
    'sub_category'      =>'الاقسام الفرعية',
    'users'              => 'المستخدمين',
    'products'           =>'المنتجات',
];
