@extends('admin.layouts.master')
@section('title','Subjects')
@push('styles')
    <link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endpush

@section('content')

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    الاقسام الفرعية
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="#" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                           إضافة قسم فرعي
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_3">
                <thead>
                <tr>
                    <th>رقم تسلسلي</th>
                    <th>الاسم</th>
                    <th>القسم الرئيسي</th>
                    <th>اجراءات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($subcategories as $catgory)
                    <tr>
                        <td>{{$catgory->id}}</td>
                        <td>{{$catgory->name}}</td>
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <a href="{{route('categories.edit',$catgory->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                                        <i class="la la-edit"></i>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-md-6">
                                        <form action="{{route('categories.destroy',$catgory->id)}}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="delete">
                                            <button type="submit" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"><i class="la la-remove"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>

@endsection

@push('scripts')
    <!--begin::Page Vendors(used by this page) -->
    <script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>

    <!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
    <script src="{{asset('assets/js/pages/crud/datatables/data-sources/html.js')}}" type="text/javascript"></script>

    <!--end::Page Scripts -->

@endpush
