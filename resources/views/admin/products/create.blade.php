@extends('admin.layouts.master')
@section('title','اضافة منتج')
@push('styles')
@endpush

@section('content')

<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                اضافة منتج
            </h3>
        </div>
    </div>

    <!--begin::Form-->
    <form class="kt-form kt-form--label-right">
        <div class="kt-portlet__body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>اسم المنتج:</label>
                    <input type="email" class="form-control" placeholder="ادخل اسم المنتج">
                    <span class="form-text text-muted">ادخل اسم المنتج وسيتم عرضة للهاتف</span>
                </div>
                <div class="col-lg-6">
                    <label class="col-3 col-form-label">حالة المنتج</label>
                    <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                        <label>
                            <input type="checkbox" checked="checked" name="">
                            <span></span>
                        </label>
                    </span>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>القسم الرئيسي</label>
                    <div class="col-lg-12">
                        <select class="form-control kt-select2" id="kt_select2_1" name="param">
                            <option value="AK">ايفون</option>
                            <option value="HI">سامسونج</option>
                            <option value="CA">هواوي</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label>القسم الفرعي</label>
                    <div class="col-lg-12">
                        <select class="form-control kt-select2" id="kt_select2_1" name="param">
                            <option value="AK">11 برو ماكس</option>
                            <option value="HI">12</option>
                            <option value="CA">iphone SE</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div id="kt_repeater_1">
                        <div class="form-group form-group-last row" id="kt_repeater_1">
                            <label class="col-lg-4 col-form-label" style="text-align: right">الوان المنتج:</label>
                            <div data-repeater-list="" class="col-lg-12">
                                <div data-repeater-item class="form-group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="kt-form__group--inline">
                                            <div class="kt-form__control">
                                                <input type="color" class="form-control" placeholder="Enter full name">
                                            </div>
                                        </div>
                                        <div class="d-md-none kt-margin-b-10"></div>
                                    </div>
                                    <div class="col-md-4">
                                        <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                            <i class="la la-trash-o"></i>
                                            حذف
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-group-last row">
                            <label class="col-lg-2 col-form-label" style="text-align: right"></label>
                            <div class="col-lg-4">
                                <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                                    <i class="la la-plus"></i> اضافة لون اخر
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">

                    <div id="kt_repeater_2">
                        <div class="form-group form-group-last row" id="kt_repeater_1">
                            <label class="col-lg-4 col-form-label" style="text-align: right">مساحات المنتج:</label>
                            <div data-repeater-list="" class="col-lg-12">
                                <div data-repeater-item class="form-group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="kt-form__group--inline">
                                            <div class="kt-form__control">
													<select class="form-control" id="exampleSelectd">
														<option>32 GB</option>
														<option>64 GB</option>
														<option>128 GB</option>
														<option>265 GB</option>
														<option>512 GB</option>
													</select>
												</div>
                                        </div>
                                        <div class="d-md-none kt-margin-b-10"></div>
                                    </div>
                                    <div class="col-md-4">
                                        <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                            <i class="la la-trash-o"></i>
                                            حذف
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-group-last row">
                            <label class="col-lg-2 col-form-label" style="text-align: right"></label>
                            <div class="col-lg-4">
                                <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                                    <i class="la la-plus"></i> اضافة مساحة اخرى
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-6">
                        <button type="reset" class="btn btn-primary">حفظ</button>
                        <button type="reset" class="btn btn-secondary">الغاء</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!--end::Form-->
</div>
@endsection

@push('scripts')
    <script>
        $('#kt_select2_1, #kt_select2_1_validate').select2({
            placeholder: "Select a state"
        });
        $('#kt_repeater_1').repeater({
            initEmpty: false,
            defaultValues: {
                'text-input': 'foo'
            },
            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
        $('#kt_repeater_2').repeater({
            initEmpty: false,
            defaultValues: {
                'text-input': 'foo'
            },
            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    </script>
@endpush
