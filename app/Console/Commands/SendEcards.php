<?php

namespace App\Console\Commands;

use View;
use App\Library\ElasticMail;
use App\Models\ScheduledEcard;
use App\SendEcardApi\SendEcard;
use Illuminate\Console\Command;

class SendEcards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:eCards';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $ecards = ScheduledEcard::ReadyToSend()->get();
        foreach($ecards as $ecard)
        {
            $sender = $ecard->user->email;
            $sender_name = $ecard->user->first_name.' '.$ecard->user->last_name;

            $subject = $ecard->subject;
            $msgTo = $ecard->beneficiary->email;

            //Get Email Body
            $data['is_preview']=0;
            $data['isVideo']=($ecard->media->type == 'video') ? 1 :0;
            $data['media_link']=$ecard->media_link;
            $data['recipient_name'] = $ecard->beneficiary->first_name.' '.$ecard->beneficiary->last_name;
            $data['content'] = $ecard->message;
            $data['signature'] = $sender_name;

            $bodyHtml = view('emails.ecard_template',$data)->render();

            $send_eCard = new ElasticMail();

            $response = $send_eCard->send($sender,$sender_name, $subject, $msgTo, $bodyHtml);

            if ($response['success'] == true)
            {

                $transaction_id = $response['data']['transactionid'];
                $message_id = $response['data']['messageid'];

                $ecard->is_sent = true;
                $ecard->transaction_id = $transaction_id;
                $ecard->message_id = $message_id;
                $ecard->save();
            }


        }

    }
}
