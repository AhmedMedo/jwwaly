<?php
    /**
     * Developed by Ahmed Mahmoud.
     * Year: 2019
     */

    namespace App\Exceptions;

    use App\Traits\ApiResponser;
    use Exception;
    use Illuminate\Auth\Access\AuthorizationException;
    use Illuminate\Auth\AuthenticationException;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Database\QueryException;
    use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
    use Illuminate\Foundation\Http\Exceptions\MaintenanceModeException as MaintenanceModeExceptionAlias;
    use Illuminate\Http\JsonResponse;
    use Illuminate\Http\Request;
    use Illuminate\Validation\ValidationException;
    use Symfony\Component\Debug\Exception\FatalThrowableError;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpKernel\Exception\HttpException;
    use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
    use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
    use Zendesk\API\Exceptions\ApiResponseException;


    /**
     * Class Handler
     *
     * @package App\Exceptions
     */
    class Handler extends ExceptionHandler
    {
        use ApiResponser;

        /**
         * A list of the exception types that are not reported.
         *
         * @var array
         */
        protected $dontReport = [
            //
        ];

        /**
         * A list of the inputs that are never flashed for validation exceptions.
         *
         * @var array
         */
        protected $dontFlash = [
            'password',
            'password_confirmation',
        ];

        /**
         * Report or log an exception.
         *
         * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
         *
         * @param Exception $exception
         *
         * @return void
         * @throws Exception
         */
        public function report(Exception $exception)
        {
            // dd($exception->getMessage());
            if (app()->bound('sentry') && $this->shouldReport($exception)) {
                app('sentry')->captureException($exception);
            }
            parent::report($exception);
        }

        /**
         * Render an exception into an HTTP response.
         *
         * @param Request $request
         * @param Exception $exception
         *
         * @return Response
         */
        public function render($request, Exception $exception)
        {
            if ($exception instanceof MaintenanceModeExceptionAlias) {
                return response()
                    ->view('maintenance', [
                        'message' => 'Come back later.'
                    ], 200)
                    ->header('Content-Type', 'text/html; charset=utf-8');
            }

            // Validations Exception
            if ($exception instanceof ValidationException && $request->wantsJson())
            {
                return $this->convertValidationExceptionToResponse($exception, $request);
            }

            if ($exception instanceof ModelNotFoundException && $request->wantsJson()) {
                return $this->errorResponse(trans('translate.ModelNotExist'), 404);
            }


            if ($exception instanceof AuthenticationException && $request->wantsJson())
            {
                return $this->unauthenticated($request, $exception);
            }

            // permissions Exception
            if ($exception instanceof AuthorizationException && $request->wantsJson())
            {
                return $this->errorResponse(trans('translate.AuthException'), 403);
            }

            // Not Found Pages Exception
            if ($exception instanceof NotFoundHttpException && $request->wantsJson()) {
                return $this->errorResponse(trans('translate.SpecifiedURLCannotFound'), 404);
            }
            // Method Request Exception
            if ($exception instanceof MethodNotAllowedHttpException && $request->wantsJson()) {
                return $this->errorResponse(trans('translate.SpecifiedMethodRequestInvalid'), 405);
            }

            // General Http Exceptions
            if ($exception instanceof HttpException && $request->wantsJson()) {
                return $this->errorResponse($exception->getMessage(), $exception->getStatusCode());
            }

            if ($exception instanceof FatalThrowableError && $request->wantsJson()) {
                //return $this->errorResponse($exception->getMessage(),405);
                return $this->errorResponse(trans('translate.SpecifiedMethodRequestInvalid'), 405);
            }

            //Operation Error in DB as condition Or constriction relationship
            if ($exception instanceof QueryException && $request->wantsJson()) {
                $errorCode = $exception->errorInfo[1];
                //$sqlMsg = $exception->errorInfo[2];
                if ($errorCode == 1451) {
                    return $this->errorResponse(trans('translate.CannotRemoveResourceItRelated'), 409);
                }

                if ($exception->getCode() == 2002) {
                    return $this->errorResponse('No connection could be made because the target machine actively refused it', 503);
                }
                /*else{
                    //only for me to view & handel errors
                    //return $this->errorResponse('[Code] '.$errorCode.' [Msg] '.$sqlMsg,500);
                }*/
            }

            if ($exception instanceof ApiResponseException) {
                throw new HttpException(404, trans('translate.BadTypeClient'));
            }

            if ($exception instanceof ModelNotFoundException)
            {
                throw new HttpException(404, trans('translate.ModelNotExist'));
            }

            //Check if we in debug mode so view original Exception
            if ($this->isFronted($request))
            {
                return parent::render($request, $exception);
            }

            return $this->errorResponse(trans('translate.InternalServerErrorTryLater'), 500);
        }

        /**
         * Create a response object from the given validation exception.
         *
         * @param ValidationException $e
         * @param Request $request
         *
         * @return Response
         */
        protected function convertValidationExceptionToResponse(ValidationException $e, $request)
        {
            $errors = $e->validator->errors()->getMessages();
            if ($this->isFronted($request) && !$request->is('web/auth/login') && !$request->is('web/auth/login-otp')) {
                return redirect()->back()->withInput($request->input())->withErrors($errors);
            }
            return $this->errorResponse($errors, $e->status);
        }

        /**
         * Convert an authentication exception into a response.
         *
         * @param Request $request
         * @param AuthenticationException $exception
         *
         * @return JsonResponse
         */
        protected function unauthenticated($request, AuthenticationException $exception)
        {
            if ($this->isFronted($request))
            {
                return redirect()->route('admin.showLoginForm');
            }

            return $this->errorResponse(trans('translate.Unauthenticated'), 401);
        }

        //check if it from auth:admin or api method

        /**
         * @param $request
         *
         * @return bool
         */
        private function isFronted($request)
        {

            if ($request->acceptsHtml() && $request->route()) {
                return $request->acceptsHtml() && ($request->route() ? collect($request->route()->middleware())->contains('IsAdmin') || collect($request->route()->middleware())->contains('web') : false);
            }
            return $request->acceptsHtml();
        }

    }
