<?php

namespace App\Emails;

use Illuminate\Support\Facades\Log;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use App\Library\ElasticMail;
use View;
class ForgetPasswordEmail
{
    protected $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function send()
    {
        $data['user'] = $this->user;
        $view = view('emails.forget_password',$data)->render();
        $elasticMail = new ElasticMail();
        $elasticMail->send('noreply@ewysh.com','Ewysh','Forget Password',$this->user->email,$view);

    }


}
