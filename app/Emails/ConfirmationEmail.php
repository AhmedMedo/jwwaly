<?php

namespace App\Emails;

use Illuminate\Support\Facades\Log;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use App\Library\ElasticMail;
use View;
class ConfirmationEmail
{
    protected $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function send()
    {
        $data['user'] = $this->user;
        $view = view('emails.confirmation_email',$data)->render();
        $elasticMail = new ElasticMail();
        $elasticMail->send('noreply@ewysh.com','Ewysh','Confirmation Email',$this->user->email,$view);

    }


}
