<?php

namespace App\Emails;

use Illuminate\Support\Facades\Log;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use App\Library\ElasticMail;
use View;
class NextOfKinMail
{
    protected $user;
    protected $next_of_kin;

    public function __construct($user , $next_of_kin)
    {
        $this->user = $user;

        $this->next_of_kin =$next_of_kin;
    }

    public function send()
    {
        $data['next_of_kin'] = $this->next_of_kin;
        $data['user'] = $this->user;
        $data['password'] = 'afterlife@2021';
        $view = view('emails.next_of_kin',$data)->render();
        $elasticMail = new ElasticMail();
        $elasticMail->send('noreply@ewysh.com','Ewysh','Next Of Kin',$this->next_of_kin->email,$view);

    }


}
