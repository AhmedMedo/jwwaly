<?php
    namespace App\Http\Middleware;

    use Closure;
    use Illuminate\Http\Request;
    use Illuminate\Validation\ValidationException;

    /**
     * Class TransformInput
     *
     * @package App\Http\Middleware
     */
    class TransformInput
    {
        /**
         * Handle an incoming request.
         *
         * @param Request $request
         * @param Closure $next
         *
         * @return mixed
         */
        public function handle($request, Closure $next, $transformer)
        {
            //Change the transformer input to get original field to do save in db successfully
            $transformedInput = [];

            foreach ($request->all() as $input => $value) {
                $transformedInput[$transformer::originalAttribute($input)] = $value;
            }

            $request->replace($transformedInput);
            $response = $next($request);
            //Return to transform name when in errors like validation or others
            if (isset($response->exception) && $response->exception instanceof ValidationException) {
                $data = $response->getData();
                $transformedError = [];
                foreach ($data->errors as $field => $error) {
                    $transformField = $transformer::transformerAttribute($field);
                    $transformedError[$transformField] = str_replace($field, $transformField, $error);
                }
                $data->errors = $transformedError;
                $response->setData($data);
            }
            return $response;
        }
    }
