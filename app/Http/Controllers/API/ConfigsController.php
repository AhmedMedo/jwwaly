<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiController;
use App\Models\City;
use App\Models\Country;
use App\Models\Package;
use App\Models\Relation;
use Illuminate\Http\Request;

class ConfigsController extends ApiController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function listPackages()
    {
        return $this->showAllNoPaginate(Package::IsActive()->get());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function listRelations()
    {
        return $this->showAllNoPaginate(Relation::all());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function listCountries()
    {
        return $this->showAllNoPaginate(Country::all());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function listCities(Request $request)
    {
        return $this->showAllNoPaginate(City::whereCountryId($request->input('country_id'))->get());
    }
}
