<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;

/**
 * Class ApiController
 *
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{
    use ApiResponser;


    /**
     * ApiController constructor.
     */
    public function __construct()
    {

    }
}
