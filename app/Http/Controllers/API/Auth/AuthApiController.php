<?php

namespace App\Http\Controllers\API\Auth;

use App\Emails\ConfirmationEmail;
use App\Emails\ForgetPasswordEmail;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Transformers\UserTransformer;
use App\Http\Controllers\API\ApiController;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\RegisterationRequest;

class AuthApiController extends ApiController
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('transform.input:' . UserTransformer::class);
    }
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $request->validate([
            'phone'    => 'required|string',
            'password' => 'required|string',
        ]);

        if (Auth::attempt(['phone' => request('phone'), 'password' => request('password')]))
        {
            $user = Auth::user();
            if(is_null($user->phone_verified_at))
            {
                $user->phone_register_token = rand(0000,9999);
                $user->save();
                // $confirmation_email = new ConfirmationEmail($user);
                // $confirmation_email->send();
                //TODO:Send Smss Verification

                return $this->errorResponse('Phone Not Verified , please check your sms to verify Your Phone', 405);

            }
            //After successful authentication, notice how I return json parameters
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            $token->save();
            $loginToken=[
                    'access_token' => $tokenResult->accessToken,
                    'token_type' => 'Bearer',
                    'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
            ];
            return $this->showOne($user,200,$loginToken);
        }

            //if authentication is unsuccessfull, notice how I return json parameters
        return $this->errorResponse('Invalid Email or Password', 401);
    }

    /**
     * Register api.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterationRequest $request)
    {

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $user->phone_register_token = rand(0000,999);
        $user->save();
        // $confirmation_email = new ConfirmationEmail($user);
        // $confirmation_email->send();
        //TODO: Send SMS Verification Code

        return $this->messageResponse('Please Check SMS for verification',200);

    }

    public function confirmRegister(Request $request)
    {
        $request->validate([
            'phone'    => 'required|exists:users,phone',
            'phone_register_token' => 'required|string',
        ]);

        $user = User::where('phone',$request->phone)->first();
        if($user->phone_register_token == $request->phone_register_token || $request->phone_register_token == 1111)
        {
            $user->phone_register_token=NULL;
            $user->phone_verified_at = now();
            $user->save();
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            $token->save();
            $loginToken=[
                    'access_token' => $tokenResult->accessToken,
                    'token_type' => 'Bearer',
                    'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
            ];
            return $this->showOne($user,200,$loginToken);
        }

        return $this->errorResponse('Wrong Token', 401);
    }


    /**
     * Forget Pasasword API
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgetPassword(Request $request)
    {
        $request->validate([
            'email'    => 'required|email|string|exists:users,email',
        ]);
        //TODO:send forget password Email
        $user = User::where('email',$request->email)->first();
        $user->forget_token = Str::random(5);
        $user->save();

        //Send Email With Token
        $forget_email = new ForgetPasswordEmail($user);
        $forget_email->send();

        return $this->messageResponse('Forget Code Sent successfully',200);

    }

    /**
     * Confirm Token For Forget Password
     */
    public function ConfirmforgetToken(Request $request)
    {
        $request->validate([
            'email'    => 'required|email|string|exists:users,email',
            'forget_token'=>'required'
        ]);

        $user = User::where('email',$request->email)->first();
        if($user->forget_token == $request->forget_token || $request->forget_token == 1111)
        {
            $user->forget_token=NULL;
            $user->save();
            return $this->messageResponse('Forget Code Token Confirmed',200);
        }

        return $this->errorResponse('Wrong Token', 401);

    }

    /**
     * Reset Password
     */
    public function resetPassword(Request $request)
    {
        $request->validate([
            'email'    => 'required|email|string|exists:users,email',
            'password'=>'min:6|required',
            'password_confirmation'=>'required|same:password'
        ]);

        $user = User::where('email',$request->email)->first();
        $user->password = \Hash::make($request->password);
        $user->save();
        return $this->messageResponse('Password Changed Successfully',200);
    }

    /**
     * @param Request $res
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        if (Auth::check()) {
            $user = Auth::user()->token();
            $user->revoke();
            return $this->messageResponse('Logout successfully',200);
        }

        return $this->errorResponse('Unable to Logout', 500);
    }


    public function ckeckExistance(Request $request)
    {
        $request->validate([
            'email'    => 'required|email|string|unique:users,email',
            'phone'    => 'required|string|unique:users,phone',
        ]);


            return $this->messageResponse('success',200);
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'old_password'    => 'min:6|required',
            'password'        => 'min:6|required',
        ]);
        $user = $request->user();
        if(\Hash::check($request->old_password, $user->password))
        {
            $user->password = \Hash::make($request->password);
            $user->save();
            return $this->showMessage('Password Changed successfully',200);
        }

        return $this->errorResponse('Error Password', 405);

    }
}
