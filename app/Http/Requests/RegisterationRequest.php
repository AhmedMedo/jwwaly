<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\ApiResponser;

class RegisterationRequest extends FormRequest
{

    use ApiResponser;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'phone'         => 'required|unique:users,phone',
            'email'         => 'required|email|unique:users,email',
            'country_id'    => 'required|integer',
            'city_id'       => 'required|integer',
            'password'      =>'min:6|required',
            'password_confirmation'=>'required|same:password',
            'type'          => 'required|in:user,store'
        ];
    }

}
