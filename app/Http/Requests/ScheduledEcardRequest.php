<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ScheduledEcardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'beneficiary_id' => 'required|exists:beneficiaries,id',
            'media_link'     => 'required|url',
            'sending_date' => 'date_format:Y-m-d',
            'subject'      => 'required',
            'message'      => 'required'
        ];
    }
}
