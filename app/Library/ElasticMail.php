<?php

namespace App\Library;

use Illuminate\Support\Facades\Log;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class ElasticMail
{
    protected $sender;

    protected $subject;

    protected $msgTo;

    protected $bodyHtml;

    protected $api_key;

    protected $base_uri;

    public function __construct()
    {
        $this->api_key = env('ELASTIC_MAIL_KEY');

        $this->base_uri = "https://api.elasticemail.com/";
    }

    public function send($sender, $sender_name, $subject, $msgTo, $bodyHtml)
    {

        $base_uri = $this->base_uri;

        $api_key = $this->api_key;

        $client = new \GuzzleHttp\Client(['base_uri' => $base_uri]);

        try {
            $apiRequest = $client->post('/v2/email/send', [
                'form_params'     => [
                    'apikey'      =>  $api_key,
                    'from'        =>  $sender,
                    'fromName'    =>  $sender_name,
                    'to'          =>  $msgTo,
                    'subject'     =>  $subject,
                    'bodyHtml'    =>  $bodyHtml,
                ],
            ]);


            $response_data = json_decode($apiRequest->getBody(), true);

            return $response_data;
        } catch (RequestException $e) {

            if ($e->hasResponse()) {
            }

            return \Response::json(array(
                'success'    => false,
                'message'    => 'Email sending failed!',
            ));
        }
    }
}
