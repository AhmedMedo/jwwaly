<?php
    namespace App\Traits;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Support\Collection;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Support\Facades\Validator;

    /**
     * Trait ApiResponser
     *
     * @package App\Traits
     */
    trait ApiResponser
    {

        /**
         * @param $data
         * @param $code
         *
         * @return \Illuminate\Http\JsonResponse
         */
        private function successResponse($data, $code)
        {
            return response()->json($data, $code);
        }

        /**
         * @param $message
         * @param $code
         *
         * @param null $httpCode
         * @return \Illuminate\Http\JsonResponse
         */
        protected function errorResponse($message, $code, $httpCode = null)
        {
            $codeH = !empty($httpCode)? $httpCode:$code;
            $errorMsg = "Validation Erros Please Check";
            if(is_array($message))
            {
                $errorMsg = array_values($message)[0][0];
            }
            return response()->json(['errors' => $message, 'code' => $code,'errorMsg'=>$errorMsg], $codeH);
        }

        /**
         * @param $message
         * @param $code
         *
         * @return \Illuminate\Http\JsonResponse
         */
        protected function messageResponse($message, $code)
        {
            return response()->json(['message' => $message, 'code' => $code], $code);
        }

        /**
         * @param Collection $collection
         * @param int $code
         *
         * @return \Illuminate\Http\JsonResponse
         * @throws \Illuminate\Validation\ValidationException
         */
        protected function showAll(Collection $collection, $code = 200)
        {
            if ($collection->isEmpty()) {
                return $this->successResponse(['entityList' => $collection], $code);
            }

            $transformer = $collection->first()->transformer;
            $collection = $this->filterData($collection, $transformer);
            $collection = $this->sortData($collection, $transformer);
            $collection = $this->paginate($collection);
            $collection = $this->transformData($collection, $transformer);
            $entity['entityList'] = $collection['data'];
            $entity['meta'] = $collection['meta'];
            $entity = $this->cacheResponse($entity);
            return $this->successResponse($entity, $code);
        }

        public function showAllNoPaginate(Collection $collection, $customTransformer = null, $code = 200)
        {
            if ($collection->isEmpty()) {
                return $this->successResponse(['entityList' => $collection], $code);
            }

            $transformer = (!empty($customTransformer)) ? $customTransformer : $collection->first()->transformer;
            if (request()->has('list')) {
                $transformer = new $transformer;
                $transformer->setTransView(request()->list);
            }
            $collection = $this->transformData($collection, $transformer);
            $entity['entityList'] = $collection['data'];
            $entity = $this->cacheResponse($entity);
            return $this->successResponse($entity, $code);
        }

        /**
         * @param Model $instance
         * @param int $code
         *
         * @return \Illuminate\Http\JsonResponse
         */
        protected function showOne(Model $instance, $code = 200,$extra=[])
        {

            $transformer = $instance->transformer;
            $instance = $this->transformData($instance, $transformer);
            if(count($extra))
            {
                $entity['entity'] = $instance['data']+ $extra;

            }else{

                $entity['entity'] = $instance['data'];
            }
            return $this->successResponse($entity, $code);
        }

        /**
         * @param $message
         * @param int $code
         *
         * @return \Illuminate\Http\JsonResponse
         */
        protected function showMessage($message, $code = 200)
        {
            return $this->successResponse(['data' => $message], $code);
        }

        /**
         * @param $message
         * @param int $code
         *
         * @return \Illuminate\Http\JsonResponse
         */
        protected function showEntity($data, $code = 200)
        {
            return $this->successResponse(['entity' => $data], $code);
        }

        /**
         * @param $message
         * @param int $code
         *
         * @return \Illuminate\Http\JsonResponse
         */
        protected function showEntityList($data, $code = 200)
        {
            return $this->successResponse(['entityList' => $data], $code);
        }

        /**
         * @param Collection $collection
         * @param $transformer
         *
         * @return Collection
         */
        private function sortData(Collection $collection, $transformer)
        {
            if (request()->has('sort_by')) {
                $attribute = $transformer::originalAttribute(request()->sort_by);
                //$collection = $collection->sortBy($attribute);
                $collection = $collection->sortBy->{$attribute};
            }elseif (request()->has('sort_by_desc')) {
                $attribute = $transformer::originalAttribute(request()->sort_by_desc);
                $collection = $collection->sortByDesc->{$attribute};
            }
            return $collection;
        }

        /**
         * @param Collection $collection
         * @param $transformer
         *
         * @return Collection
         */
        private function filterData(Collection $collection, $transformer)
        {
            foreach (request()->query() as $query => $value)
            {
                if(in_array($query,['sort_by','sort_by_desc','per_page','page'])) {
                    continue;
                }
                $attribute = $transformer::originalAttribute($query);
                if (isset($attribute, $value)) {
                    if (strpos(urldecode($value), ',') !== false) {
                        $dataArray = explode(',', urldecode($value));
                        $collection = $collection->whereIn($attribute, $dataArray);
                    } else {
                        $collection = $collection->where($attribute, $value);
                    }
                }
            }
            return $collection;
        }


        /**
         * @param Collection $collection
         *
         * @return LengthAwarePaginator
         * @throws \Illuminate\Validation\ValidationException
         */
        private function paginate(Collection $collection)
        {

            $rules = [
                'per_page' => 'integer|min:2|max:50'
            ];

            Validator::validate(request()->all(), $rules);

            $page = LengthAwarePaginator::resolveCurrentPage();
            $perPage = 15;

            if (request()->has('per_page')) {
                $perPage = (int)request()->get('per_page');
            }

            $results = $collection->slice(($page - 1) * $perPage, $perPage)->values();
            $paginated = new LengthAwarePaginator($results, $collection->count(), $perPage, $page, [
                'path' => LengthAwarePaginator::resolveCurrentPath()
            ]);

            // Add other data/parameters request in Url
            $paginated->appends(request()->all());

            return $paginated;
        }

        private function cacheResponse($data)
        {
            $url = request()->url();
            $queryParams = request()->query();

            ksort($queryParams);
            $queryString = http_build_query($queryParams);
            $fullUrl = "{$url}?{$queryString}";

            // time in mint so we need it in second (30/60) = 30 sec
            return Cache::remember($fullUrl, 30 / 60, function () use ($data) {
                return $data;
            });
        }

        /**
         * @param $data
         * @param $transformer
         *
         * @return array
         */
        /**
         * @param $data
         * @param $transformer
         *
         * @return array
         */
        private function transformData($data, $transformer)
        {
            $transformation = fractal($data, $transformer);
            return $transformation->toArray();
        }
    }
