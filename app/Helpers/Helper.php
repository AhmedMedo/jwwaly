<?php
namespace App\Helpers;

use Illuminate\Support\Str;

class Helper
{

    public static function convertFromBytes($bytes,$to='M',$decimal_places=1)
    {
        $formulas = array(
            'K' => number_format($bytes / 1024, $decimal_places),
            'M' => number_format($bytes / 1048576, $decimal_places),
            'G' => number_format($bytes / 1073741824, $decimal_places)
        );
        return isset($formulas[$to]) ? $formulas[$to] : 0;

    }


    public static function getFileType($extension)
    {
        $type = "not found";
        $images_extenions = ['jpeg','jpg','png','tiff','gif'];
        $videos_extensions = ['mp4','mov','ogg','avi','wmv','3gp','webm'];
        $audio_extensions = ['m4a','flac','mp3','wav','pcm','aiff','mpga'];
        $document_extensions = ['pdf','docx','doc','xlsx','csv'];

        if(in_array($extension,$images_extenions))
        {
            $type="image";

        }elseif(in_array($extension,$videos_extensions))
        {
            $type = "video";

        }elseif(in_array($extension,$audio_extensions))
        {
            $type = "audio";

        }elseif(in_array($extension,$document_extensions))
        {
            $type = "document";

        }

        return $type;
    }


    public static function getFileExtension($type)
    {
        $extension = 'png';
        switch ($type) {
            case 'video/webm;codecs="vp8,opus':
            case 'video/mp4':
                # code...
                $extension ='mp4';
                break;
            case 'application/pdf':
                $extension = 'pdf';
                break;
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                # code...
                $extension = 'xlsx';
                break;
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                # code...
                $extension = 'docx';
                break;
            default:
                # code...
                $extension = 'png';
                break;
        }

        return $extension;

    }


}
