<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class AwsStorage{

    protected $base_uri;

    public function __construct()
    {
        $this->base_uri = 'https://s3.' . env('AWS_DEFAULT_REGION') . '.amazonaws.com/' . env('AWS_BUCKET') . '/';
    }

    public static function base_url()
    {
        return 'https://s3.' . env('AWS_DEFAULT_REGION') . '.amazonaws.com/' . env('AWS_BUCKET') . '/';
    }

    public static function store($file ,$path ,$item_id=null,$extension)
    {
        $name = $item_id . '_' . time() . '.' . $extension;
        $filePath = $path.$name;
        Storage::disk('s3')->put($filePath, file_get_contents($file));
        return $name;
    }

    public static function storeBinary($file ,$path ,$name=null)
    {
        $filePath = $path.$name;
        if(env('APP_ENV')=='local'){
            $file->move($path, $name);
        }else{
            Storage::disk('s3')->put($filePath,$file);
        }
        return true;
    }


    public static function exists($file ,$path)
    {
        $exists = Storage::disk('s3')->exists($path.$file);
        return $exists;
    }


    public static function getUrl($path ,$file)
    {
       // return url($path.$file);
        $exists = Storage::disk('s3')->exists($path.$file);
        if($exists){
            return self::base_url().$path.$file;
        }else{
            return url($path.$file);
        }

    }

    public static function destroy($image ,$path=null)
    {
        // if(env('APP_ENV')=='local'){
        // if (file_exists(public_path().'/'.$path.$image)) {
        //     $delete =unlink(public_path().'/'.$path.$image);
        // }else{
        //     $delete=false;
        // }
        // }else{
        //     $delete =  Storage::disk('s3')->delete($path.$image);
        // }

        $delete =  Storage::disk('s3')->delete($path.$image);

        if($delete){
            return true;
        }
         return false;
    }




}
