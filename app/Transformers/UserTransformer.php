<?php

namespace App\Transformers;

use Carbon\Carbon;
use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    public function __construct($typeView = 'A')
    {
        $this->transView = $typeView;
    }
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        switch ($this->transView) {
            case 'compo':
                # code...
                $dataReturn = [
                    'Identifier' => $user->id,
                    'Name'       => $user->first_name.' '.$user->last_name
                ];
                break;

            default:

                $dataReturn= [
                    'Identifier'        => $user->id,
                    'FirstName'         => $user->first_name,
                    'LastName'          => $user->last_name,
                    'Phone'             => $user->phone,
                    'Email'             => $user->email,
                    'CountryID'         => $user->country_id,
                    'CityID'            => $user->city_id,
                    'Country'           => is_object($user->country) ? $user->country->name : "",
                    'City'              => is_object($user->city) ? $user->city->name : "",
                    'Status'            => $user->status,
                    'Type'              => $this->type,
                    'MemberSince'       => Carbon::parse($user->created_at)->translatedFormat('jS F, Y'),
                    'RegisteredAt'      => $user->created_at->diffForHumans(),
                ];
                break;
        }

        return $dataReturn;

    }

    /**
     * @param $key
     *
     * @return mixed|null
     */
    public static function originalAttribute($key)
    {
        $newKey = explode('-', $key);
        $key = $newKey[0];

        $attribute = [
            'FirstName'     => 'first_name',
            'LastName'      => 'last_name',
            'Phone'         => 'phone',
            'Email'         => 'email',
            'Password'      =>'password',
            'CountryID'     => 'country_id',
            'CityID'        => 'city_id',
            'Address'       => 'address',
            'State'         => 'state',
            'PostalCode'    => 'postal_code',
            'StreetOne'     => 'street_one',
            'StreetTwo'     => 'street_two',
            'Type'          => 'type',
            'Status'        => 'status',
            'ForgetToken'   => 'forget_token',
            'EmailRegisterToken'   => 'email_register_token',
            'PasswordConfirmation'=>'password_confirmation',
            'OldPassword'       => 'old_password',
        ];

        return isset($attribute[$key]) ? $attribute[$key] : $key;
    }

    /**
     * @param $key
     *
     * @return mixed|null
     */
    public static function transformerAttribute($key)
    {
        $newKey = explode('.', $key);
        $key = $newKey[0];
        $index_error = (isset($newKey[1]) && !empty($newKey[1])) ? '[' . $newKey[1] . ']' : '';

        $attribute =[
            'first_name'     => 'FirstName',
            'last_name'      => 'LastName',
            'phone'          => 'Phone',
            'email'          => 'Email',
            'password'       => 'Password',
            'country_id'     => 'CountryID',
            'city_id'        => 'CityID',
            'address'        => 'Address',
            'state'          => 'State',
            'postal_code'    => 'PostalCode',
            'street_one'     => 'StreetOne',
            'street_two'     => 'StreetTwo',
            'status'         => 'Status',
            'type'           => 'Type',
            'forget_token'   => 'ForgetToken',
            'email_register_token'   => 'EmailRegisterToken',
            'password_confirmation'=>'PasswordConfirmation',
            'old_password'         => 'OldPassword'

        ];
        return isset($attribute[$key]) ? $attribute[$key] . $index_error : null;
    }
}
