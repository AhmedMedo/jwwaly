<?php

namespace App\Transformers;

use App\Models\Country;
use App\Models\FamilyMember;
use Illuminate\Support\Carbon;
use League\Fractal\TransformerAbstract;

class CountryTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Country $country)
    {
        return [
            'Identifier'    => $country->id,
            'Name'          => $country->name,
            'Code'          => $country->code,
            'IsoCode'       => $country->iso_code,
            'IsActive'      => $country->is_active,
        ];
    }

    /**
     * @param $key
     *
     * @return mixed|null
     */
    public static function originalAttribute($key)
    {
        $newKey = explode('.', $key);
        $key = $newKey[0];

        $attribute = [
            'Identifier'    => 'id',
            'Name'          => 'name',
            'Code'          => 'code',
            'IsoCode'       => 'iso_code',
            'IsActive'      => 'is_active',
        ];

        return isset($attribute[$key]) ? $attribute[$key] : $key;
    }
}
