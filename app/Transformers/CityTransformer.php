<?php

namespace App\Transformers;

use App\Models\City;
use App\Models\FamilyMember;
use Illuminate\Support\Carbon;
use League\Fractal\TransformerAbstract;

class CityTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(City $city)
    {
        return [
            'Identifier'    => $city->id,
            'Name'          => $city->name,
            'CountryID'     => $city->country_id,
            'IsActive'      => $city->is_active,
        ];
    }

    /**
     * @param $key
     *
     * @return mixed|null
     */
    public static function originalAttribute($key)
    {
        $newKey = explode('.', $key);
        $key = $newKey[0];

        $attribute = [
            'Identifier'    => 'id',
            'Name'          => 'name',
            'CountryID'     => 'country_id',
            'IsActive'      => 'is_active',
        ];

        return isset($attribute[$key]) ? $attribute[$key] : $key;
    }
}
