<?php

use App\Library\ElasticMail;
use App\Models\ScheduledEcard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
 })->name('home');

 Route::get('/login', function () {

    return view('adminlte::auth.login');
 })->name('home');
Route::get('/payment/{id}', function ($id) {
   return view('payment',compact('id'));
});

Route::get('payment-status','Payment\PaymentController@status')->name('status');



Route::group(['prefix'=>'admin','namespace'=>'Admin'],function (){
    //Auth Routes For Admin
    Route::get('login','Auth\LoginController@showLoginForm')->name('admin.showLogin');
    Route::post('login','Auth\LoginController@login')->name('admin.login');
    Route::group(['middleware' =>'admin'],function(){
        Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');
        Route::get('/','DashboardController@index')->name('admin.index');
        Route::resource('users', 'UsersController');
        Route::resource('categories', 'CategoriesController');
        Route::resource('subcategories', 'SubCategoriesController');
        Route::resource('products', 'ProductsController');


    });
});
