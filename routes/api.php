<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('countries', 'API\ConfigsController@listCountries');
Route::get('cities', 'API\ConfigsController@listCities');

Route::group(['namespace' => 'API'], function () {


    Route::group(['namespace' => 'Auth'], function () {

        Route::post('/login', 'AuthApiController@login');
        Route::post('/register', 'AuthApiController@register');
        Route::post('/check-exist', 'AuthApiController@ckeckExistance');
        Route::post('/confirm-register', 'AuthApiController@confirmRegister');
        Route::post('/forget-password', 'AuthApiController@forgetPassword');
        Route::post('/confirm-forget-token', 'AuthApiController@ConfirmforgetToken');
        Route::post('/reset-password', 'AuthApiController@resetPassword');

    });


    Route::middleware('auth:api')->group(function () {

        Route::group(['namespace' => 'Auth'], function () {
            Route::get('/logout', 'AuthApiController@logout');
            Route::post('/change-password', 'AuthApiController@changePassword');

        });



    });


});
