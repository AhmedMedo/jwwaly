<?php

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Admin::firstOrCreate([
            'email' =>'admin@jawally.com'
        ],[
            'name'     => 'admin',
            'email'    => 'admin@jawally.com',
            'password' => \Hash::make('12345678')
        ]);
    }
}
