<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('countries')->delete();
        
        \DB::table('countries')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Afghanistan',
                'code' => '93',
                'iso_code' => 'AF',
                'is_active' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Albania',
                'code' => '355',
                'iso_code' => 'AL',
                'is_active' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Algeria',
                'code' => '213',
                'iso_code' => 'DZ',
                'is_active' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'American Samoa',
                'code' => '1684',
                'iso_code' => 'AS',
                'is_active' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Andorra',
                'code' => '376',
                'iso_code' => 'AD',
                'is_active' => 1,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Angola',
                'code' => '244',
                'iso_code' => 'AO',
                'is_active' => 1,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Anguilla',
                'code' => '1264',
                'iso_code' => 'AI',
                'is_active' => 1,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Antarctica',
                'code' => '672',
                'iso_code' => 'AQ',
                'is_active' => 1,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Antigua & Barbuda',
                'code' => '1268',
                'iso_code' => 'AG',
                'is_active' => 1,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Argentina',
                'code' => '54',
                'iso_code' => 'AR',
                'is_active' => 1,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Armenia',
                'code' => '374',
                'iso_code' => 'AM',
                'is_active' => 1,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Aruba',
                'code' => '297',
                'iso_code' => 'AW',
                'is_active' => 1,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Australia',
                'code' => '61',
                'iso_code' => 'AU',
                'is_active' => 1,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Austria',
                'code' => '43',
                'iso_code' => 'AT',
                'is_active' => 1,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Azerbaijan',
                'code' => '994',
                'iso_code' => 'AZ',
                'is_active' => 1,
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Bahamas',
                'code' => '1242',
                'iso_code' => 'BS',
                'is_active' => 1,
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Bahrain',
                'code' => '973',
                'iso_code' => 'BH',
                'is_active' => 1,
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Bangladesh',
                'code' => '880',
                'iso_code' => 'BD',
                'is_active' => 1,
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Barbados',
                'code' => '1246',
                'iso_code' => 'BB',
                'is_active' => 1,
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Belarus',
                'code' => '375',
                'iso_code' => 'BY',
                'is_active' => 1,
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Belgium',
                'code' => '32',
                'iso_code' => 'BE',
                'is_active' => 1,
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Belize',
                'code' => '501',
                'iso_code' => 'BZ',
                'is_active' => 1,
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Benin',
                'code' => '229',
                'iso_code' => 'BJ',
                'is_active' => 1,
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Bermuda',
                'code' => '1441',
                'iso_code' => 'BM',
                'is_active' => 1,
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Bhutan',
                'code' => '975',
                'iso_code' => 'BT',
                'is_active' => 1,
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Bolivia',
                'code' => '591',
                'iso_code' => 'BO',
                'is_active' => 1,
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'Bosnia-Herzegovina',
                'code' => '387',
                'iso_code' => 'BA',
                'is_active' => 1,
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Botswana',
                'code' => '267',
                'iso_code' => 'BW',
                'is_active' => 1,
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'Brazil',
                'code' => '55',
                'iso_code' => 'BR',
                'is_active' => 1,
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'British Virgin Islands',
                'code' => '1284',
                'iso_code' => 'VG',
                'is_active' => 1,
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'Brunei',
                'code' => '673',
                'iso_code' => 'BN',
                'is_active' => 1,
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'Bulgaria',
                'code' => '359',
                'iso_code' => 'BG',
                'is_active' => 1,
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'Burkina Faso',
                'code' => '226',
                'iso_code' => 'BF',
                'is_active' => 1,
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'Burundi',
                'code' => '257',
                'iso_code' => 'BI',
                'is_active' => 1,
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'Cambodia',
                'code' => '855',
                'iso_code' => 'KH',
                'is_active' => 1,
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'Cameroon',
                'code' => '237',
                'iso_code' => 'CM',
                'is_active' => 1,
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'Canada',
                'code' => '1',
                'iso_code' => 'CA',
                'is_active' => 1,
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'Cape Verde',
                'code' => '238',
                'iso_code' => 'CV',
                'is_active' => 1,
            ),
            38 => 
            array (
                'id' => 39,
                'name' => 'Cayman Islands',
                'code' => '1345',
                'iso_code' => 'KY',
                'is_active' => 1,
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'Central African Republic',
                'code' => '236',
                'iso_code' => 'CF',
                'is_active' => 1,
            ),
            40 => 
            array (
                'id' => 41,
                'name' => 'Chad',
                'code' => '235',
                'iso_code' => 'TD',
                'is_active' => 1,
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'Chile',
                'code' => '56',
                'iso_code' => 'CL',
                'is_active' => 1,
            ),
            42 => 
            array (
                'id' => 43,
                'name' => 'China',
                'code' => '86',
                'iso_code' => 'CN',
                'is_active' => 1,
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'Christmas Island',
                'code' => '61',
                'iso_code' => 'CX',
                'is_active' => 1,
            ),
            44 => 
            array (
                'id' => 45,
            'name' => 'Cocos (Keeling), Islands',
                'code' => '61',
                'iso_code' => 'CC',
                'is_active' => 1,
            ),
            45 => 
            array (
                'id' => 46,
                'name' => 'Colombia',
                'code' => '57',
                'iso_code' => 'CO',
                'is_active' => 1,
            ),
            46 => 
            array (
                'id' => 47,
                'name' => 'Comoros',
                'code' => '269',
                'iso_code' => 'KM',
                'is_active' => 1,
            ),
            47 => 
            array (
                'id' => 48,
                'name' => 'Congo',
                'code' => '242',
                'iso_code' => 'CG',
                'is_active' => 1,
            ),
            48 => 
            array (
                'id' => 49,
                'name' => 'Congo Democratic Republic',
                'code' => '243',
                'iso_code' => 'CD',
                'is_active' => 1,
            ),
            49 => 
            array (
                'id' => 50,
                'name' => 'Cook Islands',
                'code' => '682',
                'iso_code' => 'CK',
                'is_active' => 1,
            ),
            50 => 
            array (
                'id' => 51,
                'name' => 'Costa Rica',
                'code' => '506',
                'iso_code' => 'CR',
                'is_active' => 1,
            ),
            51 => 
            array (
                'id' => 52,
                'name' => 'Cote d’Ivoire',
                'code' => '225',
                'iso_code' => 'CI',
                'is_active' => 1,
            ),
            52 => 
            array (
                'id' => 53,
                'name' => 'Croatia',
                'code' => '385',
                'iso_code' => 'HR',
                'is_active' => 1,
            ),
            53 => 
            array (
                'id' => 54,
                'name' => 'Cuba',
                'code' => '53',
                'iso_code' => 'CU',
                'is_active' => 1,
            ),
            54 => 
            array (
                'id' => 55,
                'name' => 'Curacao',
                'code' => '599',
                'iso_code' => 'CW',
                'is_active' => 1,
            ),
            55 => 
            array (
                'id' => 56,
                'name' => 'Cyprus',
                'code' => '357',
                'iso_code' => 'CY',
                'is_active' => 1,
            ),
            56 => 
            array (
                'id' => 57,
                'name' => 'Czech Republic',
                'code' => '420',
                'iso_code' => 'CZ',
                'is_active' => 1,
            ),
            57 => 
            array (
                'id' => 58,
                'name' => 'Denmark',
                'code' => '45',
                'iso_code' => 'DK',
                'is_active' => 1,
            ),
            58 => 
            array (
                'id' => 59,
                'name' => 'Diego Garcia',
                'code' => '246',
                'iso_code' => 'DG',
                'is_active' => 1,
            ),
            59 => 
            array (
                'id' => 60,
                'name' => 'Djibouti',
                'code' => '253',
                'iso_code' => 'DJ',
                'is_active' => 1,
            ),
            60 => 
            array (
                'id' => 61,
                'name' => 'Dominica',
                'code' => '1767',
                'iso_code' => 'DM',
                'is_active' => 1,
            ),
            61 => 
            array (
                'id' => 62,
                'name' => 'Dominican Republic',
                'code' => '1809',
                'iso_code' => 'DO',
                'is_active' => 1,
            ),
            62 => 
            array (
                'id' => 63,
                'name' => 'East Timor',
                'code' => '670',
                'iso_code' => 'TL',
                'is_active' => 1,
            ),
            63 => 
            array (
                'id' => 64,
                'name' => 'Ecuador',
                'code' => '593',
                'iso_code' => 'EC',
                'is_active' => 1,
            ),
            64 => 
            array (
                'id' => 65,
                'name' => 'Egypt',
                'code' => '20',
                'iso_code' => 'EG',
                'is_active' => 1,
            ),
            65 => 
            array (
                'id' => 66,
                'name' => 'El Salvador',
                'code' => '503',
                'iso_code' => 'SV',
                'is_active' => 1,
            ),
            66 => 
            array (
                'id' => 67,
                'name' => 'Equatorial Guinea',
                'code' => '240',
                'iso_code' => 'GQ',
                'is_active' => 1,
            ),
            67 => 
            array (
                'id' => 68,
                'name' => 'Eritrea',
                'code' => '291',
                'iso_code' => 'ER',
                'is_active' => 1,
            ),
            68 => 
            array (
                'id' => 69,
                'name' => 'Estonia',
                'code' => '372',
                'iso_code' => 'EE',
                'is_active' => 1,
            ),
            69 => 
            array (
                'id' => 70,
                'name' => 'Ethiopia',
                'code' => '251',
                'iso_code' => 'ET',
                'is_active' => 1,
            ),
            70 => 
            array (
                'id' => 71,
                'name' => 'Falkland Islands',
                'code' => '500',
                'iso_code' => 'FK',
                'is_active' => 1,
            ),
            71 => 
            array (
                'id' => 72,
                'name' => 'Faroe Islands',
                'code' => '298',
                'iso_code' => 'FO',
                'is_active' => 1,
            ),
            72 => 
            array (
                'id' => 73,
                'name' => 'Fiji',
                'code' => '679',
                'iso_code' => 'FJ',
                'is_active' => 1,
            ),
            73 => 
            array (
                'id' => 74,
                'name' => 'Finland',
                'code' => '358',
                'iso_code' => 'FI',
                'is_active' => 1,
            ),
            74 => 
            array (
                'id' => 75,
                'name' => 'France',
                'code' => '33',
                'iso_code' => 'FR',
                'is_active' => 1,
            ),
            75 => 
            array (
                'id' => 76,
                'name' => 'French Guiana',
                'code' => '594',
                'iso_code' => 'GF',
                'is_active' => 1,
            ),
            76 => 
            array (
                'id' => 77,
                'name' => 'French Polynesia',
                'code' => '262',
                'iso_code' => 'PF',
                'is_active' => 1,
            ),
            77 => 
            array (
                'id' => 78,
                'name' => 'Gabon',
                'code' => '241',
                'iso_code' => 'GA',
                'is_active' => 1,
            ),
            78 => 
            array (
                'id' => 79,
                'name' => 'Gambia',
                'code' => '220',
                'iso_code' => 'GM',
                'is_active' => 1,
            ),
            79 => 
            array (
                'id' => 80,
                'name' => 'Georgia',
                'code' => '995',
                'iso_code' => 'GE',
                'is_active' => 1,
            ),
            80 => 
            array (
                'id' => 81,
                'name' => 'Germany',
                'code' => '49',
                'iso_code' => 'DE',
                'is_active' => 1,
            ),
            81 => 
            array (
                'id' => 82,
                'name' => 'Ghana',
                'code' => '233',
                'iso_code' => 'GH',
                'is_active' => 1,
            ),
            82 => 
            array (
                'id' => 83,
                'name' => 'Gibraltar',
                'code' => '350',
                'iso_code' => 'GI',
                'is_active' => 1,
            ),
            83 => 
            array (
                'id' => 84,
                'name' => 'Greece',
                'code' => '30',
                'iso_code' => 'GR',
                'is_active' => 1,
            ),
            84 => 
            array (
                'id' => 85,
                'name' => 'Greenland',
                'code' => '299',
                'iso_code' => 'GL',
                'is_active' => 1,
            ),
            85 => 
            array (
                'id' => 86,
                'name' => 'Grenada',
                'code' => '1473',
                'iso_code' => 'GD',
                'is_active' => 1,
            ),
            86 => 
            array (
                'id' => 87,
                'name' => 'Guadeloupe',
                'code' => '590',
                'iso_code' => 'GP',
                'is_active' => 1,
            ),
            87 => 
            array (
                'id' => 88,
                'name' => 'Guam',
                'code' => '1671',
                'iso_code' => 'GU',
                'is_active' => 1,
            ),
            88 => 
            array (
                'id' => 89,
                'name' => 'Guatemala',
                'code' => '502',
                'iso_code' => 'GT',
                'is_active' => 1,
            ),
            89 => 
            array (
                'id' => 90,
                'name' => 'Guernsey',
                'code' => '44',
                'iso_code' => 'GG',
                'is_active' => 1,
            ),
            90 => 
            array (
                'id' => 91,
                'name' => 'Guinea',
                'code' => '224',
                'iso_code' => 'GN',
                'is_active' => 1,
            ),
            91 => 
            array (
                'id' => 92,
                'name' => 'Guinea-Bissau',
                'code' => '245',
                'iso_code' => 'GW',
                'is_active' => 1,
            ),
            92 => 
            array (
                'id' => 93,
                'name' => 'Guyana',
                'code' => '592',
                'iso_code' => 'GY',
                'is_active' => 1,
            ),
            93 => 
            array (
                'id' => 94,
                'name' => 'Haiti',
                'code' => '509',
                'iso_code' => 'HT',
                'is_active' => 1,
            ),
            94 => 
            array (
                'id' => 95,
            'name' => 'Holy See (Vatican City),',
                'code' => '39',
                'iso_code' => 'VA',
                'is_active' => 1,
            ),
            95 => 
            array (
                'id' => 96,
                'name' => 'Honduras',
                'code' => '504',
                'iso_code' => 'HN',
                'is_active' => 1,
            ),
            96 => 
            array (
                'id' => 97,
                'name' => 'HongKong',
                'code' => '852',
                'iso_code' => 'HK',
                'is_active' => 1,
            ),
            97 => 
            array (
                'id' => 98,
                'name' => 'Hungary',
                'code' => '36',
                'iso_code' => 'HU',
                'is_active' => 1,
            ),
            98 => 
            array (
                'id' => 99,
                'name' => 'Iceland',
                'code' => '354',
                'iso_code' => 'IS',
                'is_active' => 1,
            ),
            99 => 
            array (
                'id' => 100,
                'name' => 'India',
                'code' => '91',
                'iso_code' => 'IN',
                'is_active' => 1,
            ),
            100 => 
            array (
                'id' => 101,
                'name' => 'Indonesia',
                'code' => '62',
                'iso_code' => 'ID',
                'is_active' => 1,
            ),
            101 => 
            array (
                'id' => 102,
                'name' => 'Iran',
                'code' => '98',
                'iso_code' => 'IR',
                'is_active' => 1,
            ),
            102 => 
            array (
                'id' => 103,
                'name' => 'Iraq',
                'code' => '964',
                'iso_code' => 'IQ',
                'is_active' => 1,
            ),
            103 => 
            array (
                'id' => 104,
                'name' => 'Ireland',
                'code' => '353',
                'iso_code' => 'IE',
                'is_active' => 1,
            ),
            104 => 
            array (
                'id' => 105,
                'name' => 'Isle of Man',
                'code' => '44',
                'iso_code' => 'IM',
                'is_active' => 1,
            ),
            105 => 
            array (
                'id' => 106,
                'name' => 'Israel',
                'code' => '972',
                'iso_code' => 'IL',
                'is_active' => 1,
            ),
            106 => 
            array (
                'id' => 107,
                'name' => 'Italy',
                'code' => '39',
                'iso_code' => 'IT',
                'is_active' => 1,
            ),
            107 => 
            array (
                'id' => 108,
                'name' => 'Jamaica',
                'code' => '1876',
                'iso_code' => 'JM',
                'is_active' => 1,
            ),
            108 => 
            array (
                'id' => 109,
                'name' => 'Japan',
                'code' => '81',
                'iso_code' => 'JP',
                'is_active' => 1,
            ),
            109 => 
            array (
                'id' => 110,
                'name' => 'Jersey',
                'code' => '44',
                'iso_code' => 'JE',
                'is_active' => 1,
            ),
            110 => 
            array (
                'id' => 111,
                'name' => 'Jordan',
                'code' => '962',
                'iso_code' => 'JO',
                'is_active' => 1,
            ),
            111 => 
            array (
                'id' => 112,
                'name' => 'Kazakhstan',
                'code' => '7',
                'iso_code' => 'KZ',
                'is_active' => 1,
            ),
            112 => 
            array (
                'id' => 113,
                'name' => 'Kenya',
                'code' => '254',
                'iso_code' => 'KE',
                'is_active' => 1,
            ),
            113 => 
            array (
                'id' => 114,
                'name' => 'Kiribati',
                'code' => '686',
                'iso_code' => 'KI',
                'is_active' => 1,
            ),
            114 => 
            array (
                'id' => 115,
            'name' => 'Korea (North),',
                'code' => '850',
                'iso_code' => 'KP',
                'is_active' => 1,
            ),
            115 => 
            array (
                'id' => 116,
            'name' => 'Korea (South),',
                'code' => '82',
                'iso_code' => 'KR',
                'is_active' => 1,
            ),
            116 => 
            array (
                'id' => 117,
                'name' => 'Kuwait',
                'code' => '965',
                'iso_code' => 'KW',
                'is_active' => 1,
            ),
            117 => 
            array (
                'id' => 118,
                'name' => 'Kyrgyzstan',
                'code' => '996',
                'iso_code' => 'KG',
                'is_active' => 1,
            ),
            118 => 
            array (
                'id' => 119,
                'name' => 'Laos',
                'code' => '856',
                'iso_code' => 'LA',
                'is_active' => 1,
            ),
            119 => 
            array (
                'id' => 120,
                'name' => 'Latvia',
                'code' => '371',
                'iso_code' => 'LV',
                'is_active' => 1,
            ),
            120 => 
            array (
                'id' => 121,
                'name' => 'Lebanon',
                'code' => '961',
                'iso_code' => 'LB',
                'is_active' => 1,
            ),
            121 => 
            array (
                'id' => 122,
                'name' => 'Lesotho',
                'code' => '266',
                'iso_code' => 'LS',
                'is_active' => 1,
            ),
            122 => 
            array (
                'id' => 123,
                'name' => 'Liberia',
                'code' => '231',
                'iso_code' => 'LR',
                'is_active' => 1,
            ),
            123 => 
            array (
                'id' => 124,
                'name' => 'Libya',
                'code' => '218',
                'iso_code' => 'LY',
                'is_active' => 1,
            ),
            124 => 
            array (
                'id' => 125,
                'name' => 'Liechtenstein',
                'code' => '423',
                'iso_code' => 'LI',
                'is_active' => 1,
            ),
            125 => 
            array (
                'id' => 126,
                'name' => 'Lithuania',
                'code' => '370',
                'iso_code' => 'LT',
                'is_active' => 1,
            ),
            126 => 
            array (
                'id' => 127,
                'name' => 'Luxembourg',
                'code' => '352',
                'iso_code' => 'LU',
                'is_active' => 1,
            ),
            127 => 
            array (
                'id' => 128,
                'name' => 'Macau',
                'code' => '853',
                'iso_code' => 'MO',
                'is_active' => 1,
            ),
            128 => 
            array (
                'id' => 129,
                'name' => 'Macedonia',
                'code' => '389',
                'iso_code' => 'MK',
                'is_active' => 1,
            ),
            129 => 
            array (
                'id' => 130,
                'name' => 'Madagascar',
                'code' => '261',
                'iso_code' => 'MG',
                'is_active' => 1,
            ),
            130 => 
            array (
                'id' => 131,
                'name' => 'Malawi',
                'code' => '265',
                'iso_code' => 'MW',
                'is_active' => 1,
            ),
            131 => 
            array (
                'id' => 132,
                'name' => 'Malaysia',
                'code' => '60',
                'iso_code' => 'MY',
                'is_active' => 1,
            ),
            132 => 
            array (
                'id' => 133,
                'name' => 'Maldives',
                'code' => '960',
                'iso_code' => 'MV',
                'is_active' => 1,
            ),
            133 => 
            array (
                'id' => 134,
                'name' => 'Mali',
                'code' => '223',
                'iso_code' => 'ML',
                'is_active' => 1,
            ),
            134 => 
            array (
                'id' => 135,
                'name' => 'Malta',
                'code' => '356',
                'iso_code' => 'MT',
                'is_active' => 1,
            ),
            135 => 
            array (
                'id' => 136,
                'name' => 'Marshall Islands',
                'code' => '692',
                'iso_code' => 'MH',
                'is_active' => 1,
            ),
            136 => 
            array (
                'id' => 137,
                'name' => 'Martinique',
                'code' => '596',
                'iso_code' => 'MQ',
                'is_active' => 1,
            ),
            137 => 
            array (
                'id' => 138,
                'name' => 'Mauritania',
                'code' => '222',
                'iso_code' => 'MR',
                'is_active' => 1,
            ),
            138 => 
            array (
                'id' => 139,
                'name' => 'Mauritius',
                'code' => '230',
                'iso_code' => 'MU',
                'is_active' => 1,
            ),
            139 => 
            array (
                'id' => 140,
                'name' => 'Mayotte Island',
                'code' => '262',
                'iso_code' => 'YT',
                'is_active' => 1,
            ),
            140 => 
            array (
                'id' => 141,
                'name' => 'Mexico',
                'code' => '52',
                'iso_code' => 'MX',
                'is_active' => 1,
            ),
            141 => 
            array (
                'id' => 142,
                'name' => 'Micronesia',
                'code' => '691',
                'iso_code' => 'FM',
                'is_active' => 1,
            ),
            142 => 
            array (
                'id' => 143,
                'name' => 'Moldova',
                'code' => '373',
                'iso_code' => 'MD',
                'is_active' => 1,
            ),
            143 => 
            array (
                'id' => 144,
                'name' => 'Monaco',
                'code' => '377',
                'iso_code' => 'MC',
                'is_active' => 1,
            ),
            144 => 
            array (
                'id' => 145,
                'name' => 'Mongolia',
                'code' => '976',
                'iso_code' => 'MN',
                'is_active' => 1,
            ),
            145 => 
            array (
                'id' => 146,
                'name' => 'Montenegro',
                'code' => '382',
                'iso_code' => 'ME',
                'is_active' => 1,
            ),
            146 => 
            array (
                'id' => 147,
                'name' => 'Montserrat',
                'code' => '1664',
                'iso_code' => 'MS',
                'is_active' => 1,
            ),
            147 => 
            array (
                'id' => 148,
                'name' => 'Morocco',
                'code' => '212',
                'iso_code' => 'MA',
                'is_active' => 1,
            ),
            148 => 
            array (
                'id' => 149,
                'name' => 'Mozambique',
                'code' => '258',
                'iso_code' => 'MZ',
                'is_active' => 1,
            ),
            149 => 
            array (
                'id' => 150,
                'name' => 'Myanmar',
                'code' => '95',
                'iso_code' => 'MM',
                'is_active' => 1,
            ),
            150 => 
            array (
                'id' => 151,
                'name' => 'Namibia',
                'code' => '264',
                'iso_code' => 'NA',
                'is_active' => 1,
            ),
            151 => 
            array (
                'id' => 152,
                'name' => 'Nauru',
                'code' => '674',
                'iso_code' => 'NR',
                'is_active' => 1,
            ),
            152 => 
            array (
                'id' => 153,
                'name' => 'Nepal',
                'code' => '977',
                'iso_code' => 'NP',
                'is_active' => 1,
            ),
            153 => 
            array (
                'id' => 154,
                'name' => 'Netherlands',
                'code' => '31',
                'iso_code' => 'NL',
                'is_active' => 1,
            ),
            154 => 
            array (
                'id' => 155,
                'name' => 'Netherlands Antilles',
                'code' => '599',
                'iso_code' => 'AN',
                'is_active' => 1,
            ),
            155 => 
            array (
                'id' => 156,
                'name' => 'New Caledonia',
                'code' => '687',
                'iso_code' => 'NC',
                'is_active' => 1,
            ),
            156 => 
            array (
                'id' => 157,
                'name' => 'New Zealand',
                'code' => '64',
                'iso_code' => 'NZ',
                'is_active' => 1,
            ),
            157 => 
            array (
                'id' => 158,
                'name' => 'Nicaragua',
                'code' => '505',
                'iso_code' => 'NI',
                'is_active' => 1,
            ),
            158 => 
            array (
                'id' => 159,
                'name' => 'Niger',
                'code' => '227',
                'iso_code' => 'NE',
                'is_active' => 1,
            ),
            159 => 
            array (
                'id' => 160,
                'name' => 'Nigeria',
                'code' => '234',
                'iso_code' => 'NG',
                'is_active' => 1,
            ),
            160 => 
            array (
                'id' => 161,
                'name' => 'Niue',
                'code' => '683',
                'iso_code' => 'NU',
                'is_active' => 1,
            ),
            161 => 
            array (
                'id' => 162,
                'name' => 'Norfolk Island',
                'code' => '672',
                'iso_code' => 'NF',
                'is_active' => 1,
            ),
            162 => 
            array (
                'id' => 163,
                'name' => 'Northern Mariana Islands',
                'code' => '1670',
                'iso_code' => 'MP',
                'is_active' => 1,
            ),
            163 => 
            array (
                'id' => 164,
                'name' => 'Norway',
                'code' => '47',
                'iso_code' => 'NO',
                'is_active' => 1,
            ),
            164 => 
            array (
                'id' => 165,
                'name' => 'Oman',
                'code' => '968',
                'iso_code' => 'OM',
                'is_active' => 1,
            ),
            165 => 
            array (
                'id' => 166,
                'name' => 'Pakistan',
                'code' => '92',
                'iso_code' => 'PK',
                'is_active' => 1,
            ),
            166 => 
            array (
                'id' => 167,
                'name' => 'Palau',
                'code' => '680',
                'iso_code' => 'PW',
                'is_active' => 1,
            ),
            167 => 
            array (
                'id' => 168,
                'name' => 'Palestine',
                'code' => '970',
                'iso_code' => 'PS',
                'is_active' => 1,
            ),
            168 => 
            array (
                'id' => 169,
                'name' => 'Panama',
                'code' => '507',
                'iso_code' => 'PA',
                'is_active' => 1,
            ),
            169 => 
            array (
                'id' => 170,
                'name' => 'Papua New Guinea',
                'code' => '675',
                'iso_code' => 'PG',
                'is_active' => 1,
            ),
            170 => 
            array (
                'id' => 171,
                'name' => 'Paraguay',
                'code' => '595',
                'iso_code' => 'PY',
                'is_active' => 1,
            ),
            171 => 
            array (
                'id' => 172,
                'name' => 'Peru',
                'code' => '51',
                'iso_code' => 'PE',
                'is_active' => 1,
            ),
            172 => 
            array (
                'id' => 173,
                'name' => 'Philippines',
                'code' => '63',
                'iso_code' => 'PH',
                'is_active' => 1,
            ),
            173 => 
            array (
                'id' => 174,
                'name' => 'Pitcairn Islands',
                'code' => '870',
                'iso_code' => 'PN',
                'is_active' => 1,
            ),
            174 => 
            array (
                'id' => 175,
                'name' => 'Poland',
                'code' => '48',
                'iso_code' => 'PL',
                'is_active' => 1,
            ),
            175 => 
            array (
                'id' => 176,
                'name' => 'Portugal',
                'code' => '351',
                'iso_code' => 'PT',
                'is_active' => 1,
            ),
            176 => 
            array (
                'id' => 177,
                'name' => 'Puerto Rico',
                'code' => '1787',
                'iso_code' => 'PR',
                'is_active' => 1,
            ),
            177 => 
            array (
                'id' => 178,
                'name' => 'Qatar',
                'code' => '974',
                'iso_code' => 'QA',
                'is_active' => 1,
            ),
            178 => 
            array (
                'id' => 179,
                'name' => 'Reunion',
                'code' => '262',
                'iso_code' => 'RE',
                'is_active' => 1,
            ),
            179 => 
            array (
                'id' => 180,
                'name' => 'Romania',
                'code' => '40',
                'iso_code' => 'RO',
                'is_active' => 1,
            ),
            180 => 
            array (
                'id' => 181,
                'name' => 'Russia',
                'code' => '7',
                'iso_code' => 'RU',
                'is_active' => 1,
            ),
            181 => 
            array (
                'id' => 182,
                'name' => 'Rwanda',
                'code' => '250',
                'iso_code' => 'RW',
                'is_active' => 1,
            ),
            182 => 
            array (
                'id' => 183,
                'name' => 'Saint Helena',
                'code' => '290',
                'iso_code' => 'SH',
                'is_active' => 1,
            ),
            183 => 
            array (
                'id' => 184,
                'name' => 'Saint Kitts and Nevis',
                'code' => '1869',
                'iso_code' => 'KN',
                'is_active' => 1,
            ),
            184 => 
            array (
                'id' => 185,
                'name' => 'Saint Lucia',
                'code' => '1758',
                'iso_code' => 'LC',
                'is_active' => 1,
            ),
            185 => 
            array (
                'id' => 186,
                'name' => 'Saint Pierre and Miquelon',
                'code' => '508',
                'iso_code' => 'PM',
                'is_active' => 1,
            ),
            186 => 
            array (
                'id' => 187,
                'name' => 'Saint Vincent and the Grenadines',
                'code' => '1784',
                'iso_code' => 'VC',
                'is_active' => 1,
            ),
            187 => 
            array (
                'id' => 188,
            'name' => 'Samoa (Western),',
                'code' => '685',
                'iso_code' => 'WS',
                'is_active' => 1,
            ),
            188 => 
            array (
                'id' => 189,
                'name' => 'San Marino',
                'code' => '378',
                'iso_code' => 'SM',
                'is_active' => 1,
            ),
            189 => 
            array (
                'id' => 190,
                'name' => 'Sao Tome and Principe',
                'code' => '239',
                'iso_code' => 'ST',
                'is_active' => 1,
            ),
            190 => 
            array (
                'id' => 191,
                'name' => 'Saudi Arabia',
                'code' => '966',
                'iso_code' => 'SA',
                'is_active' => 1,
            ),
            191 => 
            array (
                'id' => 192,
                'name' => 'Senegal',
                'code' => '221',
                'iso_code' => 'SN',
                'is_active' => 1,
            ),
            192 => 
            array (
                'id' => 193,
                'name' => 'Serbia',
                'code' => '381',
                'iso_code' => 'RS',
                'is_active' => 1,
            ),
            193 => 
            array (
                'id' => 194,
                'name' => 'Seychelles',
                'code' => '248',
                'iso_code' => 'SC',
                'is_active' => 1,
            ),
            194 => 
            array (
                'id' => 195,
                'name' => 'Sierra Leone',
                'code' => '232',
                'iso_code' => 'SL',
                'is_active' => 1,
            ),
            195 => 
            array (
                'id' => 196,
                'name' => 'Singapore',
                'code' => '65',
                'iso_code' => 'SG',
                'is_active' => 1,
            ),
            196 => 
            array (
                'id' => 197,
                'name' => 'Slovakia',
                'code' => '421',
                'iso_code' => 'SK',
                'is_active' => 1,
            ),
            197 => 
            array (
                'id' => 198,
                'name' => 'Slovenia',
                'code' => '386',
                'iso_code' => 'SI',
                'is_active' => 1,
            ),
            198 => 
            array (
                'id' => 199,
                'name' => 'Solomon Islands',
                'code' => '677',
                'iso_code' => 'SB',
                'is_active' => 1,
            ),
            199 => 
            array (
                'id' => 200,
                'name' => 'Somalia',
                'code' => '252',
                'iso_code' => 'SO',
                'is_active' => 1,
            ),
            200 => 
            array (
                'id' => 201,
                'name' => 'South Africa',
                'code' => '27',
                'iso_code' => 'ZA',
                'is_active' => 1,
            ),
            201 => 
            array (
                'id' => 202,
                'name' => 'South Sudan',
                'code' => '211',
                'iso_code' => 'SS',
                'is_active' => 1,
            ),
            202 => 
            array (
                'id' => 203,
                'name' => 'Spain',
                'code' => '34',
                'iso_code' => 'ES',
                'is_active' => 1,
            ),
            203 => 
            array (
                'id' => 204,
                'name' => 'Sri Lanka',
                'code' => '94',
                'iso_code' => 'LK',
                'is_active' => 1,
            ),
            204 => 
            array (
                'id' => 205,
                'name' => 'Sudan',
                'code' => '249',
                'iso_code' => 'SD',
                'is_active' => 1,
            ),
            205 => 
            array (
                'id' => 206,
                'name' => 'Suriname',
                'code' => '597',
                'iso_code' => 'SR',
                'is_active' => 1,
            ),
            206 => 
            array (
                'id' => 207,
                'name' => 'Swaziland',
                'code' => '268',
                'iso_code' => 'SZ',
                'is_active' => 1,
            ),
            207 => 
            array (
                'id' => 208,
                'name' => 'Sweden',
                'code' => '46',
                'iso_code' => 'SE',
                'is_active' => 1,
            ),
            208 => 
            array (
                'id' => 209,
                'name' => 'Switzerland',
                'code' => '41',
                'iso_code' => 'CH',
                'is_active' => 1,
            ),
            209 => 
            array (
                'id' => 210,
                'name' => 'Syria',
                'code' => '963',
                'iso_code' => 'SY',
                'is_active' => 1,
            ),
            210 => 
            array (
                'id' => 211,
                'name' => 'Taiwan',
                'code' => '886',
                'iso_code' => 'TW',
                'is_active' => 1,
            ),
            211 => 
            array (
                'id' => 212,
                'name' => 'Tajikistan',
                'code' => '992',
                'iso_code' => 'TJ',
                'is_active' => 1,
            ),
            212 => 
            array (
                'id' => 213,
                'name' => 'Tanzania',
                'code' => '255',
                'iso_code' => 'TZ',
                'is_active' => 1,
            ),
            213 => 
            array (
                'id' => 214,
                'name' => 'Thailand',
                'code' => '66',
                'iso_code' => 'TH',
                'is_active' => 1,
            ),
            214 => 
            array (
                'id' => 215,
                'name' => 'Togo',
                'code' => '228',
                'iso_code' => 'TG',
                'is_active' => 1,
            ),
            215 => 
            array (
                'id' => 216,
                'name' => 'Tokelau',
                'code' => '690',
                'iso_code' => 'TK',
                'is_active' => 1,
            ),
            216 => 
            array (
                'id' => 217,
                'name' => 'Tonga Islands',
                'code' => '676',
                'iso_code' => 'TO',
                'is_active' => 1,
            ),
            217 => 
            array (
                'id' => 218,
                'name' => 'Trinidad and Tobago',
                'code' => '1868',
                'iso_code' => 'TT',
                'is_active' => 1,
            ),
            218 => 
            array (
                'id' => 219,
                'name' => 'Tunisia',
                'code' => '216',
                'iso_code' => 'TN',
                'is_active' => 1,
            ),
            219 => 
            array (
                'id' => 220,
                'name' => 'Turkey',
                'code' => '90',
                'iso_code' => 'TR',
                'is_active' => 1,
            ),
            220 => 
            array (
                'id' => 221,
                'name' => 'Turkmenistan',
                'code' => '993',
                'iso_code' => 'TM',
                'is_active' => 1,
            ),
            221 => 
            array (
                'id' => 222,
                'name' => 'Turks and Caicos Islands',
                'code' => '1649',
                'iso_code' => 'TC',
                'is_active' => 1,
            ),
            222 => 
            array (
                'id' => 223,
                'name' => 'Tuvalu',
                'code' => '688',
                'iso_code' => 'TV',
                'is_active' => 1,
            ),
            223 => 
            array (
                'id' => 224,
                'name' => 'Uganda',
                'code' => '256',
                'iso_code' => 'UG',
                'is_active' => 1,
            ),
            224 => 
            array (
                'id' => 225,
                'name' => 'Ukraine',
                'code' => '380',
                'iso_code' => 'UA',
                'is_active' => 1,
            ),
            225 => 
            array (
                'id' => 226,
                'name' => 'United Arab Emirates',
                'code' => '971',
                'iso_code' => 'AE',
                'is_active' => 1,
            ),
            226 => 
            array (
                'id' => 227,
                'name' => 'United Kingdom',
                'code' => '44',
                'iso_code' => 'UK',
                'is_active' => 1,
            ),
            227 => 
            array (
                'id' => 228,
                'name' => 'United States of America',
                'code' => '1',
                'iso_code' => 'US',
                'is_active' => 1,
            ),
            228 => 
            array (
                'id' => 229,
                'name' => 'Uruguay',
                'code' => '598',
                'iso_code' => 'UY',
                'is_active' => 1,
            ),
            229 => 
            array (
                'id' => 230,
                'name' => 'US Virgin Islands',
                'code' => '1340',
                'iso_code' => 'VI',
                'is_active' => 1,
            ),
            230 => 
            array (
                'id' => 231,
                'name' => 'Uzbekistan',
                'code' => '998',
                'iso_code' => 'UZ',
                'is_active' => 1,
            ),
            231 => 
            array (
                'id' => 232,
                'name' => 'Vanuatu',
                'code' => '678',
                'iso_code' => 'VU',
                'is_active' => 1,
            ),
            232 => 
            array (
                'id' => 233,
                'name' => 'Venezuela',
                'code' => '58',
                'iso_code' => 'VE',
                'is_active' => 1,
            ),
            233 => 
            array (
                'id' => 234,
                'name' => 'Vietnam',
                'code' => '84',
                'iso_code' => 'VN',
                'is_active' => 1,
            ),
            234 => 
            array (
                'id' => 235,
                'name' => 'Wallis and Futuna',
                'code' => '681',
                'iso_code' => 'WF',
                'is_active' => 1,
            ),
            235 => 
            array (
                'id' => 236,
                'name' => 'Yemen',
                'code' => '967',
                'iso_code' => 'YE',
                'is_active' => 1,
            ),
            236 => 
            array (
                'id' => 237,
                'name' => 'Zambia',
                'code' => '260',
                'iso_code' => 'ZM',
                'is_active' => 1,
            ),
            237 => 
            array (
                'id' => 238,
                'name' => 'Zimbabwe',
                'code' => '263',
                'iso_code' => 'ZW',
                'is_active' => 1,
            ),
            238 => 
            array (
                'id' => 239,
                'name' => 'Abkhazia',
                'code' => '7',
                'iso_code' => 'AB',
                'is_active' => 1,
            ),
            239 => 
            array (
                'id' => 240,
                'name' => 'Sint Maarten',
                'code' => '1599',
                'iso_code' => 'MF',
                'is_active' => 1,
            ),
        ));
        
        
    }
}