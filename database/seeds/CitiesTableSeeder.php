<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cities')->delete();
        
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Ad Daqahliyah',
                'country_id' => 65,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Al Bahr al Ahmar',
                'country_id' => 65,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Al Buhayrah',
                'country_id' => 65,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Al Fayyum',
                'country_id' => 65,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Al Gharbiyah',
                'country_id' => 65,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Al Iskandariyah',
                'country_id' => 65,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Al Ismailiyah',
                'country_id' => 65,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Al Jizah',
                'country_id' => 65,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Al Minufiyah',
                'country_id' => 65,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Al Minya',
                'country_id' => 65,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Al Qahirah',
                'country_id' => 65,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Al Qalyubiyah',
                'country_id' => 65,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Al Wadi al Jadid',
                'country_id' => 65,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Ash Sharqiyah',
                'country_id' => 65,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'As Suways',
                'country_id' => 65,
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Aswan',
                'country_id' => 65,
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Asyut',
                'country_id' => 65,
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Bani Suwayf',
                'country_id' => 65,
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Bur Said',
                'country_id' => 65,
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Dumyat',
                'country_id' => 65,
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Kafr ash Shaykh',
                'country_id' => 65,
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Matruh',
                'country_id' => 65,
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Qina',
                'country_id' => 65,
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Suhaj',
                'country_id' => 65,
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Janub Sina',
                'country_id' => 65,
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Shamal Sina',
                'country_id' => 65,
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'Al Ahmadi',
                'country_id' => 117,
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Al Kuwayt',
                'country_id' => 117,
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'Al Jahra',
                'country_id' => 117,
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'Al Farwaniyah',
                'country_id' => 117,
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'Hawalli',
                'country_id' => 117,
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'Mubarak al Kabir',
                'country_id' => 117,
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'Alger',
                'country_id' => 3,
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'Batna',
                'country_id' => 3,
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'Constantine',
                'country_id' => 3,
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'Medea',
                'country_id' => 3,
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'Mostaganem',
                'country_id' => 3,
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'Oran',
                'country_id' => 3,
            ),
            38 => 
            array (
                'id' => 39,
                'name' => 'Saida',
                'country_id' => 3,
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'Setif',
                'country_id' => 3,
            ),
            40 => 
            array (
                'id' => 41,
                'name' => 'Tiaret',
                'country_id' => 3,
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'Tizi Ouzou',
                'country_id' => 3,
            ),
            42 => 
            array (
                'id' => 43,
                'name' => 'Tlemcen',
                'country_id' => 3,
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'Bejaia',
                'country_id' => 3,
            ),
            44 => 
            array (
                'id' => 45,
                'name' => 'Biskra',
                'country_id' => 3,
            ),
            45 => 
            array (
                'id' => 46,
                'name' => 'Blida',
                'country_id' => 3,
            ),
            46 => 
            array (
                'id' => 47,
                'name' => 'Bouira',
                'country_id' => 3,
            ),
            47 => 
            array (
                'id' => 48,
                'name' => 'Djelfa',
                'country_id' => 3,
            ),
            48 => 
            array (
                'id' => 49,
                'name' => 'Guelma',
                'country_id' => 3,
            ),
            49 => 
            array (
                'id' => 50,
                'name' => 'Jijel',
                'country_id' => 3,
            ),
            50 => 
            array (
                'id' => 51,
                'name' => 'Laghouat',
                'country_id' => 3,
            ),
            51 => 
            array (
                'id' => 52,
                'name' => 'Mascara',
                'country_id' => 3,
            ),
            52 => 
            array (
                'id' => 53,
                'name' => 'Msila',
                'country_id' => 3,
            ),
            53 => 
            array (
                'id' => 54,
                'name' => 'Oum el Bouaghi',
                'country_id' => 3,
            ),
            54 => 
            array (
                'id' => 55,
                'name' => 'Sidi Bel Abbes',
                'country_id' => 3,
            ),
            55 => 
            array (
                'id' => 56,
                'name' => 'Skikda',
                'country_id' => 3,
            ),
            56 => 
            array (
                'id' => 57,
                'name' => 'Tebessa',
                'country_id' => 3,
            ),
            57 => 
            array (
                'id' => 58,
                'name' => 'Adrar',
                'country_id' => 3,
            ),
            58 => 
            array (
                'id' => 59,
                'name' => 'Ain Defla',
                'country_id' => 3,
            ),
            59 => 
            array (
                'id' => 60,
                'name' => 'Ain Temouchent',
                'country_id' => 3,
            ),
            60 => 
            array (
                'id' => 61,
                'name' => 'Annaba',
                'country_id' => 3,
            ),
            61 => 
            array (
                'id' => 62,
                'name' => 'Bechar',
                'country_id' => 3,
            ),
            62 => 
            array (
                'id' => 63,
                'name' => 'Bordj Bou Arreridj',
                'country_id' => 3,
            ),
            63 => 
            array (
                'id' => 64,
                'name' => 'Boumerdes',
                'country_id' => 3,
            ),
            64 => 
            array (
                'id' => 65,
                'name' => 'Chlef',
                'country_id' => 3,
            ),
            65 => 
            array (
                'id' => 66,
                'name' => 'El Bayadh',
                'country_id' => 3,
            ),
            66 => 
            array (
                'id' => 67,
                'name' => 'El Oued',
                'country_id' => 3,
            ),
            67 => 
            array (
                'id' => 68,
                'name' => 'El Tarf',
                'country_id' => 3,
            ),
            68 => 
            array (
                'id' => 69,
                'name' => 'Ghardaia',
                'country_id' => 3,
            ),
            69 => 
            array (
                'id' => 70,
                'name' => 'Illizi',
                'country_id' => 3,
            ),
            70 => 
            array (
                'id' => 71,
                'name' => 'Khenchela',
                'country_id' => 3,
            ),
            71 => 
            array (
                'id' => 72,
                'name' => 'Mila',
                'country_id' => 3,
            ),
            72 => 
            array (
                'id' => 73,
                'name' => 'Naama',
                'country_id' => 3,
            ),
            73 => 
            array (
                'id' => 74,
                'name' => 'Ouargla',
                'country_id' => 3,
            ),
            74 => 
            array (
                'id' => 75,
                'name' => 'Relizane',
                'country_id' => 3,
            ),
            75 => 
            array (
                'id' => 76,
                'name' => 'Souk Ahras',
                'country_id' => 3,
            ),
            76 => 
            array (
                'id' => 77,
                'name' => 'Tamanghasset',
                'country_id' => 3,
            ),
            77 => 
            array (
                'id' => 78,
                'name' => 'Tindouf',
                'country_id' => 3,
            ),
            78 => 
            array (
                'id' => 79,
                'name' => 'Al Hadd',
                'country_id' => 17,
            ),
            79 => 
            array (
                'id' => 80,
                'name' => 'Al Manamah',
                'country_id' => 17,
            ),
            80 => 
            array (
                'id' => 81,
                'name' => 'Jidd Hafs',
                'country_id' => 17,
            ),
            81 => 
            array (
                'id' => 82,
                'name' => 'Sitrah',
                'country_id' => 17,
            ),
            82 => 
            array (
                'id' => 83,
                'name' => 'Al Mintaqah al Gharbiyah',
                'country_id' => 17,
            ),
            83 => 
            array (
                'id' => 84,
                'name' => 'Mintaqat Juzur Hawar',
                'country_id' => 17,
            ),
            84 => 
            array (
                'id' => 85,
                'name' => 'Al Mintaqah ash Shamaliyah',
                'country_id' => 17,
            ),
            85 => 
            array (
                'id' => 86,
                'name' => 'Al Mintaqah al Wusta',
                'country_id' => 17,
            ),
            86 => 
            array (
                'id' => 87,
                'name' => 'Madinat',
                'country_id' => 17,
            ),
            87 => 
            array (
                'id' => 88,
                'name' => 'Ar Rifa',
                'country_id' => 17,
            ),
            88 => 
            array (
                'id' => 89,
                'name' => 'Madinat Hamad',
                'country_id' => 17,
            ),
            89 => 
            array (
                'id' => 90,
                'name' => 'Al Muharraq',
                'country_id' => 17,
            ),
            90 => 
            array (
                'id' => 91,
                'name' => 'Al Asimah',
                'country_id' => 17,
            ),
            91 => 
            array (
                'id' => 92,
                'name' => 'Al Janubiyah',
                'country_id' => 17,
            ),
            92 => 
            array (
                'id' => 93,
                'name' => 'Ash Shamaliyah',
                'country_id' => 17,
            ),
            93 => 
            array (
                'id' => 94,
                'name' => 'Al Anbar',
                'country_id' => 103,
            ),
            94 => 
            array (
                'id' => 95,
                'name' => 'Al Basrah',
                'country_id' => 103,
            ),
            95 => 
            array (
                'id' => 96,
                'name' => 'Al Muthanna',
                'country_id' => 103,
            ),
            96 => 
            array (
                'id' => 97,
                'name' => 'Al Qadisiyah',
                'country_id' => 103,
            ),
            97 => 
            array (
                'id' => 98,
                'name' => 'As Sulaymaniyah',
                'country_id' => 103,
            ),
            98 => 
            array (
                'id' => 99,
                'name' => 'Babil',
                'country_id' => 103,
            ),
            99 => 
            array (
                'id' => 100,
                'name' => 'Baghdad',
                'country_id' => 103,
            ),
            100 => 
            array (
                'id' => 101,
                'name' => 'Dahuk',
                'country_id' => 103,
            ),
            101 => 
            array (
                'id' => 102,
                'name' => 'Dhi Qar',
                'country_id' => 103,
            ),
            102 => 
            array (
                'id' => 103,
                'name' => 'Diyala',
                'country_id' => 103,
            ),
            103 => 
            array (
                'id' => 104,
                'name' => 'Arbil',
                'country_id' => 103,
            ),
            104 => 
            array (
                'id' => 105,
                'name' => 'Karbala',
                'country_id' => 103,
            ),
            105 => 
            array (
                'id' => 106,
                'name' => 'At Tamim',
                'country_id' => 103,
            ),
            106 => 
            array (
                'id' => 107,
                'name' => 'Maysan',
                'country_id' => 103,
            ),
            107 => 
            array (
                'id' => 108,
                'name' => 'Ninawa',
                'country_id' => 103,
            ),
            108 => 
            array (
                'id' => 109,
                'name' => 'Wasit',
                'country_id' => 103,
            ),
            109 => 
            array (
                'id' => 110,
                'name' => 'An Najaf',
                'country_id' => 103,
            ),
            110 => 
            array (
                'id' => 111,
                'name' => 'Salah ad Din',
                'country_id' => 103,
            ),
            111 => 
            array (
                'id' => 112,
                'name' => 'Al Balqa',
                'country_id' => 111,
            ),
            112 => 
            array (
                'id' => 113,
                'name' => 'Ma',
                'country_id' => 111,
            ),
            113 => 
            array (
                'id' => 114,
                'name' => 'Al Karak',
                'country_id' => 111,
            ),
            114 => 
            array (
                'id' => 115,
                'name' => 'Al Mafraq',
                'country_id' => 111,
            ),
            115 => 
            array (
                'id' => 116,
                'name' => 'Amman Governorate',
                'country_id' => 111,
            ),
            116 => 
            array (
                'id' => 117,
                'name' => 'At Tafilah',
                'country_id' => 111,
            ),
            117 => 
            array (
                'id' => 118,
                'name' => 'Az Zarqa',
                'country_id' => 111,
            ),
            118 => 
            array (
                'id' => 119,
                'name' => 'Irbid',
                'country_id' => 111,
            ),
            119 => 
            array (
                'id' => 120,
                'name' => 'Amman',
                'country_id' => 111,
            ),
            120 => 
            array (
                'id' => 121,
                'name' => 'Beqaa',
                'country_id' => 121,
            ),
            121 => 
            array (
                'id' => 122,
                'name' => 'Al Janub',
                'country_id' => 121,
            ),
            122 => 
            array (
                'id' => 123,
                'name' => 'Liban-Nord',
                'country_id' => 121,
            ),
            123 => 
            array (
                'id' => 124,
                'name' => 'Beyrouth',
                'country_id' => 121,
            ),
            124 => 
            array (
                'id' => 125,
                'name' => 'Mont-Liban',
                'country_id' => 121,
            ),
            125 => 
            array (
                'id' => 126,
                'name' => 'Liban-Sud',
                'country_id' => 121,
            ),
            126 => 
            array (
                'id' => 127,
                'name' => 'Nabatiye',
                'country_id' => 121,
            ),
            127 => 
            array (
                'id' => 128,
                'name' => 'Beqaa',
                'country_id' => 121,
            ),
            128 => 
            array (
                'id' => 129,
                'name' => 'Liban-Nord',
                'country_id' => 121,
            ),
            129 => 
            array (
                'id' => 130,
                'name' => 'Aakk',
                'country_id' => 121,
            ),
            130 => 
            array (
                'id' => 131,
                'name' => 'Baalbek-Hermel',
                'country_id' => 121,
            ),
            131 => 
            array (
                'id' => 132,
                'name' => 'Al Aziziyah',
                'country_id' => 124,
            ),
            132 => 
            array (
                'id' => 133,
                'name' => 'Al Jufrah',
                'country_id' => 124,
            ),
            133 => 
            array (
                'id' => 134,
                'name' => 'Al Kufrah',
                'country_id' => 124,
            ),
            134 => 
            array (
                'id' => 135,
                'name' => 'Ash Shati',
                'country_id' => 124,
            ),
            135 => 
            array (
                'id' => 136,
                'name' => 'Murzuq',
                'country_id' => 124,
            ),
            136 => 
            array (
                'id' => 137,
                'name' => 'Sabha',
                'country_id' => 124,
            ),
            137 => 
            array (
                'id' => 138,
                'name' => 'Tarhunah',
                'country_id' => 124,
            ),
            138 => 
            array (
                'id' => 139,
                'name' => 'Tubruq',
                'country_id' => 124,
            ),
            139 => 
            array (
                'id' => 140,
                'name' => 'Zlitan',
                'country_id' => 124,
            ),
            140 => 
            array (
                'id' => 141,
                'name' => 'Ajdabiya',
                'country_id' => 124,
            ),
            141 => 
            array (
                'id' => 142,
                'name' => 'Al Fatih',
                'country_id' => 124,
            ),
            142 => 
            array (
                'id' => 143,
                'name' => 'Al Jabal al Akhdar',
                'country_id' => 124,
            ),
            143 => 
            array (
                'id' => 144,
                'name' => 'Al Khums',
                'country_id' => 124,
            ),
            144 => 
            array (
                'id' => 145,
                'name' => 'An Nuqat al Khams',
                'country_id' => 124,
            ),
            145 => 
            array (
                'id' => 146,
                'name' => 'Awbari',
                'country_id' => 124,
            ),
            146 => 
            array (
                'id' => 147,
                'name' => 'Az Zawiyah',
                'country_id' => 124,
            ),
            147 => 
            array (
                'id' => 148,
                'name' => 'Banghazi',
                'country_id' => 124,
            ),
            148 => 
            array (
                'id' => 149,
                'name' => 'Darnah',
                'country_id' => 124,
            ),
            149 => 
            array (
                'id' => 150,
                'name' => 'Ghadamis',
                'country_id' => 124,
            ),
            150 => 
            array (
                'id' => 151,
                'name' => 'Gharyan',
                'country_id' => 124,
            ),
            151 => 
            array (
                'id' => 152,
                'name' => 'Misratah',
                'country_id' => 124,
            ),
            152 => 
            array (
                'id' => 153,
                'name' => 'Sawfajjin',
                'country_id' => 124,
            ),
            153 => 
            array (
                'id' => 154,
                'name' => 'Surt',
                'country_id' => 124,
            ),
            154 => 
            array (
                'id' => 155,
                'name' => 'Tarabulus',
                'country_id' => 124,
            ),
            155 => 
            array (
                'id' => 156,
                'name' => 'Yafran',
                'country_id' => 124,
            ),
            156 => 
            array (
                'id' => 157,
                'name' => 'Grand Casablanca',
                'country_id' => 148,
            ),
            157 => 
            array (
                'id' => 158,
                'name' => 'Fes-Boulemane',
                'country_id' => 148,
            ),
            158 => 
            array (
                'id' => 159,
                'name' => 'Marrakech-Tensift-Al Haouz',
                'country_id' => 148,
            ),
            159 => 
            array (
                'id' => 160,
                'name' => 'Meknes-Tafilalet',
                'country_id' => 148,
            ),
            160 => 
            array (
                'id' => 161,
                'name' => 'Rabat-Sale-Zemmour-Zaer',
                'country_id' => 148,
            ),
            161 => 
            array (
                'id' => 162,
                'name' => 'Chaouia-Ouardigha',
                'country_id' => 148,
            ),
            162 => 
            array (
                'id' => 163,
                'name' => 'Doukkala-Abda',
                'country_id' => 148,
            ),
            163 => 
            array (
                'id' => 164,
                'name' => 'Gharb-Chrarda-Beni Hssen',
                'country_id' => 148,
            ),
            164 => 
            array (
                'id' => 165,
                'name' => 'Guelmim-Es Smara',
                'country_id' => 148,
            ),
            165 => 
            array (
                'id' => 166,
                'name' => 'Oriental',
                'country_id' => 148,
            ),
            166 => 
            array (
                'id' => 167,
                'name' => 'Souss-Massa-Dra',
                'country_id' => 148,
            ),
            167 => 
            array (
                'id' => 168,
                'name' => 'Tadla-Azilal',
                'country_id' => 148,
            ),
            168 => 
            array (
                'id' => 169,
                'name' => 'Tanger-Tetouan',
                'country_id' => 148,
            ),
            169 => 
            array (
                'id' => 170,
                'name' => 'Taza-Al Hoceima-Taounate',
                'country_id' => 148,
            ),
            170 => 
            array (
                'id' => 171,
                'name' => 'La youne-Boujdour-Sakia El Hamra',
                'country_id' => 148,
            ),
            171 => 
            array (
                'id' => 172,
                'name' => 'Ad Dakhiliyah',
                'country_id' => 165,
            ),
            172 => 
            array (
                'id' => 173,
                'name' => 'Al Batinah',
                'country_id' => 165,
            ),
            173 => 
            array (
                'id' => 174,
                'name' => 'Al Wusta',
                'country_id' => 165,
            ),
            174 => 
            array (
                'id' => 175,
                'name' => 'Ash Sharqiyah',
                'country_id' => 165,
            ),
            175 => 
            array (
                'id' => 176,
                'name' => 'Az Zahirah',
                'country_id' => 165,
            ),
            176 => 
            array (
                'id' => 177,
                'name' => 'Masqat',
                'country_id' => 165,
            ),
            177 => 
            array (
                'id' => 178,
                'name' => 'Musandam',
                'country_id' => 165,
            ),
            178 => 
            array (
                'id' => 179,
                'name' => 'Zufar',
                'country_id' => 165,
            ),
            179 => 
            array (
                'id' => 180,
                'name' => 'Ad Dawhah',
                'country_id' => 178,
            ),
            180 => 
            array (
                'id' => 181,
                'name' => 'Al Ghuwariyah',
                'country_id' => 178,
            ),
            181 => 
            array (
                'id' => 182,
                'name' => 'Al Jumaliyah',
                'country_id' => 178,
            ),
            182 => 
            array (
                'id' => 183,
                'name' => 'Al Khawr',
                'country_id' => 178,
            ),
            183 => 
            array (
                'id' => 184,
                'name' => 'Al Wakrah Municipality',
                'country_id' => 178,
            ),
            184 => 
            array (
                'id' => 185,
                'name' => 'Ar Rayyan',
                'country_id' => 178,
            ),
            185 => 
            array (
                'id' => 186,
                'name' => 'Madinat ach Shamal',
                'country_id' => 178,
            ),
            186 => 
            array (
                'id' => 187,
                'name' => 'Umm Salal',
                'country_id' => 178,
            ),
            187 => 
            array (
                'id' => 188,
                'name' => 'Al Wakrah',
                'country_id' => 178,
            ),
            188 => 
            array (
                'id' => 189,
                'name' => 'Jariyan al Batnah',
                'country_id' => 178,
            ),
            189 => 
            array (
                'id' => 190,
                'name' => 'Umm Said',
                'country_id' => 178,
            ),
            190 => 
            array (
                'id' => 191,
                'name' => 'Al Bahah',
                'country_id' => 191,
            ),
            191 => 
            array (
                'id' => 192,
                'name' => 'Al Madinah',
                'country_id' => 191,
            ),
            192 => 
            array (
                'id' => 193,
                'name' => 'Al Dammam',
                'country_id' => 191,
            ),
            193 => 
            array (
                'id' => 194,
                'name' => 'Al Qasim',
                'country_id' => 191,
            ),
            194 => 
            array (
                'id' => 195,
                'name' => 'Ar Riyad',
                'country_id' => 191,
            ),
            195 => 
            array (
                'id' => 196,
                'name' => 'Asir Province',
                'country_id' => 191,
            ),
            196 => 
            array (
                'id' => 197,
                'name' => 'Hail',
                'country_id' => 191,
            ),
            197 => 
            array (
                'id' => 198,
                'name' => 'Makkah',
                'country_id' => 191,
            ),
            198 => 
            array (
                'id' => 199,
                'name' => 'Al Hudud ash Shamaliyah',
                'country_id' => 191,
            ),
            199 => 
            array (
                'id' => 200,
                'name' => 'Najran',
                'country_id' => 191,
            ),
            200 => 
            array (
                'id' => 201,
                'name' => 'Jizan',
                'country_id' => 191,
            ),
            201 => 
            array (
                'id' => 202,
                'name' => 'Tabuk',
                'country_id' => 191,
            ),
            202 => 
            array (
                'id' => 203,
                'name' => 'Al Jawf',
                'country_id' => 191,
            ),
            203 => 
            array (
                'id' => 204,
                'name' => 'Bakool',
                'country_id' => 200,
            ),
            204 => 
            array (
                'id' => 205,
                'name' => 'Banaadir',
                'country_id' => 200,
            ),
            205 => 
            array (
                'id' => 206,
                'name' => 'Bari',
                'country_id' => 200,
            ),
            206 => 
            array (
                'id' => 207,
                'name' => 'Bay',
                'country_id' => 200,
            ),
            207 => 
            array (
                'id' => 208,
                'name' => 'Galguduud',
                'country_id' => 200,
            ),
            208 => 
            array (
                'id' => 209,
                'name' => 'Gedo',
                'country_id' => 200,
            ),
            209 => 
            array (
                'id' => 210,
                'name' => 'Hiiraan',
                'country_id' => 200,
            ),
            210 => 
            array (
                'id' => 211,
                'name' => 'Jubbada Dhexe',
                'country_id' => 200,
            ),
            211 => 
            array (
                'id' => 212,
                'name' => 'Jubbada Hoose',
                'country_id' => 200,
            ),
            212 => 
            array (
                'id' => 213,
                'name' => 'Mudug',
                'country_id' => 200,
            ),
            213 => 
            array (
                'id' => 214,
                'name' => 'Nugaal',
                'country_id' => 200,
            ),
            214 => 
            array (
                'id' => 215,
                'name' => 'Sanaag',
                'country_id' => 200,
            ),
            215 => 
            array (
                'id' => 216,
                'name' => 'Shabeellaha Dhexe',
                'country_id' => 200,
            ),
            216 => 
            array (
                'id' => 217,
                'name' => 'Shabeellaha Hoose',
                'country_id' => 200,
            ),
            217 => 
            array (
                'id' => 218,
                'name' => 'Woqooyi Galbeed',
                'country_id' => 200,
            ),
            218 => 
            array (
                'id' => 219,
                'name' => 'Togdheer',
                'country_id' => 200,
            ),
            219 => 
            array (
                'id' => 220,
                'name' => 'Awdal',
                'country_id' => 200,
            ),
            220 => 
            array (
                'id' => 221,
                'name' => 'Sool',
                'country_id' => 200,
            ),
            221 => 
            array (
                'id' => 222,
                'name' => 'Al Wusta',
                'country_id' => 205,
            ),
            222 => 
            array (
                'id' => 223,
                'name' => 'Al Istiwaiyah',
                'country_id' => 205,
            ),
            223 => 
            array (
                'id' => 224,
                'name' => 'Al Khartum',
                'country_id' => 205,
            ),
            224 => 
            array (
                'id' => 225,
                'name' => 'Ash Shamaliyah',
                'country_id' => 205,
            ),
            225 => 
            array (
                'id' => 226,
                'name' => 'Ash Sharqiyah',
                'country_id' => 205,
            ),
            226 => 
            array (
                'id' => 227,
                'name' => 'Bahr al Ghazal',
                'country_id' => 205,
            ),
            227 => 
            array (
                'id' => 228,
                'name' => 'Darfur',
                'country_id' => 205,
            ),
            228 => 
            array (
                'id' => 229,
                'name' => 'Kurdufan',
                'country_id' => 205,
            ),
            229 => 
            array (
                'id' => 230,
                'name' => 'Upper Nile',
                'country_id' => 205,
            ),
            230 => 
            array (
                'id' => 231,
                'name' => 'Al Wahadah State',
                'country_id' => 205,
            ),
            231 => 
            array (
                'id' => 232,
                'name' => 'Central Equatoria State',
                'country_id' => 205,
            ),
            232 => 
            array (
                'id' => 233,
                'name' => 'Al Hasakah',
                'country_id' => 210,
            ),
            233 => 
            array (
                'id' => 234,
                'name' => 'Al Ladhiqiyah',
                'country_id' => 210,
            ),
            234 => 
            array (
                'id' => 235,
                'name' => 'Al Qunaytirah',
                'country_id' => 210,
            ),
            235 => 
            array (
                'id' => 236,
                'name' => 'Ar Raqqah',
                'country_id' => 210,
            ),
            236 => 
            array (
                'id' => 237,
                'name' => 'As Suwayda',
                'country_id' => 210,
            ),
            237 => 
            array (
                'id' => 238,
                'name' => 'Dar',
                'country_id' => 210,
            ),
            238 => 
            array (
                'id' => 239,
                'name' => 'Dayr az Zawr',
                'country_id' => 210,
            ),
            239 => 
            array (
                'id' => 240,
                'name' => 'Rif Dimashq',
                'country_id' => 210,
            ),
            240 => 
            array (
                'id' => 241,
                'name' => 'Halab',
                'country_id' => 210,
            ),
            241 => 
            array (
                'id' => 242,
                'name' => 'Hamah',
                'country_id' => 210,
            ),
            242 => 
            array (
                'id' => 243,
                'name' => 'Hims',
                'country_id' => 210,
            ),
            243 => 
            array (
                'id' => 244,
                'name' => 'Idlib',
                'country_id' => 210,
            ),
            244 => 
            array (
                'id' => 245,
                'name' => 'Dimashq',
                'country_id' => 210,
            ),
            245 => 
            array (
                'id' => 246,
                'name' => 'Tartus',
                'country_id' => 210,
            ),
            246 => 
            array (
                'id' => 247,
                'name' => 'Kasserine',
                'country_id' => 219,
            ),
            247 => 
            array (
                'id' => 248,
                'name' => 'Kairouan',
                'country_id' => 219,
            ),
            248 => 
            array (
                'id' => 249,
                'name' => 'Jendouba',
                'country_id' => 219,
            ),
            249 => 
            array (
                'id' => 250,
                'name' => 'Qafsah',
                'country_id' => 219,
            ),
            250 => 
            array (
                'id' => 251,
                'name' => 'El Kef',
                'country_id' => 219,
            ),
            251 => 
            array (
                'id' => 252,
                'name' => 'Al Mahdia',
                'country_id' => 219,
            ),
            252 => 
            array (
                'id' => 253,
                'name' => 'Al Munastir',
                'country_id' => 219,
            ),
            253 => 
            array (
                'id' => 254,
                'name' => 'Bajah',
                'country_id' => 219,
            ),
            254 => 
            array (
                'id' => 255,
                'name' => 'Bizerte',
                'country_id' => 219,
            ),
            255 => 
            array (
                'id' => 256,
                'name' => 'Nabeul',
                'country_id' => 219,
            ),
            256 => 
            array (
                'id' => 257,
                'name' => 'Siliana',
                'country_id' => 219,
            ),
            257 => 
            array (
                'id' => 258,
                'name' => 'Sousse',
                'country_id' => 219,
            ),
            258 => 
            array (
                'id' => 259,
                'name' => 'Ben Arous',
                'country_id' => 219,
            ),
            259 => 
            array (
                'id' => 260,
                'name' => 'Madanin',
                'country_id' => 219,
            ),
            260 => 
            array (
                'id' => 261,
                'name' => 'Gabes',
                'country_id' => 219,
            ),
            261 => 
            array (
                'id' => 262,
                'name' => 'Kebili',
                'country_id' => 219,
            ),
            262 => 
            array (
                'id' => 263,
                'name' => 'Sfax',
                'country_id' => 219,
            ),
            263 => 
            array (
                'id' => 264,
                'name' => 'Sidi Bou Zid',
                'country_id' => 219,
            ),
            264 => 
            array (
                'id' => 265,
                'name' => 'Tataouine',
                'country_id' => 219,
            ),
            265 => 
            array (
                'id' => 266,
                'name' => 'Tozeur',
                'country_id' => 219,
            ),
            266 => 
            array (
                'id' => 267,
                'name' => 'Tunis',
                'country_id' => 219,
            ),
            267 => 
            array (
                'id' => 268,
                'name' => 'Zaghouan',
                'country_id' => 219,
            ),
            268 => 
            array (
                'id' => 269,
                'name' => 'Aiana',
                'country_id' => 219,
            ),
            269 => 
            array (
                'id' => 270,
                'name' => 'Manouba',
                'country_id' => 219,
            ),
            270 => 
            array (
                'id' => 271,
                'name' => 'Abu Dhabi',
                'country_id' => 226,
            ),
            271 => 
            array (
                'id' => 272,
                'name' => 'Ajman',
                'country_id' => 226,
            ),
            272 => 
            array (
                'id' => 273,
                'name' => 'Dubai',
                'country_id' => 226,
            ),
            273 => 
            array (
                'id' => 274,
                'name' => 'Fujairah',
                'country_id' => 226,
            ),
            274 => 
            array (
                'id' => 275,
                'name' => 'Ras Al Khaimah',
                'country_id' => 226,
            ),
            275 => 
            array (
                'id' => 276,
                'name' => 'Sharjah',
                'country_id' => 226,
            ),
            276 => 
            array (
                'id' => 277,
                'name' => 'Umm Al Quwain',
                'country_id' => 226,
            ),
            277 => 
            array (
                'id' => 278,
                'name' => 'Abyan',
                'country_id' => 236,
            ),
            278 => 
            array (
                'id' => 279,
                'name' => 'Adan',
                'country_id' => 236,
            ),
            279 => 
            array (
                'id' => 280,
                'name' => 'Al Mahrah',
                'country_id' => 236,
            ),
            280 => 
            array (
                'id' => 281,
                'name' => 'Hadramawt',
                'country_id' => 236,
            ),
            281 => 
            array (
                'id' => 282,
                'name' => 'Shabwah',
                'country_id' => 236,
            ),
            282 => 
            array (
                'id' => 283,
                'name' => 'Lahij',
                'country_id' => 236,
            ),
            283 => 
            array (
                'id' => 284,
                'name' => 'Al Bayda',
                'country_id' => 236,
            ),
            284 => 
            array (
                'id' => 285,
                'name' => 'Al Hudaydah',
                'country_id' => 236,
            ),
            285 => 
            array (
                'id' => 287,
                'name' => 'Al Mahwit',
                'country_id' => 236,
            ),
            286 => 
            array (
                'id' => 288,
                'name' => 'Dhamar',
                'country_id' => 236,
            ),
            287 => 
            array (
                'id' => 289,
                'name' => 'Hajjah',
                'country_id' => 236,
            ),
            288 => 
            array (
                'id' => 290,
                'name' => 'Ibb',
                'country_id' => 236,
            ),
            289 => 
            array (
                'id' => 291,
                'name' => 'Marib',
                'country_id' => 236,
            ),
            290 => 
            array (
                'id' => 292,
                'name' => 'Sadah',
                'country_id' => 236,
            ),
            291 => 
            array (
                'id' => 293,
                'name' => 'Sana',
                'country_id' => 236,
            ),
            292 => 
            array (
                'id' => 294,
                'name' => 'Taizz',
                'country_id' => 236,
            ),
            293 => 
            array (
                'id' => 295,
                'name' => 'Ad Dali',
                'country_id' => 236,
            ),
            294 => 
            array (
                'id' => 296,
                'name' => 'Amran',
                'country_id' => 236,
            ),
            295 => 
            array (
                'id' => 297,
                'name' => 'Al Jawf',
                'country_id' => 236,
            ),
            296 => 
            array (
                'id' => 298,
                'name' => 'Ali Sabieh',
                'country_id' => 60,
            ),
            297 => 
            array (
                'id' => 299,
                'name' => 'Obock',
                'country_id' => 60,
            ),
            298 => 
            array (
                'id' => 300,
                'name' => 'Tadjoura',
                'country_id' => 60,
            ),
            299 => 
            array (
                'id' => 301,
                'name' => 'Dikhil',
                'country_id' => 60,
            ),
            300 => 
            array (
                'id' => 302,
                'name' => 'Djibouti',
                'country_id' => 60,
            ),
            301 => 
            array (
                'id' => 303,
                'name' => 'Arta',
                'country_id' => 60,
            ),
            302 => 
            array (
                'id' => 304,
                'name' => 'Hodh Ech Chargui',
                'country_id' => 138,
            ),
            303 => 
            array (
                'id' => 305,
                'name' => 'Hodh El Gharbi',
                'country_id' => 138,
            ),
            304 => 
            array (
                'id' => 306,
                'name' => 'Assaba',
                'country_id' => 138,
            ),
            305 => 
            array (
                'id' => 307,
                'name' => 'Gorgol',
                'country_id' => 138,
            ),
            306 => 
            array (
                'id' => 308,
                'name' => 'Brakna',
                'country_id' => 138,
            ),
            307 => 
            array (
                'id' => 309,
                'name' => 'Trarza',
                'country_id' => 138,
            ),
            308 => 
            array (
                'id' => 310,
                'name' => 'Adrar',
                'country_id' => 138,
            ),
            309 => 
            array (
                'id' => 311,
                'name' => 'Dakhlet Nouadhibou',
                'country_id' => 138,
            ),
            310 => 
            array (
                'id' => 312,
                'name' => 'Tagant',
                'country_id' => 138,
            ),
            311 => 
            array (
                'id' => 313,
                'name' => 'Guidimaka',
                'country_id' => 138,
            ),
            312 => 
            array (
                'id' => 314,
                'name' => 'Tiris Zemmour',
                'country_id' => 138,
            ),
            313 => 
            array (
                'id' => 315,
                'name' => 'Inchiri',
                'country_id' => 138,
            ),
            314 => 
            array (
                'id' => 316,
                'name' => 'Comoros',
                'country_id' => 47,
            ),
            315 => 
            array (
                'id' => 317,
                'name' => 'Canillo',
                'country_id' => 5,
            ),
            316 => 
            array (
                'id' => 318,
                'name' => 'Encamp',
                'country_id' => 5,
            ),
            317 => 
            array (
                'id' => 319,
                'name' => 'La Massana',
                'country_id' => 5,
            ),
            318 => 
            array (
                'id' => 320,
                'name' => 'Ordino',
                'country_id' => 5,
            ),
            319 => 
            array (
                'id' => 321,
                'name' => 'Sant Julià de Lòria',
                'country_id' => 5,
            ),
            320 => 
            array (
                'id' => 322,
                'name' => 'Andorra la Vella',
                'country_id' => 5,
            ),
            321 => 
            array (
                'id' => 323,
                'name' => 'Escaldes-Engordany',
                'country_id' => 5,
            ),
            322 => 
            array (
                'id' => 327,
                'name' => 'Al Fujayrah',
                'country_id' => 226,
            ),
            323 => 
            array (
                'id' => 331,
                'name' => 'Balkh',
                'country_id' => 1,
            ),
            324 => 
            array (
                'id' => 332,
                'name' => 'Bamyan',
                'country_id' => 1,
            ),
            325 => 
            array (
                'id' => 333,
                'name' => 'Badghis',
                'country_id' => 1,
            ),
            326 => 
            array (
                'id' => 334,
                'name' => 'Badakhshan',
                'country_id' => 1,
            ),
            327 => 
            array (
                'id' => 335,
                'name' => 'Baghlan',
                'country_id' => 1,
            ),
            328 => 
            array (
                'id' => 336,
                'name' => 'Daykundi',
                'country_id' => 1,
            ),
            329 => 
            array (
                'id' => 337,
                'name' => 'Farah',
                'country_id' => 1,
            ),
            330 => 
            array (
                'id' => 338,
                'name' => 'Faryab',
                'country_id' => 1,
            ),
            331 => 
            array (
                'id' => 339,
                'name' => 'Ghazni',
                'country_id' => 1,
            ),
            332 => 
            array (
                'id' => 340,
                'name' => 'Ghor',
                'country_id' => 1,
            ),
            333 => 
            array (
                'id' => 341,
                'name' => 'Helmand',
                'country_id' => 1,
            ),
            334 => 
            array (
                'id' => 342,
                'name' => 'Herat',
                'country_id' => 1,
            ),
            335 => 
            array (
                'id' => 343,
                'name' => 'Jowzjan',
                'country_id' => 1,
            ),
            336 => 
            array (
                'id' => 344,
                'name' => 'Kabul',
                'country_id' => 1,
            ),
            337 => 
            array (
                'id' => 345,
                'name' => 'Kandahar',
                'country_id' => 1,
            ),
            338 => 
            array (
                'id' => 346,
                'name' => 'Kapisa',
                'country_id' => 1,
            ),
            339 => 
            array (
                'id' => 347,
                'name' => 'Kunduz',
                'country_id' => 1,
            ),
            340 => 
            array (
                'id' => 348,
                'name' => 'Khost',
                'country_id' => 1,
            ),
            341 => 
            array (
                'id' => 349,
                'name' => 'Kunar',
                'country_id' => 1,
            ),
            342 => 
            array (
                'id' => 350,
                'name' => 'Laghman',
                'country_id' => 1,
            ),
            343 => 
            array (
                'id' => 351,
                'name' => 'Logar',
                'country_id' => 1,
            ),
            344 => 
            array (
                'id' => 352,
                'name' => 'Nangarhar',
                'country_id' => 1,
            ),
            345 => 
            array (
                'id' => 353,
                'name' => 'Nimroz',
                'country_id' => 1,
            ),
            346 => 
            array (
                'id' => 354,
                'name' => 'Nuristan',
                'country_id' => 1,
            ),
            347 => 
            array (
                'id' => 355,
                'name' => 'Panjshayr',
                'country_id' => 1,
            ),
            348 => 
            array (
                'id' => 356,
                'name' => 'Parwan',
                'country_id' => 1,
            ),
            349 => 
            array (
                'id' => 357,
                'name' => 'Paktiya',
                'country_id' => 1,
            ),
            350 => 
            array (
                'id' => 358,
                'name' => 'Paktika',
                'country_id' => 1,
            ),
            351 => 
            array (
                'id' => 359,
                'name' => 'Samangan',
                'country_id' => 1,
            ),
            352 => 
            array (
                'id' => 360,
                'name' => 'Sar-e Pul',
                'country_id' => 1,
            ),
            353 => 
            array (
                'id' => 361,
                'name' => 'Takhar',
                'country_id' => 1,
            ),
            354 => 
            array (
                'id' => 362,
                'name' => 'Uruzgan',
                'country_id' => 1,
            ),
            355 => 
            array (
                'id' => 363,
                'name' => 'Wardak',
                'country_id' => 1,
            ),
            356 => 
            array (
                'id' => 364,
                'name' => 'Zabul',
                'country_id' => 1,
            ),
            357 => 
            array (
                'id' => 365,
                'name' => 'Saint George',
                'country_id' => 9,
            ),
            358 => 
            array (
                'id' => 366,
                'name' => 'Saint John’s',
                'country_id' => 9,
            ),
            359 => 
            array (
                'id' => 367,
                'name' => 'Saint Mary',
                'country_id' => 9,
            ),
            360 => 
            array (
                'id' => 368,
                'name' => 'Saint Paul',
                'country_id' => 9,
            ),
            361 => 
            array (
                'id' => 369,
                'name' => 'Saint Peter',
                'country_id' => 9,
            ),
            362 => 
            array (
                'id' => 370,
                'name' => 'Saint Philip',
                'country_id' => 9,
            ),
            363 => 
            array (
                'id' => 371,
                'name' => 'Barbuda',
                'country_id' => 9,
            ),
            364 => 
            array (
                'id' => 372,
                'name' => 'Redonda',
                'country_id' => 9,
            ),
            365 => 
            array (
                'id' => 373,
                'name' => 'Berat',
                'country_id' => 2,
            ),
            366 => 
            array (
                'id' => 374,
                'name' => 'Bulqizë',
                'country_id' => 2,
            ),
            367 => 
            array (
                'id' => 375,
                'name' => 'Dibër',
                'country_id' => 2,
            ),
            368 => 
            array (
                'id' => 376,
                'name' => 'Delvinë',
                'country_id' => 2,
            ),
            369 => 
            array (
                'id' => 377,
                'name' => 'Durrës',
                'country_id' => 2,
            ),
            370 => 
            array (
                'id' => 378,
                'name' => 'Devoll',
                'country_id' => 2,
            ),
            371 => 
            array (
                'id' => 379,
                'name' => 'Elbasan',
                'country_id' => 2,
            ),
            372 => 
            array (
                'id' => 380,
                'name' => 'Kolonjë',
                'country_id' => 2,
            ),
            373 => 
            array (
                'id' => 381,
                'name' => 'Fier',
                'country_id' => 2,
            ),
            374 => 
            array (
                'id' => 382,
                'name' => 'Gjirokastër',
                'country_id' => 2,
            ),
            375 => 
            array (
                'id' => 383,
                'name' => 'Gramsh',
                'country_id' => 2,
            ),
            376 => 
            array (
                'id' => 384,
                'name' => 'Has',
                'country_id' => 2,
            ),
            377 => 
            array (
                'id' => 385,
                'name' => 'Kavajë',
                'country_id' => 2,
            ),
            378 => 
            array (
                'id' => 386,
                'name' => 'Kurbin',
                'country_id' => 2,
            ),
            379 => 
            array (
                'id' => 387,
                'name' => 'Kuçovë',
                'country_id' => 2,
            ),
            380 => 
            array (
                'id' => 388,
                'name' => 'Korçë',
                'country_id' => 2,
            ),
            381 => 
            array (
                'id' => 389,
                'name' => 'Krujë',
                'country_id' => 2,
            ),
            382 => 
            array (
                'id' => 390,
                'name' => 'Kukës',
                'country_id' => 2,
            ),
            383 => 
            array (
                'id' => 391,
                'name' => 'Librazhd',
                'country_id' => 2,
            ),
            384 => 
            array (
                'id' => 392,
                'name' => 'Lezhë',
                'country_id' => 2,
            ),
            385 => 
            array (
                'id' => 393,
                'name' => 'Lushnjë',
                'country_id' => 2,
            ),
            386 => 
            array (
                'id' => 394,
                'name' => 'Mallakastër',
                'country_id' => 2,
            ),
            387 => 
            array (
                'id' => 395,
                'name' => 'Malësi e Madhe',
                'country_id' => 2,
            ),
            388 => 
            array (
                'id' => 396,
                'name' => 'Mirditë',
                'country_id' => 2,
            ),
            389 => 
            array (
                'id' => 397,
                'name' => 'Mat',
                'country_id' => 2,
            ),
            390 => 
            array (
                'id' => 398,
                'name' => 'Pogradec',
                'country_id' => 2,
            ),
            391 => 
            array (
                'id' => 399,
                'name' => 'Peqin',
                'country_id' => 2,
            ),
            392 => 
            array (
                'id' => 400,
                'name' => 'Përmet',
                'country_id' => 2,
            ),
            393 => 
            array (
                'id' => 401,
                'name' => 'Pukë',
                'country_id' => 2,
            ),
            394 => 
            array (
                'id' => 402,
                'name' => 'Shkodër',
                'country_id' => 2,
            ),
            395 => 
            array (
                'id' => 403,
                'name' => 'Skrapar',
                'country_id' => 2,
            ),
            396 => 
            array (
                'id' => 404,
                'name' => 'Sarandë',
                'country_id' => 2,
            ),
            397 => 
            array (
                'id' => 405,
                'name' => 'Tepelenë',
                'country_id' => 2,
            ),
            398 => 
            array (
                'id' => 406,
                'name' => 'Tropojë',
                'country_id' => 2,
            ),
            399 => 
            array (
                'id' => 407,
                'name' => 'Tiranë',
                'country_id' => 2,
            ),
            400 => 
            array (
                'id' => 408,
                'name' => 'Vlorë',
                'country_id' => 2,
            ),
            401 => 
            array (
                'id' => 409,
                'name' => 'Aragac?otn',
                'country_id' => 11,
            ),
            402 => 
            array (
                'id' => 410,
                'name' => 'Ararat',
                'country_id' => 11,
            ),
            403 => 
            array (
                'id' => 411,
                'name' => 'Armavir',
                'country_id' => 11,
            ),
            404 => 
            array (
                'id' => 412,
                'name' => 'Erevan',
                'country_id' => 11,
            ),
            405 => 
            array (
                'id' => 413,
                'name' => 'Gegark\'unik\'',
                'country_id' => 11,
            ),
            406 => 
            array (
                'id' => 414,
                'name' => 'Kotayk\'',
                'country_id' => 11,
            ),
            407 => 
            array (
                'id' => 415,
                'name' => 'Lo?i',
                'country_id' => 11,
            ),
            408 => 
            array (
                'id' => 416,
                'name' => 'Širak',
                'country_id' => 11,
            ),
            409 => 
            array (
                'id' => 417,
                'name' => 'Syunik\'',
                'country_id' => 11,
            ),
            410 => 
            array (
                'id' => 418,
                'name' => 'Tavuš',
                'country_id' => 11,
            ),
            411 => 
            array (
                'id' => 419,
                'name' => 'Vayoc Jor',
                'country_id' => 11,
            ),
            412 => 
            array (
                'id' => 420,
                'name' => 'Bengo',
                'country_id' => 6,
            ),
            413 => 
            array (
                'id' => 421,
                'name' => 'Benguela',
                'country_id' => 6,
            ),
            414 => 
            array (
                'id' => 422,
                'name' => 'Bié',
                'country_id' => 6,
            ),
            415 => 
            array (
                'id' => 423,
                'name' => 'Cabinda',
                'country_id' => 6,
            ),
            416 => 
            array (
                'id' => 424,
                'name' => 'Cuando-Cubango',
                'country_id' => 6,
            ),
            417 => 
            array (
                'id' => 425,
                'name' => 'Cunene',
                'country_id' => 6,
            ),
            418 => 
            array (
                'id' => 426,
                'name' => 'Cuanza Norte',
                'country_id' => 6,
            ),
            419 => 
            array (
                'id' => 427,
                'name' => 'Cuanza Sul',
                'country_id' => 6,
            ),
            420 => 
            array (
                'id' => 428,
                'name' => 'Huambo',
                'country_id' => 6,
            ),
            421 => 
            array (
                'id' => 429,
                'name' => 'Huíla',
                'country_id' => 6,
            ),
            422 => 
            array (
                'id' => 430,
                'name' => 'Lunda Norte',
                'country_id' => 6,
            ),
            423 => 
            array (
                'id' => 431,
                'name' => 'Lunda Sul',
                'country_id' => 6,
            ),
            424 => 
            array (
                'id' => 432,
                'name' => 'Luanda',
                'country_id' => 6,
            ),
            425 => 
            array (
                'id' => 433,
                'name' => 'Malange',
                'country_id' => 6,
            ),
            426 => 
            array (
                'id' => 434,
                'name' => 'Moxico',
                'country_id' => 6,
            ),
            427 => 
            array (
                'id' => 435,
                'name' => 'Namibe',
                'country_id' => 6,
            ),
            428 => 
            array (
                'id' => 436,
                'name' => 'Uíge',
                'country_id' => 6,
            ),
            429 => 
            array (
                'id' => 437,
                'name' => 'Zaire',
                'country_id' => 6,
            ),
            430 => 
            array (
                'id' => 438,
                'name' => 'Salta',
                'country_id' => 10,
            ),
            431 => 
            array (
                'id' => 439,
                'name' => 'Buenos Aires',
                'country_id' => 10,
            ),
            432 => 
            array (
                'id' => 440,
                'name' => 'Ciudad Autónoma de Buenos Aires',
                'country_id' => 10,
            ),
            433 => 
            array (
                'id' => 441,
                'name' => 'San Luis',
                'country_id' => 10,
            ),
            434 => 
            array (
                'id' => 442,
                'name' => 'Entre Ríos',
                'country_id' => 10,
            ),
            435 => 
            array (
                'id' => 443,
                'name' => 'La Rioja',
                'country_id' => 10,
            ),
            436 => 
            array (
                'id' => 444,
                'name' => 'Santiago del Estero',
                'country_id' => 10,
            ),
            437 => 
            array (
                'id' => 445,
                'name' => 'Chaco',
                'country_id' => 10,
            ),
            438 => 
            array (
                'id' => 446,
                'name' => 'San Juan',
                'country_id' => 10,
            ),
            439 => 
            array (
                'id' => 447,
                'name' => 'Catamarca',
                'country_id' => 10,
            ),
            440 => 
            array (
                'id' => 448,
                'name' => 'La Pampa',
                'country_id' => 10,
            ),
            441 => 
            array (
                'id' => 449,
                'name' => 'Mendoza',
                'country_id' => 10,
            ),
            442 => 
            array (
                'id' => 450,
                'name' => 'Misiones',
                'country_id' => 10,
            ),
            443 => 
            array (
                'id' => 451,
                'name' => 'Formosa',
                'country_id' => 10,
            ),
            444 => 
            array (
                'id' => 452,
                'name' => 'Neuquén',
                'country_id' => 10,
            ),
            445 => 
            array (
                'id' => 453,
                'name' => 'Río Negro',
                'country_id' => 10,
            ),
            446 => 
            array (
                'id' => 454,
                'name' => 'Santa Fe',
                'country_id' => 10,
            ),
            447 => 
            array (
                'id' => 455,
                'name' => 'Tucumán',
                'country_id' => 10,
            ),
            448 => 
            array (
                'id' => 456,
                'name' => 'Chubut',
                'country_id' => 10,
            ),
            449 => 
            array (
                'id' => 457,
                'name' => 'Tierra del Fuego',
                'country_id' => 10,
            ),
            450 => 
            array (
                'id' => 458,
                'name' => 'Corrientes',
                'country_id' => 10,
            ),
            451 => 
            array (
                'id' => 459,
                'name' => 'Córdoba',
                'country_id' => 10,
            ),
            452 => 
            array (
                'id' => 460,
                'name' => 'Jujuy',
                'country_id' => 10,
            ),
            453 => 
            array (
                'id' => 461,
                'name' => 'Santa Cruz',
                'country_id' => 10,
            ),
            454 => 
            array (
                'id' => 462,
                'name' => 'Burgenland',
                'country_id' => 14,
            ),
            455 => 
            array (
                'id' => 463,
                'name' => 'Kärnten',
                'country_id' => 14,
            ),
            456 => 
            array (
                'id' => 464,
                'name' => 'Niederösterreich',
                'country_id' => 14,
            ),
            457 => 
            array (
                'id' => 465,
                'name' => 'Oberösterreich',
                'country_id' => 14,
            ),
            458 => 
            array (
                'id' => 466,
                'name' => 'Salzburg',
                'country_id' => 14,
            ),
            459 => 
            array (
                'id' => 467,
                'name' => 'Steiermark',
                'country_id' => 14,
            ),
            460 => 
            array (
                'id' => 468,
                'name' => 'Tirol',
                'country_id' => 14,
            ),
            461 => 
            array (
                'id' => 469,
                'name' => 'Vorarlberg',
                'country_id' => 14,
            ),
            462 => 
            array (
                'id' => 470,
                'name' => 'Wien',
                'country_id' => 14,
            ),
            463 => 
            array (
                'id' => 471,
                'name' => 'Australian Capital Territory',
                'country_id' => 13,
            ),
            464 => 
            array (
                'id' => 472,
                'name' => 'New South Wales',
                'country_id' => 13,
            ),
            465 => 
            array (
                'id' => 473,
                'name' => 'Northern Territory',
                'country_id' => 13,
            ),
            466 => 
            array (
                'id' => 474,
                'name' => 'Queensland',
                'country_id' => 13,
            ),
            467 => 
            array (
                'id' => 475,
                'name' => 'South Australia',
                'country_id' => 13,
            ),
            468 => 
            array (
                'id' => 476,
                'name' => 'Tasmania',
                'country_id' => 13,
            ),
            469 => 
            array (
                'id' => 477,
                'name' => 'Victoria',
                'country_id' => 13,
            ),
            470 => 
            array (
                'id' => 478,
                'name' => 'Western Australia',
                'country_id' => 13,
            ),
            471 => 
            array (
                'id' => 479,
                'name' => 'Abseron',
                'country_id' => 15,
            ),
            472 => 
            array (
                'id' => 480,
                'name' => 'Agstafa',
                'country_id' => 15,
            ),
            473 => 
            array (
                'id' => 481,
                'name' => 'Agcab?di',
                'country_id' => 15,
            ),
            474 => 
            array (
                'id' => 482,
                'name' => 'Agdam',
                'country_id' => 15,
            ),
            475 => 
            array (
                'id' => 483,
                'name' => 'Agdas',
                'country_id' => 15,
            ),
            476 => 
            array (
                'id' => 484,
                'name' => 'Agsu',
                'country_id' => 15,
            ),
            477 => 
            array (
                'id' => 485,
                'name' => 'Astara',
                'country_id' => 15,
            ),
            478 => 
            array (
                'id' => 486,
                'name' => 'Baki',
                'country_id' => 15,
            ),
            479 => 
            array (
                'id' => 487,
                'name' => 'Bab?k',
                'country_id' => 15,
            ),
            480 => 
            array (
                'id' => 488,
                'name' => 'Balak?n',
                'country_id' => 15,
            ),
            481 => 
            array (
                'id' => 489,
                'name' => 'B?rd?',
                'country_id' => 15,
            ),
            482 => 
            array (
                'id' => 490,
                'name' => 'Beyl?qan',
                'country_id' => 15,
            ),
            483 => 
            array (
                'id' => 491,
                'name' => 'Bil?suvar',
                'country_id' => 15,
            ),
            484 => 
            array (
                'id' => 492,
                'name' => 'C?brayil',
                'country_id' => 15,
            ),
            485 => 
            array (
                'id' => 493,
                'name' => 'C?lilabad',
                'country_id' => 15,
            ),
            486 => 
            array (
                'id' => 494,
                'name' => 'Culfa',
                'country_id' => 15,
            ),
            487 => 
            array (
                'id' => 495,
                'name' => 'Dask?s?n',
                'country_id' => 15,
            ),
            488 => 
            array (
                'id' => 496,
                'name' => 'Füzuli',
                'country_id' => 15,
            ),
            489 => 
            array (
                'id' => 497,
                'name' => 'G?nc?',
                'country_id' => 15,
            ),
            490 => 
            array (
                'id' => 498,
                'name' => 'G?d?b?y',
                'country_id' => 15,
            ),
            491 => 
            array (
                'id' => 499,
                'name' => 'Goranboy',
                'country_id' => 15,
            ),
            492 => 
            array (
                'id' => 500,
                'name' => 'Göyçay',
                'country_id' => 15,
            ),
            493 => 
            array (
                'id' => 501,
                'name' => 'Göygöl',
                'country_id' => 15,
            ),
            494 => 
            array (
                'id' => 502,
                'name' => 'Haciqabul',
                'country_id' => 15,
            ),
            495 => 
            array (
                'id' => 503,
                'name' => 'Imisli',
                'country_id' => 15,
            ),
            496 => 
            array (
                'id' => 504,
                'name' => 'Ismayilli',
                'country_id' => 15,
            ),
            497 => 
            array (
                'id' => 505,
                'name' => 'K?lb?c?r',
                'country_id' => 15,
            ),
            498 => 
            array (
                'id' => 506,
                'name' => 'K?ng?rli',
                'country_id' => 15,
            ),
            499 => 
            array (
                'id' => 507,
                'name' => 'Kürd?mir',
                'country_id' => 15,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 508,
                'name' => 'L?nk?ran',
                'country_id' => 15,
            ),
            1 => 
            array (
                'id' => 509,
                'name' => 'Laçin',
                'country_id' => 15,
            ),
            2 => 
            array (
                'id' => 510,
                'name' => 'Lerik',
                'country_id' => 15,
            ),
            3 => 
            array (
                'id' => 511,
                'name' => 'Masalli',
                'country_id' => 15,
            ),
            4 => 
            array (
                'id' => 512,
                'name' => 'Ming?çevir',
                'country_id' => 15,
            ),
            5 => 
            array (
                'id' => 513,
                'name' => 'Naftalan',
                'country_id' => 15,
            ),
            6 => 
            array (
                'id' => 514,
                'name' => 'Neftçala',
                'country_id' => 15,
            ),
            7 => 
            array (
                'id' => 515,
                'name' => 'Naxçivan',
                'country_id' => 15,
            ),
            8 => 
            array (
                'id' => 516,
                'name' => 'Oguz',
                'country_id' => 15,
            ),
            9 => 
            array (
                'id' => 517,
                'name' => 'Ordubad',
                'country_id' => 15,
            ),
            10 => 
            array (
                'id' => 518,
                'name' => 'Q?b?l?',
                'country_id' => 15,
            ),
            11 => 
            array (
                'id' => 519,
                'name' => 'Qax',
                'country_id' => 15,
            ),
            12 => 
            array (
                'id' => 520,
                'name' => 'Qazax',
                'country_id' => 15,
            ),
            13 => 
            array (
                'id' => 521,
                'name' => 'Quba',
                'country_id' => 15,
            ),
            14 => 
            array (
                'id' => 522,
                'name' => 'Qubadli',
                'country_id' => 15,
            ),
            15 => 
            array (
                'id' => 523,
                'name' => 'Qobustan',
                'country_id' => 15,
            ),
            16 => 
            array (
                'id' => 524,
                'name' => 'Qusar',
                'country_id' => 15,
            ),
            17 => 
            array (
                'id' => 525,
                'name' => 'S?ki',
                'country_id' => 15,
            ),
            18 => 
            array (
                'id' => 526,
                'name' => 'Sabirabad',
                'country_id' => 15,
            ),
            19 => 
            array (
                'id' => 527,
                'name' => 'S?d?r?k',
                'country_id' => 15,
            ),
            20 => 
            array (
                'id' => 528,
                'name' => 'Sahbuz',
                'country_id' => 15,
            ),
            21 => 
            array (
                'id' => 529,
                'name' => 'Salyan',
                'country_id' => 15,
            ),
            22 => 
            array (
                'id' => 530,
                'name' => 'S?rur',
                'country_id' => 15,
            ),
            23 => 
            array (
                'id' => 531,
                'name' => 'Saatli',
                'country_id' => 15,
            ),
            24 => 
            array (
                'id' => 532,
                'name' => 'Sabran',
                'country_id' => 15,
            ),
            25 => 
            array (
                'id' => 533,
                'name' => 'Siy?z?n',
                'country_id' => 15,
            ),
            26 => 
            array (
                'id' => 534,
                'name' => 'S?mkir',
                'country_id' => 15,
            ),
            27 => 
            array (
                'id' => 535,
                'name' => 'Sumqayit',
                'country_id' => 15,
            ),
            28 => 
            array (
                'id' => 536,
                'name' => 'Samaxi',
                'country_id' => 15,
            ),
            29 => 
            array (
                'id' => 537,
                'name' => 'Samux',
                'country_id' => 15,
            ),
            30 => 
            array (
                'id' => 538,
                'name' => 'Sirvan',
                'country_id' => 15,
            ),
            31 => 
            array (
                'id' => 539,
                'name' => 'Susa',
                'country_id' => 15,
            ),
            32 => 
            array (
                'id' => 540,
                'name' => 'T?rt?r',
                'country_id' => 15,
            ),
            33 => 
            array (
                'id' => 541,
                'name' => 'Tovuz',
                'country_id' => 15,
            ),
            34 => 
            array (
                'id' => 542,
                'name' => 'Ucar',
                'country_id' => 15,
            ),
            35 => 
            array (
                'id' => 543,
                'name' => 'Xank?ndi',
                'country_id' => 15,
            ),
            36 => 
            array (
                'id' => 544,
                'name' => 'Xaçmaz',
                'country_id' => 15,
            ),
            37 => 
            array (
                'id' => 545,
                'name' => 'Xocali',
                'country_id' => 15,
            ),
            38 => 
            array (
                'id' => 546,
                'name' => 'Xizi',
                'country_id' => 15,
            ),
            39 => 
            array (
                'id' => 547,
                'name' => 'Xocav?nd',
                'country_id' => 15,
            ),
            40 => 
            array (
                'id' => 548,
                'name' => 'Yardimli',
                'country_id' => 15,
            ),
            41 => 
            array (
                'id' => 549,
                'name' => 'Yevlax',
                'country_id' => 15,
            ),
            42 => 
            array (
                'id' => 550,
                'name' => 'Z?ngilan',
                'country_id' => 15,
            ),
            43 => 
            array (
                'id' => 551,
                'name' => 'Zaqatala',
                'country_id' => 15,
            ),
            44 => 
            array (
                'id' => 552,
                'name' => 'Z?rdab',
                'country_id' => 15,
            ),
            45 => 
            array (
                'id' => 553,
                'name' => 'Unsko-Sanski Kanton',
                'country_id' => 27,
            ),
            46 => 
            array (
                'id' => 554,
                'name' => 'Posavski Kanton',
                'country_id' => 27,
            ),
            47 => 
            array (
                'id' => 555,
                'name' => 'Tuzlanski Kanton',
                'country_id' => 27,
            ),
            48 => 
            array (
                'id' => 556,
                'name' => 'Zenicko-Dobojski Kanton',
                'country_id' => 27,
            ),
            49 => 
            array (
                'id' => 557,
                'name' => 'Bosansko-Podrinjski Kanton',
                'country_id' => 27,
            ),
            50 => 
            array (
                'id' => 558,
                'name' => 'Srednjobosanski Kanton',
                'country_id' => 27,
            ),
            51 => 
            array (
                'id' => 559,
                'name' => 'Hercegovacko-Neretvanski Kanton',
                'country_id' => 27,
            ),
            52 => 
            array (
                'id' => 560,
                'name' => 'Zapadnohercegovacki kanton',
                'country_id' => 27,
            ),
            53 => 
            array (
                'id' => 561,
                'name' => 'Kanton Sarajevo',
                'country_id' => 27,
            ),
            54 => 
            array (
                'id' => 562,
                'name' => '"Županija br. 10',
                'country_id' => 27,
            ),
            55 => 
            array (
                'id' => 563,
                'name' => 'Federacija Bosne i Hercegovine',
                'country_id' => 27,
            ),
            56 => 
            array (
                'id' => 564,
                'name' => 'Brcko distrikt',
                'country_id' => 27,
            ),
            57 => 
            array (
                'id' => 565,
                'name' => 'Republika Srpska',
                'country_id' => 27,
            ),
            58 => 
            array (
                'id' => 566,
                'name' => 'Christ Church',
                'country_id' => 19,
            ),
            59 => 
            array (
                'id' => 567,
                'name' => 'Saint Andrew',
                'country_id' => 19,
            ),
            60 => 
            array (
                'id' => 568,
                'name' => 'Saint George',
                'country_id' => 19,
            ),
            61 => 
            array (
                'id' => 569,
                'name' => 'Saint James',
                'country_id' => 19,
            ),
            62 => 
            array (
                'id' => 570,
                'name' => 'Saint John',
                'country_id' => 19,
            ),
            63 => 
            array (
                'id' => 571,
                'name' => 'Saint Joseph',
                'country_id' => 19,
            ),
            64 => 
            array (
                'id' => 572,
                'name' => 'Saint Lucy',
                'country_id' => 19,
            ),
            65 => 
            array (
                'id' => 573,
                'name' => 'Saint Michael',
                'country_id' => 19,
            ),
            66 => 
            array (
                'id' => 574,
                'name' => 'Saint Peter',
                'country_id' => 19,
            ),
            67 => 
            array (
                'id' => 575,
                'name' => 'Saint Philip',
                'country_id' => 19,
            ),
            68 => 
            array (
                'id' => 576,
                'name' => 'Saint Thomas',
                'country_id' => 19,
            ),
            69 => 
            array (
                'id' => 577,
                'name' => 'Bandarban',
                'country_id' => 18,
            ),
            70 => 
            array (
                'id' => 578,
                'name' => 'Barguna',
                'country_id' => 18,
            ),
            71 => 
            array (
                'id' => 579,
                'name' => 'Bogra',
                'country_id' => 18,
            ),
            72 => 
            array (
                'id' => 580,
                'name' => 'Brahmanbaria',
                'country_id' => 18,
            ),
            73 => 
            array (
                'id' => 581,
                'name' => 'Bagerhat',
                'country_id' => 18,
            ),
            74 => 
            array (
                'id' => 582,
                'name' => 'Barisal',
                'country_id' => 18,
            ),
            75 => 
            array (
                'id' => 583,
                'name' => 'Bhola',
                'country_id' => 18,
            ),
            76 => 
            array (
                'id' => 584,
                'name' => 'Comilla',
                'country_id' => 18,
            ),
            77 => 
            array (
                'id' => 585,
                'name' => 'Chandpur',
                'country_id' => 18,
            ),
            78 => 
            array (
                'id' => 586,
                'name' => 'Chittagong',
                'country_id' => 18,
            ),
            79 => 
            array (
                'id' => 587,
                'name' => 'Cox\'s Bazar',
                'country_id' => 18,
            ),
            80 => 
            array (
                'id' => 588,
                'name' => 'Chuadanga',
                'country_id' => 18,
            ),
            81 => 
            array (
                'id' => 589,
                'name' => 'Dhaka',
                'country_id' => 18,
            ),
            82 => 
            array (
                'id' => 590,
                'name' => 'Dinajpur',
                'country_id' => 18,
            ),
            83 => 
            array (
                'id' => 591,
                'name' => 'Faridpur',
                'country_id' => 18,
            ),
            84 => 
            array (
                'id' => 592,
                'name' => 'Feni',
                'country_id' => 18,
            ),
            85 => 
            array (
                'id' => 593,
                'name' => 'Gopalganj',
                'country_id' => 18,
            ),
            86 => 
            array (
                'id' => 594,
                'name' => 'Gazipur',
                'country_id' => 18,
            ),
            87 => 
            array (
                'id' => 595,
                'name' => 'Gaibandha',
                'country_id' => 18,
            ),
            88 => 
            array (
                'id' => 596,
                'name' => 'Habiganj',
                'country_id' => 18,
            ),
            89 => 
            array (
                'id' => 597,
                'name' => 'Jamalpur',
                'country_id' => 18,
            ),
            90 => 
            array (
                'id' => 598,
                'name' => 'Jessore',
                'country_id' => 18,
            ),
            91 => 
            array (
                'id' => 599,
                'name' => 'Jhenaidah',
                'country_id' => 18,
            ),
            92 => 
            array (
                'id' => 600,
                'name' => 'Jaipurhat',
                'country_id' => 18,
            ),
            93 => 
            array (
                'id' => 601,
                'name' => 'Jhalakati',
                'country_id' => 18,
            ),
            94 => 
            array (
                'id' => 602,
                'name' => 'Kishoreganj',
                'country_id' => 18,
            ),
            95 => 
            array (
                'id' => 603,
                'name' => 'Khulna',
                'country_id' => 18,
            ),
            96 => 
            array (
                'id' => 604,
                'name' => 'Kurigram',
                'country_id' => 18,
            ),
            97 => 
            array (
                'id' => 605,
                'name' => 'Khagrachari',
                'country_id' => 18,
            ),
            98 => 
            array (
                'id' => 606,
                'name' => 'Kushtia',
                'country_id' => 18,
            ),
            99 => 
            array (
                'id' => 607,
                'name' => 'Lakshmipur',
                'country_id' => 18,
            ),
            100 => 
            array (
                'id' => 608,
                'name' => 'Lalmonirhat',
                'country_id' => 18,
            ),
            101 => 
            array (
                'id' => 609,
                'name' => 'Manikganj',
                'country_id' => 18,
            ),
            102 => 
            array (
                'id' => 610,
                'name' => 'Mymensingh',
                'country_id' => 18,
            ),
            103 => 
            array (
                'id' => 611,
                'name' => 'Munshiganj',
                'country_id' => 18,
            ),
            104 => 
            array (
                'id' => 612,
                'name' => 'Madaripur',
                'country_id' => 18,
            ),
            105 => 
            array (
                'id' => 613,
                'name' => 'Magura',
                'country_id' => 18,
            ),
            106 => 
            array (
                'id' => 614,
                'name' => 'Moulvibazar',
                'country_id' => 18,
            ),
            107 => 
            array (
                'id' => 615,
                'name' => 'Meherpur',
                'country_id' => 18,
            ),
            108 => 
            array (
                'id' => 616,
                'name' => 'Narayanganj',
                'country_id' => 18,
            ),
            109 => 
            array (
                'id' => 617,
                'name' => 'Netrakona',
                'country_id' => 18,
            ),
            110 => 
            array (
                'id' => 618,
                'name' => 'Narsingdi',
                'country_id' => 18,
            ),
            111 => 
            array (
                'id' => 619,
                'name' => 'Narail',
                'country_id' => 18,
            ),
            112 => 
            array (
                'id' => 620,
                'name' => 'Natore',
                'country_id' => 18,
            ),
            113 => 
            array (
                'id' => 621,
                'name' => 'Nawabganj',
                'country_id' => 18,
            ),
            114 => 
            array (
                'id' => 622,
                'name' => 'Nilphamari',
                'country_id' => 18,
            ),
            115 => 
            array (
                'id' => 623,
                'name' => 'Noakhali',
                'country_id' => 18,
            ),
            116 => 
            array (
                'id' => 624,
                'name' => 'Naogaon',
                'country_id' => 18,
            ),
            117 => 
            array (
                'id' => 625,
                'name' => 'Pabna',
                'country_id' => 18,
            ),
            118 => 
            array (
                'id' => 626,
                'name' => 'Pirojpur',
                'country_id' => 18,
            ),
            119 => 
            array (
                'id' => 627,
                'name' => 'Patuakhali',
                'country_id' => 18,
            ),
            120 => 
            array (
                'id' => 628,
                'name' => 'Panchagarh',
                'country_id' => 18,
            ),
            121 => 
            array (
                'id' => 629,
                'name' => 'Rajbari',
                'country_id' => 18,
            ),
            122 => 
            array (
                'id' => 630,
                'name' => 'Rajshahi',
                'country_id' => 18,
            ),
            123 => 
            array (
                'id' => 631,
                'name' => 'Rangpur',
                'country_id' => 18,
            ),
            124 => 
            array (
                'id' => 632,
                'name' => 'Rangamati',
                'country_id' => 18,
            ),
            125 => 
            array (
                'id' => 633,
                'name' => 'Sherpur',
                'country_id' => 18,
            ),
            126 => 
            array (
                'id' => 634,
                'name' => 'Satkhira',
                'country_id' => 18,
            ),
            127 => 
            array (
                'id' => 635,
                'name' => 'Sirajganj',
                'country_id' => 18,
            ),
            128 => 
            array (
                'id' => 636,
                'name' => 'Sylhet',
                'country_id' => 18,
            ),
            129 => 
            array (
                'id' => 637,
                'name' => 'Sunamganj',
                'country_id' => 18,
            ),
            130 => 
            array (
                'id' => 638,
                'name' => 'Shariatpur',
                'country_id' => 18,
            ),
            131 => 
            array (
                'id' => 639,
                'name' => 'Tangail',
                'country_id' => 18,
            ),
            132 => 
            array (
                'id' => 640,
                'name' => 'Thakurgaon',
                'country_id' => 18,
            ),
            133 => 
            array (
                'id' => 641,
                'name' => '"Bruxelles-Capitale',
                'country_id' => 21,
            ),
            134 => 
            array (
                'id' => 642,
                'name' => 'Antwerpen',
                'country_id' => 21,
            ),
            135 => 
            array (
                'id' => 643,
                'name' => 'Vlaams Brabant',
                'country_id' => 21,
            ),
            136 => 
            array (
                'id' => 644,
                'name' => 'Vlaamse Gewest',
                'country_id' => 21,
            ),
            137 => 
            array (
                'id' => 645,
                'name' => 'Limburg',
                'country_id' => 21,
            ),
            138 => 
            array (
                'id' => 646,
                'name' => 'Oost-Vlaanderen',
                'country_id' => 21,
            ),
            139 => 
            array (
                'id' => 647,
                'name' => 'West-Vlaanderen',
                'country_id' => 21,
            ),
            140 => 
            array (
                'id' => 648,
                'name' => '"Wallonne',
                'country_id' => 21,
            ),
            141 => 
            array (
                'id' => 649,
                'name' => 'Brabant wallon',
                'country_id' => 21,
            ),
            142 => 
            array (
                'id' => 650,
                'name' => 'Hainaut',
                'country_id' => 21,
            ),
            143 => 
            array (
                'id' => 651,
                'name' => 'Liège',
                'country_id' => 21,
            ),
            144 => 
            array (
                'id' => 652,
                'name' => 'Luxembourg',
                'country_id' => 21,
            ),
            145 => 
            array (
                'id' => 653,
                'name' => 'Namur',
                'country_id' => 21,
            ),
            146 => 
            array (
                'id' => 654,
                'name' => 'Balé',
                'country_id' => 33,
            ),
            147 => 
            array (
                'id' => 655,
                'name' => 'Bam',
                'country_id' => 33,
            ),
            148 => 
            array (
                'id' => 656,
                'name' => 'Banwa',
                'country_id' => 33,
            ),
            149 => 
            array (
                'id' => 657,
                'name' => '"Bazèga',
                'country_id' => 33,
            ),
            150 => 
            array (
                'id' => 658,
                'name' => 'Bougouriba',
                'country_id' => 33,
            ),
            151 => 
            array (
                'id' => 659,
                'name' => 'Boulgou',
                'country_id' => 33,
            ),
            152 => 
            array (
                'id' => 660,
                'name' => 'Boulkiemdé',
                'country_id' => 33,
            ),
            153 => 
            array (
                'id' => 661,
                'name' => 'Comoé',
                'country_id' => 33,
            ),
            154 => 
            array (
                'id' => 662,
                'name' => 'Ganzourgou',
                'country_id' => 33,
            ),
            155 => 
            array (
                'id' => 663,
                'name' => 'Gnagna',
                'country_id' => 33,
            ),
            156 => 
            array (
                'id' => 664,
                'name' => 'Gourma',
                'country_id' => 33,
            ),
            157 => 
            array (
                'id' => 665,
                'name' => 'Houet',
                'country_id' => 33,
            ),
            158 => 
            array (
                'id' => 666,
                'name' => 'Ioba',
                'country_id' => 33,
            ),
            159 => 
            array (
                'id' => 667,
                'name' => 'Kadiogo',
                'country_id' => 33,
            ),
            160 => 
            array (
                'id' => 668,
                'name' => 'Kénédougou',
                'country_id' => 33,
            ),
            161 => 
            array (
                'id' => 669,
                'name' => 'Komondjari',
                'country_id' => 33,
            ),
            162 => 
            array (
                'id' => 670,
                'name' => 'Kompienga',
                'country_id' => 33,
            ),
            163 => 
            array (
                'id' => 671,
                'name' => 'Koulpélogo',
                'country_id' => 33,
            ),
            164 => 
            array (
                'id' => 672,
                'name' => 'Kossi',
                'country_id' => 33,
            ),
            165 => 
            array (
                'id' => 673,
                'name' => 'Kouritenga',
                'country_id' => 33,
            ),
            166 => 
            array (
                'id' => 674,
                'name' => 'Kourwéogo',
                'country_id' => 33,
            ),
            167 => 
            array (
                'id' => 675,
                'name' => 'Léraba',
                'country_id' => 33,
            ),
            168 => 
            array (
                'id' => 676,
                'name' => 'Loroum',
                'country_id' => 33,
            ),
            169 => 
            array (
                'id' => 677,
                'name' => 'Mouhoun',
                'country_id' => 33,
            ),
            170 => 
            array (
                'id' => 678,
                'name' => 'Namentenga',
                'country_id' => 33,
            ),
            171 => 
            array (
                'id' => 679,
                'name' => 'Nahouri',
                'country_id' => 33,
            ),
            172 => 
            array (
                'id' => 680,
                'name' => 'Nayala',
                'country_id' => 33,
            ),
            173 => 
            array (
                'id' => 681,
                'name' => 'Noumbiel',
                'country_id' => 33,
            ),
            174 => 
            array (
                'id' => 682,
                'name' => 'Oubritenga',
                'country_id' => 33,
            ),
            175 => 
            array (
                'id' => 683,
                'name' => 'Oudalan',
                'country_id' => 33,
            ),
            176 => 
            array (
                'id' => 684,
                'name' => 'Passoré',
                'country_id' => 33,
            ),
            177 => 
            array (
                'id' => 685,
                'name' => 'Poni',
                'country_id' => 33,
            ),
            178 => 
            array (
                'id' => 686,
                'name' => 'Séno',
                'country_id' => 33,
            ),
            179 => 
            array (
                'id' => 687,
                'name' => 'Sissili',
                'country_id' => 33,
            ),
            180 => 
            array (
                'id' => 688,
                'name' => 'Sanmatenga',
                'country_id' => 33,
            ),
            181 => 
            array (
                'id' => 689,
                'name' => 'Sanguié',
                'country_id' => 33,
            ),
            182 => 
            array (
                'id' => 690,
                'name' => 'Soum',
                'country_id' => 33,
            ),
            183 => 
            array (
                'id' => 691,
                'name' => 'Sourou',
                'country_id' => 33,
            ),
            184 => 
            array (
                'id' => 692,
                'name' => 'Tapoa',
                'country_id' => 33,
            ),
            185 => 
            array (
                'id' => 693,
                'name' => 'Tui',
                'country_id' => 33,
            ),
            186 => 
            array (
                'id' => 694,
                'name' => 'Yagha',
                'country_id' => 33,
            ),
            187 => 
            array (
                'id' => 695,
                'name' => 'Yatenga',
                'country_id' => 33,
            ),
            188 => 
            array (
                'id' => 696,
                'name' => 'Ziro',
                'country_id' => 33,
            ),
            189 => 
            array (
                'id' => 697,
                'name' => 'Zondoma',
                'country_id' => 33,
            ),
            190 => 
            array (
                'id' => 698,
                'name' => 'Zoundwéogo',
                'country_id' => 33,
            ),
            191 => 
            array (
                'id' => 699,
                'name' => 'Blagoevgrad',
                'country_id' => 32,
            ),
            192 => 
            array (
                'id' => 700,
                'name' => 'Burgas',
                'country_id' => 32,
            ),
            193 => 
            array (
                'id' => 701,
                'name' => 'Varna',
                'country_id' => 32,
            ),
            194 => 
            array (
                'id' => 702,
                'name' => 'Veliko Tarnovo',
                'country_id' => 32,
            ),
            195 => 
            array (
                'id' => 703,
                'name' => 'Vidin',
                'country_id' => 32,
            ),
            196 => 
            array (
                'id' => 704,
                'name' => 'Vratsa',
                'country_id' => 32,
            ),
            197 => 
            array (
                'id' => 705,
                'name' => 'Gabrovo',
                'country_id' => 32,
            ),
            198 => 
            array (
                'id' => 706,
                'name' => 'Dobrich',
                'country_id' => 32,
            ),
            199 => 
            array (
                'id' => 707,
                'name' => 'Kardzhali',
                'country_id' => 32,
            ),
            200 => 
            array (
                'id' => 708,
                'name' => 'Kjustendil',
                'country_id' => 32,
            ),
            201 => 
            array (
                'id' => 709,
                'name' => 'Lovech',
                'country_id' => 32,
            ),
            202 => 
            array (
                'id' => 710,
                'name' => 'Montana',
                'country_id' => 32,
            ),
            203 => 
            array (
                'id' => 711,
                'name' => 'Pazardzik',
                'country_id' => 32,
            ),
            204 => 
            array (
                'id' => 712,
                'name' => 'Pernik',
                'country_id' => 32,
            ),
            205 => 
            array (
                'id' => 713,
                'name' => 'Pleven',
                'country_id' => 32,
            ),
            206 => 
            array (
                'id' => 714,
                'name' => 'Plovdiv',
                'country_id' => 32,
            ),
            207 => 
            array (
                'id' => 715,
                'name' => 'Razgrad',
                'country_id' => 32,
            ),
            208 => 
            array (
                'id' => 716,
                'name' => 'Ruse',
                'country_id' => 32,
            ),
            209 => 
            array (
                'id' => 717,
                'name' => 'Silistra',
                'country_id' => 32,
            ),
            210 => 
            array (
                'id' => 718,
                'name' => 'Sliven',
                'country_id' => 32,
            ),
            211 => 
            array (
                'id' => 719,
                'name' => 'Smolyan',
                'country_id' => 32,
            ),
            212 => 
            array (
                'id' => 720,
            'name' => 'Sofia (stolitsa)',
                'country_id' => 32,
            ),
            213 => 
            array (
                'id' => 721,
                'name' => 'Sofia',
                'country_id' => 32,
            ),
            214 => 
            array (
                'id' => 722,
                'name' => 'Stara Zagora',
                'country_id' => 32,
            ),
            215 => 
            array (
                'id' => 723,
                'name' => 'Targovishte',
                'country_id' => 32,
            ),
            216 => 
            array (
                'id' => 724,
                'name' => 'Haskovo',
                'country_id' => 32,
            ),
            217 => 
            array (
                'id' => 725,
                'name' => 'Shumen',
                'country_id' => 32,
            ),
            218 => 
            array (
                'id' => 726,
                'name' => 'Yambol',
                'country_id' => 32,
            ),
            219 => 
            array (
                'id' => 730,
                'name' => 'Al Wustá',
                'country_id' => 17,
            ),
            220 => 
            array (
                'id' => 731,
                'name' => 'Bubanza',
                'country_id' => 34,
            ),
            221 => 
            array (
                'id' => 732,
                'name' => 'Bujumbura Rural',
                'country_id' => 34,
            ),
            222 => 
            array (
                'id' => 733,
                'name' => 'Bujumbura Mairie',
                'country_id' => 34,
            ),
            223 => 
            array (
                'id' => 734,
                'name' => 'Bururi',
                'country_id' => 34,
            ),
            224 => 
            array (
                'id' => 735,
                'name' => 'Cankuzo',
                'country_id' => 34,
            ),
            225 => 
            array (
                'id' => 736,
                'name' => 'Cibitoke',
                'country_id' => 34,
            ),
            226 => 
            array (
                'id' => 737,
                'name' => 'Gitega',
                'country_id' => 34,
            ),
            227 => 
            array (
                'id' => 738,
                'name' => 'Kirundo',
                'country_id' => 34,
            ),
            228 => 
            array (
                'id' => 739,
                'name' => 'Karuzi',
                'country_id' => 34,
            ),
            229 => 
            array (
                'id' => 740,
                'name' => 'Kayanza',
                'country_id' => 34,
            ),
            230 => 
            array (
                'id' => 741,
                'name' => 'Makamba',
                'country_id' => 34,
            ),
            231 => 
            array (
                'id' => 742,
                'name' => 'Muramvya',
                'country_id' => 34,
            ),
            232 => 
            array (
                'id' => 743,
                'name' => 'Mwaro',
                'country_id' => 34,
            ),
            233 => 
            array (
                'id' => 744,
                'name' => 'Muyinga',
                'country_id' => 34,
            ),
            234 => 
            array (
                'id' => 745,
                'name' => 'Ngozi',
                'country_id' => 34,
            ),
            235 => 
            array (
                'id' => 746,
                'name' => 'Rutana',
                'country_id' => 34,
            ),
            236 => 
            array (
                'id' => 747,
                'name' => 'Ruyigi',
                'country_id' => 34,
            ),
            237 => 
            array (
                'id' => 748,
                'name' => 'Atakora',
                'country_id' => 23,
            ),
            238 => 
            array (
                'id' => 749,
                'name' => 'Alibori',
                'country_id' => 23,
            ),
            239 => 
            array (
                'id' => 750,
                'name' => 'Atlantique',
                'country_id' => 23,
            ),
            240 => 
            array (
                'id' => 751,
                'name' => 'Borgou',
                'country_id' => 23,
            ),
            241 => 
            array (
                'id' => 752,
                'name' => 'Collines',
                'country_id' => 23,
            ),
            242 => 
            array (
                'id' => 753,
                'name' => 'Donga',
                'country_id' => 23,
            ),
            243 => 
            array (
                'id' => 754,
                'name' => 'Kouffo',
                'country_id' => 23,
            ),
            244 => 
            array (
                'id' => 755,
                'name' => 'Littoral',
                'country_id' => 23,
            ),
            245 => 
            array (
                'id' => 756,
                'name' => 'Mono',
                'country_id' => 23,
            ),
            246 => 
            array (
                'id' => 757,
                'name' => 'Ouémé',
                'country_id' => 23,
            ),
            247 => 
            array (
                'id' => 758,
                'name' => 'Plateau',
                'country_id' => 23,
            ),
            248 => 
            array (
                'id' => 759,
                'name' => 'Zou',
                'country_id' => 23,
            ),
            249 => 
            array (
                'id' => 760,
                'name' => 'Belait',
                'country_id' => 31,
            ),
            250 => 
            array (
                'id' => 761,
                'name' => 'Brunei-Muara',
                'country_id' => 31,
            ),
            251 => 
            array (
                'id' => 762,
                'name' => 'Temburong',
                'country_id' => 31,
            ),
            252 => 
            array (
                'id' => 763,
                'name' => 'Tutong',
                'country_id' => 31,
            ),
            253 => 
            array (
                'id' => 764,
                'name' => 'El Beni',
                'country_id' => 26,
            ),
            254 => 
            array (
                'id' => 765,
                'name' => 'Cochabamba',
                'country_id' => 26,
            ),
            255 => 
            array (
                'id' => 766,
                'name' => 'Chuquisaca',
                'country_id' => 26,
            ),
            256 => 
            array (
                'id' => 767,
                'name' => 'La Paz',
                'country_id' => 26,
            ),
            257 => 
            array (
                'id' => 768,
                'name' => 'Pando',
                'country_id' => 26,
            ),
            258 => 
            array (
                'id' => 769,
                'name' => 'Oruro',
                'country_id' => 26,
            ),
            259 => 
            array (
                'id' => 770,
                'name' => 'Potosí',
                'country_id' => 26,
            ),
            260 => 
            array (
                'id' => 771,
                'name' => 'Santa Cruz',
                'country_id' => 26,
            ),
            261 => 
            array (
                'id' => 772,
                'name' => 'Tarija',
                'country_id' => 26,
            ),
            262 => 
            array (
                'id' => 773,
                'name' => 'Acre',
                'country_id' => 29,
            ),
            263 => 
            array (
                'id' => 774,
                'name' => 'Alagoas',
                'country_id' => 29,
            ),
            264 => 
            array (
                'id' => 775,
                'name' => 'Amazonas',
                'country_id' => 29,
            ),
            265 => 
            array (
                'id' => 776,
                'name' => 'Amapá',
                'country_id' => 29,
            ),
            266 => 
            array (
                'id' => 777,
                'name' => 'Bahia',
                'country_id' => 29,
            ),
            267 => 
            array (
                'id' => 778,
                'name' => 'Ceará',
                'country_id' => 29,
            ),
            268 => 
            array (
                'id' => 779,
                'name' => 'Distrito Federal',
                'country_id' => 29,
            ),
            269 => 
            array (
                'id' => 780,
                'name' => 'Espírito Santo',
                'country_id' => 29,
            ),
            270 => 
            array (
                'id' => 781,
                'name' => 'Goiás',
                'country_id' => 29,
            ),
            271 => 
            array (
                'id' => 782,
                'name' => 'Maranhão',
                'country_id' => 29,
            ),
            272 => 
            array (
                'id' => 783,
                'name' => 'Minas Gerais',
                'country_id' => 29,
            ),
            273 => 
            array (
                'id' => 784,
                'name' => 'Mato Grosso do Sul',
                'country_id' => 29,
            ),
            274 => 
            array (
                'id' => 785,
                'name' => 'Mato Grosso',
                'country_id' => 29,
            ),
            275 => 
            array (
                'id' => 786,
                'name' => 'Pará',
                'country_id' => 29,
            ),
            276 => 
            array (
                'id' => 787,
                'name' => 'Paraíba',
                'country_id' => 29,
            ),
            277 => 
            array (
                'id' => 788,
                'name' => 'Pernambuco',
                'country_id' => 29,
            ),
            278 => 
            array (
                'id' => 789,
                'name' => 'Piauí',
                'country_id' => 29,
            ),
            279 => 
            array (
                'id' => 790,
                'name' => 'Paraná',
                'country_id' => 29,
            ),
            280 => 
            array (
                'id' => 791,
                'name' => 'Rio de Janeiro',
                'country_id' => 29,
            ),
            281 => 
            array (
                'id' => 792,
                'name' => 'Rio Grande do Norte',
                'country_id' => 29,
            ),
            282 => 
            array (
                'id' => 793,
                'name' => 'Rondônia',
                'country_id' => 29,
            ),
            283 => 
            array (
                'id' => 794,
                'name' => 'Roraima',
                'country_id' => 29,
            ),
            284 => 
            array (
                'id' => 795,
                'name' => 'Rio Grande do Sul',
                'country_id' => 29,
            ),
            285 => 
            array (
                'id' => 796,
                'name' => 'Santa Catarina',
                'country_id' => 29,
            ),
            286 => 
            array (
                'id' => 797,
                'name' => 'Sergipe',
                'country_id' => 29,
            ),
            287 => 
            array (
                'id' => 798,
                'name' => 'São Paulo',
                'country_id' => 29,
            ),
            288 => 
            array (
                'id' => 799,
                'name' => 'Tocantins',
                'country_id' => 29,
            ),
            289 => 
            array (
                'id' => 800,
                'name' => 'Acklins',
                'country_id' => 16,
            ),
            290 => 
            array (
                'id' => 801,
                'name' => 'Bimini',
                'country_id' => 16,
            ),
            291 => 
            array (
                'id' => 802,
                'name' => 'Black Point',
                'country_id' => 16,
            ),
            292 => 
            array (
                'id' => 803,
                'name' => 'Berry Islands',
                'country_id' => 16,
            ),
            293 => 
            array (
                'id' => 804,
                'name' => 'Central Eleuthera',
                'country_id' => 16,
            ),
            294 => 
            array (
                'id' => 805,
                'name' => 'Cat Island',
                'country_id' => 16,
            ),
            295 => 
            array (
                'id' => 806,
                'name' => 'Crooked Island and Long Cay',
                'country_id' => 16,
            ),
            296 => 
            array (
                'id' => 807,
                'name' => 'Central Abaco',
                'country_id' => 16,
            ),
            297 => 
            array (
                'id' => 808,
                'name' => 'Central Andros',
                'country_id' => 16,
            ),
            298 => 
            array (
                'id' => 809,
                'name' => 'East Grand Bahama',
                'country_id' => 16,
            ),
            299 => 
            array (
                'id' => 810,
                'name' => 'Exuma',
                'country_id' => 16,
            ),
            300 => 
            array (
                'id' => 811,
                'name' => 'City of Freeport',
                'country_id' => 16,
            ),
            301 => 
            array (
                'id' => 812,
                'name' => 'Grand Cay',
                'country_id' => 16,
            ),
            302 => 
            array (
                'id' => 813,
                'name' => 'Harbour Island',
                'country_id' => 16,
            ),
            303 => 
            array (
                'id' => 814,
                'name' => 'Hope Town',
                'country_id' => 16,
            ),
            304 => 
            array (
                'id' => 815,
                'name' => 'Inagua',
                'country_id' => 16,
            ),
            305 => 
            array (
                'id' => 816,
                'name' => 'Long Island',
                'country_id' => 16,
            ),
            306 => 
            array (
                'id' => 817,
                'name' => 'Mangrove Cay',
                'country_id' => 16,
            ),
            307 => 
            array (
                'id' => 818,
                'name' => 'Mayaguana',
                'country_id' => 16,
            ),
            308 => 
            array (
                'id' => 819,
                'name' => 'Moore\'s Island',
                'country_id' => 16,
            ),
            309 => 
            array (
                'id' => 820,
                'name' => 'North Eleuthera',
                'country_id' => 16,
            ),
            310 => 
            array (
                'id' => 821,
                'name' => 'North Abaco',
                'country_id' => 16,
            ),
            311 => 
            array (
                'id' => 822,
                'name' => 'North Andros',
                'country_id' => 16,
            ),
            312 => 
            array (
                'id' => 823,
                'name' => 'Rum Cay',
                'country_id' => 16,
            ),
            313 => 
            array (
                'id' => 824,
                'name' => 'Ragged Island',
                'country_id' => 16,
            ),
            314 => 
            array (
                'id' => 825,
                'name' => 'South Andros',
                'country_id' => 16,
            ),
            315 => 
            array (
                'id' => 826,
                'name' => 'South Eleuthera',
                'country_id' => 16,
            ),
            316 => 
            array (
                'id' => 827,
                'name' => 'South Abaco',
                'country_id' => 16,
            ),
            317 => 
            array (
                'id' => 828,
                'name' => 'San Salvador',
                'country_id' => 16,
            ),
            318 => 
            array (
                'id' => 829,
                'name' => 'Spanish Wells',
                'country_id' => 16,
            ),
            319 => 
            array (
                'id' => 830,
                'name' => 'West Grand Bahama',
                'country_id' => 16,
            ),
            320 => 
            array (
                'id' => 831,
                'name' => 'Paro',
                'country_id' => 25,
            ),
            321 => 
            array (
                'id' => 832,
                'name' => 'Chhukha',
                'country_id' => 25,
            ),
            322 => 
            array (
                'id' => 833,
                'name' => 'Ha',
                'country_id' => 25,
            ),
            323 => 
            array (
                'id' => 834,
                'name' => 'Samtse',
                'country_id' => 25,
            ),
            324 => 
            array (
                'id' => 835,
                'name' => 'Thimphu',
                'country_id' => 25,
            ),
            325 => 
            array (
                'id' => 836,
                'name' => 'Tsirang',
                'country_id' => 25,
            ),
            326 => 
            array (
                'id' => 837,
                'name' => 'Dagana',
                'country_id' => 25,
            ),
            327 => 
            array (
                'id' => 838,
                'name' => 'Punakha',
                'country_id' => 25,
            ),
            328 => 
            array (
                'id' => 839,
                'name' => 'Wangdue Phodrang',
                'country_id' => 25,
            ),
            329 => 
            array (
                'id' => 840,
                'name' => 'Sarpang',
                'country_id' => 25,
            ),
            330 => 
            array (
                'id' => 841,
                'name' => 'Trongsa',
                'country_id' => 25,
            ),
            331 => 
            array (
                'id' => 842,
                'name' => 'Bumthang',
                'country_id' => 25,
            ),
            332 => 
            array (
                'id' => 843,
                'name' => 'Zhemgang',
                'country_id' => 25,
            ),
            333 => 
            array (
                'id' => 844,
                'name' => 'Trashigang',
                'country_id' => 25,
            ),
            334 => 
            array (
                'id' => 845,
                'name' => 'Monggar',
                'country_id' => 25,
            ),
            335 => 
            array (
                'id' => 846,
                'name' => 'Pemagatshel',
                'country_id' => 25,
            ),
            336 => 
            array (
                'id' => 847,
                'name' => 'Lhuentse',
                'country_id' => 25,
            ),
            337 => 
            array (
                'id' => 848,
                'name' => 'Samdrup Jongkha',
                'country_id' => 25,
            ),
            338 => 
            array (
                'id' => 849,
                'name' => 'Gasa',
                'country_id' => 25,
            ),
            339 => 
            array (
                'id' => 850,
                'name' => 'Trashi Yangtse',
                'country_id' => 25,
            ),
            340 => 
            array (
                'id' => 851,
                'name' => 'Central',
                'country_id' => 28,
            ),
            341 => 
            array (
                'id' => 852,
                'name' => 'Ghanzi',
                'country_id' => 28,
            ),
            342 => 
            array (
                'id' => 853,
                'name' => 'Kgalagadi',
                'country_id' => 28,
            ),
            343 => 
            array (
                'id' => 854,
                'name' => 'Kgatleng',
                'country_id' => 28,
            ),
            344 => 
            array (
                'id' => 855,
                'name' => 'Kweneng',
                'country_id' => 28,
            ),
            345 => 
            array (
                'id' => 856,
                'name' => 'North-East',
                'country_id' => 28,
            ),
            346 => 
            array (
                'id' => 857,
                'name' => 'North-West',
                'country_id' => 28,
            ),
            347 => 
            array (
                'id' => 858,
                'name' => 'South-East',
                'country_id' => 28,
            ),
            348 => 
            array (
                'id' => 859,
                'name' => 'Southern',
                'country_id' => 28,
            ),
            349 => 
            array (
                'id' => 860,
            'name' => 'Bresckaja voblasc (be) - Brestskaja oblast\' (ru)',
                'country_id' => 20,
            ),
            350 => 
            array (
                'id' => 861,
                'name' => 'Horad Minsk',
                'country_id' => 20,
            ),
            351 => 
            array (
                'id' => 862,
            'name' => 'Homyel\'skaya voblasts\' (be) - Gomel\'skaya oblast\' (ru)',
                'country_id' => 20,
            ),
            352 => 
            array (
                'id' => 863,
            'name' => 'Hrodzenskaya voblasts\' (be) - Grodnenskaya oblast\' (ru)',
                'country_id' => 20,
            ),
            353 => 
            array (
                'id' => 864,
            'name' => 'Mahilyowskaya voblasts\' (be) - Mogilevskaya oblast\' (ru)',
                'country_id' => 20,
            ),
            354 => 
            array (
                'id' => 865,
            'name' => 'Minskaya voblasts\' (be) - Minskaya oblast\' (ru)',
                'country_id' => 20,
            ),
            355 => 
            array (
                'id' => 866,
            'name' => 'Vitsyebskaya voblasts\' (be) - Vitebskaya oblast\' (ru)',
                'country_id' => 20,
            ),
            356 => 
            array (
                'id' => 867,
                'name' => 'Belize',
                'country_id' => 22,
            ),
            357 => 
            array (
                'id' => 868,
                'name' => 'Cayo',
                'country_id' => 22,
            ),
            358 => 
            array (
                'id' => 869,
                'name' => 'Corozal',
                'country_id' => 22,
            ),
            359 => 
            array (
                'id' => 870,
                'name' => 'Orange Walk',
                'country_id' => 22,
            ),
            360 => 
            array (
                'id' => 871,
                'name' => 'Stann Creek',
                'country_id' => 22,
            ),
            361 => 
            array (
                'id' => 872,
                'name' => 'Toledo',
                'country_id' => 22,
            ),
            362 => 
            array (
                'id' => 873,
                'name' => 'Alberta',
                'country_id' => 37,
            ),
            363 => 
            array (
                'id' => 874,
                'name' => 'British Columbia',
                'country_id' => 37,
            ),
            364 => 
            array (
                'id' => 875,
                'name' => 'Manitoba',
                'country_id' => 37,
            ),
            365 => 
            array (
                'id' => 876,
                'name' => 'New Brunswick',
                'country_id' => 37,
            ),
            366 => 
            array (
                'id' => 877,
                'name' => 'Newfoundland and Labrador',
                'country_id' => 37,
            ),
            367 => 
            array (
                'id' => 878,
                'name' => 'Nova Scotia',
                'country_id' => 37,
            ),
            368 => 
            array (
                'id' => 879,
                'name' => 'Northwest Territories',
                'country_id' => 37,
            ),
            369 => 
            array (
                'id' => 880,
                'name' => 'Nunavut',
                'country_id' => 37,
            ),
            370 => 
            array (
                'id' => 881,
                'name' => 'Ontario',
                'country_id' => 37,
            ),
            371 => 
            array (
                'id' => 882,
                'name' => 'Prince Edward Island',
                'country_id' => 37,
            ),
            372 => 
            array (
                'id' => 883,
                'name' => 'Quebec',
                'country_id' => 37,
            ),
            373 => 
            array (
                'id' => 884,
                'name' => 'Saskatchewan',
                'country_id' => 37,
            ),
            374 => 
            array (
                'id' => 885,
                'name' => 'Yukon Territory',
                'country_id' => 37,
            ),
            375 => 
            array (
                'id' => 886,
                'name' => 'Bas-Congo',
                'country_id' => 49,
            ),
            376 => 
            array (
                'id' => 887,
                'name' => 'Bandundu',
                'country_id' => 49,
            ),
            377 => 
            array (
                'id' => 888,
                'name' => 'Équateur',
                'country_id' => 49,
            ),
            378 => 
            array (
                'id' => 889,
                'name' => 'Katanga',
                'country_id' => 49,
            ),
            379 => 
            array (
                'id' => 890,
                'name' => 'Kasai-Oriental',
                'country_id' => 49,
            ),
            380 => 
            array (
                'id' => 891,
                'name' => 'Kinshasa',
                'country_id' => 49,
            ),
            381 => 
            array (
                'id' => 892,
                'name' => 'Kasai-Occidental',
                'country_id' => 49,
            ),
            382 => 
            array (
                'id' => 893,
                'name' => 'Maniema',
                'country_id' => 49,
            ),
            383 => 
            array (
                'id' => 894,
                'name' => 'Nord-Kivu',
                'country_id' => 49,
            ),
            384 => 
            array (
                'id' => 895,
                'name' => 'Orientale',
                'country_id' => 49,
            ),
            385 => 
            array (
                'id' => 896,
                'name' => 'Sud-Kivu',
                'country_id' => 49,
            ),
            386 => 
            array (
                'id' => 897,
                'name' => 'Ouham',
                'country_id' => 40,
            ),
            387 => 
            array (
                'id' => 898,
                'name' => 'Bamingui-Bangoran',
                'country_id' => 40,
            ),
            388 => 
            array (
                'id' => 899,
                'name' => 'Bangui',
                'country_id' => 40,
            ),
            389 => 
            array (
                'id' => 900,
                'name' => 'Basse-Kotto',
                'country_id' => 40,
            ),
            390 => 
            array (
                'id' => 901,
                'name' => 'Haute-Kotto',
                'country_id' => 40,
            ),
            391 => 
            array (
                'id' => 902,
                'name' => 'Haut-Mbomou',
                'country_id' => 40,
            ),
            392 => 
            array (
                'id' => 903,
                'name' => 'Mambéré-Kadéï',
                'country_id' => 40,
            ),
            393 => 
            array (
                'id' => 904,
                'name' => 'Gribingui',
                'country_id' => 40,
            ),
            394 => 
            array (
                'id' => 905,
                'name' => 'Kémo-Gribingui',
                'country_id' => 40,
            ),
            395 => 
            array (
                'id' => 906,
                'name' => 'Lobaye',
                'country_id' => 40,
            ),
            396 => 
            array (
                'id' => 907,
                'name' => 'Mbomou',
                'country_id' => 40,
            ),
            397 => 
            array (
                'id' => 908,
                'name' => 'Ombella-Mpoko',
                'country_id' => 40,
            ),
            398 => 
            array (
                'id' => 909,
                'name' => 'Nana-Mambéré',
                'country_id' => 40,
            ),
            399 => 
            array (
                'id' => 910,
                'name' => 'Ouham-Pendé',
                'country_id' => 40,
            ),
            400 => 
            array (
                'id' => 911,
                'name' => 'Sangha',
                'country_id' => 40,
            ),
            401 => 
            array (
                'id' => 912,
                'name' => 'Ouaka',
                'country_id' => 40,
            ),
            402 => 
            array (
                'id' => 913,
                'name' => 'Vakaga',
                'country_id' => 40,
            ),
            403 => 
            array (
                'id' => 914,
                'name' => 'Bouenza',
                'country_id' => 48,
            ),
            404 => 
            array (
                'id' => 915,
                'name' => 'Pool',
                'country_id' => 48,
            ),
            405 => 
            array (
                'id' => 916,
                'name' => 'Sangha',
                'country_id' => 48,
            ),
            406 => 
            array (
                'id' => 917,
                'name' => 'Plateaux',
                'country_id' => 48,
            ),
            407 => 
            array (
                'id' => 918,
                'name' => 'Cuvette-Ouest',
                'country_id' => 48,
            ),
            408 => 
            array (
                'id' => 919,
                'name' => 'Lékoumou',
                'country_id' => 48,
            ),
            409 => 
            array (
                'id' => 920,
                'name' => 'Kouilou',
                'country_id' => 48,
            ),
            410 => 
            array (
                'id' => 921,
                'name' => 'Likouala',
                'country_id' => 48,
            ),
            411 => 
            array (
                'id' => 922,
                'name' => 'Cuvette',
                'country_id' => 48,
            ),
            412 => 
            array (
                'id' => 923,
                'name' => 'Niari',
                'country_id' => 48,
            ),
            413 => 
            array (
                'id' => 924,
                'name' => 'Brazzaville',
                'country_id' => 48,
            ),
            414 => 
            array (
                'id' => 925,
            'name' => 'Aargau (de)',
                'country_id' => 209,
            ),
            415 => 
            array (
                'id' => 926,
            'name' => 'Appenzell Innerrhoden (de)',
                'country_id' => 209,
            ),
            416 => 
            array (
                'id' => 927,
            'name' => 'Appenzell Ausserrhoden (de)',
                'country_id' => 209,
            ),
            417 => 
            array (
                'id' => 928,
            'name' => 'Bern (de)',
                'country_id' => 209,
            ),
            418 => 
            array (
                'id' => 929,
            'name' => 'Basel-Landschaft (de)',
                'country_id' => 209,
            ),
            419 => 
            array (
                'id' => 930,
            'name' => 'Basel-Stadt (de)',
                'country_id' => 209,
            ),
            420 => 
            array (
                'id' => 931,
            'name' => 'Fribourg (fr)',
                'country_id' => 209,
            ),
            421 => 
            array (
                'id' => 932,
            'name' => 'Genève (fr)',
                'country_id' => 209,
            ),
            422 => 
            array (
                'id' => 933,
            'name' => 'Glarus (de)',
                'country_id' => 209,
            ),
            423 => 
            array (
                'id' => 934,
            'name' => 'Graubünden (de)',
                'country_id' => 209,
            ),
            424 => 
            array (
                'id' => 935,
            'name' => 'Jura (fr)',
                'country_id' => 209,
            ),
            425 => 
            array (
                'id' => 936,
            'name' => 'Luzern (de)',
                'country_id' => 209,
            ),
            426 => 
            array (
                'id' => 937,
            'name' => 'Neuchâtel (fr)',
                'country_id' => 209,
            ),
            427 => 
            array (
                'id' => 938,
            'name' => 'Nidwalden (de)',
                'country_id' => 209,
            ),
            428 => 
            array (
                'id' => 939,
            'name' => 'Obwalden (de)',
                'country_id' => 209,
            ),
            429 => 
            array (
                'id' => 940,
            'name' => 'Sankt Gallen (de)',
                'country_id' => 209,
            ),
            430 => 
            array (
                'id' => 941,
            'name' => 'Schaffhausen (de)',
                'country_id' => 209,
            ),
            431 => 
            array (
                'id' => 942,
            'name' => 'Solothurn (de)',
                'country_id' => 209,
            ),
            432 => 
            array (
                'id' => 943,
            'name' => 'Schwyz (de)',
                'country_id' => 209,
            ),
            433 => 
            array (
                'id' => 944,
            'name' => 'Thurgau (de)',
                'country_id' => 209,
            ),
            434 => 
            array (
                'id' => 945,
            'name' => 'Ticino (it)',
                'country_id' => 209,
            ),
            435 => 
            array (
                'id' => 946,
            'name' => 'Uri (de)',
                'country_id' => 209,
            ),
            436 => 
            array (
                'id' => 947,
            'name' => 'Vaud (fr)',
                'country_id' => 209,
            ),
            437 => 
            array (
                'id' => 948,
            'name' => 'Valais (fr)',
                'country_id' => 209,
            ),
            438 => 
            array (
                'id' => 949,
            'name' => 'Zug (de)',
                'country_id' => 209,
            ),
            439 => 
            array (
                'id' => 950,
            'name' => 'Zürich (de)',
                'country_id' => 209,
            ),
            440 => 
            array (
                'id' => 951,
            'name' => 'Lagunes (Région des)',
                'country_id' => 52,
            ),
            441 => 
            array (
                'id' => 952,
            'name' => 'Haut-Sassandra (Région du)',
                'country_id' => 52,
            ),
            442 => 
            array (
                'id' => 953,
            'name' => 'Savanes (Région des)',
                'country_id' => 52,
            ),
            443 => 
            array (
                'id' => 954,
            'name' => 'Vallée du Bandama (Région de la)',
                'country_id' => 52,
            ),
            444 => 
            array (
                'id' => 955,
            'name' => 'Moyen-Comoé (Région du)',
                'country_id' => 52,
            ),
            445 => 
            array (
                'id' => 956,
            'name' => '18 Montagnes (Région des)',
                'country_id' => 52,
            ),
            446 => 
            array (
                'id' => 957,
            'name' => 'Lacs (Région des)',
                'country_id' => 52,
            ),
            447 => 
            array (
                'id' => 958,
            'name' => 'Zanzan (Région du)',
                'country_id' => 52,
            ),
            448 => 
            array (
                'id' => 959,
            'name' => 'Bas-Sassandra (Région du)',
                'country_id' => 52,
            ),
            449 => 
            array (
                'id' => 960,
            'name' => 'Denguélé (Région du)',
                'country_id' => 52,
            ),
            450 => 
            array (
                'id' => 961,
            'name' => 'Nzi-Comoé (Région)',
                'country_id' => 52,
            ),
            451 => 
            array (
                'id' => 962,
            'name' => 'Marahoué (Région de la)',
                'country_id' => 52,
            ),
            452 => 
            array (
                'id' => 963,
            'name' => 'Sud-Comoé (Région du)',
                'country_id' => 52,
            ),
            453 => 
            array (
                'id' => 964,
            'name' => 'Worodougou (Région du)',
                'country_id' => 52,
            ),
            454 => 
            array (
                'id' => 965,
            'name' => 'Sud-Bandama (Région du)',
                'country_id' => 52,
            ),
            455 => 
            array (
                'id' => 966,
            'name' => 'Agnébi (Région de l\')',
                'country_id' => 52,
            ),
            456 => 
            array (
                'id' => 967,
            'name' => 'Bafing (Région du)',
                'country_id' => 52,
            ),
            457 => 
            array (
                'id' => 968,
            'name' => 'Fromager (Région du)',
                'country_id' => 52,
            ),
            458 => 
            array (
                'id' => 969,
            'name' => 'Moyen-Cavally (Région du)',
                'country_id' => 52,
            ),
            459 => 
            array (
                'id' => 970,
                'name' => 'Aisén del General Carlos Ibáñez del Campo',
                'country_id' => 42,
            ),
            460 => 
            array (
                'id' => 971,
                'name' => 'Antofagasta',
                'country_id' => 42,
            ),
            461 => 
            array (
                'id' => 972,
                'name' => 'Arica y Parinacota',
                'country_id' => 42,
            ),
            462 => 
            array (
                'id' => 973,
                'name' => 'Araucanía',
                'country_id' => 42,
            ),
            463 => 
            array (
                'id' => 974,
                'name' => 'Atacama',
                'country_id' => 42,
            ),
            464 => 
            array (
                'id' => 975,
                'name' => 'Bío-Bío',
                'country_id' => 42,
            ),
            465 => 
            array (
                'id' => 976,
                'name' => 'Coquimbo',
                'country_id' => 42,
            ),
            466 => 
            array (
                'id' => 977,
                'name' => 'Libertador General Bernardo O\'Higgins',
                'country_id' => 42,
            ),
            467 => 
            array (
                'id' => 978,
                'name' => 'Los Lagos',
                'country_id' => 42,
            ),
            468 => 
            array (
                'id' => 979,
                'name' => 'Los Ríos',
                'country_id' => 42,
            ),
            469 => 
            array (
                'id' => 980,
                'name' => 'Magallanes',
                'country_id' => 42,
            ),
            470 => 
            array (
                'id' => 981,
                'name' => 'Maule',
                'country_id' => 42,
            ),
            471 => 
            array (
                'id' => 982,
                'name' => 'Región Metropolitana de Santiago',
                'country_id' => 42,
            ),
            472 => 
            array (
                'id' => 983,
                'name' => 'Tarapacá',
                'country_id' => 42,
            ),
            473 => 
            array (
                'id' => 984,
                'name' => 'Valparaíso',
                'country_id' => 42,
            ),
            474 => 
            array (
                'id' => 985,
                'name' => 'Adamaoua',
                'country_id' => 36,
            ),
            475 => 
            array (
                'id' => 986,
                'name' => 'Centre',
                'country_id' => 36,
            ),
            476 => 
            array (
                'id' => 987,
                'name' => 'Far North',
                'country_id' => 36,
            ),
            477 => 
            array (
                'id' => 988,
                'name' => 'East',
                'country_id' => 36,
            ),
            478 => 
            array (
                'id' => 989,
                'name' => 'Littoral',
                'country_id' => 36,
            ),
            479 => 
            array (
                'id' => 990,
                'name' => 'North',
                'country_id' => 36,
            ),
            480 => 
            array (
                'id' => 991,
                'name' => 'North-West',
                'country_id' => 36,
            ),
            481 => 
            array (
                'id' => 992,
                'name' => 'West',
                'country_id' => 36,
            ),
            482 => 
            array (
                'id' => 993,
                'name' => 'South',
                'country_id' => 36,
            ),
            483 => 
            array (
                'id' => 994,
                'name' => 'South-West',
                'country_id' => 36,
            ),
            484 => 
            array (
                'id' => 995,
                'name' => 'Beijing',
                'country_id' => 43,
            ),
            485 => 
            array (
                'id' => 996,
                'name' => 'Tianjin',
                'country_id' => 43,
            ),
            486 => 
            array (
                'id' => 997,
                'name' => 'Hebei',
                'country_id' => 43,
            ),
            487 => 
            array (
                'id' => 998,
                'name' => 'Shanxi',
                'country_id' => 43,
            ),
            488 => 
            array (
                'id' => 999,
            'name' => 'Nei Mongol (mn)',
                'country_id' => 43,
            ),
            489 => 
            array (
                'id' => 1000,
                'name' => 'Liaoning',
                'country_id' => 43,
            ),
            490 => 
            array (
                'id' => 1001,
                'name' => 'Jilin',
                'country_id' => 43,
            ),
            491 => 
            array (
                'id' => 1002,
                'name' => 'Heilongjiang',
                'country_id' => 43,
            ),
            492 => 
            array (
                'id' => 1003,
                'name' => 'Shanghai',
                'country_id' => 43,
            ),
            493 => 
            array (
                'id' => 1004,
                'name' => 'Jiangsu',
                'country_id' => 43,
            ),
            494 => 
            array (
                'id' => 1005,
                'name' => 'Zhejiang',
                'country_id' => 43,
            ),
            495 => 
            array (
                'id' => 1006,
                'name' => 'Anhui',
                'country_id' => 43,
            ),
            496 => 
            array (
                'id' => 1007,
                'name' => 'Fujian',
                'country_id' => 43,
            ),
            497 => 
            array (
                'id' => 1008,
                'name' => 'Jiangxi',
                'country_id' => 43,
            ),
            498 => 
            array (
                'id' => 1009,
                'name' => 'Shandong',
                'country_id' => 43,
            ),
            499 => 
            array (
                'id' => 1010,
                'name' => 'Henan',
                'country_id' => 43,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 1011,
                'name' => 'Hubei',
                'country_id' => 43,
            ),
            1 => 
            array (
                'id' => 1012,
                'name' => 'Hunan',
                'country_id' => 43,
            ),
            2 => 
            array (
                'id' => 1013,
                'name' => 'Guangdong',
                'country_id' => 43,
            ),
            3 => 
            array (
                'id' => 1014,
                'name' => 'Guangxi',
                'country_id' => 43,
            ),
            4 => 
            array (
                'id' => 1015,
                'name' => 'Hainan',
                'country_id' => 43,
            ),
            5 => 
            array (
                'id' => 1016,
                'name' => 'Chongqing',
                'country_id' => 43,
            ),
            6 => 
            array (
                'id' => 1017,
                'name' => 'Sichuan',
                'country_id' => 43,
            ),
            7 => 
            array (
                'id' => 1018,
                'name' => 'Guizhou',
                'country_id' => 43,
            ),
            8 => 
            array (
                'id' => 1019,
                'name' => 'Yunnan',
                'country_id' => 43,
            ),
            9 => 
            array (
                'id' => 1020,
                'name' => 'Xizang',
                'country_id' => 43,
            ),
            10 => 
            array (
                'id' => 1021,
                'name' => 'Shaanxi',
                'country_id' => 43,
            ),
            11 => 
            array (
                'id' => 1022,
                'name' => 'Gansu',
                'country_id' => 43,
            ),
            12 => 
            array (
                'id' => 1023,
                'name' => 'Qinghai',
                'country_id' => 43,
            ),
            13 => 
            array (
                'id' => 1024,
                'name' => 'Ningxia',
                'country_id' => 43,
            ),
            14 => 
            array (
                'id' => 1025,
                'name' => 'Xinjiang',
                'country_id' => 43,
            ),
            15 => 
            array (
                'id' => 1026,
                'name' => 'Taiwan',
                'country_id' => 43,
            ),
            16 => 
            array (
                'id' => 1027,
                'name' => 'Xianggang',
                'country_id' => 43,
            ),
            17 => 
            array (
                'id' => 1028,
                'name' => 'Aomen',
                'country_id' => 43,
            ),
            18 => 
            array (
                'id' => 1029,
                'name' => 'Amazonas',
                'country_id' => 46,
            ),
            19 => 
            array (
                'id' => 1030,
                'name' => 'Antioquia',
                'country_id' => 46,
            ),
            20 => 
            array (
                'id' => 1031,
                'name' => 'Arauca',
                'country_id' => 46,
            ),
            21 => 
            array (
                'id' => 1032,
                'name' => 'Atlántico',
                'country_id' => 46,
            ),
            22 => 
            array (
                'id' => 1033,
                'name' => 'Bolívar',
                'country_id' => 46,
            ),
            23 => 
            array (
                'id' => 1034,
                'name' => 'Boyacá',
                'country_id' => 46,
            ),
            24 => 
            array (
                'id' => 1035,
                'name' => 'Caldas',
                'country_id' => 46,
            ),
            25 => 
            array (
                'id' => 1036,
                'name' => 'Caquetá',
                'country_id' => 46,
            ),
            26 => 
            array (
                'id' => 1037,
                'name' => 'Casanare',
                'country_id' => 46,
            ),
            27 => 
            array (
                'id' => 1038,
                'name' => 'Cauca',
                'country_id' => 46,
            ),
            28 => 
            array (
                'id' => 1039,
                'name' => 'Cesar',
                'country_id' => 46,
            ),
            29 => 
            array (
                'id' => 1040,
                'name' => 'Chocó',
                'country_id' => 46,
            ),
            30 => 
            array (
                'id' => 1041,
                'name' => 'Córdoba',
                'country_id' => 46,
            ),
            31 => 
            array (
                'id' => 1042,
                'name' => 'Cundinamarca',
                'country_id' => 46,
            ),
            32 => 
            array (
                'id' => 1043,
                'name' => 'Distrito Capital de Bogotá',
                'country_id' => 46,
            ),
            33 => 
            array (
                'id' => 1044,
                'name' => 'Guainía',
                'country_id' => 46,
            ),
            34 => 
            array (
                'id' => 1045,
                'name' => 'Guaviare',
                'country_id' => 46,
            ),
            35 => 
            array (
                'id' => 1046,
                'name' => 'Huila',
                'country_id' => 46,
            ),
            36 => 
            array (
                'id' => 1047,
                'name' => 'La Guajira',
                'country_id' => 46,
            ),
            37 => 
            array (
                'id' => 1048,
                'name' => 'Magdalena',
                'country_id' => 46,
            ),
            38 => 
            array (
                'id' => 1049,
                'name' => 'Meta',
                'country_id' => 46,
            ),
            39 => 
            array (
                'id' => 1050,
                'name' => 'Nariño',
                'country_id' => 46,
            ),
            40 => 
            array (
                'id' => 1051,
                'name' => 'Norte de Santander',
                'country_id' => 46,
            ),
            41 => 
            array (
                'id' => 1052,
                'name' => 'Putumayo',
                'country_id' => 46,
            ),
            42 => 
            array (
                'id' => 1053,
                'name' => 'Quindío',
                'country_id' => 46,
            ),
            43 => 
            array (
                'id' => 1054,
                'name' => 'Risaralda',
                'country_id' => 46,
            ),
            44 => 
            array (
                'id' => 1055,
                'name' => 'Santander',
                'country_id' => 46,
            ),
            45 => 
            array (
                'id' => 1056,
                'name' => '"San Andrés',
                'country_id' => 46,
            ),
            46 => 
            array (
                'id' => 1057,
                'name' => 'Sucre',
                'country_id' => 46,
            ),
            47 => 
            array (
                'id' => 1058,
                'name' => 'Tolima',
                'country_id' => 46,
            ),
            48 => 
            array (
                'id' => 1059,
                'name' => 'Valle del Cauca',
                'country_id' => 46,
            ),
            49 => 
            array (
                'id' => 1060,
                'name' => 'Vaupés',
                'country_id' => 46,
            ),
            50 => 
            array (
                'id' => 1061,
                'name' => 'Vichada',
                'country_id' => 46,
            ),
            51 => 
            array (
                'id' => 1062,
                'name' => 'Alajuela',
                'country_id' => 51,
            ),
            52 => 
            array (
                'id' => 1063,
                'name' => 'Cartago',
                'country_id' => 51,
            ),
            53 => 
            array (
                'id' => 1064,
                'name' => 'Guanacaste',
                'country_id' => 51,
            ),
            54 => 
            array (
                'id' => 1065,
                'name' => 'Heredia',
                'country_id' => 51,
            ),
            55 => 
            array (
                'id' => 1066,
                'name' => 'Limón',
                'country_id' => 51,
            ),
            56 => 
            array (
                'id' => 1067,
                'name' => 'Puntarenas',
                'country_id' => 51,
            ),
            57 => 
            array (
                'id' => 1068,
                'name' => 'San José',
                'country_id' => 51,
            ),
            58 => 
            array (
                'id' => 1069,
                'name' => 'Pinar del Río',
                'country_id' => 54,
            ),
            59 => 
            array (
                'id' => 1070,
                'name' => 'La Habana',
                'country_id' => 54,
            ),
            60 => 
            array (
                'id' => 1071,
                'name' => 'Ciudad de La Habana',
                'country_id' => 54,
            ),
            61 => 
            array (
                'id' => 1072,
                'name' => 'Matanzas',
                'country_id' => 54,
            ),
            62 => 
            array (
                'id' => 1073,
                'name' => 'Villa Clara',
                'country_id' => 54,
            ),
            63 => 
            array (
                'id' => 1074,
                'name' => 'Cienfuegos',
                'country_id' => 54,
            ),
            64 => 
            array (
                'id' => 1075,
                'name' => 'Sancti Spíritus',
                'country_id' => 54,
            ),
            65 => 
            array (
                'id' => 1076,
                'name' => 'Ciego de Ávila',
                'country_id' => 54,
            ),
            66 => 
            array (
                'id' => 1077,
                'name' => 'Camagüey',
                'country_id' => 54,
            ),
            67 => 
            array (
                'id' => 1078,
                'name' => 'Las Tunas',
                'country_id' => 54,
            ),
            68 => 
            array (
                'id' => 1079,
                'name' => 'Holguín',
                'country_id' => 54,
            ),
            69 => 
            array (
                'id' => 1080,
                'name' => 'Granma',
                'country_id' => 54,
            ),
            70 => 
            array (
                'id' => 1081,
                'name' => 'Santiago de Cuba',
                'country_id' => 54,
            ),
            71 => 
            array (
                'id' => 1082,
                'name' => 'Guantánamo',
                'country_id' => 54,
            ),
            72 => 
            array (
                'id' => 1083,
                'name' => 'Isla de la Juventud',
                'country_id' => 54,
            ),
            73 => 
            array (
                'id' => 1084,
                'name' => 'Brava',
                'country_id' => 38,
            ),
            74 => 
            array (
                'id' => 1085,
                'name' => 'Boa Vista',
                'country_id' => 38,
            ),
            75 => 
            array (
                'id' => 1086,
                'name' => 'Santa Catarina',
                'country_id' => 38,
            ),
            76 => 
            array (
                'id' => 1087,
                'name' => 'Santa Catarina do Fogo',
                'country_id' => 38,
            ),
            77 => 
            array (
                'id' => 1088,
                'name' => 'Santa Cruz',
                'country_id' => 38,
            ),
            78 => 
            array (
                'id' => 1089,
                'name' => 'Maio',
                'country_id' => 38,
            ),
            79 => 
            array (
                'id' => 1090,
                'name' => 'Mosteiros',
                'country_id' => 38,
            ),
            80 => 
            array (
                'id' => 1091,
                'name' => 'Paul',
                'country_id' => 38,
            ),
            81 => 
            array (
                'id' => 1092,
                'name' => 'Porto Novo',
                'country_id' => 38,
            ),
            82 => 
            array (
                'id' => 1093,
                'name' => 'Praia',
                'country_id' => 38,
            ),
            83 => 
            array (
                'id' => 1094,
                'name' => 'Ribeira Brava',
                'country_id' => 38,
            ),
            84 => 
            array (
                'id' => 1095,
                'name' => 'Ribeira Grande',
                'country_id' => 38,
            ),
            85 => 
            array (
                'id' => 1096,
                'name' => 'Ribeira Grande de Santiago',
                'country_id' => 38,
            ),
            86 => 
            array (
                'id' => 1097,
                'name' => 'São Domingos',
                'country_id' => 38,
            ),
            87 => 
            array (
                'id' => 1098,
                'name' => 'São Filipe',
                'country_id' => 38,
            ),
            88 => 
            array (
                'id' => 1099,
                'name' => 'Sal',
                'country_id' => 38,
            ),
            89 => 
            array (
                'id' => 1100,
                'name' => 'São Miguel',
                'country_id' => 38,
            ),
            90 => 
            array (
                'id' => 1101,
                'name' => 'São Lourenço dos Órgãos',
                'country_id' => 38,
            ),
            91 => 
            array (
                'id' => 1102,
                'name' => 'São Salvador do Mundo',
                'country_id' => 38,
            ),
            92 => 
            array (
                'id' => 1103,
                'name' => 'São Vicente',
                'country_id' => 38,
            ),
            93 => 
            array (
                'id' => 1104,
                'name' => 'Tarrafal',
                'country_id' => 38,
            ),
            94 => 
            array (
                'id' => 1105,
                'name' => 'Tarrafal de São Nicolau',
                'country_id' => 38,
            ),
            95 => 
            array (
                'id' => 1106,
                'name' => 'Lefkosia',
                'country_id' => 56,
            ),
            96 => 
            array (
                'id' => 1107,
                'name' => 'Lemesos',
                'country_id' => 56,
            ),
            97 => 
            array (
                'id' => 1108,
                'name' => 'Larnaka',
                'country_id' => 56,
            ),
            98 => 
            array (
                'id' => 1109,
                'name' => 'Ammochostos',
                'country_id' => 56,
            ),
            99 => 
            array (
                'id' => 1110,
                'name' => 'Pafos',
                'country_id' => 56,
            ),
            100 => 
            array (
                'id' => 1111,
                'name' => 'Keryneia',
                'country_id' => 56,
            ),
            101 => 
            array (
                'id' => 1112,
                'name' => 'Jihoceský kraj',
                'country_id' => 57,
            ),
            102 => 
            array (
                'id' => 1113,
                'name' => 'Jihomoravský kraj',
                'country_id' => 57,
            ),
            103 => 
            array (
                'id' => 1114,
                'name' => 'Karlovarský kraj',
                'country_id' => 57,
            ),
            104 => 
            array (
                'id' => 1115,
                'name' => 'Královéhradecký kraj',
                'country_id' => 57,
            ),
            105 => 
            array (
                'id' => 1116,
                'name' => 'Liberecký kraj',
                'country_id' => 57,
            ),
            106 => 
            array (
                'id' => 1117,
                'name' => 'Moravskoslezský kraj',
                'country_id' => 57,
            ),
            107 => 
            array (
                'id' => 1118,
                'name' => 'Olomoucký kraj',
                'country_id' => 57,
            ),
            108 => 
            array (
                'id' => 1119,
                'name' => 'Pardubický kraj',
                'country_id' => 57,
            ),
            109 => 
            array (
                'id' => 1120,
                'name' => 'Plzenský kraj',
                'country_id' => 57,
            ),
            110 => 
            array (
                'id' => 1121,
                'name' => '"Praha',
                'country_id' => 57,
            ),
            111 => 
            array (
                'id' => 1122,
                'name' => 'Stredoceský kraj',
                'country_id' => 57,
            ),
            112 => 
            array (
                'id' => 1123,
                'name' => 'Ústecký kraj',
                'country_id' => 57,
            ),
            113 => 
            array (
                'id' => 1124,
                'name' => 'Vysocina',
                'country_id' => 57,
            ),
            114 => 
            array (
                'id' => 1125,
                'name' => 'Zlínský kraj',
                'country_id' => 57,
            ),
            115 => 
            array (
                'id' => 1126,
                'name' => 'Brandenburg',
                'country_id' => 81,
            ),
            116 => 
            array (
                'id' => 1127,
                'name' => 'Berlin',
                'country_id' => 81,
            ),
            117 => 
            array (
                'id' => 1128,
                'name' => 'Baden-Württemberg',
                'country_id' => 81,
            ),
            118 => 
            array (
                'id' => 1129,
                'name' => 'Bayern',
                'country_id' => 81,
            ),
            119 => 
            array (
                'id' => 1130,
                'name' => 'Bremen',
                'country_id' => 81,
            ),
            120 => 
            array (
                'id' => 1131,
                'name' => 'Hessen',
                'country_id' => 81,
            ),
            121 => 
            array (
                'id' => 1132,
                'name' => 'Hamburg',
                'country_id' => 81,
            ),
            122 => 
            array (
                'id' => 1133,
                'name' => 'Mecklenburg-Vorpommern',
                'country_id' => 81,
            ),
            123 => 
            array (
                'id' => 1134,
                'name' => 'Niedersachsen',
                'country_id' => 81,
            ),
            124 => 
            array (
                'id' => 1135,
                'name' => 'Nordrhein-Westfalen',
                'country_id' => 81,
            ),
            125 => 
            array (
                'id' => 1136,
                'name' => 'Rheinland-Pfalz',
                'country_id' => 81,
            ),
            126 => 
            array (
                'id' => 1137,
                'name' => 'Schleswig-Holstein',
                'country_id' => 81,
            ),
            127 => 
            array (
                'id' => 1138,
                'name' => 'Saarland',
                'country_id' => 81,
            ),
            128 => 
            array (
                'id' => 1139,
                'name' => 'Sachsen',
                'country_id' => 81,
            ),
            129 => 
            array (
                'id' => 1140,
                'name' => 'Sachsen-Anhalt',
                'country_id' => 81,
            ),
            130 => 
            array (
                'id' => 1141,
                'name' => 'Thüringen',
                'country_id' => 81,
            ),
            131 => 
            array (
                'id' => 1147,
                'name' => 'Tadjourah',
                'country_id' => 60,
            ),
            132 => 
            array (
                'id' => 1148,
                'name' => 'Nordjylland',
                'country_id' => 58,
            ),
            133 => 
            array (
                'id' => 1149,
                'name' => 'Midtjylland',
                'country_id' => 58,
            ),
            134 => 
            array (
                'id' => 1150,
                'name' => 'Syddanmark',
                'country_id' => 58,
            ),
            135 => 
            array (
                'id' => 1151,
                'name' => 'Hovedstaden',
                'country_id' => 58,
            ),
            136 => 
            array (
                'id' => 1152,
                'name' => 'Sjælland',
                'country_id' => 58,
            ),
            137 => 
            array (
                'id' => 1153,
                'name' => 'Saint Andrew',
                'country_id' => 61,
            ),
            138 => 
            array (
                'id' => 1154,
                'name' => 'Saint David',
                'country_id' => 61,
            ),
            139 => 
            array (
                'id' => 1155,
                'name' => 'Saint George',
                'country_id' => 61,
            ),
            140 => 
            array (
                'id' => 1156,
                'name' => 'Saint John',
                'country_id' => 61,
            ),
            141 => 
            array (
                'id' => 1157,
                'name' => 'Saint Joseph',
                'country_id' => 61,
            ),
            142 => 
            array (
                'id' => 1158,
                'name' => 'Saint Luke',
                'country_id' => 61,
            ),
            143 => 
            array (
                'id' => 1159,
                'name' => 'Saint Mark',
                'country_id' => 61,
            ),
            144 => 
            array (
                'id' => 1160,
                'name' => 'Saint Patrick',
                'country_id' => 61,
            ),
            145 => 
            array (
                'id' => 1161,
                'name' => 'Saint Paul',
                'country_id' => 61,
            ),
            146 => 
            array (
                'id' => 1162,
                'name' => 'Saint Peter',
                'country_id' => 61,
            ),
            147 => 
            array (
                'id' => 1163,
            'name' => 'Distrito Nacional (Santo Domingo)',
                'country_id' => 62,
            ),
            148 => 
            array (
                'id' => 1164,
                'name' => 'Azua',
                'country_id' => 62,
            ),
            149 => 
            array (
                'id' => 1165,
                'name' => 'Bahoruco',
                'country_id' => 62,
            ),
            150 => 
            array (
                'id' => 1166,
                'name' => 'Barahona',
                'country_id' => 62,
            ),
            151 => 
            array (
                'id' => 1167,
                'name' => 'Dajabón',
                'country_id' => 62,
            ),
            152 => 
            array (
                'id' => 1168,
                'name' => 'Duarte',
                'country_id' => 62,
            ),
            153 => 
            array (
                'id' => 1169,
                'name' => 'La Estrelleta [Elías Piña]',
                'country_id' => 62,
            ),
            154 => 
            array (
                'id' => 1170,
                'name' => 'El Seybo [El Seibo]',
                'country_id' => 62,
            ),
            155 => 
            array (
                'id' => 1171,
                'name' => 'Espaillat',
                'country_id' => 62,
            ),
            156 => 
            array (
                'id' => 1172,
                'name' => 'Independencia',
                'country_id' => 62,
            ),
            157 => 
            array (
                'id' => 1173,
                'name' => 'La Altagracia',
                'country_id' => 62,
            ),
            158 => 
            array (
                'id' => 1174,
                'name' => 'La Romana',
                'country_id' => 62,
            ),
            159 => 
            array (
                'id' => 1175,
                'name' => 'La Vega',
                'country_id' => 62,
            ),
            160 => 
            array (
                'id' => 1176,
                'name' => 'María Trinidad Sánchez',
                'country_id' => 62,
            ),
            161 => 
            array (
                'id' => 1177,
                'name' => 'Monte Cristi',
                'country_id' => 62,
            ),
            162 => 
            array (
                'id' => 1178,
                'name' => 'Pedernales',
                'country_id' => 62,
            ),
            163 => 
            array (
                'id' => 1179,
                'name' => 'Peravia',
                'country_id' => 62,
            ),
            164 => 
            array (
                'id' => 1180,
                'name' => 'Puerto Plata',
                'country_id' => 62,
            ),
            165 => 
            array (
                'id' => 1181,
                'name' => 'Salcedo',
                'country_id' => 62,
            ),
            166 => 
            array (
                'id' => 1182,
                'name' => 'Samaná',
                'country_id' => 62,
            ),
            167 => 
            array (
                'id' => 1183,
                'name' => 'San Cristóbal',
                'country_id' => 62,
            ),
            168 => 
            array (
                'id' => 1184,
                'name' => 'San Juan',
                'country_id' => 62,
            ),
            169 => 
            array (
                'id' => 1185,
                'name' => 'San Pedro de Macorís',
                'country_id' => 62,
            ),
            170 => 
            array (
                'id' => 1186,
                'name' => 'Sánchez Ramírez',
                'country_id' => 62,
            ),
            171 => 
            array (
                'id' => 1187,
                'name' => 'Santiago',
                'country_id' => 62,
            ),
            172 => 
            array (
                'id' => 1188,
                'name' => 'Santiago Rodríguez',
                'country_id' => 62,
            ),
            173 => 
            array (
                'id' => 1189,
                'name' => 'Valverde',
                'country_id' => 62,
            ),
            174 => 
            array (
                'id' => 1190,
                'name' => 'Monseñor Nouel',
                'country_id' => 62,
            ),
            175 => 
            array (
                'id' => 1191,
                'name' => 'Monte Plata',
                'country_id' => 62,
            ),
            176 => 
            array (
                'id' => 1192,
                'name' => 'Hato Mayor',
                'country_id' => 62,
            ),
            177 => 
            array (
                'id' => 1193,
                'name' => 'San José de Ocoa',
                'country_id' => 62,
            ),
            178 => 
            array (
                'id' => 1194,
                'name' => 'Santo Domingo',
                'country_id' => 62,
            ),
            179 => 
            array (
                'id' => 1195,
                'name' => 'Tissemsilt',
                'country_id' => 3,
            ),
            180 => 
            array (
                'id' => 1196,
                'name' => 'Tipaza',
                'country_id' => 3,
            ),
            181 => 
            array (
                'id' => 1197,
                'name' => 'Azuay',
                'country_id' => 64,
            ),
            182 => 
            array (
                'id' => 1198,
                'name' => 'Bolívar',
                'country_id' => 64,
            ),
            183 => 
            array (
                'id' => 1199,
                'name' => 'Carchi',
                'country_id' => 64,
            ),
            184 => 
            array (
                'id' => 1200,
                'name' => 'Orellana',
                'country_id' => 64,
            ),
            185 => 
            array (
                'id' => 1201,
                'name' => 'Esmeraldas',
                'country_id' => 64,
            ),
            186 => 
            array (
                'id' => 1202,
                'name' => 'Cañar',
                'country_id' => 64,
            ),
            187 => 
            array (
                'id' => 1203,
                'name' => 'Guayas',
                'country_id' => 64,
            ),
            188 => 
            array (
                'id' => 1204,
                'name' => 'Chimborazo',
                'country_id' => 64,
            ),
            189 => 
            array (
                'id' => 1205,
                'name' => 'Imbabura',
                'country_id' => 64,
            ),
            190 => 
            array (
                'id' => 1206,
                'name' => 'Loja',
                'country_id' => 64,
            ),
            191 => 
            array (
                'id' => 1207,
                'name' => 'Manabí',
                'country_id' => 64,
            ),
            192 => 
            array (
                'id' => 1208,
                'name' => 'Napo',
                'country_id' => 64,
            ),
            193 => 
            array (
                'id' => 1209,
                'name' => 'El Oro',
                'country_id' => 64,
            ),
            194 => 
            array (
                'id' => 1210,
                'name' => 'Pichincha',
                'country_id' => 64,
            ),
            195 => 
            array (
                'id' => 1211,
                'name' => 'Los Ríos',
                'country_id' => 64,
            ),
            196 => 
            array (
                'id' => 1212,
                'name' => 'Morona-Santiago',
                'country_id' => 64,
            ),
            197 => 
            array (
                'id' => 1213,
                'name' => 'Santo Domingo de los Tsáchilas',
                'country_id' => 64,
            ),
            198 => 
            array (
                'id' => 1214,
                'name' => 'Santa Elena',
                'country_id' => 64,
            ),
            199 => 
            array (
                'id' => 1215,
                'name' => 'Tungurahua',
                'country_id' => 64,
            ),
            200 => 
            array (
                'id' => 1216,
                'name' => 'Sucumbíos',
                'country_id' => 64,
            ),
            201 => 
            array (
                'id' => 1217,
                'name' => 'Galápagos',
                'country_id' => 64,
            ),
            202 => 
            array (
                'id' => 1218,
                'name' => 'Cotopaxi',
                'country_id' => 64,
            ),
            203 => 
            array (
                'id' => 1219,
                'name' => 'Pastaza',
                'country_id' => 64,
            ),
            204 => 
            array (
                'id' => 1220,
                'name' => 'Zamora-Chinchipe',
                'country_id' => 64,
            ),
            205 => 
            array (
                'id' => 1221,
                'name' => 'Harjumaa',
                'country_id' => 69,
            ),
            206 => 
            array (
                'id' => 1222,
                'name' => 'Hiiumaa',
                'country_id' => 69,
            ),
            207 => 
            array (
                'id' => 1223,
                'name' => 'Ida-Virumaa',
                'country_id' => 69,
            ),
            208 => 
            array (
                'id' => 1224,
                'name' => 'Jõgevamaa',
                'country_id' => 69,
            ),
            209 => 
            array (
                'id' => 1225,
                'name' => 'Järvamaa',
                'country_id' => 69,
            ),
            210 => 
            array (
                'id' => 1226,
                'name' => 'Läänemaa',
                'country_id' => 69,
            ),
            211 => 
            array (
                'id' => 1227,
                'name' => 'Lääne-Virumaa',
                'country_id' => 69,
            ),
            212 => 
            array (
                'id' => 1228,
                'name' => 'Põlvamaa',
                'country_id' => 69,
            ),
            213 => 
            array (
                'id' => 1229,
                'name' => 'Pärnumaa',
                'country_id' => 69,
            ),
            214 => 
            array (
                'id' => 1230,
                'name' => 'Raplamaa',
                'country_id' => 69,
            ),
            215 => 
            array (
                'id' => 1231,
                'name' => 'Saaremaa',
                'country_id' => 69,
            ),
            216 => 
            array (
                'id' => 1232,
                'name' => 'Tartumaa',
                'country_id' => 69,
            ),
            217 => 
            array (
                'id' => 1233,
                'name' => 'Valgamaa',
                'country_id' => 69,
            ),
            218 => 
            array (
                'id' => 1234,
                'name' => 'Viljandimaa',
                'country_id' => 69,
            ),
            219 => 
            array (
                'id' => 1235,
                'name' => 'Võrumaa',
                'country_id' => 69,
            ),
            220 => 
            array (
                'id' => 1248,
                'name' => 'Hulwan',
                'country_id' => 65,
            ),
            221 => 
            array (
                'id' => 1254,
                'name' => 'Al Uqsur',
                'country_id' => 65,
            ),
            222 => 
            array (
                'id' => 1261,
                'name' => 'As Sadis min Uktubar',
                'country_id' => 65,
            ),
            223 => 
            array (
                'id' => 1264,
                'name' => '‘Anseba',
                'country_id' => 68,
            ),
            224 => 
            array (
                'id' => 1265,
                'name' => 'Debubawi K’eyyi? Ba?ri',
                'country_id' => 68,
            ),
            225 => 
            array (
                'id' => 1266,
                'name' => 'Debub',
                'country_id' => 68,
            ),
            226 => 
            array (
                'id' => 1267,
                'name' => 'Gash-Barka',
                'country_id' => 68,
            ),
            227 => 
            array (
                'id' => 1268,
                'name' => 'Ma’ikel',
                'country_id' => 68,
            ),
            228 => 
            array (
                'id' => 1269,
                'name' => 'Semienawi K’eyyi? Ba?ri',
                'country_id' => 68,
            ),
            229 => 
            array (
                'id' => 1270,
                'name' => 'Alicante / Alacant',
                'country_id' => 203,
            ),
            230 => 
            array (
                'id' => 1271,
                'name' => 'Albacete',
                'country_id' => 203,
            ),
            231 => 
            array (
                'id' => 1272,
                'name' => 'Almería',
                'country_id' => 203,
            ),
            232 => 
            array (
                'id' => 1273,
                'name' => 'Ávila',
                'country_id' => 203,
            ),
            233 => 
            array (
                'id' => 1274,
                'name' => 'Barcelona  [Barcelona]',
                'country_id' => 203,
            ),
            234 => 
            array (
                'id' => 1275,
                'name' => 'Badajoz',
                'country_id' => 203,
            ),
            235 => 
            array (
                'id' => 1276,
                'name' => 'Vizcaya / Biskaia',
                'country_id' => 203,
            ),
            236 => 
            array (
                'id' => 1277,
                'name' => 'Burgos',
                'country_id' => 203,
            ),
            237 => 
            array (
                'id' => 1278,
                'name' => 'A Coruña  [La Coruña]',
                'country_id' => 203,
            ),
            238 => 
            array (
                'id' => 1279,
                'name' => 'Cádiz',
                'country_id' => 203,
            ),
            239 => 
            array (
                'id' => 1280,
                'name' => 'Cáceres',
                'country_id' => 203,
            ),
            240 => 
            array (
                'id' => 1281,
                'name' => 'Ceuta',
                'country_id' => 203,
            ),
            241 => 
            array (
                'id' => 1282,
                'name' => 'Córdoba',
                'country_id' => 203,
            ),
            242 => 
            array (
                'id' => 1283,
                'name' => 'Ciudad Real',
                'country_id' => 203,
            ),
            243 => 
            array (
                'id' => 1284,
                'name' => 'Castellón / Castelló',
                'country_id' => 203,
            ),
            244 => 
            array (
                'id' => 1285,
                'name' => 'Cuenca',
                'country_id' => 203,
            ),
            245 => 
            array (
                'id' => 1286,
                'name' => 'Las Palmas',
                'country_id' => 203,
            ),
            246 => 
            array (
                'id' => 1287,
                'name' => 'Girona  [Gerona]',
                'country_id' => 203,
            ),
            247 => 
            array (
                'id' => 1288,
                'name' => 'Granada',
                'country_id' => 203,
            ),
            248 => 
            array (
                'id' => 1289,
                'name' => 'Guadalajara',
                'country_id' => 203,
            ),
            249 => 
            array (
                'id' => 1290,
                'name' => 'Huelva',
                'country_id' => 203,
            ),
            250 => 
            array (
                'id' => 1291,
                'name' => 'Huesca',
                'country_id' => 203,
            ),
            251 => 
            array (
                'id' => 1292,
                'name' => 'Jaén',
                'country_id' => 203,
            ),
            252 => 
            array (
                'id' => 1293,
                'name' => 'Lleida  [Lérida]',
                'country_id' => 203,
            ),
            253 => 
            array (
                'id' => 1294,
                'name' => 'León',
                'country_id' => 203,
            ),
            254 => 
            array (
                'id' => 1295,
                'name' => 'La Rioja',
                'country_id' => 203,
            ),
            255 => 
            array (
                'id' => 1296,
                'name' => 'Lugo  [Lugo]',
                'country_id' => 203,
            ),
            256 => 
            array (
                'id' => 1297,
                'name' => 'Madrid',
                'country_id' => 203,
            ),
            257 => 
            array (
                'id' => 1298,
                'name' => 'Málaga',
                'country_id' => 203,
            ),
            258 => 
            array (
                'id' => 1299,
                'name' => 'Melilla',
                'country_id' => 203,
            ),
            259 => 
            array (
                'id' => 1300,
                'name' => 'Murcia',
                'country_id' => 203,
            ),
            260 => 
            array (
                'id' => 1301,
                'name' => 'Navarra / Nafarroa',
                'country_id' => 203,
            ),
            261 => 
            array (
                'id' => 1302,
                'name' => 'Asturias',
                'country_id' => 203,
            ),
            262 => 
            array (
                'id' => 1303,
                'name' => 'Ourense  [Orense]',
                'country_id' => 203,
            ),
            263 => 
            array (
                'id' => 1304,
                'name' => 'Palencia',
                'country_id' => 203,
            ),
            264 => 
            array (
                'id' => 1305,
                'name' => 'Balears  [Baleares]',
                'country_id' => 203,
            ),
            265 => 
            array (
                'id' => 1306,
                'name' => 'Pontevedra  [Pontevedra]',
                'country_id' => 203,
            ),
            266 => 
            array (
                'id' => 1307,
                'name' => 'Cantabria',
                'country_id' => 203,
            ),
            267 => 
            array (
                'id' => 1308,
                'name' => 'Salamanca',
                'country_id' => 203,
            ),
            268 => 
            array (
                'id' => 1309,
                'name' => 'Sevilla',
                'country_id' => 203,
            ),
            269 => 
            array (
                'id' => 1310,
                'name' => 'Segovia',
                'country_id' => 203,
            ),
            270 => 
            array (
                'id' => 1311,
                'name' => 'Soria',
                'country_id' => 203,
            ),
            271 => 
            array (
                'id' => 1312,
                'name' => 'Guipúzcoa / Gipuzkoa',
                'country_id' => 203,
            ),
            272 => 
            array (
                'id' => 1313,
                'name' => 'Tarragona  [Tarragona]',
                'country_id' => 203,
            ),
            273 => 
            array (
                'id' => 1314,
                'name' => 'Teruel',
                'country_id' => 203,
            ),
            274 => 
            array (
                'id' => 1315,
                'name' => 'Santa Cruz de Tenerife',
                'country_id' => 203,
            ),
            275 => 
            array (
                'id' => 1316,
                'name' => 'Toledo',
                'country_id' => 203,
            ),
            276 => 
            array (
                'id' => 1317,
                'name' => 'Valencia / València',
                'country_id' => 203,
            ),
            277 => 
            array (
                'id' => 1318,
                'name' => 'Valladolid',
                'country_id' => 203,
            ),
            278 => 
            array (
                'id' => 1319,
                'name' => 'Álava / Araba',
                'country_id' => 203,
            ),
            279 => 
            array (
                'id' => 1320,
                'name' => 'Zaragoza',
                'country_id' => 203,
            ),
            280 => 
            array (
                'id' => 1321,
                'name' => 'Zamora',
                'country_id' => 203,
            ),
            281 => 
            array (
                'id' => 1322,
                'name' => 'Adis Abeba',
                'country_id' => 70,
            ),
            282 => 
            array (
                'id' => 1323,
                'name' => 'Afar',
                'country_id' => 70,
            ),
            283 => 
            array (
                'id' => 1324,
                'name' => 'Amara',
                'country_id' => 70,
            ),
            284 => 
            array (
                'id' => 1325,
                'name' => 'Binshangul Gumuz',
                'country_id' => 70,
            ),
            285 => 
            array (
                'id' => 1326,
                'name' => 'Dire Dawa',
                'country_id' => 70,
            ),
            286 => 
            array (
                'id' => 1327,
                'name' => 'Gambela Hizboch',
                'country_id' => 70,
            ),
            287 => 
            array (
                'id' => 1328,
                'name' => 'Hareri Hizb',
                'country_id' => 70,
            ),
            288 => 
            array (
                'id' => 1329,
                'name' => 'Oromiya',
                'country_id' => 70,
            ),
            289 => 
            array (
                'id' => 1330,
                'name' => 'YeDebub Biheroch Bihereseboch na Hizboch',
                'country_id' => 70,
            ),
            290 => 
            array (
                'id' => 1331,
                'name' => 'Sumale',
                'country_id' => 70,
            ),
            291 => 
            array (
                'id' => 1332,
                'name' => 'Tigray',
                'country_id' => 70,
            ),
            292 => 
            array (
                'id' => 1333,
                'name' => 'Ahvenanmaan maakunta',
                'country_id' => 74,
            ),
            293 => 
            array (
                'id' => 1334,
                'name' => 'Etelä-Karjala',
                'country_id' => 74,
            ),
            294 => 
            array (
                'id' => 1335,
                'name' => 'Etelä-Pohjanmaa',
                'country_id' => 74,
            ),
            295 => 
            array (
                'id' => 1336,
                'name' => 'Etelä-Savo',
                'country_id' => 74,
            ),
            296 => 
            array (
                'id' => 1337,
                'name' => 'Kainuu',
                'country_id' => 74,
            ),
            297 => 
            array (
                'id' => 1338,
                'name' => 'Kanta-Häme',
                'country_id' => 74,
            ),
            298 => 
            array (
                'id' => 1339,
                'name' => 'Keski-Pohjanmaa',
                'country_id' => 74,
            ),
            299 => 
            array (
                'id' => 1340,
                'name' => 'Keski-Suomi',
                'country_id' => 74,
            ),
            300 => 
            array (
                'id' => 1341,
                'name' => 'Kymenlaakso',
                'country_id' => 74,
            ),
            301 => 
            array (
                'id' => 1342,
                'name' => 'Lappi',
                'country_id' => 74,
            ),
            302 => 
            array (
                'id' => 1343,
                'name' => 'Pirkanmaa',
                'country_id' => 74,
            ),
            303 => 
            array (
                'id' => 1344,
                'name' => 'Pohjanmaa',
                'country_id' => 74,
            ),
            304 => 
            array (
                'id' => 1345,
                'name' => 'Pohjois-Karjala',
                'country_id' => 74,
            ),
            305 => 
            array (
                'id' => 1346,
                'name' => 'Pohjois-Pohjanmaa',
                'country_id' => 74,
            ),
            306 => 
            array (
                'id' => 1347,
                'name' => 'Pohjois-Savo',
                'country_id' => 74,
            ),
            307 => 
            array (
                'id' => 1348,
                'name' => 'Päijät-Häme',
                'country_id' => 74,
            ),
            308 => 
            array (
                'id' => 1349,
                'name' => 'Satakunta',
                'country_id' => 74,
            ),
            309 => 
            array (
                'id' => 1350,
                'name' => 'Uusimaa',
                'country_id' => 74,
            ),
            310 => 
            array (
                'id' => 1351,
                'name' => 'Varsinais-Suomi',
                'country_id' => 74,
            ),
            311 => 
            array (
                'id' => 1352,
                'name' => 'Central',
                'country_id' => 73,
            ),
            312 => 
            array (
                'id' => 1353,
                'name' => 'Eastern',
                'country_id' => 73,
            ),
            313 => 
            array (
                'id' => 1354,
                'name' => 'Northern',
                'country_id' => 73,
            ),
            314 => 
            array (
                'id' => 1355,
                'name' => 'Rotuma',
                'country_id' => 73,
            ),
            315 => 
            array (
                'id' => 1356,
                'name' => 'Western',
                'country_id' => 73,
            ),
            316 => 
            array (
                'id' => 1357,
                'name' => 'Kosrae',
                'country_id' => 142,
            ),
            317 => 
            array (
                'id' => 1358,
                'name' => 'Pohnpei',
                'country_id' => 142,
            ),
            318 => 
            array (
                'id' => 1359,
                'name' => 'Chuuk',
                'country_id' => 142,
            ),
            319 => 
            array (
                'id' => 1360,
                'name' => 'Yap',
                'country_id' => 142,
            ),
            320 => 
            array (
                'id' => 1361,
                'name' => 'Ain',
                'country_id' => 75,
            ),
            321 => 
            array (
                'id' => 1362,
                'name' => 'Aisne',
                'country_id' => 75,
            ),
            322 => 
            array (
                'id' => 1363,
                'name' => 'Allier',
                'country_id' => 75,
            ),
            323 => 
            array (
                'id' => 1364,
                'name' => 'Alpes-de-Haute-Provence',
                'country_id' => 75,
            ),
            324 => 
            array (
                'id' => 1365,
                'name' => 'Hautes-Alpes',
                'country_id' => 75,
            ),
            325 => 
            array (
                'id' => 1366,
                'name' => 'Alpes-Maritimes',
                'country_id' => 75,
            ),
            326 => 
            array (
                'id' => 1367,
                'name' => 'Ardèche',
                'country_id' => 75,
            ),
            327 => 
            array (
                'id' => 1368,
                'name' => 'Ardennes',
                'country_id' => 75,
            ),
            328 => 
            array (
                'id' => 1369,
                'name' => 'Ariège',
                'country_id' => 75,
            ),
            329 => 
            array (
                'id' => 1370,
                'name' => 'Aube',
                'country_id' => 75,
            ),
            330 => 
            array (
                'id' => 1371,
                'name' => 'Aude',
                'country_id' => 75,
            ),
            331 => 
            array (
                'id' => 1372,
                'name' => 'Aveyron',
                'country_id' => 75,
            ),
            332 => 
            array (
                'id' => 1373,
                'name' => 'Bouches-du-Rhône',
                'country_id' => 75,
            ),
            333 => 
            array (
                'id' => 1374,
                'name' => 'Calvados',
                'country_id' => 75,
            ),
            334 => 
            array (
                'id' => 1375,
                'name' => 'Cantal',
                'country_id' => 75,
            ),
            335 => 
            array (
                'id' => 1376,
                'name' => 'Charente',
                'country_id' => 75,
            ),
            336 => 
            array (
                'id' => 1377,
                'name' => 'Charente-Maritime',
                'country_id' => 75,
            ),
            337 => 
            array (
                'id' => 1378,
                'name' => 'Cher',
                'country_id' => 75,
            ),
            338 => 
            array (
                'id' => 1379,
                'name' => 'Corrèze',
                'country_id' => 75,
            ),
            339 => 
            array (
                'id' => 1380,
                'name' => 'Côte-d\'Or',
                'country_id' => 75,
            ),
            340 => 
            array (
                'id' => 1381,
                'name' => 'Côtes-d\'Armor',
                'country_id' => 75,
            ),
            341 => 
            array (
                'id' => 1382,
                'name' => 'Creuse',
                'country_id' => 75,
            ),
            342 => 
            array (
                'id' => 1383,
                'name' => 'Dordogne',
                'country_id' => 75,
            ),
            343 => 
            array (
                'id' => 1384,
                'name' => 'Doubs',
                'country_id' => 75,
            ),
            344 => 
            array (
                'id' => 1385,
                'name' => 'Drôme',
                'country_id' => 75,
            ),
            345 => 
            array (
                'id' => 1386,
                'name' => 'Eure',
                'country_id' => 75,
            ),
            346 => 
            array (
                'id' => 1387,
                'name' => 'Eure-et-Loir',
                'country_id' => 75,
            ),
            347 => 
            array (
                'id' => 1388,
                'name' => 'Finistère',
                'country_id' => 75,
            ),
            348 => 
            array (
                'id' => 1389,
                'name' => 'Corse-du-Sud',
                'country_id' => 75,
            ),
            349 => 
            array (
                'id' => 1390,
                'name' => 'Haute-Corse',
                'country_id' => 75,
            ),
            350 => 
            array (
                'id' => 1391,
                'name' => 'Gard',
                'country_id' => 75,
            ),
            351 => 
            array (
                'id' => 1392,
                'name' => 'Haute-Garonne',
                'country_id' => 75,
            ),
            352 => 
            array (
                'id' => 1393,
                'name' => 'Gers',
                'country_id' => 75,
            ),
            353 => 
            array (
                'id' => 1394,
                'name' => 'Gironde',
                'country_id' => 75,
            ),
            354 => 
            array (
                'id' => 1395,
                'name' => 'Hérault',
                'country_id' => 75,
            ),
            355 => 
            array (
                'id' => 1396,
                'name' => 'Ille-et-Vilaine',
                'country_id' => 75,
            ),
            356 => 
            array (
                'id' => 1397,
                'name' => 'Indre',
                'country_id' => 75,
            ),
            357 => 
            array (
                'id' => 1398,
                'name' => 'Indre-et-Loire',
                'country_id' => 75,
            ),
            358 => 
            array (
                'id' => 1399,
                'name' => 'Isère',
                'country_id' => 75,
            ),
            359 => 
            array (
                'id' => 1400,
                'name' => 'Jura',
                'country_id' => 75,
            ),
            360 => 
            array (
                'id' => 1401,
                'name' => 'Landes',
                'country_id' => 75,
            ),
            361 => 
            array (
                'id' => 1402,
                'name' => 'Loir-et-Cher',
                'country_id' => 75,
            ),
            362 => 
            array (
                'id' => 1403,
                'name' => 'Loire',
                'country_id' => 75,
            ),
            363 => 
            array (
                'id' => 1404,
                'name' => 'Haute-Loire',
                'country_id' => 75,
            ),
            364 => 
            array (
                'id' => 1405,
                'name' => 'Loire-Atlantique',
                'country_id' => 75,
            ),
            365 => 
            array (
                'id' => 1406,
                'name' => 'Loiret',
                'country_id' => 75,
            ),
            366 => 
            array (
                'id' => 1407,
                'name' => 'Lot',
                'country_id' => 75,
            ),
            367 => 
            array (
                'id' => 1408,
                'name' => 'Lot-et-Garonne',
                'country_id' => 75,
            ),
            368 => 
            array (
                'id' => 1409,
                'name' => 'Lozère',
                'country_id' => 75,
            ),
            369 => 
            array (
                'id' => 1410,
                'name' => 'Maine-et-Loire',
                'country_id' => 75,
            ),
            370 => 
            array (
                'id' => 1411,
                'name' => 'Manche',
                'country_id' => 75,
            ),
            371 => 
            array (
                'id' => 1412,
                'name' => 'Marne',
                'country_id' => 75,
            ),
            372 => 
            array (
                'id' => 1413,
                'name' => 'Haute-Marne',
                'country_id' => 75,
            ),
            373 => 
            array (
                'id' => 1414,
                'name' => 'Mayenne',
                'country_id' => 75,
            ),
            374 => 
            array (
                'id' => 1415,
                'name' => 'Meurthe-et-Moselle',
                'country_id' => 75,
            ),
            375 => 
            array (
                'id' => 1416,
                'name' => 'Meuse',
                'country_id' => 75,
            ),
            376 => 
            array (
                'id' => 1417,
                'name' => 'Morbihan',
                'country_id' => 75,
            ),
            377 => 
            array (
                'id' => 1418,
                'name' => 'Moselle',
                'country_id' => 75,
            ),
            378 => 
            array (
                'id' => 1419,
                'name' => 'Nièvre',
                'country_id' => 75,
            ),
            379 => 
            array (
                'id' => 1420,
                'name' => 'Nord',
                'country_id' => 75,
            ),
            380 => 
            array (
                'id' => 1421,
                'name' => 'Oise',
                'country_id' => 75,
            ),
            381 => 
            array (
                'id' => 1422,
                'name' => 'Orne',
                'country_id' => 75,
            ),
            382 => 
            array (
                'id' => 1423,
                'name' => 'Pas-de-Calais',
                'country_id' => 75,
            ),
            383 => 
            array (
                'id' => 1424,
                'name' => 'Puy-de-Dôme',
                'country_id' => 75,
            ),
            384 => 
            array (
                'id' => 1425,
                'name' => 'Pyrénées-Atlantiques',
                'country_id' => 75,
            ),
            385 => 
            array (
                'id' => 1426,
                'name' => 'Hautes-Pyrénées',
                'country_id' => 75,
            ),
            386 => 
            array (
                'id' => 1427,
                'name' => 'Pyrénées-Orientales',
                'country_id' => 75,
            ),
            387 => 
            array (
                'id' => 1428,
                'name' => 'Bas-Rhin',
                'country_id' => 75,
            ),
            388 => 
            array (
                'id' => 1429,
                'name' => 'Haut-Rhin',
                'country_id' => 75,
            ),
            389 => 
            array (
                'id' => 1430,
                'name' => 'Rhône',
                'country_id' => 75,
            ),
            390 => 
            array (
                'id' => 1431,
                'name' => 'Haute-Saône',
                'country_id' => 75,
            ),
            391 => 
            array (
                'id' => 1432,
                'name' => 'Saône-et-Loire',
                'country_id' => 75,
            ),
            392 => 
            array (
                'id' => 1433,
                'name' => 'Sarthe',
                'country_id' => 75,
            ),
            393 => 
            array (
                'id' => 1434,
                'name' => 'Savoie',
                'country_id' => 75,
            ),
            394 => 
            array (
                'id' => 1435,
                'name' => 'Haute-Savoie',
                'country_id' => 75,
            ),
            395 => 
            array (
                'id' => 1436,
                'name' => 'Paris',
                'country_id' => 75,
            ),
            396 => 
            array (
                'id' => 1437,
                'name' => 'Seine-Maritime',
                'country_id' => 75,
            ),
            397 => 
            array (
                'id' => 1438,
                'name' => 'Seine-et-Marne',
                'country_id' => 75,
            ),
            398 => 
            array (
                'id' => 1439,
                'name' => 'Yvelines',
                'country_id' => 75,
            ),
            399 => 
            array (
                'id' => 1440,
                'name' => 'Deux-Sèvres',
                'country_id' => 75,
            ),
            400 => 
            array (
                'id' => 1441,
                'name' => 'Somme',
                'country_id' => 75,
            ),
            401 => 
            array (
                'id' => 1442,
                'name' => 'Tarn',
                'country_id' => 75,
            ),
            402 => 
            array (
                'id' => 1443,
                'name' => 'Tarn-et-Garonne',
                'country_id' => 75,
            ),
            403 => 
            array (
                'id' => 1444,
                'name' => 'Var',
                'country_id' => 75,
            ),
            404 => 
            array (
                'id' => 1445,
                'name' => 'Vaucluse',
                'country_id' => 75,
            ),
            405 => 
            array (
                'id' => 1446,
                'name' => 'Vendée',
                'country_id' => 75,
            ),
            406 => 
            array (
                'id' => 1447,
                'name' => 'Vienne',
                'country_id' => 75,
            ),
            407 => 
            array (
                'id' => 1448,
                'name' => 'Haute-Vienne',
                'country_id' => 75,
            ),
            408 => 
            array (
                'id' => 1449,
                'name' => 'Vosges',
                'country_id' => 75,
            ),
            409 => 
            array (
                'id' => 1450,
                'name' => 'Yonne',
                'country_id' => 75,
            ),
            410 => 
            array (
                'id' => 1451,
                'name' => 'Territoire de Belfort',
                'country_id' => 75,
            ),
            411 => 
            array (
                'id' => 1452,
                'name' => 'Essonne',
                'country_id' => 75,
            ),
            412 => 
            array (
                'id' => 1453,
                'name' => 'Hauts-de-Seine',
                'country_id' => 75,
            ),
            413 => 
            array (
                'id' => 1454,
                'name' => 'Seine-Saint-Denis',
                'country_id' => 75,
            ),
            414 => 
            array (
                'id' => 1455,
                'name' => 'Val-de-Marne',
                'country_id' => 75,
            ),
            415 => 
            array (
                'id' => 1456,
                'name' => 'Val-d\'Oise',
                'country_id' => 75,
            ),
            416 => 
            array (
                'id' => 1457,
                'name' => 'Estuaire',
                'country_id' => 78,
            ),
            417 => 
            array (
                'id' => 1458,
                'name' => 'Haut-Ogooué',
                'country_id' => 78,
            ),
            418 => 
            array (
                'id' => 1459,
                'name' => 'Moyen-Ogooué',
                'country_id' => 78,
            ),
            419 => 
            array (
                'id' => 1460,
                'name' => 'Ngounié',
                'country_id' => 78,
            ),
            420 => 
            array (
                'id' => 1461,
                'name' => 'Nyanga',
                'country_id' => 78,
            ),
            421 => 
            array (
                'id' => 1462,
                'name' => 'Ogooué-Ivindo',
                'country_id' => 78,
            ),
            422 => 
            array (
                'id' => 1463,
                'name' => 'Ogooué-Lolo',
                'country_id' => 78,
            ),
            423 => 
            array (
                'id' => 1464,
                'name' => 'Ogooué-Maritime',
                'country_id' => 78,
            ),
            424 => 
            array (
                'id' => 1465,
                'name' => 'Woleu-Ntem',
                'country_id' => 78,
            ),
            425 => 
            array (
                'id' => 1466,
                'name' => 'Saint Andrew',
                'country_id' => 86,
            ),
            426 => 
            array (
                'id' => 1467,
                'name' => 'Saint David',
                'country_id' => 86,
            ),
            427 => 
            array (
                'id' => 1468,
                'name' => 'Saint George',
                'country_id' => 86,
            ),
            428 => 
            array (
                'id' => 1469,
                'name' => 'Saint John',
                'country_id' => 86,
            ),
            429 => 
            array (
                'id' => 1470,
                'name' => 'Saint Mark',
                'country_id' => 86,
            ),
            430 => 
            array (
                'id' => 1471,
                'name' => 'Saint Patrick',
                'country_id' => 86,
            ),
            431 => 
            array (
                'id' => 1472,
                'name' => 'Southern Grenadine Islands',
                'country_id' => 86,
            ),
            432 => 
            array (
                'id' => 1473,
                'name' => 'Abkhazia',
                'country_id' => 80,
            ),
            433 => 
            array (
                'id' => 1474,
                'name' => 'Ajaria',
                'country_id' => 80,
            ),
            434 => 
            array (
                'id' => 1475,
                'name' => 'Guria',
                'country_id' => 80,
            ),
            435 => 
            array (
                'id' => 1476,
                'name' => 'Imeret\'i',
                'country_id' => 80,
            ),
            436 => 
            array (
                'id' => 1477,
                'name' => 'Kakhet\'i',
                'country_id' => 80,
            ),
            437 => 
            array (
                'id' => 1478,
                'name' => 'K\'vemo K\'art\'li',
                'country_id' => 80,
            ),
            438 => 
            array (
                'id' => 1479,
                'name' => 'Mts\'khet\'a-Mt\'ianet\'i',
                'country_id' => 80,
            ),
            439 => 
            array (
                'id' => 1480,
                'name' => 'Racha-Lech’khumi-K’vemo Svanet’i',
                'country_id' => 80,
            ),
            440 => 
            array (
                'id' => 1481,
                'name' => 'Samts\'khe-Javakhet\'i',
                'country_id' => 80,
            ),
            441 => 
            array (
                'id' => 1482,
                'name' => 'Shida K\'art\'li',
                'country_id' => 80,
            ),
            442 => 
            array (
                'id' => 1483,
                'name' => 'Samegrelo-Zemo Svanet\'i',
                'country_id' => 80,
            ),
            443 => 
            array (
                'id' => 1484,
                'name' => 'T\'bilisi',
                'country_id' => 80,
            ),
            444 => 
            array (
                'id' => 1485,
                'name' => 'Greater Accra',
                'country_id' => 82,
            ),
            445 => 
            array (
                'id' => 1486,
                'name' => 'Ashanti',
                'country_id' => 82,
            ),
            446 => 
            array (
                'id' => 1487,
                'name' => 'Brong-Ahafo',
                'country_id' => 82,
            ),
            447 => 
            array (
                'id' => 1488,
                'name' => 'Central',
                'country_id' => 82,
            ),
            448 => 
            array (
                'id' => 1489,
                'name' => 'Eastern',
                'country_id' => 82,
            ),
            449 => 
            array (
                'id' => 1490,
                'name' => 'Northern',
                'country_id' => 82,
            ),
            450 => 
            array (
                'id' => 1491,
                'name' => 'Volta',
                'country_id' => 82,
            ),
            451 => 
            array (
                'id' => 1492,
                'name' => 'Upper East',
                'country_id' => 82,
            ),
            452 => 
            array (
                'id' => 1493,
                'name' => 'Upper West',
                'country_id' => 82,
            ),
            453 => 
            array (
                'id' => 1494,
                'name' => 'Western',
                'country_id' => 82,
            ),
            454 => 
            array (
                'id' => 1495,
                'name' => 'Kommune Kujalleq',
                'country_id' => 85,
            ),
            455 => 
            array (
                'id' => 1496,
                'name' => 'Qaasuitsup Kommunia',
                'country_id' => 85,
            ),
            456 => 
            array (
                'id' => 1497,
                'name' => 'Qeqqata Kommunia',
                'country_id' => 85,
            ),
            457 => 
            array (
                'id' => 1498,
                'name' => 'Kommuneqarfik Sermersooq',
                'country_id' => 85,
            ),
            458 => 
            array (
                'id' => 1499,
                'name' => 'Banjul',
                'country_id' => 79,
            ),
            459 => 
            array (
                'id' => 1500,
                'name' => 'Lower River',
                'country_id' => 79,
            ),
            460 => 
            array (
                'id' => 1501,
                'name' => 'Central River',
                'country_id' => 79,
            ),
            461 => 
            array (
                'id' => 1502,
                'name' => 'North Bank',
                'country_id' => 79,
            ),
            462 => 
            array (
                'id' => 1503,
                'name' => 'Upper River',
                'country_id' => 79,
            ),
            463 => 
            array (
                'id' => 1504,
                'name' => 'Western',
                'country_id' => 79,
            ),
            464 => 
            array (
                'id' => 1505,
                'name' => 'Beyla',
                'country_id' => 91,
            ),
            465 => 
            array (
                'id' => 1506,
                'name' => 'Boffa',
                'country_id' => 91,
            ),
            466 => 
            array (
                'id' => 1507,
                'name' => 'Boké',
                'country_id' => 91,
            ),
            467 => 
            array (
                'id' => 1508,
                'name' => 'Coyah',
                'country_id' => 91,
            ),
            468 => 
            array (
                'id' => 1509,
                'name' => 'Dabola',
                'country_id' => 91,
            ),
            469 => 
            array (
                'id' => 1510,
                'name' => 'Dinguiraye',
                'country_id' => 91,
            ),
            470 => 
            array (
                'id' => 1511,
                'name' => 'Dalaba',
                'country_id' => 91,
            ),
            471 => 
            array (
                'id' => 1512,
                'name' => 'Dubréka',
                'country_id' => 91,
            ),
            472 => 
            array (
                'id' => 1513,
                'name' => 'Faranah',
                'country_id' => 91,
            ),
            473 => 
            array (
                'id' => 1514,
                'name' => 'Forécariah',
                'country_id' => 91,
            ),
            474 => 
            array (
                'id' => 1515,
                'name' => 'Fria',
                'country_id' => 91,
            ),
            475 => 
            array (
                'id' => 1516,
                'name' => 'Gaoual',
                'country_id' => 91,
            ),
            476 => 
            array (
                'id' => 1517,
                'name' => 'Guékédou',
                'country_id' => 91,
            ),
            477 => 
            array (
                'id' => 1518,
                'name' => 'Kankan',
                'country_id' => 91,
            ),
            478 => 
            array (
                'id' => 1519,
                'name' => 'Koubia',
                'country_id' => 91,
            ),
            479 => 
            array (
                'id' => 1520,
                'name' => 'Kindia',
                'country_id' => 91,
            ),
            480 => 
            array (
                'id' => 1521,
                'name' => 'Kérouané',
                'country_id' => 91,
            ),
            481 => 
            array (
                'id' => 1522,
                'name' => 'Koundara',
                'country_id' => 91,
            ),
            482 => 
            array (
                'id' => 1523,
                'name' => 'Kouroussa',
                'country_id' => 91,
            ),
            483 => 
            array (
                'id' => 1524,
                'name' => 'Kissidougou',
                'country_id' => 91,
            ),
            484 => 
            array (
                'id' => 1525,
                'name' => 'Labé',
                'country_id' => 91,
            ),
            485 => 
            array (
                'id' => 1526,
                'name' => 'Lélouma',
                'country_id' => 91,
            ),
            486 => 
            array (
                'id' => 1527,
                'name' => 'Lola',
                'country_id' => 91,
            ),
            487 => 
            array (
                'id' => 1528,
                'name' => 'Macenta',
                'country_id' => 91,
            ),
            488 => 
            array (
                'id' => 1529,
                'name' => 'Mandiana',
                'country_id' => 91,
            ),
            489 => 
            array (
                'id' => 1530,
                'name' => 'Mali',
                'country_id' => 91,
            ),
            490 => 
            array (
                'id' => 1531,
                'name' => 'Mamou',
                'country_id' => 91,
            ),
            491 => 
            array (
                'id' => 1532,
                'name' => 'Nzérékoré',
                'country_id' => 91,
            ),
            492 => 
            array (
                'id' => 1533,
                'name' => 'Pita',
                'country_id' => 91,
            ),
            493 => 
            array (
                'id' => 1534,
                'name' => 'Siguiri',
                'country_id' => 91,
            ),
            494 => 
            array (
                'id' => 1535,
                'name' => 'Télimélé',
                'country_id' => 91,
            ),
            495 => 
            array (
                'id' => 1536,
                'name' => 'Tougué',
                'country_id' => 91,
            ),
            496 => 
            array (
                'id' => 1537,
                'name' => 'Yomou',
                'country_id' => 91,
            ),
            497 => 
            array (
                'id' => 1538,
                'name' => 'Annobón',
                'country_id' => 67,
            ),
            498 => 
            array (
                'id' => 1539,
                'name' => 'Bioko Norte',
                'country_id' => 67,
            ),
            499 => 
            array (
                'id' => 1540,
                'name' => 'Bioko Sur',
                'country_id' => 67,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 1541,
                'name' => 'Centro Sur',
                'country_id' => 67,
            ),
            1 => 
            array (
                'id' => 1542,
                'name' => 'Kié-Ntem',
                'country_id' => 67,
            ),
            2 => 
            array (
                'id' => 1543,
                'name' => 'Litoral',
                'country_id' => 67,
            ),
            3 => 
            array (
                'id' => 1544,
                'name' => 'Wele-Nzas',
                'country_id' => 67,
            ),
            4 => 
            array (
                'id' => 1545,
                'name' => 'Aitolia kai Akarnania',
                'country_id' => 84,
            ),
            5 => 
            array (
                'id' => 1546,
                'name' => 'Voiotia',
                'country_id' => 84,
            ),
            6 => 
            array (
                'id' => 1547,
                'name' => 'Evvoia',
                'country_id' => 84,
            ),
            7 => 
            array (
                'id' => 1548,
                'name' => 'Evrytania',
                'country_id' => 84,
            ),
            8 => 
            array (
                'id' => 1549,
                'name' => 'Fthiotida',
                'country_id' => 84,
            ),
            9 => 
            array (
                'id' => 1550,
                'name' => 'Fokida',
                'country_id' => 84,
            ),
            10 => 
            array (
                'id' => 1551,
                'name' => 'Argolida',
                'country_id' => 84,
            ),
            11 => 
            array (
                'id' => 1552,
                'name' => 'Arkadia',
                'country_id' => 84,
            ),
            12 => 
            array (
                'id' => 1553,
                'name' => 'Achaïa',
                'country_id' => 84,
            ),
            13 => 
            array (
                'id' => 1554,
                'name' => 'Ileia',
                'country_id' => 84,
            ),
            14 => 
            array (
                'id' => 1555,
                'name' => 'Korinthia',
                'country_id' => 84,
            ),
            15 => 
            array (
                'id' => 1556,
                'name' => 'Lakonia',
                'country_id' => 84,
            ),
            16 => 
            array (
                'id' => 1557,
                'name' => 'Messinia',
                'country_id' => 84,
            ),
            17 => 
            array (
                'id' => 1558,
                'name' => 'Zakynthos',
                'country_id' => 84,
            ),
            18 => 
            array (
                'id' => 1559,
                'name' => 'Kerkyra',
                'country_id' => 84,
            ),
            19 => 
            array (
                'id' => 1560,
                'name' => 'Kefallonia',
                'country_id' => 84,
            ),
            20 => 
            array (
                'id' => 1561,
                'name' => 'Lefkada',
                'country_id' => 84,
            ),
            21 => 
            array (
                'id' => 1562,
                'name' => 'Arta',
                'country_id' => 84,
            ),
            22 => 
            array (
                'id' => 1563,
                'name' => 'Thesprotia',
                'country_id' => 84,
            ),
            23 => 
            array (
                'id' => 1564,
                'name' => 'Ioannina',
                'country_id' => 84,
            ),
            24 => 
            array (
                'id' => 1565,
                'name' => 'Preveza',
                'country_id' => 84,
            ),
            25 => 
            array (
                'id' => 1566,
                'name' => 'Karditsa',
                'country_id' => 84,
            ),
            26 => 
            array (
                'id' => 1567,
                'name' => 'Larisa',
                'country_id' => 84,
            ),
            27 => 
            array (
                'id' => 1568,
                'name' => 'Magnisia',
                'country_id' => 84,
            ),
            28 => 
            array (
                'id' => 1569,
                'name' => 'Trikala',
                'country_id' => 84,
            ),
            29 => 
            array (
                'id' => 1570,
                'name' => 'Grevena',
                'country_id' => 84,
            ),
            30 => 
            array (
                'id' => 1571,
                'name' => 'Drama',
                'country_id' => 84,
            ),
            31 => 
            array (
                'id' => 1572,
                'name' => 'Imathia',
                'country_id' => 84,
            ),
            32 => 
            array (
                'id' => 1573,
                'name' => 'Thessaloniki',
                'country_id' => 84,
            ),
            33 => 
            array (
                'id' => 1574,
                'name' => 'Kavala',
                'country_id' => 84,
            ),
            34 => 
            array (
                'id' => 1575,
                'name' => 'Kastoria',
                'country_id' => 84,
            ),
            35 => 
            array (
                'id' => 1576,
                'name' => 'Kilkis',
                'country_id' => 84,
            ),
            36 => 
            array (
                'id' => 1577,
                'name' => 'Kozani',
                'country_id' => 84,
            ),
            37 => 
            array (
                'id' => 1578,
                'name' => 'Pella',
                'country_id' => 84,
            ),
            38 => 
            array (
                'id' => 1579,
                'name' => 'Pieria',
                'country_id' => 84,
            ),
            39 => 
            array (
                'id' => 1580,
                'name' => 'Serres',
                'country_id' => 84,
            ),
            40 => 
            array (
                'id' => 1581,
                'name' => 'Florina',
                'country_id' => 84,
            ),
            41 => 
            array (
                'id' => 1582,
                'name' => 'Chalkidiki',
                'country_id' => 84,
            ),
            42 => 
            array (
                'id' => 1583,
                'name' => 'Agio Oros',
                'country_id' => 84,
            ),
            43 => 
            array (
                'id' => 1584,
                'name' => 'Evros',
                'country_id' => 84,
            ),
            44 => 
            array (
                'id' => 1585,
                'name' => 'Xanthi',
                'country_id' => 84,
            ),
            45 => 
            array (
                'id' => 1586,
                'name' => 'Rodopi',
                'country_id' => 84,
            ),
            46 => 
            array (
                'id' => 1587,
                'name' => 'Dodekanisos',
                'country_id' => 84,
            ),
            47 => 
            array (
                'id' => 1588,
                'name' => 'Kyklades',
                'country_id' => 84,
            ),
            48 => 
            array (
                'id' => 1589,
                'name' => 'Lesvos',
                'country_id' => 84,
            ),
            49 => 
            array (
                'id' => 1590,
                'name' => 'Samos',
                'country_id' => 84,
            ),
            50 => 
            array (
                'id' => 1591,
                'name' => 'Chios',
                'country_id' => 84,
            ),
            51 => 
            array (
                'id' => 1592,
                'name' => 'Irakleio',
                'country_id' => 84,
            ),
            52 => 
            array (
                'id' => 1593,
                'name' => 'Lasithi',
                'country_id' => 84,
            ),
            53 => 
            array (
                'id' => 1594,
                'name' => 'Rethymno',
                'country_id' => 84,
            ),
            54 => 
            array (
                'id' => 1595,
                'name' => 'Chania',
                'country_id' => 84,
            ),
            55 => 
            array (
                'id' => 1596,
                'name' => 'Attiki',
                'country_id' => 84,
            ),
            56 => 
            array (
                'id' => 1597,
                'name' => 'Alta Verapaz',
                'country_id' => 89,
            ),
            57 => 
            array (
                'id' => 1598,
                'name' => 'Baja Verapaz',
                'country_id' => 89,
            ),
            58 => 
            array (
                'id' => 1599,
                'name' => 'Chimaltenango',
                'country_id' => 89,
            ),
            59 => 
            array (
                'id' => 1600,
                'name' => 'Chiquimula',
                'country_id' => 89,
            ),
            60 => 
            array (
                'id' => 1601,
                'name' => 'Escuintla',
                'country_id' => 89,
            ),
            61 => 
            array (
                'id' => 1602,
                'name' => 'Guatemala',
                'country_id' => 89,
            ),
            62 => 
            array (
                'id' => 1603,
                'name' => 'Huehuetenango',
                'country_id' => 89,
            ),
            63 => 
            array (
                'id' => 1604,
                'name' => 'Izabal',
                'country_id' => 89,
            ),
            64 => 
            array (
                'id' => 1605,
                'name' => 'Jalapa',
                'country_id' => 89,
            ),
            65 => 
            array (
                'id' => 1606,
                'name' => 'Jutiapa',
                'country_id' => 89,
            ),
            66 => 
            array (
                'id' => 1607,
                'name' => 'Petén',
                'country_id' => 89,
            ),
            67 => 
            array (
                'id' => 1608,
                'name' => 'El Progreso',
                'country_id' => 89,
            ),
            68 => 
            array (
                'id' => 1609,
                'name' => 'Quiché',
                'country_id' => 89,
            ),
            69 => 
            array (
                'id' => 1610,
                'name' => 'Quetzaltenango',
                'country_id' => 89,
            ),
            70 => 
            array (
                'id' => 1611,
                'name' => 'Retalhuleu',
                'country_id' => 89,
            ),
            71 => 
            array (
                'id' => 1612,
                'name' => 'Sacatepéquez',
                'country_id' => 89,
            ),
            72 => 
            array (
                'id' => 1613,
                'name' => 'San Marcos',
                'country_id' => 89,
            ),
            73 => 
            array (
                'id' => 1614,
                'name' => 'Sololá',
                'country_id' => 89,
            ),
            74 => 
            array (
                'id' => 1615,
                'name' => 'Santa Rosa',
                'country_id' => 89,
            ),
            75 => 
            array (
                'id' => 1616,
                'name' => 'Suchitepéquez',
                'country_id' => 89,
            ),
            76 => 
            array (
                'id' => 1617,
                'name' => 'Totonicapán',
                'country_id' => 89,
            ),
            77 => 
            array (
                'id' => 1618,
                'name' => 'Zacapa',
                'country_id' => 89,
            ),
            78 => 
            array (
                'id' => 1619,
                'name' => 'Bafatá',
                'country_id' => 92,
            ),
            79 => 
            array (
                'id' => 1620,
                'name' => 'Bolama',
                'country_id' => 92,
            ),
            80 => 
            array (
                'id' => 1621,
                'name' => 'Biombo',
                'country_id' => 92,
            ),
            81 => 
            array (
                'id' => 1622,
                'name' => 'Bissau',
                'country_id' => 92,
            ),
            82 => 
            array (
                'id' => 1623,
                'name' => 'Cacheu',
                'country_id' => 92,
            ),
            83 => 
            array (
                'id' => 1624,
                'name' => 'Gabú',
                'country_id' => 92,
            ),
            84 => 
            array (
                'id' => 1625,
                'name' => 'Oio',
                'country_id' => 92,
            ),
            85 => 
            array (
                'id' => 1626,
                'name' => 'Quinara',
                'country_id' => 92,
            ),
            86 => 
            array (
                'id' => 1627,
                'name' => 'Tombali ',
                'country_id' => 92,
            ),
            87 => 
            array (
                'id' => 1628,
                'name' => 'Barima-Waini',
                'country_id' => 93,
            ),
            88 => 
            array (
                'id' => 1629,
                'name' => 'Cuyuni-Mazaruni',
                'country_id' => 93,
            ),
            89 => 
            array (
                'id' => 1630,
                'name' => 'Demerara-Mahaica',
                'country_id' => 93,
            ),
            90 => 
            array (
                'id' => 1631,
                'name' => 'East Berbice-Corentyne',
                'country_id' => 93,
            ),
            91 => 
            array (
                'id' => 1632,
                'name' => 'Essequibo Islands-West Demerara',
                'country_id' => 93,
            ),
            92 => 
            array (
                'id' => 1633,
                'name' => 'Mahaica-Berbice',
                'country_id' => 93,
            ),
            93 => 
            array (
                'id' => 1634,
                'name' => 'Pomeroon-Supenaam',
                'country_id' => 93,
            ),
            94 => 
            array (
                'id' => 1635,
                'name' => 'Potaro-Siparuni',
                'country_id' => 93,
            ),
            95 => 
            array (
                'id' => 1636,
                'name' => 'Upper Demerara-Berbice',
                'country_id' => 93,
            ),
            96 => 
            array (
                'id' => 1637,
                'name' => 'Upper Takutu-Upper Essequibo',
                'country_id' => 93,
            ),
            97 => 
            array (
                'id' => 1638,
                'name' => 'Atlántida',
                'country_id' => 96,
            ),
            98 => 
            array (
                'id' => 1639,
                'name' => 'Choluteca',
                'country_id' => 96,
            ),
            99 => 
            array (
                'id' => 1640,
                'name' => 'Colón',
                'country_id' => 96,
            ),
            100 => 
            array (
                'id' => 1641,
                'name' => 'Comayagua',
                'country_id' => 96,
            ),
            101 => 
            array (
                'id' => 1642,
                'name' => 'Copán',
                'country_id' => 96,
            ),
            102 => 
            array (
                'id' => 1643,
                'name' => 'Cortés',
                'country_id' => 96,
            ),
            103 => 
            array (
                'id' => 1644,
                'name' => 'El Paraíso',
                'country_id' => 96,
            ),
            104 => 
            array (
                'id' => 1645,
                'name' => 'Francisco Morazán',
                'country_id' => 96,
            ),
            105 => 
            array (
                'id' => 1646,
                'name' => 'Gracias a Dios',
                'country_id' => 96,
            ),
            106 => 
            array (
                'id' => 1647,
                'name' => 'Islas de la Bahía',
                'country_id' => 96,
            ),
            107 => 
            array (
                'id' => 1648,
                'name' => 'Intibucá',
                'country_id' => 96,
            ),
            108 => 
            array (
                'id' => 1649,
                'name' => 'Lempira',
                'country_id' => 96,
            ),
            109 => 
            array (
                'id' => 1650,
                'name' => 'La Paz',
                'country_id' => 96,
            ),
            110 => 
            array (
                'id' => 1651,
                'name' => 'Ocotepeque',
                'country_id' => 96,
            ),
            111 => 
            array (
                'id' => 1652,
                'name' => 'Olancho',
                'country_id' => 96,
            ),
            112 => 
            array (
                'id' => 1653,
                'name' => 'Santa Bárbara',
                'country_id' => 96,
            ),
            113 => 
            array (
                'id' => 1654,
                'name' => 'Valle',
                'country_id' => 96,
            ),
            114 => 
            array (
                'id' => 1655,
                'name' => 'Yoro',
                'country_id' => 96,
            ),
            115 => 
            array (
                'id' => 1656,
                'name' => 'Zagrebacka županija',
                'country_id' => 53,
            ),
            116 => 
            array (
                'id' => 1657,
                'name' => 'Krapinsko-zagorska županija',
                'country_id' => 53,
            ),
            117 => 
            array (
                'id' => 1658,
                'name' => 'Sisacko-moslavacka županija',
                'country_id' => 53,
            ),
            118 => 
            array (
                'id' => 1659,
                'name' => 'Karlovacka županija',
                'country_id' => 53,
            ),
            119 => 
            array (
                'id' => 1660,
                'name' => 'Varaždinska županija',
                'country_id' => 53,
            ),
            120 => 
            array (
                'id' => 1661,
                'name' => 'Koprivnicko-križevacka županija',
                'country_id' => 53,
            ),
            121 => 
            array (
                'id' => 1662,
                'name' => 'Bjelovarsko-bilogorska županija',
                'country_id' => 53,
            ),
            122 => 
            array (
                'id' => 1663,
                'name' => 'Primorsko-goranska županija',
                'country_id' => 53,
            ),
            123 => 
            array (
                'id' => 1664,
                'name' => 'Licko-senjska županija',
                'country_id' => 53,
            ),
            124 => 
            array (
                'id' => 1665,
                'name' => 'Viroviticko-podravska županija',
                'country_id' => 53,
            ),
            125 => 
            array (
                'id' => 1666,
                'name' => 'Požeško-slavonska županija',
                'country_id' => 53,
            ),
            126 => 
            array (
                'id' => 1667,
                'name' => 'Brodsko-posavska županija',
                'country_id' => 53,
            ),
            127 => 
            array (
                'id' => 1668,
                'name' => 'Zadarska županija',
                'country_id' => 53,
            ),
            128 => 
            array (
                'id' => 1669,
                'name' => 'Osjecko-baranjska županija',
                'country_id' => 53,
            ),
            129 => 
            array (
                'id' => 1670,
                'name' => 'Šibensko-kninska županija',
                'country_id' => 53,
            ),
            130 => 
            array (
                'id' => 1671,
                'name' => 'Vukovarsko-srijemska županija',
                'country_id' => 53,
            ),
            131 => 
            array (
                'id' => 1672,
                'name' => 'Splitsko-dalmatinska županija',
                'country_id' => 53,
            ),
            132 => 
            array (
                'id' => 1673,
                'name' => 'Istarska županija',
                'country_id' => 53,
            ),
            133 => 
            array (
                'id' => 1674,
                'name' => 'Dubrovacko-neretvanska županija',
                'country_id' => 53,
            ),
            134 => 
            array (
                'id' => 1675,
                'name' => 'Medimurska županija',
                'country_id' => 53,
            ),
            135 => 
            array (
                'id' => 1676,
                'name' => 'Grad Zagreb',
                'country_id' => 53,
            ),
            136 => 
            array (
                'id' => 1677,
                'name' => 'Artibonite',
                'country_id' => 94,
            ),
            137 => 
            array (
                'id' => 1678,
                'name' => 'Centre',
                'country_id' => 94,
            ),
            138 => 
            array (
                'id' => 1679,
                'name' => 'Grande-Anse',
                'country_id' => 94,
            ),
            139 => 
            array (
                'id' => 1680,
                'name' => 'Nord',
                'country_id' => 94,
            ),
            140 => 
            array (
                'id' => 1681,
                'name' => 'Nord-Est',
                'country_id' => 94,
            ),
            141 => 
            array (
                'id' => 1682,
                'name' => 'Nippes',
                'country_id' => 94,
            ),
            142 => 
            array (
                'id' => 1683,
                'name' => 'Nord-Ouest',
                'country_id' => 94,
            ),
            143 => 
            array (
                'id' => 1684,
                'name' => 'Ouest',
                'country_id' => 94,
            ),
            144 => 
            array (
                'id' => 1685,
                'name' => 'Sud',
                'country_id' => 94,
            ),
            145 => 
            array (
                'id' => 1686,
                'name' => 'Sud-Est',
                'country_id' => 94,
            ),
            146 => 
            array (
                'id' => 1687,
                'name' => 'Baranya',
                'country_id' => 98,
            ),
            147 => 
            array (
                'id' => 1688,
                'name' => 'Békéscsaba',
                'country_id' => 98,
            ),
            148 => 
            array (
                'id' => 1689,
                'name' => 'Békés',
                'country_id' => 98,
            ),
            149 => 
            array (
                'id' => 1690,
                'name' => 'Bács-Kiskun',
                'country_id' => 98,
            ),
            150 => 
            array (
                'id' => 1691,
                'name' => 'Budapest',
                'country_id' => 98,
            ),
            151 => 
            array (
                'id' => 1692,
                'name' => 'Borsod-Abaúj-Zemplén',
                'country_id' => 98,
            ),
            152 => 
            array (
                'id' => 1693,
                'name' => 'Csongrád',
                'country_id' => 98,
            ),
            153 => 
            array (
                'id' => 1694,
                'name' => 'Debrecen',
                'country_id' => 98,
            ),
            154 => 
            array (
                'id' => 1695,
                'name' => 'Dunaújváros',
                'country_id' => 98,
            ),
            155 => 
            array (
                'id' => 1696,
                'name' => 'Eger',
                'country_id' => 98,
            ),
            156 => 
            array (
                'id' => 1697,
                'name' => 'Érd',
                'country_id' => 98,
            ),
            157 => 
            array (
                'id' => 1698,
                'name' => 'Fejér',
                'country_id' => 98,
            ),
            158 => 
            array (
                'id' => 1699,
                'name' => 'Gyor-Moson-Sopron',
                'country_id' => 98,
            ),
            159 => 
            array (
                'id' => 1700,
                'name' => 'Gyor',
                'country_id' => 98,
            ),
            160 => 
            array (
                'id' => 1701,
                'name' => 'Hajdú-Bihar',
                'country_id' => 98,
            ),
            161 => 
            array (
                'id' => 1702,
                'name' => 'Heves',
                'country_id' => 98,
            ),
            162 => 
            array (
                'id' => 1703,
                'name' => 'Hódmezovásárhely',
                'country_id' => 98,
            ),
            163 => 
            array (
                'id' => 1704,
                'name' => 'Jász-Nagykun-Szolnok',
                'country_id' => 98,
            ),
            164 => 
            array (
                'id' => 1705,
                'name' => 'Komárom-Esztergom',
                'country_id' => 98,
            ),
            165 => 
            array (
                'id' => 1706,
                'name' => 'Kecskemét',
                'country_id' => 98,
            ),
            166 => 
            array (
                'id' => 1707,
                'name' => 'Kaposvár',
                'country_id' => 98,
            ),
            167 => 
            array (
                'id' => 1708,
                'name' => 'Miskolc',
                'country_id' => 98,
            ),
            168 => 
            array (
                'id' => 1709,
                'name' => 'Nagykanizsa',
                'country_id' => 98,
            ),
            169 => 
            array (
                'id' => 1710,
                'name' => 'Nógrád',
                'country_id' => 98,
            ),
            170 => 
            array (
                'id' => 1711,
                'name' => 'Nyíregyháza',
                'country_id' => 98,
            ),
            171 => 
            array (
                'id' => 1712,
                'name' => 'Pest',
                'country_id' => 98,
            ),
            172 => 
            array (
                'id' => 1713,
                'name' => 'Pécs',
                'country_id' => 98,
            ),
            173 => 
            array (
                'id' => 1714,
                'name' => 'Szeged',
                'country_id' => 98,
            ),
            174 => 
            array (
                'id' => 1715,
                'name' => 'Székesfehérvár',
                'country_id' => 98,
            ),
            175 => 
            array (
                'id' => 1716,
                'name' => 'Szombathely',
                'country_id' => 98,
            ),
            176 => 
            array (
                'id' => 1717,
                'name' => 'Szolnok',
                'country_id' => 98,
            ),
            177 => 
            array (
                'id' => 1718,
                'name' => 'Sopron',
                'country_id' => 98,
            ),
            178 => 
            array (
                'id' => 1719,
                'name' => 'Somogy',
                'country_id' => 98,
            ),
            179 => 
            array (
                'id' => 1720,
                'name' => 'Szekszárd',
                'country_id' => 98,
            ),
            180 => 
            array (
                'id' => 1721,
                'name' => 'Salgótarján',
                'country_id' => 98,
            ),
            181 => 
            array (
                'id' => 1722,
                'name' => 'Szabolcs-Szatmár-Bereg',
                'country_id' => 98,
            ),
            182 => 
            array (
                'id' => 1723,
                'name' => 'Tatabánya',
                'country_id' => 98,
            ),
            183 => 
            array (
                'id' => 1724,
                'name' => 'Tolna',
                'country_id' => 98,
            ),
            184 => 
            array (
                'id' => 1725,
                'name' => 'Vas',
                'country_id' => 98,
            ),
            185 => 
            array (
                'id' => 1726,
                'name' => 'Veszprém',
                'country_id' => 98,
            ),
            186 => 
            array (
                'id' => 1727,
                'name' => 'Zala',
                'country_id' => 98,
            ),
            187 => 
            array (
                'id' => 1728,
                'name' => 'Zalaegerszeg',
                'country_id' => 98,
            ),
            188 => 
            array (
                'id' => 1729,
                'name' => 'Aceh',
                'country_id' => 101,
            ),
            189 => 
            array (
                'id' => 1730,
                'name' => 'Bali',
                'country_id' => 101,
            ),
            190 => 
            array (
                'id' => 1731,
                'name' => 'Bangka Belitung',
                'country_id' => 101,
            ),
            191 => 
            array (
                'id' => 1732,
                'name' => 'Bengkulu',
                'country_id' => 101,
            ),
            192 => 
            array (
                'id' => 1733,
                'name' => 'Banten',
                'country_id' => 101,
            ),
            193 => 
            array (
                'id' => 1734,
                'name' => 'Gorontalo',
                'country_id' => 101,
            ),
            194 => 
            array (
                'id' => 1735,
                'name' => 'Jambi',
                'country_id' => 101,
            ),
            195 => 
            array (
                'id' => 1736,
                'name' => 'Jawa Barat',
                'country_id' => 101,
            ),
            196 => 
            array (
                'id' => 1737,
                'name' => 'Jawa Timur',
                'country_id' => 101,
            ),
            197 => 
            array (
                'id' => 1738,
                'name' => 'Jakarta Raya',
                'country_id' => 101,
            ),
            198 => 
            array (
                'id' => 1739,
                'name' => 'Jawa Tengah',
                'country_id' => 101,
            ),
            199 => 
            array (
                'id' => 1740,
                'name' => 'Kalimantan Barat',
                'country_id' => 101,
            ),
            200 => 
            array (
                'id' => 1741,
                'name' => 'Kalimantan Timur',
                'country_id' => 101,
            ),
            201 => 
            array (
                'id' => 1742,
                'name' => 'Kepulauan Riau',
                'country_id' => 101,
            ),
            202 => 
            array (
                'id' => 1743,
                'name' => 'Kalimantan Selatan',
                'country_id' => 101,
            ),
            203 => 
            array (
                'id' => 1744,
                'name' => 'Kalimantan Tengah',
                'country_id' => 101,
            ),
            204 => 
            array (
                'id' => 1745,
                'name' => 'Lampung',
                'country_id' => 101,
            ),
            205 => 
            array (
                'id' => 1746,
                'name' => 'Maluku',
                'country_id' => 101,
            ),
            206 => 
            array (
                'id' => 1747,
                'name' => 'Maluku Utara',
                'country_id' => 101,
            ),
            207 => 
            array (
                'id' => 1748,
                'name' => 'Nusa Tenggara Barat',
                'country_id' => 101,
            ),
            208 => 
            array (
                'id' => 1749,
                'name' => 'Nusa Tenggara Timur',
                'country_id' => 101,
            ),
            209 => 
            array (
                'id' => 1750,
                'name' => 'Papua',
                'country_id' => 101,
            ),
            210 => 
            array (
                'id' => 1751,
                'name' => 'Papua Barat',
                'country_id' => 101,
            ),
            211 => 
            array (
                'id' => 1752,
                'name' => 'Riau',
                'country_id' => 101,
            ),
            212 => 
            array (
                'id' => 1753,
                'name' => 'Sulawesi Utara',
                'country_id' => 101,
            ),
            213 => 
            array (
                'id' => 1754,
                'name' => 'Sumatera Barat',
                'country_id' => 101,
            ),
            214 => 
            array (
                'id' => 1755,
                'name' => 'Sulawesi Tenggara',
                'country_id' => 101,
            ),
            215 => 
            array (
                'id' => 1756,
                'name' => 'Sulawesi Selatan',
                'country_id' => 101,
            ),
            216 => 
            array (
                'id' => 1757,
                'name' => 'Sulawesi Barat',
                'country_id' => 101,
            ),
            217 => 
            array (
                'id' => 1758,
                'name' => 'Sumatera Selatan',
                'country_id' => 101,
            ),
            218 => 
            array (
                'id' => 1759,
                'name' => 'Sulawesi Tengah',
                'country_id' => 101,
            ),
            219 => 
            array (
                'id' => 1760,
                'name' => 'Sumatera Utara',
                'country_id' => 101,
            ),
            220 => 
            array (
                'id' => 1761,
                'name' => 'Yogyakarta',
                'country_id' => 101,
            ),
            221 => 
            array (
                'id' => 1762,
                'name' => 'Clare',
                'country_id' => 104,
            ),
            222 => 
            array (
                'id' => 1763,
                'name' => 'Cavan',
                'country_id' => 104,
            ),
            223 => 
            array (
                'id' => 1764,
                'name' => 'Cork',
                'country_id' => 104,
            ),
            224 => 
            array (
                'id' => 1765,
                'name' => 'Carlow',
                'country_id' => 104,
            ),
            225 => 
            array (
                'id' => 1766,
                'name' => 'Dublin',
                'country_id' => 104,
            ),
            226 => 
            array (
                'id' => 1767,
                'name' => 'Donegal',
                'country_id' => 104,
            ),
            227 => 
            array (
                'id' => 1768,
                'name' => 'Galway',
                'country_id' => 104,
            ),
            228 => 
            array (
                'id' => 1769,
                'name' => 'Kildare',
                'country_id' => 104,
            ),
            229 => 
            array (
                'id' => 1770,
                'name' => 'Kilkenny',
                'country_id' => 104,
            ),
            230 => 
            array (
                'id' => 1771,
                'name' => 'Kerry',
                'country_id' => 104,
            ),
            231 => 
            array (
                'id' => 1772,
                'name' => 'Longford',
                'country_id' => 104,
            ),
            232 => 
            array (
                'id' => 1773,
                'name' => 'Louth',
                'country_id' => 104,
            ),
            233 => 
            array (
                'id' => 1774,
                'name' => 'Limerick',
                'country_id' => 104,
            ),
            234 => 
            array (
                'id' => 1775,
                'name' => 'Leitrim',
                'country_id' => 104,
            ),
            235 => 
            array (
                'id' => 1776,
                'name' => 'Laois',
                'country_id' => 104,
            ),
            236 => 
            array (
                'id' => 1777,
                'name' => 'Meath',
                'country_id' => 104,
            ),
            237 => 
            array (
                'id' => 1778,
                'name' => 'Monaghan',
                'country_id' => 104,
            ),
            238 => 
            array (
                'id' => 1779,
                'name' => 'Mayo',
                'country_id' => 104,
            ),
            239 => 
            array (
                'id' => 1780,
                'name' => 'Offaly',
                'country_id' => 104,
            ),
            240 => 
            array (
                'id' => 1781,
                'name' => 'Roscommon',
                'country_id' => 104,
            ),
            241 => 
            array (
                'id' => 1782,
                'name' => 'Sligo',
                'country_id' => 104,
            ),
            242 => 
            array (
                'id' => 1783,
                'name' => 'Tipperary',
                'country_id' => 104,
            ),
            243 => 
            array (
                'id' => 1784,
                'name' => 'Waterford',
                'country_id' => 104,
            ),
            244 => 
            array (
                'id' => 1785,
                'name' => 'Westmeath',
                'country_id' => 104,
            ),
            245 => 
            array (
                'id' => 1786,
                'name' => 'Wicklow',
                'country_id' => 104,
            ),
            246 => 
            array (
                'id' => 1787,
                'name' => 'Wexford',
                'country_id' => 104,
            ),
            247 => 
            array (
                'id' => 1788,
                'name' => 'HaDarom',
                'country_id' => 106,
            ),
            248 => 
            array (
                'id' => 1789,
                'name' => 'Hefa',
                'country_id' => 106,
            ),
            249 => 
            array (
                'id' => 1790,
                'name' => 'Yerushalayim',
                'country_id' => 106,
            ),
            250 => 
            array (
                'id' => 1791,
                'name' => 'HaMerkaz',
                'country_id' => 106,
            ),
            251 => 
            array (
                'id' => 1792,
                'name' => 'Tel-Aviv',
                'country_id' => 106,
            ),
            252 => 
            array (
                'id' => 1793,
                'name' => 'HaZafon',
                'country_id' => 106,
            ),
            253 => 
            array (
                'id' => 1794,
                'name' => 'Andaman and Nicobar Islands',
                'country_id' => 100,
            ),
            254 => 
            array (
                'id' => 1795,
                'name' => 'Andhra Pradesh',
                'country_id' => 100,
            ),
            255 => 
            array (
                'id' => 1796,
                'name' => 'Arunachal Pradesh',
                'country_id' => 100,
            ),
            256 => 
            array (
                'id' => 1797,
                'name' => 'Assam',
                'country_id' => 100,
            ),
            257 => 
            array (
                'id' => 1798,
                'name' => 'Bihar',
                'country_id' => 100,
            ),
            258 => 
            array (
                'id' => 1799,
                'name' => 'Chandigarh',
                'country_id' => 100,
            ),
            259 => 
            array (
                'id' => 1800,
                'name' => 'Chhattisgarh',
                'country_id' => 100,
            ),
            260 => 
            array (
                'id' => 1801,
                'name' => 'Daman and Diu',
                'country_id' => 100,
            ),
            261 => 
            array (
                'id' => 1802,
                'name' => 'Delhi',
                'country_id' => 100,
            ),
            262 => 
            array (
                'id' => 1803,
                'name' => 'Dadra and Nagar Haveli',
                'country_id' => 100,
            ),
            263 => 
            array (
                'id' => 1804,
                'name' => 'Goa',
                'country_id' => 100,
            ),
            264 => 
            array (
                'id' => 1805,
                'name' => 'Gujarat',
                'country_id' => 100,
            ),
            265 => 
            array (
                'id' => 1806,
                'name' => 'Himachal Pradesh',
                'country_id' => 100,
            ),
            266 => 
            array (
                'id' => 1807,
                'name' => 'Haryana',
                'country_id' => 100,
            ),
            267 => 
            array (
                'id' => 1808,
                'name' => 'Jharkhand',
                'country_id' => 100,
            ),
            268 => 
            array (
                'id' => 1809,
                'name' => 'Jammu and Kashmir',
                'country_id' => 100,
            ),
            269 => 
            array (
                'id' => 1810,
                'name' => 'Karnataka',
                'country_id' => 100,
            ),
            270 => 
            array (
                'id' => 1811,
                'name' => 'Kerala',
                'country_id' => 100,
            ),
            271 => 
            array (
                'id' => 1812,
                'name' => 'Lakshadweep',
                'country_id' => 100,
            ),
            272 => 
            array (
                'id' => 1813,
                'name' => 'Maharashtra',
                'country_id' => 100,
            ),
            273 => 
            array (
                'id' => 1814,
                'name' => 'Meghalaya',
                'country_id' => 100,
            ),
            274 => 
            array (
                'id' => 1815,
                'name' => 'Manipur',
                'country_id' => 100,
            ),
            275 => 
            array (
                'id' => 1816,
                'name' => 'Madhya Pradesh',
                'country_id' => 100,
            ),
            276 => 
            array (
                'id' => 1817,
                'name' => 'Mizoram',
                'country_id' => 100,
            ),
            277 => 
            array (
                'id' => 1818,
                'name' => 'Nagaland',
                'country_id' => 100,
            ),
            278 => 
            array (
                'id' => 1819,
                'name' => 'Orissa',
                'country_id' => 100,
            ),
            279 => 
            array (
                'id' => 1820,
                'name' => 'Punjab',
                'country_id' => 100,
            ),
            280 => 
            array (
                'id' => 1821,
                'name' => 'Puducherry',
                'country_id' => 100,
            ),
            281 => 
            array (
                'id' => 1822,
                'name' => 'Rajasthan',
                'country_id' => 100,
            ),
            282 => 
            array (
                'id' => 1823,
                'name' => 'Sikkim',
                'country_id' => 100,
            ),
            283 => 
            array (
                'id' => 1824,
                'name' => 'Tamil Nadu',
                'country_id' => 100,
            ),
            284 => 
            array (
                'id' => 1825,
                'name' => 'Tripura',
                'country_id' => 100,
            ),
            285 => 
            array (
                'id' => 1826,
                'name' => 'Uttar Pradesh',
                'country_id' => 100,
            ),
            286 => 
            array (
                'id' => 1827,
                'name' => 'Uttarakhand',
                'country_id' => 100,
            ),
            287 => 
            array (
                'id' => 1828,
                'name' => 'West Bengal',
                'country_id' => 100,
            ),
            288 => 
            array (
                'id' => 1847,
                'name' => 'AZarbayjan-e Sharqi',
                'country_id' => 102,
            ),
            289 => 
            array (
                'id' => 1848,
                'name' => 'AZarbayjan-e Gharbi',
                'country_id' => 102,
            ),
            290 => 
            array (
                'id' => 1849,
                'name' => 'Ardabil',
                'country_id' => 102,
            ),
            291 => 
            array (
                'id' => 1850,
                'name' => 'Esfahan',
                'country_id' => 102,
            ),
            292 => 
            array (
                'id' => 1851,
                'name' => 'Ilam',
                'country_id' => 102,
            ),
            293 => 
            array (
                'id' => 1852,
                'name' => 'Bushehr',
                'country_id' => 102,
            ),
            294 => 
            array (
                'id' => 1853,
                'name' => 'Tehran',
                'country_id' => 102,
            ),
            295 => 
            array (
                'id' => 1854,
                'name' => 'Chahar Mahall va Bakhtiari',
                'country_id' => 102,
            ),
            296 => 
            array (
                'id' => 1855,
                'name' => 'Khuzestan',
                'country_id' => 102,
            ),
            297 => 
            array (
                'id' => 1856,
                'name' => 'Zanjan',
                'country_id' => 102,
            ),
            298 => 
            array (
                'id' => 1857,
                'name' => 'Semnan',
                'country_id' => 102,
            ),
            299 => 
            array (
                'id' => 1858,
                'name' => 'Sistan va Baluchestan',
                'country_id' => 102,
            ),
            300 => 
            array (
                'id' => 1859,
                'name' => 'Fars',
                'country_id' => 102,
            ),
            301 => 
            array (
                'id' => 1860,
                'name' => 'Kerman',
                'country_id' => 102,
            ),
            302 => 
            array (
                'id' => 1861,
                'name' => 'Kordestan',
                'country_id' => 102,
            ),
            303 => 
            array (
                'id' => 1862,
                'name' => 'Kermanshah',
                'country_id' => 102,
            ),
            304 => 
            array (
                'id' => 1863,
                'name' => 'Kohkiluyeh va Buyer Ahmad',
                'country_id' => 102,
            ),
            305 => 
            array (
                'id' => 1864,
                'name' => 'Gilan',
                'country_id' => 102,
            ),
            306 => 
            array (
                'id' => 1865,
                'name' => 'Lorestan',
                'country_id' => 102,
            ),
            307 => 
            array (
                'id' => 1866,
                'name' => 'Mazandaran',
                'country_id' => 102,
            ),
            308 => 
            array (
                'id' => 1867,
                'name' => 'Markazi',
                'country_id' => 102,
            ),
            309 => 
            array (
                'id' => 1868,
                'name' => 'Hormozgan',
                'country_id' => 102,
            ),
            310 => 
            array (
                'id' => 1869,
                'name' => 'Hamadan',
                'country_id' => 102,
            ),
            311 => 
            array (
                'id' => 1870,
                'name' => 'Yazd',
                'country_id' => 102,
            ),
            312 => 
            array (
                'id' => 1871,
                'name' => 'Qom',
                'country_id' => 102,
            ),
            313 => 
            array (
                'id' => 1872,
                'name' => 'Golestan',
                'country_id' => 102,
            ),
            314 => 
            array (
                'id' => 1873,
                'name' => 'Qazvin',
                'country_id' => 102,
            ),
            315 => 
            array (
                'id' => 1874,
                'name' => 'Khorasan-e Janubi',
                'country_id' => 102,
            ),
            316 => 
            array (
                'id' => 1875,
                'name' => 'Khorasan-e Razavi',
                'country_id' => 102,
            ),
            317 => 
            array (
                'id' => 1876,
                'name' => 'Khorasan-e Shemali',
                'country_id' => 102,
            ),
            318 => 
            array (
                'id' => 1877,
                'name' => 'Reykjavík',
                'country_id' => 99,
            ),
            319 => 
            array (
                'id' => 1878,
                'name' => 'Höfuðborgarsvæði utan Reykjavíkur',
                'country_id' => 99,
            ),
            320 => 
            array (
                'id' => 1879,
                'name' => 'Suðurnes',
                'country_id' => 99,
            ),
            321 => 
            array (
                'id' => 1880,
                'name' => 'Vesturland',
                'country_id' => 99,
            ),
            322 => 
            array (
                'id' => 1881,
                'name' => 'Vestfirðir',
                'country_id' => 99,
            ),
            323 => 
            array (
                'id' => 1882,
                'name' => 'Norðurland vestra',
                'country_id' => 99,
            ),
            324 => 
            array (
                'id' => 1883,
                'name' => 'Norðurland eystra',
                'country_id' => 99,
            ),
            325 => 
            array (
                'id' => 1884,
                'name' => 'Austurland',
                'country_id' => 99,
            ),
            326 => 
            array (
                'id' => 1885,
                'name' => 'Suðurland',
                'country_id' => 99,
            ),
            327 => 
            array (
                'id' => 1886,
                'name' => 'Agrigento',
                'country_id' => 107,
            ),
            328 => 
            array (
                'id' => 1887,
                'name' => 'Alessandria',
                'country_id' => 107,
            ),
            329 => 
            array (
                'id' => 1888,
                'name' => 'Ancona',
                'country_id' => 107,
            ),
            330 => 
            array (
                'id' => 1889,
            'name' => 'Aosta / Aoste (fr)',
                'country_id' => 107,
            ),
            331 => 
            array (
                'id' => 1890,
                'name' => 'Ascoli Piceno',
                'country_id' => 107,
            ),
            332 => 
            array (
                'id' => 1891,
                'name' => 'L\'Aquila',
                'country_id' => 107,
            ),
            333 => 
            array (
                'id' => 1892,
                'name' => 'Arezzo',
                'country_id' => 107,
            ),
            334 => 
            array (
                'id' => 1893,
                'name' => 'Asti',
                'country_id' => 107,
            ),
            335 => 
            array (
                'id' => 1894,
                'name' => 'Avellino',
                'country_id' => 107,
            ),
            336 => 
            array (
                'id' => 1895,
                'name' => 'Bari',
                'country_id' => 107,
            ),
            337 => 
            array (
                'id' => 1896,
                'name' => 'Bergamo',
                'country_id' => 107,
            ),
            338 => 
            array (
                'id' => 1897,
                'name' => 'Biella',
                'country_id' => 107,
            ),
            339 => 
            array (
                'id' => 1898,
                'name' => 'Belluno',
                'country_id' => 107,
            ),
            340 => 
            array (
                'id' => 1899,
                'name' => 'Benevento',
                'country_id' => 107,
            ),
            341 => 
            array (
                'id' => 1900,
                'name' => 'Bologna',
                'country_id' => 107,
            ),
            342 => 
            array (
                'id' => 1901,
                'name' => 'Brindisi',
                'country_id' => 107,
            ),
            343 => 
            array (
                'id' => 1902,
                'name' => 'Brescia',
                'country_id' => 107,
            ),
            344 => 
            array (
                'id' => 1903,
                'name' => 'Barletta-Andria-Trani',
                'country_id' => 107,
            ),
            345 => 
            array (
                'id' => 1904,
            'name' => 'Bolzano / Bozen (de)',
                'country_id' => 107,
            ),
            346 => 
            array (
                'id' => 1905,
                'name' => 'Cagliari',
                'country_id' => 107,
            ),
            347 => 
            array (
                'id' => 1906,
                'name' => 'Campobasso',
                'country_id' => 107,
            ),
            348 => 
            array (
                'id' => 1907,
                'name' => 'Caserta',
                'country_id' => 107,
            ),
            349 => 
            array (
                'id' => 1908,
                'name' => 'Chieti',
                'country_id' => 107,
            ),
            350 => 
            array (
                'id' => 1909,
                'name' => 'Carbonia-Iglesias',
                'country_id' => 107,
            ),
            351 => 
            array (
                'id' => 1910,
                'name' => 'Caltanissetta',
                'country_id' => 107,
            ),
            352 => 
            array (
                'id' => 1911,
                'name' => 'Cuneo',
                'country_id' => 107,
            ),
            353 => 
            array (
                'id' => 1912,
                'name' => 'Como',
                'country_id' => 107,
            ),
            354 => 
            array (
                'id' => 1913,
                'name' => 'Cremona',
                'country_id' => 107,
            ),
            355 => 
            array (
                'id' => 1914,
                'name' => 'Cosenza',
                'country_id' => 107,
            ),
            356 => 
            array (
                'id' => 1915,
                'name' => 'Catania',
                'country_id' => 107,
            ),
            357 => 
            array (
                'id' => 1916,
                'name' => 'Catanzaro',
                'country_id' => 107,
            ),
            358 => 
            array (
                'id' => 1917,
                'name' => 'Enna',
                'country_id' => 107,
            ),
            359 => 
            array (
                'id' => 1918,
                'name' => 'Forlì-Cesena',
                'country_id' => 107,
            ),
            360 => 
            array (
                'id' => 1919,
                'name' => 'Ferrara',
                'country_id' => 107,
            ),
            361 => 
            array (
                'id' => 1920,
                'name' => 'Foggia',
                'country_id' => 107,
            ),
            362 => 
            array (
                'id' => 1921,
                'name' => 'Firenze',
                'country_id' => 107,
            ),
            363 => 
            array (
                'id' => 1922,
                'name' => 'Fermo',
                'country_id' => 107,
            ),
            364 => 
            array (
                'id' => 1923,
                'name' => 'Frosinone',
                'country_id' => 107,
            ),
            365 => 
            array (
                'id' => 1924,
                'name' => 'Genova',
                'country_id' => 107,
            ),
            366 => 
            array (
                'id' => 1925,
                'name' => 'Gorizia',
                'country_id' => 107,
            ),
            367 => 
            array (
                'id' => 1926,
                'name' => 'Grosseto',
                'country_id' => 107,
            ),
            368 => 
            array (
                'id' => 1927,
                'name' => 'Imperia',
                'country_id' => 107,
            ),
            369 => 
            array (
                'id' => 1928,
                'name' => 'Isernia',
                'country_id' => 107,
            ),
            370 => 
            array (
                'id' => 1929,
                'name' => 'Crotone',
                'country_id' => 107,
            ),
            371 => 
            array (
                'id' => 1930,
                'name' => 'Lecco',
                'country_id' => 107,
            ),
            372 => 
            array (
                'id' => 1931,
                'name' => 'Lecce',
                'country_id' => 107,
            ),
            373 => 
            array (
                'id' => 1932,
                'name' => 'Livorno',
                'country_id' => 107,
            ),
            374 => 
            array (
                'id' => 1933,
                'name' => 'Lodi',
                'country_id' => 107,
            ),
            375 => 
            array (
                'id' => 1934,
                'name' => 'Latina',
                'country_id' => 107,
            ),
            376 => 
            array (
                'id' => 1935,
                'name' => 'Lucca',
                'country_id' => 107,
            ),
            377 => 
            array (
                'id' => 1936,
                'name' => 'Monza e Brianza',
                'country_id' => 107,
            ),
            378 => 
            array (
                'id' => 1937,
                'name' => 'Macerata',
                'country_id' => 107,
            ),
            379 => 
            array (
                'id' => 1938,
                'name' => 'Messina',
                'country_id' => 107,
            ),
            380 => 
            array (
                'id' => 1939,
                'name' => 'Milano',
                'country_id' => 107,
            ),
            381 => 
            array (
                'id' => 1940,
                'name' => 'Mantova',
                'country_id' => 107,
            ),
            382 => 
            array (
                'id' => 1941,
                'name' => 'Modena',
                'country_id' => 107,
            ),
            383 => 
            array (
                'id' => 1942,
                'name' => 'Massa-Carrara',
                'country_id' => 107,
            ),
            384 => 
            array (
                'id' => 1943,
                'name' => 'Matera',
                'country_id' => 107,
            ),
            385 => 
            array (
                'id' => 1944,
                'name' => 'Napoli',
                'country_id' => 107,
            ),
            386 => 
            array (
                'id' => 1945,
                'name' => 'Novara',
                'country_id' => 107,
            ),
            387 => 
            array (
                'id' => 1946,
                'name' => 'Nuoro',
                'country_id' => 107,
            ),
            388 => 
            array (
                'id' => 1947,
                'name' => 'Ogliastra',
                'country_id' => 107,
            ),
            389 => 
            array (
                'id' => 1948,
                'name' => 'Oristano',
                'country_id' => 107,
            ),
            390 => 
            array (
                'id' => 1949,
                'name' => 'Olbia-Tempio',
                'country_id' => 107,
            ),
            391 => 
            array (
                'id' => 1950,
                'name' => 'Palermo',
                'country_id' => 107,
            ),
            392 => 
            array (
                'id' => 1951,
                'name' => 'Piacenza',
                'country_id' => 107,
            ),
            393 => 
            array (
                'id' => 1952,
                'name' => 'Padova',
                'country_id' => 107,
            ),
            394 => 
            array (
                'id' => 1953,
                'name' => 'Pescara',
                'country_id' => 107,
            ),
            395 => 
            array (
                'id' => 1954,
                'name' => 'Perugia',
                'country_id' => 107,
            ),
            396 => 
            array (
                'id' => 1955,
                'name' => 'Pisa',
                'country_id' => 107,
            ),
            397 => 
            array (
                'id' => 1956,
                'name' => 'Pordenone',
                'country_id' => 107,
            ),
            398 => 
            array (
                'id' => 1957,
                'name' => 'Prato',
                'country_id' => 107,
            ),
            399 => 
            array (
                'id' => 1958,
                'name' => 'Parma',
                'country_id' => 107,
            ),
            400 => 
            array (
                'id' => 1959,
                'name' => 'Pistoia',
                'country_id' => 107,
            ),
            401 => 
            array (
                'id' => 1960,
                'name' => 'Pesaro e Urbino',
                'country_id' => 107,
            ),
            402 => 
            array (
                'id' => 1961,
                'name' => 'Pavia',
                'country_id' => 107,
            ),
            403 => 
            array (
                'id' => 1962,
                'name' => 'Potenza',
                'country_id' => 107,
            ),
            404 => 
            array (
                'id' => 1963,
                'name' => 'Ravenna',
                'country_id' => 107,
            ),
            405 => 
            array (
                'id' => 1964,
                'name' => 'Reggio Calabria',
                'country_id' => 107,
            ),
            406 => 
            array (
                'id' => 1965,
                'name' => 'Reggio Emilia',
                'country_id' => 107,
            ),
            407 => 
            array (
                'id' => 1966,
                'name' => 'Ragusa',
                'country_id' => 107,
            ),
            408 => 
            array (
                'id' => 1967,
                'name' => 'Rieti',
                'country_id' => 107,
            ),
            409 => 
            array (
                'id' => 1968,
                'name' => 'Roma',
                'country_id' => 107,
            ),
            410 => 
            array (
                'id' => 1969,
                'name' => 'Rimini',
                'country_id' => 107,
            ),
            411 => 
            array (
                'id' => 1970,
                'name' => 'Rovigo',
                'country_id' => 107,
            ),
            412 => 
            array (
                'id' => 1971,
                'name' => 'Salerno',
                'country_id' => 107,
            ),
            413 => 
            array (
                'id' => 1972,
                'name' => 'Siena',
                'country_id' => 107,
            ),
            414 => 
            array (
                'id' => 1973,
                'name' => 'Sondrio',
                'country_id' => 107,
            ),
            415 => 
            array (
                'id' => 1974,
                'name' => 'La Spezia',
                'country_id' => 107,
            ),
            416 => 
            array (
                'id' => 1975,
                'name' => 'Siracusa',
                'country_id' => 107,
            ),
            417 => 
            array (
                'id' => 1976,
                'name' => 'Sassari',
                'country_id' => 107,
            ),
            418 => 
            array (
                'id' => 1977,
                'name' => 'Savona',
                'country_id' => 107,
            ),
            419 => 
            array (
                'id' => 1978,
                'name' => 'Taranto',
                'country_id' => 107,
            ),
            420 => 
            array (
                'id' => 1979,
                'name' => 'Teramo',
                'country_id' => 107,
            ),
            421 => 
            array (
                'id' => 1980,
                'name' => 'Trento',
                'country_id' => 107,
            ),
            422 => 
            array (
                'id' => 1981,
                'name' => 'Torino',
                'country_id' => 107,
            ),
            423 => 
            array (
                'id' => 1982,
                'name' => 'Trapani',
                'country_id' => 107,
            ),
            424 => 
            array (
                'id' => 1983,
                'name' => 'Terni',
                'country_id' => 107,
            ),
            425 => 
            array (
                'id' => 1984,
                'name' => 'Trieste',
                'country_id' => 107,
            ),
            426 => 
            array (
                'id' => 1985,
                'name' => 'Treviso',
                'country_id' => 107,
            ),
            427 => 
            array (
                'id' => 1986,
                'name' => 'Udine',
                'country_id' => 107,
            ),
            428 => 
            array (
                'id' => 1987,
                'name' => 'Varese',
                'country_id' => 107,
            ),
            429 => 
            array (
                'id' => 1988,
                'name' => 'Verbano-Cusio-Ossola',
                'country_id' => 107,
            ),
            430 => 
            array (
                'id' => 1989,
                'name' => 'Vercelli',
                'country_id' => 107,
            ),
            431 => 
            array (
                'id' => 1990,
                'name' => 'Venezia',
                'country_id' => 107,
            ),
            432 => 
            array (
                'id' => 1991,
                'name' => 'Vicenza',
                'country_id' => 107,
            ),
            433 => 
            array (
                'id' => 1992,
                'name' => 'Verona',
                'country_id' => 107,
            ),
            434 => 
            array (
                'id' => 1993,
                'name' => 'Medio Campidano',
                'country_id' => 107,
            ),
            435 => 
            array (
                'id' => 1994,
                'name' => 'Viterbo',
                'country_id' => 107,
            ),
            436 => 
            array (
                'id' => 1995,
                'name' => 'Vibo Valentia',
                'country_id' => 107,
            ),
            437 => 
            array (
                'id' => 1996,
                'name' => 'Kingston',
                'country_id' => 108,
            ),
            438 => 
            array (
                'id' => 1997,
                'name' => 'Saint Andrew',
                'country_id' => 108,
            ),
            439 => 
            array (
                'id' => 1998,
                'name' => 'Saint Thomas',
                'country_id' => 108,
            ),
            440 => 
            array (
                'id' => 1999,
                'name' => 'Portland',
                'country_id' => 108,
            ),
            441 => 
            array (
                'id' => 2000,
                'name' => 'Saint Mary',
                'country_id' => 108,
            ),
            442 => 
            array (
                'id' => 2001,
                'name' => 'Saint Ann',
                'country_id' => 108,
            ),
            443 => 
            array (
                'id' => 2002,
                'name' => 'Trelawny',
                'country_id' => 108,
            ),
            444 => 
            array (
                'id' => 2003,
                'name' => 'Saint James',
                'country_id' => 108,
            ),
            445 => 
            array (
                'id' => 2004,
                'name' => 'Hanover',
                'country_id' => 108,
            ),
            446 => 
            array (
                'id' => 2005,
                'name' => 'Westmoreland',
                'country_id' => 108,
            ),
            447 => 
            array (
                'id' => 2006,
                'name' => 'Saint Elizabeth',
                'country_id' => 108,
            ),
            448 => 
            array (
                'id' => 2007,
                'name' => 'Manchester',
                'country_id' => 108,
            ),
            449 => 
            array (
                'id' => 2008,
                'name' => 'Clarendon',
                'country_id' => 108,
            ),
            450 => 
            array (
                'id' => 2009,
                'name' => 'Saint Catherine',
                'country_id' => 108,
            ),
            451 => 
            array (
                'id' => 2010,
                'name' => '?Ajlun',
                'country_id' => 111,
            ),
            452 => 
            array (
                'id' => 2012,
                'name' => 'Al\'Aqaba',
                'country_id' => 111,
            ),
            453 => 
            array (
                'id' => 2017,
                'name' => 'Jarash',
                'country_id' => 111,
            ),
            454 => 
            array (
                'id' => 2020,
                'name' => 'Madaba',
                'country_id' => 111,
            ),
            455 => 
            array (
                'id' => 2021,
                'name' => 'Ma\'an',
                'country_id' => 111,
            ),
            456 => 
            array (
                'id' => 2022,
                'name' => 'Hokkaidô [Hokkaido]',
                'country_id' => 109,
            ),
            457 => 
            array (
                'id' => 2023,
                'name' => 'Aomori',
                'country_id' => 109,
            ),
            458 => 
            array (
                'id' => 2024,
                'name' => 'Iwate',
                'country_id' => 109,
            ),
            459 => 
            array (
                'id' => 2025,
                'name' => 'Miyagi',
                'country_id' => 109,
            ),
            460 => 
            array (
                'id' => 2026,
                'name' => 'Akita',
                'country_id' => 109,
            ),
            461 => 
            array (
                'id' => 2027,
                'name' => 'Yamagata',
                'country_id' => 109,
            ),
            462 => 
            array (
                'id' => 2028,
                'name' => 'Hukusima [Fukushima]',
                'country_id' => 109,
            ),
            463 => 
            array (
                'id' => 2029,
                'name' => 'Ibaraki',
                'country_id' => 109,
            ),
            464 => 
            array (
                'id' => 2030,
                'name' => 'Totigi [Tochigi]',
                'country_id' => 109,
            ),
            465 => 
            array (
                'id' => 2031,
                'name' => 'Gunma',
                'country_id' => 109,
            ),
            466 => 
            array (
                'id' => 2032,
                'name' => 'Saitama',
                'country_id' => 109,
            ),
            467 => 
            array (
                'id' => 2033,
                'name' => 'Tiba [Chiba]',
                'country_id' => 109,
            ),
            468 => 
            array (
                'id' => 2034,
                'name' => 'Tôkyô [Tokyo]',
                'country_id' => 109,
            ),
            469 => 
            array (
                'id' => 2035,
                'name' => 'Kanagawa',
                'country_id' => 109,
            ),
            470 => 
            array (
                'id' => 2036,
                'name' => 'Niigata',
                'country_id' => 109,
            ),
            471 => 
            array (
                'id' => 2037,
                'name' => 'Toyama',
                'country_id' => 109,
            ),
            472 => 
            array (
                'id' => 2038,
                'name' => 'Isikawa [Ishikawa]',
                'country_id' => 109,
            ),
            473 => 
            array (
                'id' => 2039,
                'name' => 'Hukui [Fukui]',
                'country_id' => 109,
            ),
            474 => 
            array (
                'id' => 2040,
                'name' => 'Yamanasi [Yamanashi]',
                'country_id' => 109,
            ),
            475 => 
            array (
                'id' => 2041,
                'name' => 'Nagano',
                'country_id' => 109,
            ),
            476 => 
            array (
                'id' => 2042,
                'name' => 'Gihu [Gifu]',
                'country_id' => 109,
            ),
            477 => 
            array (
                'id' => 2043,
                'name' => 'Sizuoka [Shizuoka]',
                'country_id' => 109,
            ),
            478 => 
            array (
                'id' => 2044,
                'name' => 'Aiti [Aichi]',
                'country_id' => 109,
            ),
            479 => 
            array (
                'id' => 2045,
                'name' => 'Mie',
                'country_id' => 109,
            ),
            480 => 
            array (
                'id' => 2046,
                'name' => 'Siga [Shiga]',
                'country_id' => 109,
            ),
            481 => 
            array (
                'id' => 2047,
                'name' => 'Hyôgo [Kyoto]',
                'country_id' => 109,
            ),
            482 => 
            array (
                'id' => 2048,
                'name' => 'Ôsaka [Osaka]',
                'country_id' => 109,
            ),
            483 => 
            array (
                'id' => 2049,
                'name' => 'Hyôgo[Hyogo]',
                'country_id' => 109,
            ),
            484 => 
            array (
                'id' => 2050,
                'name' => 'Nara',
                'country_id' => 109,
            ),
            485 => 
            array (
                'id' => 2051,
                'name' => 'Wakayama',
                'country_id' => 109,
            ),
            486 => 
            array (
                'id' => 2052,
                'name' => 'Tottori',
                'country_id' => 109,
            ),
            487 => 
            array (
                'id' => 2053,
                'name' => 'Simane [Shimane]',
                'country_id' => 109,
            ),
            488 => 
            array (
                'id' => 2054,
                'name' => 'Okayama',
                'country_id' => 109,
            ),
            489 => 
            array (
                'id' => 2055,
                'name' => 'Hirosima [Hiroshima]',
                'country_id' => 109,
            ),
            490 => 
            array (
                'id' => 2056,
                'name' => 'Yamaguti [Yamaguchi]',
                'country_id' => 109,
            ),
            491 => 
            array (
                'id' => 2057,
                'name' => 'Tokusima [Tokushima]',
                'country_id' => 109,
            ),
            492 => 
            array (
                'id' => 2058,
                'name' => 'Kagawa',
                'country_id' => 109,
            ),
            493 => 
            array (
                'id' => 2059,
                'name' => 'Ehime',
                'country_id' => 109,
            ),
            494 => 
            array (
                'id' => 2060,
                'name' => 'Kôti [Kochi]',
                'country_id' => 109,
            ),
            495 => 
            array (
                'id' => 2061,
                'name' => 'Hukuoka [Fukuoka]',
                'country_id' => 109,
            ),
            496 => 
            array (
                'id' => 2062,
                'name' => 'Saga',
                'country_id' => 109,
            ),
            497 => 
            array (
                'id' => 2063,
                'name' => 'Nagasaki',
                'country_id' => 109,
            ),
            498 => 
            array (
                'id' => 2064,
                'name' => 'Kumamoto',
                'country_id' => 109,
            ),
            499 => 
            array (
                'id' => 2065,
                'name' => 'Ôita [Oita]',
                'country_id' => 109,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 2066,
                'name' => 'Miyazaki',
                'country_id' => 109,
            ),
            1 => 
            array (
                'id' => 2067,
                'name' => 'Kagosima [Kagoshima]',
                'country_id' => 109,
            ),
            2 => 
            array (
                'id' => 2068,
                'name' => 'Okinawa',
                'country_id' => 109,
            ),
            3 => 
            array (
                'id' => 2069,
                'name' => 'Nairobi',
                'country_id' => 113,
            ),
            4 => 
            array (
                'id' => 2070,
                'name' => 'Central',
                'country_id' => 113,
            ),
            5 => 
            array (
                'id' => 2071,
                'name' => 'Coast',
                'country_id' => 113,
            ),
            6 => 
            array (
                'id' => 2072,
                'name' => 'Eastern',
                'country_id' => 113,
            ),
            7 => 
            array (
                'id' => 2073,
                'name' => 'North-Eastern',
                'country_id' => 113,
            ),
            8 => 
            array (
                'id' => 2074,
                'name' => 'Nyanza',
                'country_id' => 113,
            ),
            9 => 
            array (
                'id' => 2075,
                'name' => 'Bonde la Ufa',
                'country_id' => 113,
            ),
            10 => 
            array (
                'id' => 2076,
                'name' => 'Western',
                'country_id' => 113,
            ),
            11 => 
            array (
                'id' => 2077,
                'name' => 'Batken',
                'country_id' => 118,
            ),
            12 => 
            array (
                'id' => 2078,
                'name' => 'Chü',
                'country_id' => 118,
            ),
            13 => 
            array (
                'id' => 2079,
                'name' => 'Bishkek',
                'country_id' => 118,
            ),
            14 => 
            array (
                'id' => 2080,
                'name' => 'Jalal-Abad',
                'country_id' => 118,
            ),
            15 => 
            array (
                'id' => 2081,
                'name' => 'Naryn',
                'country_id' => 118,
            ),
            16 => 
            array (
                'id' => 2082,
                'name' => 'Osh',
                'country_id' => 118,
            ),
            17 => 
            array (
                'id' => 2083,
                'name' => 'Talas',
                'country_id' => 118,
            ),
            18 => 
            array (
                'id' => 2084,
                'name' => 'Ysyk-Köl',
                'country_id' => 118,
            ),
            19 => 
            array (
                'id' => 2085,
                'name' => 'Banteay Mean Chey [Bântéay Méanchey]',
                'country_id' => 35,
            ),
            20 => 
            array (
                'id' => 2086,
                'name' => 'Kracheh [Krâchéh]',
                'country_id' => 35,
            ),
            21 => 
            array (
                'id' => 2087,
                'name' => 'Mondol Kiri [Môndól Kiri]',
                'country_id' => 35,
            ),
            22 => 
            array (
                'id' => 2088,
                'name' => 'Phnom Penh [Phnum Pénh]',
                'country_id' => 35,
            ),
            23 => 
            array (
                'id' => 2089,
                'name' => 'Preah Vihear [Preah Vihéar]',
                'country_id' => 35,
            ),
            24 => 
            array (
                'id' => 2090,
                'name' => 'Prey Veaeng [Prey Vêng]',
                'country_id' => 35,
            ),
            25 => 
            array (
                'id' => 2091,
                'name' => 'Pousaat [Pouthisat]',
                'country_id' => 35,
            ),
            26 => 
            array (
                'id' => 2092,
                'name' => 'Rotanak Kiri [Rôtânôkiri]',
                'country_id' => 35,
            ),
            27 => 
            array (
                'id' => 2093,
                'name' => 'Siem Reab [Siemréab]',
                'country_id' => 35,
            ),
            28 => 
            array (
                'id' => 2094,
                'name' => 'Krong Preah Sihanouk [Krong Preah Sihanouk]',
                'country_id' => 35,
            ),
            29 => 
            array (
                'id' => 2095,
                'name' => 'Stueng Traeng [Stoeng Trêng]',
                'country_id' => 35,
            ),
            30 => 
            array (
                'id' => 2096,
                'name' => 'Baat Dambang [Batdâmbâng]',
                'country_id' => 35,
            ),
            31 => 
            array (
                'id' => 2097,
                'name' => 'Svaay Rieng [Svay Rieng]',
                'country_id' => 35,
            ),
            32 => 
            array (
                'id' => 2098,
                'name' => 'Taakaev [Takêv]',
                'country_id' => 35,
            ),
            33 => 
            array (
                'id' => 2099,
                'name' => 'Otdar Mean Chey [Otdâr Méanchey]',
                'country_id' => 35,
            ),
            34 => 
            array (
                'id' => 2100,
                'name' => 'Krong Kaeb [Krong Kêb]',
                'country_id' => 35,
            ),
            35 => 
            array (
                'id' => 2101,
                'name' => 'Krong Pailin [Krong Pailin]',
                'country_id' => 35,
            ),
            36 => 
            array (
                'id' => 2102,
                'name' => 'Kampong Chaam [Kâmpóng Cham]',
                'country_id' => 35,
            ),
            37 => 
            array (
                'id' => 2103,
                'name' => 'Kampong Chhnang [Kâmpóng Chhnang]',
                'country_id' => 35,
            ),
            38 => 
            array (
                'id' => 2104,
                'name' => 'Kampong Spueu [Kâmpóng Spœ]',
                'country_id' => 35,
            ),
            39 => 
            array (
                'id' => 2105,
                'name' => 'Kampong Thum [Kâmpóng Thum]',
                'country_id' => 35,
            ),
            40 => 
            array (
                'id' => 2106,
                'name' => 'Kampot [Kâmpôt]',
                'country_id' => 35,
            ),
            41 => 
            array (
                'id' => 2107,
                'name' => 'Kandaal [Kândal]',
                'country_id' => 35,
            ),
            42 => 
            array (
                'id' => 2108,
                'name' => 'Kaoh Kong [Kaôh Kong]',
                'country_id' => 35,
            ),
            43 => 
            array (
                'id' => 2109,
                'name' => 'Gilbert Islands',
                'country_id' => 114,
            ),
            44 => 
            array (
                'id' => 2110,
                'name' => 'Line Islands',
                'country_id' => 114,
            ),
            45 => 
            array (
                'id' => 2111,
                'name' => 'Phoenix Islands',
                'country_id' => 114,
            ),
            46 => 
            array (
                'id' => 2112,
                'name' => 'Anjouan',
                'country_id' => 47,
            ),
            47 => 
            array (
                'id' => 2113,
                'name' => 'Grande Comore',
                'country_id' => 47,
            ),
            48 => 
            array (
                'id' => 2114,
                'name' => 'Mohéli',
                'country_id' => 47,
            ),
            49 => 
            array (
                'id' => 2115,
                'name' => 'Christ Church Nichola Town',
                'country_id' => 184,
            ),
            50 => 
            array (
                'id' => 2116,
                'name' => 'Saint Anne Sandy Point',
                'country_id' => 184,
            ),
            51 => 
            array (
                'id' => 2117,
                'name' => 'Saint George Basseterre',
                'country_id' => 184,
            ),
            52 => 
            array (
                'id' => 2118,
                'name' => 'Saint George Gingerland',
                'country_id' => 184,
            ),
            53 => 
            array (
                'id' => 2119,
                'name' => 'Saint James Windward',
                'country_id' => 184,
            ),
            54 => 
            array (
                'id' => 2120,
                'name' => 'Saint John Capisterre',
                'country_id' => 184,
            ),
            55 => 
            array (
                'id' => 2121,
                'name' => 'Saint John Figtree',
                'country_id' => 184,
            ),
            56 => 
            array (
                'id' => 2122,
                'name' => 'Saint Mary Cayon',
                'country_id' => 184,
            ),
            57 => 
            array (
                'id' => 2123,
                'name' => 'Saint Paul Capisterre',
                'country_id' => 184,
            ),
            58 => 
            array (
                'id' => 2124,
                'name' => 'Saint Paul Charlestown',
                'country_id' => 184,
            ),
            59 => 
            array (
                'id' => 2125,
                'name' => 'Saint Peter Basseterre',
                'country_id' => 184,
            ),
            60 => 
            array (
                'id' => 2126,
                'name' => 'Saint Thomas Lowland',
                'country_id' => 184,
            ),
            61 => 
            array (
                'id' => 2127,
                'name' => 'Saint Thomas Middle Island',
                'country_id' => 184,
            ),
            62 => 
            array (
                'id' => 2128,
                'name' => 'Trinity Palmetto Point',
                'country_id' => 184,
            ),
            63 => 
            array (
                'id' => 2129,
                'name' => 'Phyeongyang',
                'country_id' => 115,
            ),
            64 => 
            array (
                'id' => 2130,
                'name' => 'Phyeongannamto',
                'country_id' => 115,
            ),
            65 => 
            array (
                'id' => 2131,
                'name' => 'Phyeonganpukto',
                'country_id' => 115,
            ),
            66 => 
            array (
                'id' => 2132,
                'name' => 'Jakangto',
                'country_id' => 115,
            ),
            67 => 
            array (
                'id' => 2133,
                'name' => 'Hwanghainamto',
                'country_id' => 115,
            ),
            68 => 
            array (
                'id' => 2134,
                'name' => 'Hwanghaipukto',
                'country_id' => 115,
            ),
            69 => 
            array (
                'id' => 2135,
                'name' => 'Kangweonto',
                'country_id' => 115,
            ),
            70 => 
            array (
                'id' => 2136,
                'name' => 'Hamkyeongnamto',
                'country_id' => 115,
            ),
            71 => 
            array (
                'id' => 2137,
                'name' => 'Hamkyeongpukto',
                'country_id' => 115,
            ),
            72 => 
            array (
                'id' => 2138,
                'name' => 'Yanggang-do',
                'country_id' => 115,
            ),
            73 => 
            array (
                'id' => 2139,
                'name' => 'Nason',
                'country_id' => 115,
            ),
            74 => 
            array (
                'id' => 2140,
                'name' => 'Seoul Teugbyeolsi [Seoul-T\'ukpyolshi]',
                'country_id' => 116,
            ),
            75 => 
            array (
                'id' => 2141,
                'name' => 'Busan Gwang\'yeogsi [Pusan-Kwangyokshi]',
                'country_id' => 116,
            ),
            76 => 
            array (
                'id' => 2142,
                'name' => 'Daegu Gwang\'yeogsi [Taegu-Kwangyokshi]',
                'country_id' => 116,
            ),
            77 => 
            array (
                'id' => 2143,
                'name' => 'Incheon Gwang\'yeogsi [Incheon]',
                'country_id' => 116,
            ),
            78 => 
            array (
                'id' => 2144,
                'name' => 'Gwangju Gwang\'yeogsi [Kwangju-Kwangyokshi]',
                'country_id' => 116,
            ),
            79 => 
            array (
                'id' => 2145,
                'name' => 'Daejeon Gwang\'yeogsi [Taejon-Kwangyokshi]',
                'country_id' => 116,
            ),
            80 => 
            array (
                'id' => 2146,
                'name' => 'Ulsan Gwang\'yeogsi [Ulsan-Kwangyokshi]',
                'country_id' => 116,
            ),
            81 => 
            array (
                'id' => 2147,
                'name' => 'Gyeonggido [Kyonggi-do]',
                'country_id' => 116,
            ),
            82 => 
            array (
                'id' => 2148,
                'name' => 'Gang\'weondo [Kang-won-do]',
                'country_id' => 116,
            ),
            83 => 
            array (
                'id' => 2149,
                'name' => 'Chungcheongbugdo [Ch\'ungch\'ongbuk-do]',
                'country_id' => 116,
            ),
            84 => 
            array (
                'id' => 2150,
                'name' => 'Chungcheongnamdo [Ch\'ungch\'ongnam-do]',
                'country_id' => 116,
            ),
            85 => 
            array (
                'id' => 2151,
                'name' => 'Jeonrabugdo [Chollabuk-do]',
                'country_id' => 116,
            ),
            86 => 
            array (
                'id' => 2152,
                'name' => 'Jeonranamdo [Chollanam-do]',
                'country_id' => 116,
            ),
            87 => 
            array (
                'id' => 2153,
                'name' => 'Gyeongsangbugdo [Kyongsangbuk-do]',
                'country_id' => 116,
            ),
            88 => 
            array (
                'id' => 2154,
                'name' => 'Gyeongsangnamdo [Kyongsangnam-do]',
                'country_id' => 116,
            ),
            89 => 
            array (
                'id' => 2155,
                'name' => 'Jejudo [Cheju-do]',
                'country_id' => 116,
            ),
            90 => 
            array (
                'id' => 2162,
                'name' => 'Aqmola oblysy',
                'country_id' => 112,
            ),
            91 => 
            array (
                'id' => 2163,
                'name' => 'Aqtöbe oblysy',
                'country_id' => 112,
            ),
            92 => 
            array (
                'id' => 2164,
                'name' => 'Almaty',
                'country_id' => 112,
            ),
            93 => 
            array (
                'id' => 2165,
                'name' => 'Almaty oblysy',
                'country_id' => 112,
            ),
            94 => 
            array (
                'id' => 2166,
                'name' => 'Astana',
                'country_id' => 112,
            ),
            95 => 
            array (
                'id' => 2167,
                'name' => 'Atyrau oblysy',
                'country_id' => 112,
            ),
            96 => 
            array (
                'id' => 2168,
                'name' => 'Qaraghandy oblysy',
                'country_id' => 112,
            ),
            97 => 
            array (
                'id' => 2169,
                'name' => 'Qostanay oblysy',
                'country_id' => 112,
            ),
            98 => 
            array (
                'id' => 2170,
                'name' => 'Qyzylorda oblysy',
                'country_id' => 112,
            ),
            99 => 
            array (
                'id' => 2171,
                'name' => 'Mangghystau oblysy',
                'country_id' => 112,
            ),
            100 => 
            array (
                'id' => 2172,
                'name' => 'Pavlodar oblysy',
                'country_id' => 112,
            ),
            101 => 
            array (
                'id' => 2173,
                'name' => 'Soltüstik Qazaqstan oblysy',
                'country_id' => 112,
            ),
            102 => 
            array (
                'id' => 2174,
                'name' => 'Shyghys Qazaqstan oblysy',
                'country_id' => 112,
            ),
            103 => 
            array (
                'id' => 2175,
                'name' => 'Ongtüstik Qazaqstan oblysy',
                'country_id' => 112,
            ),
            104 => 
            array (
                'id' => 2176,
                'name' => 'Batys Qazaqstan oblysy',
                'country_id' => 112,
            ),
            105 => 
            array (
                'id' => 2177,
                'name' => 'Zhambyl oblysy',
                'country_id' => 112,
            ),
            106 => 
            array (
                'id' => 2178,
                'name' => 'Attapu [Attopeu]',
                'country_id' => 119,
            ),
            107 => 
            array (
                'id' => 2179,
                'name' => 'Bokèo',
                'country_id' => 119,
            ),
            108 => 
            array (
                'id' => 2180,
                'name' => 'Bolikhamxai [Borikhane]',
                'country_id' => 119,
            ),
            109 => 
            array (
                'id' => 2181,
                'name' => 'Champasak [Champassak]',
                'country_id' => 119,
            ),
            110 => 
            array (
                'id' => 2182,
                'name' => 'Houaphan',
                'country_id' => 119,
            ),
            111 => 
            array (
                'id' => 2183,
                'name' => 'Khammouan',
                'country_id' => 119,
            ),
            112 => 
            array (
                'id' => 2184,
                'name' => 'Louang Namtha',
                'country_id' => 119,
            ),
            113 => 
            array (
                'id' => 2185,
            'name' => 'Louangphabang [Louang Prabang)',
            'country_id' => 119,
        ),
        114 => 
        array (
            'id' => 2186,
            'name' => 'Oudômxai [Oudomsai]',
            'country_id' => 119,
        ),
        115 => 
        array (
            'id' => 2187,
            'name' => 'Phôngsali [Phong Saly]',
            'country_id' => 119,
        ),
        116 => 
        array (
            'id' => 2188,
            'name' => 'Salavan [Saravane]',
            'country_id' => 119,
        ),
        117 => 
        array (
            'id' => 2189,
            'name' => 'Savannakhét',
            'country_id' => 119,
        ),
        118 => 
        array (
            'id' => 2190,
            'name' => 'Vientiane',
            'country_id' => 119,
        ),
        119 => 
        array (
            'id' => 2191,
            'name' => 'Xaignabouli [Sayaboury]',
            'country_id' => 119,
        ),
        120 => 
        array (
            'id' => 2192,
            'name' => 'Xékong [Sékong]',
            'country_id' => 119,
        ),
        121 => 
        array (
            'id' => 2193,
            'name' => 'Xiangkhoang [Xieng Khouang]',
            'country_id' => 119,
        ),
        122 => 
        array (
            'id' => 2194,
            'name' => 'Xaisômboun',
            'country_id' => 119,
        ),
        123 => 
        array (
            'id' => 2195,
            'name' => 'Aakkâr',
            'country_id' => 121,
        ),
        124 => 
        array (
            'id' => 2199,
            'name' => 'El Béqaa',
            'country_id' => 121,
        ),
        125 => 
        array (
            'id' => 2203,
            'name' => 'Anse la Raye',
            'country_id' => 185,
        ),
        126 => 
        array (
            'id' => 2204,
            'name' => 'Castries',
            'country_id' => 185,
        ),
        127 => 
        array (
            'id' => 2205,
            'name' => 'Choiseul',
            'country_id' => 185,
        ),
        128 => 
        array (
            'id' => 2206,
            'name' => 'Dauphin',
            'country_id' => 185,
        ),
        129 => 
        array (
            'id' => 2207,
            'name' => 'Dennery',
            'country_id' => 185,
        ),
        130 => 
        array (
            'id' => 2208,
            'name' => 'Gros Islet',
            'country_id' => 185,
        ),
        131 => 
        array (
            'id' => 2209,
            'name' => 'Laborie',
            'country_id' => 185,
        ),
        132 => 
        array (
            'id' => 2210,
            'name' => 'Micoud',
            'country_id' => 185,
        ),
        133 => 
        array (
            'id' => 2211,
            'name' => 'Praslin',
            'country_id' => 185,
        ),
        134 => 
        array (
            'id' => 2212,
            'name' => 'Soufrière',
            'country_id' => 185,
        ),
        135 => 
        array (
            'id' => 2213,
            'name' => 'Vieux Fort',
            'country_id' => 185,
        ),
        136 => 
        array (
            'id' => 2214,
            'name' => 'Balzers',
            'country_id' => 125,
        ),
        137 => 
        array (
            'id' => 2215,
            'name' => 'Eschen',
            'country_id' => 125,
        ),
        138 => 
        array (
            'id' => 2216,
            'name' => 'Gamprin',
            'country_id' => 125,
        ),
        139 => 
        array (
            'id' => 2217,
            'name' => 'Mauren',
            'country_id' => 125,
        ),
        140 => 
        array (
            'id' => 2218,
            'name' => 'Planken',
            'country_id' => 125,
        ),
        141 => 
        array (
            'id' => 2219,
            'name' => 'Ruggell',
            'country_id' => 125,
        ),
        142 => 
        array (
            'id' => 2220,
            'name' => 'Schaan',
            'country_id' => 125,
        ),
        143 => 
        array (
            'id' => 2221,
            'name' => 'Schellenberg',
            'country_id' => 125,
        ),
        144 => 
        array (
            'id' => 2222,
            'name' => 'Triesen',
            'country_id' => 125,
        ),
        145 => 
        array (
            'id' => 2223,
            'name' => 'Triesenberg',
            'country_id' => 125,
        ),
        146 => 
        array (
            'id' => 2224,
            'name' => 'Vaduz',
            'country_id' => 125,
        ),
        147 => 
        array (
            'id' => 2225,
            'name' => 'Basnahira pa?ata',
            'country_id' => 204,
        ),
        148 => 
        array (
            'id' => 2226,
            'name' => 'Colombo',
            'country_id' => 204,
        ),
        149 => 
        array (
            'id' => 2227,
            'name' => 'Gampaha',
            'country_id' => 204,
        ),
        150 => 
        array (
            'id' => 2228,
            'name' => 'Kalutara',
            'country_id' => 204,
        ),
        151 => 
        array (
            'id' => 2229,
            'name' => 'Madhyama pa?ata',
            'country_id' => 204,
        ),
        152 => 
        array (
            'id' => 2230,
            'name' => 'Kandy',
            'country_id' => 204,
        ),
        153 => 
        array (
            'id' => 2231,
            'name' => 'Matale',
            'country_id' => 204,
        ),
        154 => 
        array (
            'id' => 2232,
            'name' => 'Nuwara Eliya',
            'country_id' => 204,
        ),
        155 => 
        array (
            'id' => 2233,
            'name' => 'Daku?u pa?ata',
            'country_id' => 204,
        ),
        156 => 
        array (
            'id' => 2234,
            'name' => 'Galle',
            'country_id' => 204,
        ),
        157 => 
        array (
            'id' => 2235,
            'name' => 'Matara',
            'country_id' => 204,
        ),
        158 => 
        array (
            'id' => 2236,
            'name' => 'Hambantota',
            'country_id' => 204,
        ),
        159 => 
        array (
            'id' => 2237,
            'name' => 'Uturu pa?ata',
            'country_id' => 204,
        ),
        160 => 
        array (
            'id' => 2238,
            'name' => 'Jaffna',
            'country_id' => 204,
        ),
        161 => 
        array (
            'id' => 2239,
            'name' => 'Kilinochchi',
            'country_id' => 204,
        ),
        162 => 
        array (
            'id' => 2240,
            'name' => 'Mannar',
            'country_id' => 204,
        ),
        163 => 
        array (
            'id' => 2241,
            'name' => 'Vavuniya',
            'country_id' => 204,
        ),
        164 => 
        array (
            'id' => 2242,
            'name' => 'Mullaittivu',
            'country_id' => 204,
        ),
        165 => 
        array (
            'id' => 2243,
            'name' => 'Næ?genahira pa?ata',
            'country_id' => 204,
        ),
        166 => 
        array (
            'id' => 2244,
            'name' => 'Batticaloa',
            'country_id' => 204,
        ),
        167 => 
        array (
            'id' => 2245,
            'name' => 'Ampara',
            'country_id' => 204,
        ),
        168 => 
        array (
            'id' => 2246,
            'name' => 'Trincomalee',
            'country_id' => 204,
        ),
        169 => 
        array (
            'id' => 2247,
            'name' => 'Vayamba pa?ata',
            'country_id' => 204,
        ),
        170 => 
        array (
            'id' => 2248,
            'name' => 'Kurunegala',
            'country_id' => 204,
        ),
        171 => 
        array (
            'id' => 2249,
            'name' => 'Puttalam',
            'country_id' => 204,
        ),
        172 => 
        array (
            'id' => 2250,
            'name' => 'Uturumæ?da pa?ata',
            'country_id' => 204,
        ),
        173 => 
        array (
            'id' => 2251,
            'name' => 'Anuradhapura',
            'country_id' => 204,
        ),
        174 => 
        array (
            'id' => 2252,
            'name' => 'Polonnaruwa',
            'country_id' => 204,
        ),
        175 => 
        array (
            'id' => 2253,
            'name' => 'Uva pa?ata',
            'country_id' => 204,
        ),
        176 => 
        array (
            'id' => 2254,
            'name' => 'Badulla',
            'country_id' => 204,
        ),
        177 => 
        array (
            'id' => 2255,
            'name' => 'Monaragala',
            'country_id' => 204,
        ),
        178 => 
        array (
            'id' => 2256,
            'name' => 'Sabaragamuva pa?ata',
            'country_id' => 204,
        ),
        179 => 
        array (
            'id' => 2257,
            'name' => 'Ratnapura',
            'country_id' => 204,
        ),
        180 => 
        array (
            'id' => 2258,
            'name' => 'Kegalla',
            'country_id' => 204,
        ),
        181 => 
        array (
            'id' => 2259,
            'name' => 'Bong',
            'country_id' => 123,
        ),
        182 => 
        array (
            'id' => 2260,
            'name' => 'Bomi',
            'country_id' => 123,
        ),
        183 => 
        array (
            'id' => 2261,
            'name' => 'Grand Cape Mount',
            'country_id' => 123,
        ),
        184 => 
        array (
            'id' => 2262,
            'name' => 'Grand Bassa',
            'country_id' => 123,
        ),
        185 => 
        array (
            'id' => 2263,
            'name' => 'Grand Gedeh',
            'country_id' => 123,
        ),
        186 => 
        array (
            'id' => 2264,
            'name' => 'Grand Kru',
            'country_id' => 123,
        ),
        187 => 
        array (
            'id' => 2265,
            'name' => 'Gbarpolu',
            'country_id' => 123,
        ),
        188 => 
        array (
            'id' => 2266,
            'name' => 'Lofa',
            'country_id' => 123,
        ),
        189 => 
        array (
            'id' => 2267,
            'name' => 'Margibi',
            'country_id' => 123,
        ),
        190 => 
        array (
            'id' => 2268,
            'name' => 'Montserrado',
            'country_id' => 123,
        ),
        191 => 
        array (
            'id' => 2269,
            'name' => 'Maryland',
            'country_id' => 123,
        ),
        192 => 
        array (
            'id' => 2270,
            'name' => 'Nimba',
            'country_id' => 123,
        ),
        193 => 
        array (
            'id' => 2271,
            'name' => 'River Gee',
            'country_id' => 123,
        ),
        194 => 
        array (
            'id' => 2272,
            'name' => 'Rivercess',
            'country_id' => 123,
        ),
        195 => 
        array (
            'id' => 2273,
            'name' => 'Sinoe',
            'country_id' => 123,
        ),
        196 => 
        array (
            'id' => 2274,
            'name' => 'Maseru',
            'country_id' => 122,
        ),
        197 => 
        array (
            'id' => 2275,
            'name' => 'Butha-Buthe',
            'country_id' => 122,
        ),
        198 => 
        array (
            'id' => 2276,
            'name' => 'Leribe',
            'country_id' => 122,
        ),
        199 => 
        array (
            'id' => 2277,
            'name' => 'Berea',
            'country_id' => 122,
        ),
        200 => 
        array (
            'id' => 2278,
            'name' => 'Mafeteng',
            'country_id' => 122,
        ),
        201 => 
        array (
            'id' => 2279,
            'name' => 'Mohale\'s Hoek',
            'country_id' => 122,
        ),
        202 => 
        array (
            'id' => 2280,
            'name' => 'Quthing',
            'country_id' => 122,
        ),
        203 => 
        array (
            'id' => 2281,
            'name' => 'Qacha\'s Nek',
            'country_id' => 122,
        ),
        204 => 
        array (
            'id' => 2282,
            'name' => 'Mokhotlong',
            'country_id' => 122,
        ),
        205 => 
        array (
            'id' => 2283,
            'name' => 'Thaba-Tseka',
            'country_id' => 122,
        ),
        206 => 
        array (
            'id' => 2284,
            'name' => 'Alytaus Apskritis',
            'country_id' => 126,
        ),
        207 => 
        array (
            'id' => 2285,
            'name' => 'Klaipedos apskritis',
            'country_id' => 126,
        ),
        208 => 
        array (
            'id' => 2286,
            'name' => 'Kauno Apskritis',
            'country_id' => 126,
        ),
        209 => 
        array (
            'id' => 2287,
            'name' => 'Marijampoles apskritis',
            'country_id' => 126,
        ),
        210 => 
        array (
            'id' => 2288,
            'name' => 'Panevežio apskritis',
            'country_id' => 126,
        ),
        211 => 
        array (
            'id' => 2289,
            'name' => 'Šiauliu Apskritis',
            'country_id' => 126,
        ),
        212 => 
        array (
            'id' => 2290,
            'name' => 'Taurages apskritis',
            'country_id' => 126,
        ),
        213 => 
        array (
            'id' => 2291,
            'name' => 'Telšiu Apskritis',
            'country_id' => 126,
        ),
        214 => 
        array (
            'id' => 2292,
            'name' => 'Utenos Apskritis',
            'country_id' => 126,
        ),
        215 => 
        array (
            'id' => 2293,
            'name' => 'Vilniaus Apskritis',
            'country_id' => 126,
        ),
        216 => 
        array (
            'id' => 2294,
            'name' => 'Diekirch',
            'country_id' => 127,
        ),
        217 => 
        array (
            'id' => 2295,
            'name' => 'Grevenmacher',
            'country_id' => 127,
        ),
        218 => 
        array (
            'id' => 2296,
        'name' => 'Luxembourg (fr)',
            'country_id' => 127,
        ),
        219 => 
        array (
            'id' => 2297,
        'name' => 'Aglonas novads (Aglona)',
            'country_id' => 120,
        ),
        220 => 
        array (
            'id' => 2298,
        'name' => 'Aizkraukles novads (Aizkraukle)',
            'country_id' => 120,
        ),
        221 => 
        array (
            'id' => 2299,
        'name' => 'Aizputes novads (Aizpute)',
            'country_id' => 120,
        ),
        222 => 
        array (
            'id' => 2300,
        'name' => 'Aknistes novads (Akniste)',
            'country_id' => 120,
        ),
        223 => 
        array (
            'id' => 2301,
        'name' => 'Alojas novads (Aloja)',
            'country_id' => 120,
        ),
        224 => 
        array (
            'id' => 2302,
        'name' => 'Alsungas novads (Alsunga)',
            'country_id' => 120,
        ),
        225 => 
        array (
            'id' => 2303,
        'name' => 'Aluksnes novads (Aluksne)',
            'country_id' => 120,
        ),
        226 => 
        array (
            'id' => 2304,
        'name' => 'Amatas novads (Amata)',
            'country_id' => 120,
        ),
        227 => 
        array (
            'id' => 2305,
        'name' => 'Apes novads (Ape)',
            'country_id' => 120,
        ),
        228 => 
        array (
            'id' => 2306,
        'name' => 'Auces novads (Auce)',
            'country_id' => 120,
        ),
        229 => 
        array (
            'id' => 2307,
        'name' => 'Adažu novads (Adaži)',
            'country_id' => 120,
        ),
        230 => 
        array (
            'id' => 2308,
        'name' => 'Babites novads (Babite)',
            'country_id' => 120,
        ),
        231 => 
        array (
            'id' => 2309,
        'name' => 'Baldones novads (Baldone)',
            'country_id' => 120,
        ),
        232 => 
        array (
            'id' => 2310,
        'name' => 'Baltinavas novads (Baltinava)',
            'country_id' => 120,
        ),
        233 => 
        array (
            'id' => 2311,
        'name' => 'Balvu novads (Balvi)',
            'country_id' => 120,
        ),
        234 => 
        array (
            'id' => 2312,
        'name' => 'Bauskas novads (Bauska)',
            'country_id' => 120,
        ),
        235 => 
        array (
            'id' => 2313,
        'name' => 'Beverinas novads (Beverina)',
            'country_id' => 120,
        ),
        236 => 
        array (
            'id' => 2314,
        'name' => 'Brocenu novads (Broceni)',
            'country_id' => 120,
        ),
        237 => 
        array (
            'id' => 2315,
        'name' => 'Burtnieku novads (Burtnieki)',
            'country_id' => 120,
        ),
        238 => 
        array (
            'id' => 2316,
        'name' => 'Carnikavas novads (Carnikava)',
            'country_id' => 120,
        ),
        239 => 
        array (
            'id' => 2317,
        'name' => 'Cesvaines novads (Cesvaine)',
            'country_id' => 120,
        ),
        240 => 
        array (
            'id' => 2318,
        'name' => 'Cesu novads (Cesis)',
            'country_id' => 120,
        ),
        241 => 
        array (
            'id' => 2319,
        'name' => 'Ciblas novads (Cibla)',
            'country_id' => 120,
        ),
        242 => 
        array (
            'id' => 2320,
        'name' => 'Dagdas novads (Dagda)',
            'country_id' => 120,
        ),
        243 => 
        array (
            'id' => 2321,
        'name' => 'Daugavpils novads (Daugavpils)',
            'country_id' => 120,
        ),
        244 => 
        array (
            'id' => 2322,
        'name' => 'Dobeles novads (Dobele)',
            'country_id' => 120,
        ),
        245 => 
        array (
            'id' => 2323,
        'name' => 'Dundagas novads (Dundaga)',
            'country_id' => 120,
        ),
        246 => 
        array (
            'id' => 2324,
        'name' => 'Durbes novads (Durbe)',
            'country_id' => 120,
        ),
        247 => 
        array (
            'id' => 2325,
        'name' => 'Engures novads (Engure)',
            'country_id' => 120,
        ),
        248 => 
        array (
            'id' => 2326,
        'name' => 'Erglu novads (Ergli)',
            'country_id' => 120,
        ),
        249 => 
        array (
            'id' => 2327,
        'name' => 'Garkalnes novads (Garkalne)',
            'country_id' => 120,
        ),
        250 => 
        array (
            'id' => 2328,
        'name' => 'Grobinas novads (Grobina)',
            'country_id' => 120,
        ),
        251 => 
        array (
            'id' => 2329,
        'name' => 'Gulbenes novads (Gulbene)',
            'country_id' => 120,
        ),
        252 => 
        array (
            'id' => 2330,
        'name' => 'Iecavas novads (Iecava)',
            'country_id' => 120,
        ),
        253 => 
        array (
            'id' => 2331,
        'name' => 'Ikškiles novads (Ikškile)',
            'country_id' => 120,
        ),
        254 => 
        array (
            'id' => 2332,
        'name' => 'Ilukstes novads (Ilukste)',
            'country_id' => 120,
        ),
        255 => 
        array (
            'id' => 2333,
        'name' => 'Incukalna novads (Incukalns)',
            'country_id' => 120,
        ),
        256 => 
        array (
            'id' => 2334,
            'name' => '"Jaunjelgavas novads',
            'country_id' => 120,
        ),
        257 => 
        array (
            'id' => 2335,
            'name' => '"Jaunpiebalgas novads',
            'country_id' => 120,
        ),
        258 => 
        array (
            'id' => 2336,
        'name' => 'Jaunpils novads (Jaunpils)',
            'country_id' => 120,
        ),
        259 => 
        array (
            'id' => 2337,
        'name' => 'Jelgavas novads (Jelgava)',
            'country_id' => 120,
        ),
        260 => 
        array (
            'id' => 2338,
        'name' => 'Jekabpils novads (Jekabpils)',
            'country_id' => 120,
        ),
        261 => 
        array (
            'id' => 2339,
        'name' => 'Kandavas novads (Kandava)',
            'country_id' => 120,
        ),
        262 => 
        array (
            'id' => 2340,
        'name' => 'Karsavas novads (Karsava)',
            'country_id' => 120,
        ),
        263 => 
        array (
            'id' => 2341,
        'name' => 'Kocenu novads (Koceni)',
            'country_id' => 120,
        ),
        264 => 
        array (
            'id' => 2342,
        'name' => 'Kokneses novads (Koknese)',
            'country_id' => 120,
        ),
        265 => 
        array (
            'id' => 2343,
        'name' => 'Kraslavas novads (Kraslava)',
            'country_id' => 120,
        ),
        266 => 
        array (
            'id' => 2344,
        'name' => 'Krimuldas novads (Krimulda)',
            'country_id' => 120,
        ),
        267 => 
        array (
            'id' => 2345,
        'name' => 'Krustpils novads (Krustpils)',
            'country_id' => 120,
        ),
        268 => 
        array (
            'id' => 2346,
        'name' => 'Kuldigas novads (Kuldiga)',
            'country_id' => 120,
        ),
        269 => 
        array (
            'id' => 2347,
        'name' => 'Keguma novads (Kegums)',
            'country_id' => 120,
        ),
        270 => 
        array (
            'id' => 2348,
        'name' => 'Kekavas novads (Kekava)',
            'country_id' => 120,
        ),
        271 => 
        array (
            'id' => 2349,
        'name' => 'Lielvardes novads (Lielvarde)',
            'country_id' => 120,
        ),
        272 => 
        array (
            'id' => 2350,
        'name' => 'Limbažu novads (Limbaži)',
            'country_id' => 120,
        ),
        273 => 
        array (
            'id' => 2351,
        'name' => 'Ligatnes novads (Ligatne)',
            'country_id' => 120,
        ),
        274 => 
        array (
            'id' => 2352,
        'name' => 'Livanu novads (Livani)',
            'country_id' => 120,
        ),
        275 => 
        array (
            'id' => 2353,
        'name' => 'Lubanas novads (Lubana)',
            'country_id' => 120,
        ),
        276 => 
        array (
            'id' => 2354,
        'name' => 'Ludzas novads (Ludza)',
            'country_id' => 120,
        ),
        277 => 
        array (
            'id' => 2355,
        'name' => 'Madonas novads (Madona)',
            'country_id' => 120,
        ),
        278 => 
        array (
            'id' => 2356,
        'name' => 'Mazsalacas novads (Mazsalaca)',
            'country_id' => 120,
        ),
        279 => 
        array (
            'id' => 2357,
        'name' => 'Malpils novads (Malpils)',
            'country_id' => 120,
        ),
        280 => 
        array (
            'id' => 2358,
        'name' => 'Marupes novads (Marupe)',
            'country_id' => 120,
        ),
        281 => 
        array (
            'id' => 2359,
        'name' => 'Mersraga novads (Mersrags)',
            'country_id' => 120,
        ),
        282 => 
        array (
            'id' => 2360,
        'name' => 'Naukšenu novads (Naukšeni)',
            'country_id' => 120,
        ),
        283 => 
        array (
            'id' => 2361,
        'name' => 'Neretas novads (Nereta)',
            'country_id' => 120,
        ),
        284 => 
        array (
            'id' => 2362,
        'name' => 'Nicas novads (Nica)',
            'country_id' => 120,
        ),
        285 => 
        array (
            'id' => 2363,
        'name' => 'Ogres novads (Ogre)',
            'country_id' => 120,
        ),
        286 => 
        array (
            'id' => 2364,
        'name' => 'Olaines novads (Olaine)',
            'country_id' => 120,
        ),
        287 => 
        array (
            'id' => 2365,
        'name' => 'Ozolnieku novads (Ozolnieki)',
            'country_id' => 120,
        ),
        288 => 
        array (
            'id' => 2366,
        'name' => 'Pargaujas novads (Pargauja)',
            'country_id' => 120,
        ),
        289 => 
        array (
            'id' => 2367,
        'name' => 'Pavilostas novads (Pavilosta)',
            'country_id' => 120,
        ),
        290 => 
        array (
            'id' => 2368,
        'name' => 'Plavinu novads (Plavinas)',
            'country_id' => 120,
        ),
        291 => 
        array (
            'id' => 2369,
        'name' => 'Preilu novads (Preili)',
            'country_id' => 120,
        ),
        292 => 
        array (
            'id' => 2370,
        'name' => 'Priekules novads (Priekule)',
            'country_id' => 120,
        ),
        293 => 
        array (
            'id' => 2371,
        'name' => 'Priekulu novads (Priekuli)',
            'country_id' => 120,
        ),
        294 => 
        array (
            'id' => 2372,
        'name' => 'Raunas novads (Rauna)',
            'country_id' => 120,
        ),
        295 => 
        array (
            'id' => 2373,
        'name' => 'Rezeknes novads (Rezekne)',
            'country_id' => 120,
        ),
        296 => 
        array (
            'id' => 2374,
        'name' => 'Riebinu novads (Riebini)',
            'country_id' => 120,
        ),
        297 => 
        array (
            'id' => 2375,
        'name' => 'Rojas novads (Roja)',
            'country_id' => 120,
        ),
        298 => 
        array (
            'id' => 2376,
        'name' => 'Ropažu novads (Ropaži)',
            'country_id' => 120,
        ),
        299 => 
        array (
            'id' => 2377,
        'name' => 'Rucavas novads (Rucava)',
            'country_id' => 120,
        ),
        300 => 
        array (
            'id' => 2378,
        'name' => 'Rugaju novads (Rugaji)',
            'country_id' => 120,
        ),
        301 => 
        array (
            'id' => 2379,
        'name' => 'Rundales novads (Rundale)',
            'country_id' => 120,
        ),
        302 => 
        array (
            'id' => 2380,
        'name' => 'Rujienas novads (Rujiena)',
            'country_id' => 120,
        ),
        303 => 
        array (
            'id' => 2381,
        'name' => 'Salas novads (Sala)',
            'country_id' => 120,
        ),
        304 => 
        array (
            'id' => 2382,
        'name' => 'Salacgrivas novads (Salacgriva)',
            'country_id' => 120,
        ),
        305 => 
        array (
            'id' => 2383,
        'name' => 'Salaspils novads (Salaspils)',
            'country_id' => 120,
        ),
        306 => 
        array (
            'id' => 2384,
        'name' => 'Saldus novads (Saldus)',
            'country_id' => 120,
        ),
        307 => 
        array (
            'id' => 2385,
        'name' => 'Saulkrastu novads (Saulkrasti)',
            'country_id' => 120,
        ),
        308 => 
        array (
            'id' => 2386,
        'name' => 'Sejas novads (Seja)',
            'country_id' => 120,
        ),
        309 => 
        array (
            'id' => 2387,
        'name' => 'Siguldas novads (Sigulda)',
            'country_id' => 120,
        ),
        310 => 
        array (
            'id' => 2388,
        'name' => 'Skriveru novads (Skriveri)',
            'country_id' => 120,
        ),
        311 => 
        array (
            'id' => 2389,
        'name' => 'Skrundas novads (Skrunda)',
            'country_id' => 120,
        ),
        312 => 
        array (
            'id' => 2390,
        'name' => 'Smiltenes novads (Smiltene)',
            'country_id' => 120,
        ),
        313 => 
        array (
            'id' => 2391,
        'name' => 'Stopinu novads (Stopini)',
            'country_id' => 120,
        ),
        314 => 
        array (
            'id' => 2392,
        'name' => 'Strencu novads (Strenci)',
            'country_id' => 120,
        ),
        315 => 
        array (
            'id' => 2393,
        'name' => 'Talsu novads (Talsi)',
            'country_id' => 120,
        ),
        316 => 
        array (
            'id' => 2394,
        'name' => 'Tervetes novads (Tervete)',
            'country_id' => 120,
        ),
        317 => 
        array (
            'id' => 2395,
        'name' => 'Tukuma novads (Tukums)',
            'country_id' => 120,
        ),
        318 => 
        array (
            'id' => 2396,
        'name' => 'Vainodes novads (Vainode)',
            'country_id' => 120,
        ),
        319 => 
        array (
            'id' => 2397,
        'name' => 'Valkas novads (Valka)',
            'country_id' => 120,
        ),
        320 => 
        array (
            'id' => 2398,
        'name' => 'Varaklanu novads (Varaklani)',
            'country_id' => 120,
        ),
        321 => 
        array (
            'id' => 2399,
        'name' => 'Varkavas novads (Varkava)',
            'country_id' => 120,
        ),
        322 => 
        array (
            'id' => 2400,
            'name' => '"Vecpiebalgas novads',
            'country_id' => 120,
        ),
        323 => 
        array (
            'id' => 2401,
        'name' => 'Vecumnieku novads (Vecumnieki)',
            'country_id' => 120,
        ),
        324 => 
        array (
            'id' => 2402,
        'name' => 'Ventspils novads (Ventspils)',
            'country_id' => 120,
        ),
        325 => 
        array (
            'id' => 2403,
        'name' => 'Viesites novads (Viesite)',
            'country_id' => 120,
        ),
        326 => 
        array (
            'id' => 2404,
        'name' => 'Vilakas novads (Vilaka)',
            'country_id' => 120,
        ),
        327 => 
        array (
            'id' => 2405,
        'name' => 'Vilanu novads (Vilani)',
            'country_id' => 120,
        ),
        328 => 
        array (
            'id' => 2406,
        'name' => 'Zilupes novads (Zilupe)',
            'country_id' => 120,
        ),
        329 => 
        array (
            'id' => 2407,
            'name' => 'Daugavpils',
            'country_id' => 120,
        ),
        330 => 
        array (
            'id' => 2408,
            'name' => 'Jelgava',
            'country_id' => 120,
        ),
        331 => 
        array (
            'id' => 2409,
            'name' => 'Jekabpils',
            'country_id' => 120,
        ),
        332 => 
        array (
            'id' => 2410,
            'name' => 'Jurmala',
            'country_id' => 120,
        ),
        333 => 
        array (
            'id' => 2411,
            'name' => 'Liepaja',
            'country_id' => 120,
        ),
        334 => 
        array (
            'id' => 2412,
            'name' => 'Rezekne',
            'country_id' => 120,
        ),
        335 => 
        array (
            'id' => 2413,
            'name' => 'Riga',
            'country_id' => 120,
        ),
        336 => 
        array (
            'id' => 2414,
            'name' => 'Ventspils',
            'country_id' => 120,
        ),
        337 => 
        array (
            'id' => 2415,
            'name' => 'Valmiera',
            'country_id' => 120,
        ),
        338 => 
        array (
            'id' => 2417,
            'name' => 'Al Butnan',
            'country_id' => 124,
        ),
        339 => 
        array (
            'id' => 2419,
            'name' => 'Ghat',
            'country_id' => 124,
        ),
        340 => 
        array (
            'id' => 2421,
            'name' => 'Al Jabal al Gharbi',
            'country_id' => 124,
        ),
        341 => 
        array (
            'id' => 2422,
            'name' => 'Al Jifarah',
            'country_id' => 124,
        ),
        342 => 
        array (
            'id' => 2425,
            'name' => 'Al Marqab',
            'country_id' => 124,
        ),
        343 => 
        array (
            'id' => 2427,
            'name' => 'Al Marj',
            'country_id' => 124,
        ),
        344 => 
        array (
            'id' => 2429,
            'name' => 'Nalut',
            'country_id' => 124,
        ),
        345 => 
        array (
            'id' => 2434,
            'name' => 'Al Wa?at',
            'country_id' => 124,
        ),
        346 => 
        array (
            'id' => 2435,
            'name' => 'Wadi al Hayat',
            'country_id' => 124,
        ),
        347 => 
        array (
            'id' => 2436,
            'name' => 'Wadi ash Shati?',
            'country_id' => 124,
        ),
        348 => 
        array (
            'id' => 2441,
            'name' => 'L\'Oriental',
            'country_id' => 148,
        ),
        349 => 
        array (
            'id' => 2452,
            'name' => 'Laâyoune-Boujdour-Sakia el Hamra',
            'country_id' => 148,
        ),
        350 => 
        array (
            'id' => 2453,
            'name' => 'Oued ed Dahab-Lagouira',
            'country_id' => 148,
        ),
        351 => 
        array (
            'id' => 2454,
            'name' => 'Agadir-Ida-Outanane',
            'country_id' => 148,
        ),
        352 => 
        array (
            'id' => 2455,
            'name' => 'Aousserd',
            'country_id' => 148,
        ),
        353 => 
        array (
            'id' => 2456,
            'name' => 'Assa-Zag',
            'country_id' => 148,
        ),
        354 => 
        array (
            'id' => 2457,
            'name' => 'Azilal',
            'country_id' => 148,
        ),
        355 => 
        array (
            'id' => 2458,
            'name' => 'Beni Mellal',
            'country_id' => 148,
        ),
        356 => 
        array (
            'id' => 2459,
            'name' => 'Berkane',
            'country_id' => 148,
        ),
        357 => 
        array (
            'id' => 2460,
            'name' => 'Ben Slimane',
            'country_id' => 148,
        ),
        358 => 
        array (
            'id' => 2461,
        'name' => 'Boujdour (EH)',
            'country_id' => 148,
        ),
        359 => 
        array (
            'id' => 2462,
            'name' => 'Boulemane',
            'country_id' => 148,
        ),
        360 => 
        array (
            'id' => 2463,
            'name' => 'Casablanca [Dar el Beïda]*',
            'country_id' => 148,
        ),
        361 => 
        array (
            'id' => 2464,
            'name' => 'Chefchaouene',
            'country_id' => 148,
        ),
        362 => 
        array (
            'id' => 2465,
            'name' => 'Chichaoua',
            'country_id' => 148,
        ),
        363 => 
        array (
            'id' => 2466,
            'name' => 'Chtouka-Ait Baha',
            'country_id' => 148,
        ),
        364 => 
        array (
            'id' => 2467,
            'name' => 'Errachidia',
            'country_id' => 148,
        ),
        365 => 
        array (
            'id' => 2468,
            'name' => 'Essaouira',
            'country_id' => 148,
        ),
        366 => 
        array (
            'id' => 2469,
        'name' => 'Es Smara (EH)',
            'country_id' => 148,
        ),
        367 => 
        array (
            'id' => 2470,
            'name' => 'Fahs-Beni Makada',
            'country_id' => 148,
        ),
        368 => 
        array (
            'id' => 2471,
            'name' => 'Fès-Dar-Dbibegh',
            'country_id' => 148,
        ),
        369 => 
        array (
            'id' => 2472,
            'name' => 'Figuig',
            'country_id' => 148,
        ),
        370 => 
        array (
            'id' => 2473,
            'name' => 'Guelmim',
            'country_id' => 148,
        ),
        371 => 
        array (
            'id' => 2474,
            'name' => 'El Hajeb',
            'country_id' => 148,
        ),
        372 => 
        array (
            'id' => 2475,
            'name' => 'Al Haouz',
            'country_id' => 148,
        ),
        373 => 
        array (
            'id' => 2476,
            'name' => 'Al Hoceïma',
            'country_id' => 148,
        ),
        374 => 
        array (
            'id' => 2477,
            'name' => 'Ifrane',
            'country_id' => 148,
        ),
        375 => 
        array (
            'id' => 2478,
            'name' => 'Inezgane-Ait Melloul',
            'country_id' => 148,
        ),
        376 => 
        array (
            'id' => 2479,
            'name' => 'El Jadida',
            'country_id' => 148,
        ),
        377 => 
        array (
            'id' => 2480,
            'name' => 'Jrada',
            'country_id' => 148,
        ),
        378 => 
        array (
            'id' => 2481,
            'name' => 'Kénitra',
            'country_id' => 148,
        ),
        379 => 
        array (
            'id' => 2482,
            'name' => 'Kelaat Sraghna',
            'country_id' => 148,
        ),
        380 => 
        array (
            'id' => 2483,
            'name' => 'Khemisset',
            'country_id' => 148,
        ),
        381 => 
        array (
            'id' => 2484,
            'name' => 'Khenifra',
            'country_id' => 148,
        ),
        382 => 
        array (
            'id' => 2485,
            'name' => 'Khouribga',
            'country_id' => 148,
        ),
        383 => 
        array (
            'id' => 2486,
            'name' => 'Laâyoune',
            'country_id' => 148,
        ),
        384 => 
        array (
            'id' => 2487,
            'name' => 'Larache',
            'country_id' => 148,
        ),
        385 => 
        array (
            'id' => 2488,
            'name' => 'Médiouna',
            'country_id' => 148,
        ),
        386 => 
        array (
            'id' => 2489,
            'name' => 'Meknès*',
            'country_id' => 148,
        ),
        387 => 
        array (
            'id' => 2490,
            'name' => 'Marrakech-Medina',
            'country_id' => 148,
        ),
        388 => 
        array (
            'id' => 2491,
            'name' => 'Marrakech-Menara',
            'country_id' => 148,
        ),
        389 => 
        array (
            'id' => 2492,
            'name' => 'Mohammadia',
            'country_id' => 148,
        ),
        390 => 
        array (
            'id' => 2493,
            'name' => 'Moulay Yacoub',
            'country_id' => 148,
        ),
        391 => 
        array (
            'id' => 2494,
            'name' => 'Nador',
            'country_id' => 148,
        ),
        392 => 
        array (
            'id' => 2495,
            'name' => 'Nouaceur',
            'country_id' => 148,
        ),
        393 => 
        array (
            'id' => 2496,
            'name' => 'Ouarzazate',
            'country_id' => 148,
        ),
        394 => 
        array (
            'id' => 2497,
        'name' => 'Oued ed Dahab (EH)',
            'country_id' => 148,
        ),
        395 => 
        array (
            'id' => 2498,
            'name' => 'Oujda-Angad',
            'country_id' => 148,
        ),
        396 => 
        array (
            'id' => 2499,
            'name' => 'Rabat',
            'country_id' => 148,
        ),
        397 => 
        array (
            'id' => 2500,
            'name' => 'Safi',
            'country_id' => 148,
        ),
        398 => 
        array (
            'id' => 2501,
            'name' => 'Salé',
            'country_id' => 148,
        ),
        399 => 
        array (
            'id' => 2502,
            'name' => 'Sefrou',
            'country_id' => 148,
        ),
        400 => 
        array (
            'id' => 2503,
            'name' => 'Settat',
            'country_id' => 148,
        ),
        401 => 
        array (
            'id' => 2504,
            'name' => 'Sidi Kacem',
            'country_id' => 148,
        ),
        402 => 
        array (
            'id' => 2505,
            'name' => 'Skhirate-Témara',
            'country_id' => 148,
        ),
        403 => 
        array (
            'id' => 2506,
            'name' => 'Sidi Youssef Ben Ali',
            'country_id' => 148,
        ),
        404 => 
        array (
            'id' => 2507,
            'name' => 'Taourirt',
            'country_id' => 148,
        ),
        405 => 
        array (
            'id' => 2508,
            'name' => 'Taounate',
            'country_id' => 148,
        ),
        406 => 
        array (
            'id' => 2509,
            'name' => 'Taroudannt',
            'country_id' => 148,
        ),
        407 => 
        array (
            'id' => 2510,
            'name' => 'Tata',
            'country_id' => 148,
        ),
        408 => 
        array (
            'id' => 2511,
            'name' => 'Taza',
            'country_id' => 148,
        ),
        409 => 
        array (
            'id' => 2512,
            'name' => 'Tétouan*',
            'country_id' => 148,
        ),
        410 => 
        array (
            'id' => 2513,
            'name' => 'Tiznit',
            'country_id' => 148,
        ),
        411 => 
        array (
            'id' => 2514,
            'name' => 'Tanger-Assilah',
            'country_id' => 148,
        ),
        412 => 
        array (
            'id' => 2515,
            'name' => 'Tan-Tan',
            'country_id' => 148,
        ),
        413 => 
        array (
            'id' => 2516,
            'name' => 'Zagora',
            'country_id' => 148,
        ),
        414 => 
        array (
            'id' => 2517,
            'name' => 'La Colle',
            'country_id' => 144,
        ),
        415 => 
        array (
            'id' => 2518,
            'name' => 'La Condamine',
            'country_id' => 144,
        ),
        416 => 
        array (
            'id' => 2519,
            'name' => 'Fontvieille',
            'country_id' => 144,
        ),
        417 => 
        array (
            'id' => 2520,
            'name' => 'La Gare',
            'country_id' => 144,
        ),
        418 => 
        array (
            'id' => 2521,
            'name' => 'Jardin Exotique',
            'country_id' => 144,
        ),
        419 => 
        array (
            'id' => 2522,
            'name' => 'Larvotto',
            'country_id' => 144,
        ),
        420 => 
        array (
            'id' => 2523,
            'name' => 'Malbousquet',
            'country_id' => 144,
        ),
        421 => 
        array (
            'id' => 2524,
            'name' => 'Monte-Carlo',
            'country_id' => 144,
        ),
        422 => 
        array (
            'id' => 2525,
            'name' => 'Moneghetti',
            'country_id' => 144,
        ),
        423 => 
        array (
            'id' => 2526,
            'name' => 'Monaco-Ville',
            'country_id' => 144,
        ),
        424 => 
        array (
            'id' => 2527,
            'name' => 'Moulins',
            'country_id' => 144,
        ),
        425 => 
        array (
            'id' => 2528,
            'name' => 'Port-Hercule',
            'country_id' => 144,
        ),
        426 => 
        array (
            'id' => 2529,
            'name' => 'Sainte-Dévote',
            'country_id' => 144,
        ),
        427 => 
        array (
            'id' => 2530,
            'name' => 'La Source',
            'country_id' => 144,
        ),
        428 => 
        array (
            'id' => 2531,
            'name' => 'Spélugues',
            'country_id' => 144,
        ),
        429 => 
        array (
            'id' => 2532,
            'name' => 'Saint-Roman',
            'country_id' => 144,
        ),
        430 => 
        array (
            'id' => 2533,
            'name' => 'Vallon de la Rousse',
            'country_id' => 144,
        ),
        431 => 
        array (
            'id' => 2534,
            'name' => 'Anenii Noi',
            'country_id' => 143,
        ),
        432 => 
        array (
            'id' => 2535,
            'name' => 'Balti',
            'country_id' => 143,
        ),
        433 => 
        array (
            'id' => 2536,
            'name' => 'Tighina',
            'country_id' => 143,
        ),
        434 => 
        array (
            'id' => 2537,
            'name' => 'Briceni',
            'country_id' => 143,
        ),
        435 => 
        array (
            'id' => 2538,
            'name' => 'Basarabeasca',
            'country_id' => 143,
        ),
        436 => 
        array (
            'id' => 2539,
            'name' => 'Cahul',
            'country_id' => 143,
        ),
        437 => 
        array (
            'id' => 2540,
            'name' => 'Calarasi',
            'country_id' => 143,
        ),
        438 => 
        array (
            'id' => 2541,
            'name' => 'Cimislia',
            'country_id' => 143,
        ),
        439 => 
        array (
            'id' => 2542,
            'name' => 'Criuleni',
            'country_id' => 143,
        ),
        440 => 
        array (
            'id' => 2543,
            'name' => 'Causeni',
            'country_id' => 143,
        ),
        441 => 
        array (
            'id' => 2544,
            'name' => 'Cantemir',
            'country_id' => 143,
        ),
        442 => 
        array (
            'id' => 2545,
            'name' => 'Chisinau',
            'country_id' => 143,
        ),
        443 => 
        array (
            'id' => 2546,
            'name' => 'Donduseni',
            'country_id' => 143,
        ),
        444 => 
        array (
            'id' => 2547,
            'name' => 'Drochia',
            'country_id' => 143,
        ),
        445 => 
        array (
            'id' => 2548,
            'name' => 'Dubasari',
            'country_id' => 143,
        ),
        446 => 
        array (
            'id' => 2549,
            'name' => 'Edinet',
            'country_id' => 143,
        ),
        447 => 
        array (
            'id' => 2550,
            'name' => 'Falesti',
            'country_id' => 143,
        ),
        448 => 
        array (
            'id' => 2551,
            'name' => 'Floresti',
            'country_id' => 143,
        ),
        449 => 
        array (
            'id' => 2552,
            'name' => '"Gagauzia',
            'country_id' => 143,
        ),
        450 => 
        array (
            'id' => 2553,
            'name' => 'Glodeni',
            'country_id' => 143,
        ),
        451 => 
        array (
            'id' => 2554,
            'name' => 'Hîncesti',
            'country_id' => 143,
        ),
        452 => 
        array (
            'id' => 2555,
            'name' => 'Ialoveni',
            'country_id' => 143,
        ),
        453 => 
        array (
            'id' => 2556,
            'name' => 'Leova',
            'country_id' => 143,
        ),
        454 => 
        array (
            'id' => 2557,
            'name' => 'Nisporeni',
            'country_id' => 143,
        ),
        455 => 
        array (
            'id' => 2558,
            'name' => 'Ocniþa',
            'country_id' => 143,
        ),
        456 => 
        array (
            'id' => 2559,
            'name' => 'Orhei',
            'country_id' => 143,
        ),
        457 => 
        array (
            'id' => 2560,
            'name' => 'Rezina',
            'country_id' => 143,
        ),
        458 => 
        array (
            'id' => 2561,
            'name' => 'Rîscani',
            'country_id' => 143,
        ),
        459 => 
        array (
            'id' => 2562,
            'name' => 'Soldanesti',
            'country_id' => 143,
        ),
        460 => 
        array (
            'id' => 2563,
            'name' => 'Sîngerei',
            'country_id' => 143,
        ),
        461 => 
        array (
            'id' => 2564,
            'name' => '"Stînga Nistrului',
            'country_id' => 143,
        ),
        462 => 
        array (
            'id' => 2565,
            'name' => 'Soroca',
            'country_id' => 143,
        ),
        463 => 
        array (
            'id' => 2566,
            'name' => 'Straseni',
            'country_id' => 143,
        ),
        464 => 
        array (
            'id' => 2567,
            'name' => 'Stefan Voda',
            'country_id' => 143,
        ),
        465 => 
        array (
            'id' => 2568,
            'name' => 'Taraclia',
            'country_id' => 143,
        ),
        466 => 
        array (
            'id' => 2569,
            'name' => 'Telenesti',
            'country_id' => 143,
        ),
        467 => 
        array (
            'id' => 2570,
            'name' => 'Ungheni',
            'country_id' => 143,
        ),
        468 => 
        array (
            'id' => 2571,
            'name' => 'Andrijevica',
            'country_id' => 146,
        ),
        469 => 
        array (
            'id' => 2572,
            'name' => 'Bar',
            'country_id' => 146,
        ),
        470 => 
        array (
            'id' => 2573,
            'name' => 'Berane',
            'country_id' => 146,
        ),
        471 => 
        array (
            'id' => 2574,
            'name' => 'Bijelo Polje',
            'country_id' => 146,
        ),
        472 => 
        array (
            'id' => 2575,
            'name' => 'Budva',
            'country_id' => 146,
        ),
        473 => 
        array (
            'id' => 2576,
            'name' => 'Cetinje',
            'country_id' => 146,
        ),
        474 => 
        array (
            'id' => 2577,
            'name' => 'Danilovgrad',
            'country_id' => 146,
        ),
        475 => 
        array (
            'id' => 2578,
            'name' => 'Herceg-Novi',
            'country_id' => 146,
        ),
        476 => 
        array (
            'id' => 2579,
            'name' => 'Kolašin',
            'country_id' => 146,
        ),
        477 => 
        array (
            'id' => 2580,
            'name' => 'Kotor',
            'country_id' => 146,
        ),
        478 => 
        array (
            'id' => 2581,
            'name' => 'Mojkovac',
            'country_id' => 146,
        ),
        479 => 
        array (
            'id' => 2582,
            'name' => 'Nikšic´',
            'country_id' => 146,
        ),
        480 => 
        array (
            'id' => 2583,
            'name' => 'Plav',
            'country_id' => 146,
        ),
        481 => 
        array (
            'id' => 2584,
            'name' => 'Pljevlja',
            'country_id' => 146,
        ),
        482 => 
        array (
            'id' => 2585,
            'name' => 'Plužine',
            'country_id' => 146,
        ),
        483 => 
        array (
            'id' => 2586,
            'name' => 'Podgorica',
            'country_id' => 146,
        ),
        484 => 
        array (
            'id' => 2587,
            'name' => 'Rožaje',
            'country_id' => 146,
        ),
        485 => 
        array (
            'id' => 2588,
            'name' => 'Šavnik',
            'country_id' => 146,
        ),
        486 => 
        array (
            'id' => 2589,
            'name' => 'Tivat',
            'country_id' => 146,
        ),
        487 => 
        array (
            'id' => 2590,
            'name' => 'Ulcinj',
            'country_id' => 146,
        ),
        488 => 
        array (
            'id' => 2591,
            'name' => 'Žabljak',
            'country_id' => 146,
        ),
        489 => 
        array (
            'id' => 2592,
            'name' => 'Toamasina',
            'country_id' => 130,
        ),
        490 => 
        array (
            'id' => 2593,
            'name' => 'Antsiranana',
            'country_id' => 130,
        ),
        491 => 
        array (
            'id' => 2594,
            'name' => 'Fianarantsoa',
            'country_id' => 130,
        ),
        492 => 
        array (
            'id' => 2595,
            'name' => 'Mahajanga',
            'country_id' => 130,
        ),
        493 => 
        array (
            'id' => 2596,
            'name' => 'Antananarivo',
            'country_id' => 130,
        ),
        494 => 
        array (
            'id' => 2597,
            'name' => 'Toliara',
            'country_id' => 130,
        ),
        495 => 
        array (
            'id' => 2598,
            'name' => 'Ailuk',
            'country_id' => 136,
        ),
        496 => 
        array (
            'id' => 2599,
            'name' => 'Ailinglapalap',
            'country_id' => 136,
        ),
        497 => 
        array (
            'id' => 2600,
            'name' => 'Arno',
            'country_id' => 136,
        ),
        498 => 
        array (
            'id' => 2601,
            'name' => 'Aur',
            'country_id' => 136,
        ),
        499 => 
        array (
            'id' => 2602,
            'name' => 'Ebon',
            'country_id' => 136,
        ),
    ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 2603,
                'name' => 'Enewetak',
                'country_id' => 136,
            ),
            1 => 
            array (
                'id' => 2604,
                'name' => 'Jabat',
                'country_id' => 136,
            ),
            2 => 
            array (
                'id' => 2605,
                'name' => 'Jaluit',
                'country_id' => 136,
            ),
            3 => 
            array (
                'id' => 2606,
                'name' => 'Kili',
                'country_id' => 136,
            ),
            4 => 
            array (
                'id' => 2607,
                'name' => 'Kwajalein',
                'country_id' => 136,
            ),
            5 => 
            array (
                'id' => 2608,
                'name' => 'Ralik chain',
                'country_id' => 136,
            ),
            6 => 
            array (
                'id' => 2609,
                'name' => 'Lae',
                'country_id' => 136,
            ),
            7 => 
            array (
                'id' => 2610,
                'name' => 'Lib',
                'country_id' => 136,
            ),
            8 => 
            array (
                'id' => 2611,
                'name' => 'Likiep',
                'country_id' => 136,
            ),
            9 => 
            array (
                'id' => 2612,
                'name' => 'Majuro',
                'country_id' => 136,
            ),
            10 => 
            array (
                'id' => 2613,
                'name' => 'Maloelap',
                'country_id' => 136,
            ),
            11 => 
            array (
                'id' => 2614,
                'name' => 'Mejit',
                'country_id' => 136,
            ),
            12 => 
            array (
                'id' => 2615,
                'name' => 'Mili',
                'country_id' => 136,
            ),
            13 => 
            array (
                'id' => 2616,
                'name' => 'Namorik',
                'country_id' => 136,
            ),
            14 => 
            array (
                'id' => 2617,
                'name' => 'Namu',
                'country_id' => 136,
            ),
            15 => 
            array (
                'id' => 2618,
                'name' => 'Rongelap',
                'country_id' => 136,
            ),
            16 => 
            array (
                'id' => 2619,
                'name' => 'Ratak chain',
                'country_id' => 136,
            ),
            17 => 
            array (
                'id' => 2620,
                'name' => 'Ujae',
                'country_id' => 136,
            ),
            18 => 
            array (
                'id' => 2621,
                'name' => 'Utirik',
                'country_id' => 136,
            ),
            19 => 
            array (
                'id' => 2622,
                'name' => 'Wotho',
                'country_id' => 136,
            ),
            20 => 
            array (
                'id' => 2623,
                'name' => 'Wotje',
                'country_id' => 136,
            ),
            21 => 
            array (
                'id' => 2624,
                'name' => 'Aerodrom *',
                'country_id' => 129,
            ),
            22 => 
            array (
                'id' => 2625,
                'name' => 'Aracinovo',
                'country_id' => 129,
            ),
            23 => 
            array (
                'id' => 2626,
                'name' => 'Berovo',
                'country_id' => 129,
            ),
            24 => 
            array (
                'id' => 2627,
                'name' => 'Bitola',
                'country_id' => 129,
            ),
            25 => 
            array (
                'id' => 2628,
                'name' => 'Bogdanci',
                'country_id' => 129,
            ),
            26 => 
            array (
                'id' => 2629,
                'name' => 'Bogovinje',
                'country_id' => 129,
            ),
            27 => 
            array (
                'id' => 2630,
                'name' => 'Bosilovo',
                'country_id' => 129,
            ),
            28 => 
            array (
                'id' => 2631,
                'name' => 'Brvenica',
                'country_id' => 129,
            ),
            29 => 
            array (
                'id' => 2632,
                'name' => 'Butel *',
                'country_id' => 129,
            ),
            30 => 
            array (
                'id' => 2633,
                'name' => 'Valandovo',
                'country_id' => 129,
            ),
            31 => 
            array (
                'id' => 2634,
                'name' => 'Vasilevo',
                'country_id' => 129,
            ),
            32 => 
            array (
                'id' => 2635,
                'name' => 'Vevcani',
                'country_id' => 129,
            ),
            33 => 
            array (
                'id' => 2636,
                'name' => 'Veles',
                'country_id' => 129,
            ),
            34 => 
            array (
                'id' => 2637,
                'name' => 'Vinica',
                'country_id' => 129,
            ),
            35 => 
            array (
                'id' => 2638,
                'name' => 'Vraneštica',
                'country_id' => 129,
            ),
            36 => 
            array (
                'id' => 2639,
                'name' => 'Vrapcište',
                'country_id' => 129,
            ),
            37 => 
            array (
                'id' => 2640,
                'name' => 'Gazi Baba *',
                'country_id' => 129,
            ),
            38 => 
            array (
                'id' => 2641,
                'name' => 'Gevgelija',
                'country_id' => 129,
            ),
            39 => 
            array (
                'id' => 2642,
                'name' => 'Gostivar',
                'country_id' => 129,
            ),
            40 => 
            array (
                'id' => 2643,
                'name' => 'Gradsko',
                'country_id' => 129,
            ),
            41 => 
            array (
                'id' => 2644,
                'name' => 'Debar',
                'country_id' => 129,
            ),
            42 => 
            array (
                'id' => 2645,
                'name' => 'Debarca',
                'country_id' => 129,
            ),
            43 => 
            array (
                'id' => 2646,
                'name' => 'Delcevo',
                'country_id' => 129,
            ),
            44 => 
            array (
                'id' => 2647,
                'name' => 'Demir Kapija',
                'country_id' => 129,
            ),
            45 => 
            array (
                'id' => 2648,
                'name' => 'Demir Hisar',
                'country_id' => 129,
            ),
            46 => 
            array (
                'id' => 2649,
                'name' => 'Dojran',
                'country_id' => 129,
            ),
            47 => 
            array (
                'id' => 2650,
                'name' => 'Dolneni',
                'country_id' => 129,
            ),
            48 => 
            array (
                'id' => 2651,
                'name' => 'Drugovo',
                'country_id' => 129,
            ),
            49 => 
            array (
                'id' => 2652,
                'name' => 'Gjorce Petrov *',
                'country_id' => 129,
            ),
            50 => 
            array (
                'id' => 2653,
                'name' => 'Želino',
                'country_id' => 129,
            ),
            51 => 
            array (
                'id' => 2654,
                'name' => 'Zajas',
                'country_id' => 129,
            ),
            52 => 
            array (
                'id' => 2655,
                'name' => 'Zelenikovo',
                'country_id' => 129,
            ),
            53 => 
            array (
                'id' => 2656,
                'name' => 'Zrnovci',
                'country_id' => 129,
            ),
            54 => 
            array (
                'id' => 2657,
                'name' => 'Ilinden',
                'country_id' => 129,
            ),
            55 => 
            array (
                'id' => 2658,
                'name' => 'Jegunovce',
                'country_id' => 129,
            ),
            56 => 
            array (
                'id' => 2659,
                'name' => 'Kavadarci',
                'country_id' => 129,
            ),
            57 => 
            array (
                'id' => 2660,
                'name' => 'Karbinci',
                'country_id' => 129,
            ),
            58 => 
            array (
                'id' => 2661,
                'name' => 'Karpoš *',
                'country_id' => 129,
            ),
            59 => 
            array (
                'id' => 2662,
                'name' => 'Kisela Voda *',
                'country_id' => 129,
            ),
            60 => 
            array (
                'id' => 2663,
                'name' => 'Kicevo',
                'country_id' => 129,
            ),
            61 => 
            array (
                'id' => 2664,
                'name' => 'Konce',
                'country_id' => 129,
            ),
            62 => 
            array (
                'id' => 2665,
                'name' => 'Kocani',
                'country_id' => 129,
            ),
            63 => 
            array (
                'id' => 2666,
                'name' => 'Kratovo',
                'country_id' => 129,
            ),
            64 => 
            array (
                'id' => 2667,
                'name' => 'Kriva Palanka',
                'country_id' => 129,
            ),
            65 => 
            array (
                'id' => 2668,
                'name' => 'Krivogaštani',
                'country_id' => 129,
            ),
            66 => 
            array (
                'id' => 2669,
                'name' => 'Kruševo',
                'country_id' => 129,
            ),
            67 => 
            array (
                'id' => 2670,
                'name' => 'Kumanovo',
                'country_id' => 129,
            ),
            68 => 
            array (
                'id' => 2671,
                'name' => 'Lipkovo',
                'country_id' => 129,
            ),
            69 => 
            array (
                'id' => 2672,
                'name' => 'Lozovo',
                'country_id' => 129,
            ),
            70 => 
            array (
                'id' => 2673,
                'name' => 'Mavrovo-i-Rostuša',
                'country_id' => 129,
            ),
            71 => 
            array (
                'id' => 2674,
                'name' => 'Makedonska Kamenica',
                'country_id' => 129,
            ),
            72 => 
            array (
                'id' => 2675,
                'name' => 'Makedonski Brod',
                'country_id' => 129,
            ),
            73 => 
            array (
                'id' => 2676,
                'name' => 'Mogila',
                'country_id' => 129,
            ),
            74 => 
            array (
                'id' => 2677,
                'name' => 'Negotino',
                'country_id' => 129,
            ),
            75 => 
            array (
                'id' => 2678,
                'name' => 'Novaci',
                'country_id' => 129,
            ),
            76 => 
            array (
                'id' => 2679,
                'name' => 'Novo Selo',
                'country_id' => 129,
            ),
            77 => 
            array (
                'id' => 2680,
                'name' => 'Oslomej',
                'country_id' => 129,
            ),
            78 => 
            array (
                'id' => 2681,
                'name' => 'Ohrid',
                'country_id' => 129,
            ),
            79 => 
            array (
                'id' => 2682,
                'name' => 'Petrovec',
                'country_id' => 129,
            ),
            80 => 
            array (
                'id' => 2683,
                'name' => 'Pehcevo',
                'country_id' => 129,
            ),
            81 => 
            array (
                'id' => 2684,
                'name' => 'Plasnica',
                'country_id' => 129,
            ),
            82 => 
            array (
                'id' => 2685,
                'name' => 'Prilep',
                'country_id' => 129,
            ),
            83 => 
            array (
                'id' => 2686,
                'name' => 'Probištip',
                'country_id' => 129,
            ),
            84 => 
            array (
                'id' => 2687,
                'name' => 'Radoviš',
                'country_id' => 129,
            ),
            85 => 
            array (
                'id' => 2688,
                'name' => 'Rankovce',
                'country_id' => 129,
            ),
            86 => 
            array (
                'id' => 2689,
                'name' => 'Resen',
                'country_id' => 129,
            ),
            87 => 
            array (
                'id' => 2690,
                'name' => 'Rosoman',
                'country_id' => 129,
            ),
            88 => 
            array (
                'id' => 2691,
                'name' => 'Saraj *',
                'country_id' => 129,
            ),
            89 => 
            array (
                'id' => 2692,
                'name' => 'Sveti Nikole',
                'country_id' => 129,
            ),
            90 => 
            array (
                'id' => 2693,
                'name' => 'Sopište',
                'country_id' => 129,
            ),
            91 => 
            array (
                'id' => 2694,
                'name' => 'Staro Nagoricane',
                'country_id' => 129,
            ),
            92 => 
            array (
                'id' => 2695,
                'name' => 'Struga',
                'country_id' => 129,
            ),
            93 => 
            array (
                'id' => 2696,
                'name' => 'Strumica',
                'country_id' => 129,
            ),
            94 => 
            array (
                'id' => 2697,
                'name' => 'Studenicani',
                'country_id' => 129,
            ),
            95 => 
            array (
                'id' => 2698,
                'name' => 'Tearce',
                'country_id' => 129,
            ),
            96 => 
            array (
                'id' => 2699,
                'name' => 'Tetovo',
                'country_id' => 129,
            ),
            97 => 
            array (
                'id' => 2700,
                'name' => 'Centar *',
                'country_id' => 129,
            ),
            98 => 
            array (
                'id' => 2701,
                'name' => 'Centar Župa',
                'country_id' => 129,
            ),
            99 => 
            array (
                'id' => 2702,
                'name' => 'Cair *',
                'country_id' => 129,
            ),
            100 => 
            array (
                'id' => 2703,
                'name' => 'Caška',
                'country_id' => 129,
            ),
            101 => 
            array (
                'id' => 2704,
                'name' => 'Cešinovo-Obleševo',
                'country_id' => 129,
            ),
            102 => 
            array (
                'id' => 2705,
                'name' => 'Cucer Sandevo',
                'country_id' => 129,
            ),
            103 => 
            array (
                'id' => 2706,
                'name' => 'Štip',
                'country_id' => 129,
            ),
            104 => 
            array (
                'id' => 2707,
                'name' => 'Šuto Orizari *',
                'country_id' => 129,
            ),
            105 => 
            array (
                'id' => 2708,
                'name' => 'Kayes',
                'country_id' => 134,
            ),
            106 => 
            array (
                'id' => 2709,
                'name' => 'Koulikoro',
                'country_id' => 134,
            ),
            107 => 
            array (
                'id' => 2710,
                'name' => 'Sikasso',
                'country_id' => 134,
            ),
            108 => 
            array (
                'id' => 2711,
                'name' => 'Ségou',
                'country_id' => 134,
            ),
            109 => 
            array (
                'id' => 2712,
                'name' => 'Mopti',
                'country_id' => 134,
            ),
            110 => 
            array (
                'id' => 2713,
                'name' => 'Tombouctou',
                'country_id' => 134,
            ),
            111 => 
            array (
                'id' => 2714,
                'name' => 'Gao',
                'country_id' => 134,
            ),
            112 => 
            array (
                'id' => 2715,
                'name' => 'Kidal',
                'country_id' => 134,
            ),
            113 => 
            array (
                'id' => 2716,
                'name' => 'Bamako',
                'country_id' => 134,
            ),
            114 => 
            array (
                'id' => 2717,
                'name' => 'Sagaing',
                'country_id' => 150,
            ),
            115 => 
            array (
                'id' => 2718,
                'name' => 'Bago',
                'country_id' => 150,
            ),
            116 => 
            array (
                'id' => 2719,
                'name' => 'Magway',
                'country_id' => 150,
            ),
            117 => 
            array (
                'id' => 2720,
                'name' => 'Mandalay',
                'country_id' => 150,
            ),
            118 => 
            array (
                'id' => 2721,
                'name' => 'Tanintharyi',
                'country_id' => 150,
            ),
            119 => 
            array (
                'id' => 2722,
                'name' => 'Yangon',
                'country_id' => 150,
            ),
            120 => 
            array (
                'id' => 2723,
                'name' => 'Ayeyarwady',
                'country_id' => 150,
            ),
            121 => 
            array (
                'id' => 2724,
                'name' => 'Kachin',
                'country_id' => 150,
            ),
            122 => 
            array (
                'id' => 2725,
                'name' => 'Kayah',
                'country_id' => 150,
            ),
            123 => 
            array (
                'id' => 2726,
                'name' => 'Kayin',
                'country_id' => 150,
            ),
            124 => 
            array (
                'id' => 2727,
                'name' => 'Chin',
                'country_id' => 150,
            ),
            125 => 
            array (
                'id' => 2728,
                'name' => 'Mon',
                'country_id' => 150,
            ),
            126 => 
            array (
                'id' => 2729,
                'name' => 'Rakhine',
                'country_id' => 150,
            ),
            127 => 
            array (
                'id' => 2730,
                'name' => 'Shan',
                'country_id' => 150,
            ),
            128 => 
            array (
                'id' => 2731,
                'name' => 'Orhon',
                'country_id' => 145,
            ),
            129 => 
            array (
                'id' => 2732,
                'name' => 'Darhan uul',
                'country_id' => 145,
            ),
            130 => 
            array (
                'id' => 2733,
                'name' => 'Hentiy',
                'country_id' => 145,
            ),
            131 => 
            array (
                'id' => 2734,
                'name' => 'Hövagöl',
                'country_id' => 145,
            ),
            132 => 
            array (
                'id' => 2735,
                'name' => 'Hovd',
                'country_id' => 145,
            ),
            133 => 
            array (
                'id' => 2736,
                'name' => 'Uvs',
                'country_id' => 145,
            ),
            134 => 
            array (
                'id' => 2737,
                'name' => 'Töv',
                'country_id' => 145,
            ),
            135 => 
            array (
                'id' => 2738,
                'name' => 'Selenge',
                'country_id' => 145,
            ),
            136 => 
            array (
                'id' => 2739,
                'name' => 'Sühbaatar',
                'country_id' => 145,
            ),
            137 => 
            array (
                'id' => 2740,
                'name' => 'Ömnögovi',
                'country_id' => 145,
            ),
            138 => 
            array (
                'id' => 2741,
                'name' => 'Övörhangay',
                'country_id' => 145,
            ),
            139 => 
            array (
                'id' => 2742,
                'name' => 'Dzavhan',
                'country_id' => 145,
            ),
            140 => 
            array (
                'id' => 2743,
                'name' => 'Dundgovi',
                'country_id' => 145,
            ),
            141 => 
            array (
                'id' => 2744,
                'name' => 'Dornod',
                'country_id' => 145,
            ),
            142 => 
            array (
                'id' => 2745,
                'name' => 'Dornogovi',
                'country_id' => 145,
            ),
            143 => 
            array (
                'id' => 2746,
                'name' => 'Govi-Sümber',
                'country_id' => 145,
            ),
            144 => 
            array (
                'id' => 2747,
                'name' => 'Govi-Altay',
                'country_id' => 145,
            ),
            145 => 
            array (
                'id' => 2748,
                'name' => 'Bulgan',
                'country_id' => 145,
            ),
            146 => 
            array (
                'id' => 2749,
                'name' => 'Bayanhongor',
                'country_id' => 145,
            ),
            147 => 
            array (
                'id' => 2750,
                'name' => 'Bayan-Ölgiy',
                'country_id' => 145,
            ),
            148 => 
            array (
                'id' => 2751,
                'name' => 'Arhangay',
                'country_id' => 145,
            ),
            149 => 
            array (
                'id' => 2752,
                'name' => 'Ulaanbaatar',
                'country_id' => 145,
            ),
            150 => 
            array (
                'id' => 2755,
                'name' => 'Assaba',
                'country_id' => 138,
            ),
            151 => 
            array (
                'id' => 2765,
                'name' => 'Nouakchott',
                'country_id' => 138,
            ),
            152 => 
            array (
                'id' => 2766,
                'name' => 'Attard',
                'country_id' => 135,
            ),
            153 => 
            array (
                'id' => 2767,
                'name' => 'Balzan',
                'country_id' => 135,
            ),
            154 => 
            array (
                'id' => 2768,
                'name' => 'Birgu',
                'country_id' => 135,
            ),
            155 => 
            array (
                'id' => 2769,
                'name' => 'Birkirkara',
                'country_id' => 135,
            ),
            156 => 
            array (
                'id' => 2770,
                'name' => 'Birzebbuga',
                'country_id' => 135,
            ),
            157 => 
            array (
                'id' => 2771,
                'name' => 'Bormla',
                'country_id' => 135,
            ),
            158 => 
            array (
                'id' => 2772,
                'name' => 'Dingli',
                'country_id' => 135,
            ),
            159 => 
            array (
                'id' => 2773,
                'name' => 'Fgura',
                'country_id' => 135,
            ),
            160 => 
            array (
                'id' => 2774,
                'name' => 'Floriana',
                'country_id' => 135,
            ),
            161 => 
            array (
                'id' => 2775,
                'name' => 'Fontana',
                'country_id' => 135,
            ),
            162 => 
            array (
                'id' => 2776,
                'name' => 'Gudja',
                'country_id' => 135,
            ),
            163 => 
            array (
                'id' => 2777,
                'name' => 'Gzira',
                'country_id' => 135,
            ),
            164 => 
            array (
                'id' => 2778,
                'name' => 'Ghajnsielem',
                'country_id' => 135,
            ),
            165 => 
            array (
                'id' => 2779,
                'name' => 'Gharb',
                'country_id' => 135,
            ),
            166 => 
            array (
                'id' => 2780,
                'name' => 'Gharghur',
                'country_id' => 135,
            ),
            167 => 
            array (
                'id' => 2781,
                'name' => 'Ghasri',
                'country_id' => 135,
            ),
            168 => 
            array (
                'id' => 2782,
                'name' => 'Ghaxaq',
                'country_id' => 135,
            ),
            169 => 
            array (
                'id' => 2783,
                'name' => 'Hamrun',
                'country_id' => 135,
            ),
            170 => 
            array (
                'id' => 2784,
                'name' => 'Iklin',
                'country_id' => 135,
            ),
            171 => 
            array (
                'id' => 2785,
                'name' => 'Isla',
                'country_id' => 135,
            ),
            172 => 
            array (
                'id' => 2786,
                'name' => 'Kalkara',
                'country_id' => 135,
            ),
            173 => 
            array (
                'id' => 2787,
                'name' => 'Kercem',
                'country_id' => 135,
            ),
            174 => 
            array (
                'id' => 2788,
                'name' => 'Kirkop',
                'country_id' => 135,
            ),
            175 => 
            array (
                'id' => 2789,
                'name' => 'Lija',
                'country_id' => 135,
            ),
            176 => 
            array (
                'id' => 2790,
                'name' => 'Luqa',
                'country_id' => 135,
            ),
            177 => 
            array (
                'id' => 2791,
                'name' => 'Marsa',
                'country_id' => 135,
            ),
            178 => 
            array (
                'id' => 2792,
                'name' => 'Marsaskala',
                'country_id' => 135,
            ),
            179 => 
            array (
                'id' => 2793,
                'name' => 'Marsaxlokk',
                'country_id' => 135,
            ),
            180 => 
            array (
                'id' => 2794,
                'name' => 'Mdina',
                'country_id' => 135,
            ),
            181 => 
            array (
                'id' => 2795,
                'name' => 'Mellieha',
                'country_id' => 135,
            ),
            182 => 
            array (
                'id' => 2796,
                'name' => 'Mgarr',
                'country_id' => 135,
            ),
            183 => 
            array (
                'id' => 2797,
                'name' => 'Mosta',
                'country_id' => 135,
            ),
            184 => 
            array (
                'id' => 2798,
                'name' => 'Mqabba',
                'country_id' => 135,
            ),
            185 => 
            array (
                'id' => 2799,
                'name' => 'Msida',
                'country_id' => 135,
            ),
            186 => 
            array (
                'id' => 2800,
                'name' => 'Mtarfa',
                'country_id' => 135,
            ),
            187 => 
            array (
                'id' => 2801,
                'name' => 'Munxar',
                'country_id' => 135,
            ),
            188 => 
            array (
                'id' => 2802,
                'name' => 'Nadur',
                'country_id' => 135,
            ),
            189 => 
            array (
                'id' => 2803,
                'name' => 'Naxxar',
                'country_id' => 135,
            ),
            190 => 
            array (
                'id' => 2804,
                'name' => 'Paola',
                'country_id' => 135,
            ),
            191 => 
            array (
                'id' => 2805,
                'name' => 'Pembroke',
                'country_id' => 135,
            ),
            192 => 
            array (
                'id' => 2806,
                'name' => 'Pietà',
                'country_id' => 135,
            ),
            193 => 
            array (
                'id' => 2807,
                'name' => 'Qala',
                'country_id' => 135,
            ),
            194 => 
            array (
                'id' => 2808,
                'name' => 'Qormi',
                'country_id' => 135,
            ),
            195 => 
            array (
                'id' => 2809,
                'name' => 'Qrendi',
                'country_id' => 135,
            ),
            196 => 
            array (
                'id' => 2810,
                'name' => 'Rabat Gozo',
                'country_id' => 135,
            ),
            197 => 
            array (
                'id' => 2811,
                'name' => 'Rabat Malta',
                'country_id' => 135,
            ),
            198 => 
            array (
                'id' => 2812,
                'name' => 'Safi',
                'country_id' => 135,
            ),
            199 => 
            array (
                'id' => 2813,
                'name' => 'Saint Julian\'s',
                'country_id' => 135,
            ),
            200 => 
            array (
                'id' => 2814,
                'name' => 'Saint John',
                'country_id' => 135,
            ),
            201 => 
            array (
                'id' => 2815,
                'name' => 'Saint Lawrence',
                'country_id' => 135,
            ),
            202 => 
            array (
                'id' => 2816,
                'name' => 'Saint Paul\'s Bay',
                'country_id' => 135,
            ),
            203 => 
            array (
                'id' => 2817,
                'name' => 'Sannat',
                'country_id' => 135,
            ),
            204 => 
            array (
                'id' => 2818,
                'name' => 'Saint Lucia\'s',
                'country_id' => 135,
            ),
            205 => 
            array (
                'id' => 2819,
                'name' => 'Santa Venera',
                'country_id' => 135,
            ),
            206 => 
            array (
                'id' => 2820,
                'name' => 'Siggiewi',
                'country_id' => 135,
            ),
            207 => 
            array (
                'id' => 2821,
                'name' => 'Sliema',
                'country_id' => 135,
            ),
            208 => 
            array (
                'id' => 2822,
                'name' => 'Swieqi',
                'country_id' => 135,
            ),
            209 => 
            array (
                'id' => 2823,
                'name' => 'Ta\' Xbiex',
                'country_id' => 135,
            ),
            210 => 
            array (
                'id' => 2824,
                'name' => 'Tarxien',
                'country_id' => 135,
            ),
            211 => 
            array (
                'id' => 2825,
                'name' => 'Valletta',
                'country_id' => 135,
            ),
            212 => 
            array (
                'id' => 2826,
                'name' => 'Xaghra',
                'country_id' => 135,
            ),
            213 => 
            array (
                'id' => 2827,
                'name' => 'Xewkija',
                'country_id' => 135,
            ),
            214 => 
            array (
                'id' => 2828,
                'name' => 'Xghajra',
                'country_id' => 135,
            ),
            215 => 
            array (
                'id' => 2829,
                'name' => 'Zabbar',
                'country_id' => 135,
            ),
            216 => 
            array (
                'id' => 2830,
                'name' => 'Zebbug Gozo',
                'country_id' => 135,
            ),
            217 => 
            array (
                'id' => 2831,
                'name' => 'Zebbug Malta',
                'country_id' => 135,
            ),
            218 => 
            array (
                'id' => 2832,
                'name' => 'Zejtun',
                'country_id' => 135,
            ),
            219 => 
            array (
                'id' => 2833,
                'name' => 'Zurrieq',
                'country_id' => 135,
            ),
            220 => 
            array (
                'id' => 2834,
                'name' => 'Agalega Islands',
                'country_id' => 139,
            ),
            221 => 
            array (
                'id' => 2835,
                'name' => 'Black River',
                'country_id' => 139,
            ),
            222 => 
            array (
                'id' => 2836,
                'name' => 'Beau Bassin-Rose Hill',
                'country_id' => 139,
            ),
            223 => 
            array (
                'id' => 2837,
                'name' => 'Cargados Carajos Shoals [Saint Brandon Islands]',
                'country_id' => 139,
            ),
            224 => 
            array (
                'id' => 2838,
                'name' => 'Curepipe',
                'country_id' => 139,
            ),
            225 => 
            array (
                'id' => 2839,
                'name' => 'Flacq',
                'country_id' => 139,
            ),
            226 => 
            array (
                'id' => 2840,
                'name' => 'Grand Port',
                'country_id' => 139,
            ),
            227 => 
            array (
                'id' => 2841,
                'name' => 'Moka',
                'country_id' => 139,
            ),
            228 => 
            array (
                'id' => 2842,
                'name' => 'Pamplemousses',
                'country_id' => 139,
            ),
            229 => 
            array (
                'id' => 2843,
                'name' => 'Port Louis',
                'country_id' => 139,
            ),
            230 => 
            array (
                'id' => 2844,
                'name' => 'Plaines wilhems',
                'country_id' => 139,
            ),
            231 => 
            array (
                'id' => 2845,
                'name' => 'Quatre Bornes',
                'country_id' => 139,
            ),
            232 => 
            array (
                'id' => 2846,
                'name' => 'Rodrigues Island',
                'country_id' => 139,
            ),
            233 => 
            array (
                'id' => 2847,
                'name' => 'Rivière du Rempart',
                'country_id' => 139,
            ),
            234 => 
            array (
                'id' => 2848,
                'name' => 'Savanne',
                'country_id' => 139,
            ),
            235 => 
            array (
                'id' => 2849,
                'name' => 'Vacoas-Phoenix',
                'country_id' => 139,
            ),
            236 => 
            array (
                'id' => 2850,
                'name' => 'Alifu Dhaalu',
                'country_id' => 133,
            ),
            237 => 
            array (
                'id' => 2851,
                'name' => 'Seenu',
                'country_id' => 133,
            ),
            238 => 
            array (
                'id' => 2852,
                'name' => 'Alifu Alifu',
                'country_id' => 133,
            ),
            239 => 
            array (
                'id' => 2853,
                'name' => 'Lhaviyani',
                'country_id' => 133,
            ),
            240 => 
            array (
                'id' => 2854,
                'name' => 'Vaavu',
                'country_id' => 133,
            ),
            241 => 
            array (
                'id' => 2855,
                'name' => 'Laamu',
                'country_id' => 133,
            ),
            242 => 
            array (
                'id' => 2856,
                'name' => 'Haa Alif',
                'country_id' => 133,
            ),
            243 => 
            array (
                'id' => 2857,
                'name' => 'Thaa',
                'country_id' => 133,
            ),
            244 => 
            array (
                'id' => 2858,
                'name' => 'Meemu',
                'country_id' => 133,
            ),
            245 => 
            array (
                'id' => 2859,
                'name' => 'Raa',
                'country_id' => 133,
            ),
            246 => 
            array (
                'id' => 2860,
                'name' => 'Faafu',
                'country_id' => 133,
            ),
            247 => 
            array (
                'id' => 2861,
                'name' => 'Dhaalu',
                'country_id' => 133,
            ),
            248 => 
            array (
                'id' => 2862,
                'name' => 'Baa',
                'country_id' => 133,
            ),
            249 => 
            array (
                'id' => 2863,
                'name' => 'Haa Dhaalu',
                'country_id' => 133,
            ),
            250 => 
            array (
                'id' => 2864,
                'name' => 'Shaviyani',
                'country_id' => 133,
            ),
            251 => 
            array (
                'id' => 2865,
                'name' => 'Noonu',
                'country_id' => 133,
            ),
            252 => 
            array (
                'id' => 2866,
                'name' => 'Kaafu',
                'country_id' => 133,
            ),
            253 => 
            array (
                'id' => 2867,
                'name' => 'Gaafu Alifu',
                'country_id' => 133,
            ),
            254 => 
            array (
                'id' => 2868,
                'name' => 'Gaafu Dhaalu',
                'country_id' => 133,
            ),
            255 => 
            array (
                'id' => 2869,
                'name' => 'Gnaviyani',
                'country_id' => 133,
            ),
            256 => 
            array (
                'id' => 2870,
                'name' => 'Medhu',
                'country_id' => 133,
            ),
            257 => 
            array (
                'id' => 2871,
                'name' => 'Male',
                'country_id' => 133,
            ),
            258 => 
            array (
                'id' => 2872,
                'name' => 'Medhu-Uthuru',
                'country_id' => 133,
            ),
            259 => 
            array (
                'id' => 2873,
                'name' => 'Uthuru',
                'country_id' => 133,
            ),
            260 => 
            array (
                'id' => 2874,
                'name' => 'Medhu-Dhekunu',
                'country_id' => 133,
            ),
            261 => 
            array (
                'id' => 2875,
                'name' => 'Dhekunu',
                'country_id' => 133,
            ),
            262 => 
            array (
                'id' => 2876,
                'name' => 'Mathi-Uthuru',
                'country_id' => 133,
            ),
            263 => 
            array (
                'id' => 2877,
                'name' => 'Mathi-Dhekunu',
                'country_id' => 133,
            ),
            264 => 
            array (
                'id' => 2878,
                'name' => 'Balaka',
                'country_id' => 131,
            ),
            265 => 
            array (
                'id' => 2879,
                'name' => 'Blantyre',
                'country_id' => 131,
            ),
            266 => 
            array (
                'id' => 2880,
                'name' => 'Central',
                'country_id' => 131,
            ),
            267 => 
            array (
                'id' => 2881,
                'name' => 'Chikwawa',
                'country_id' => 131,
            ),
            268 => 
            array (
                'id' => 2882,
                'name' => 'Chiradzulu',
                'country_id' => 131,
            ),
            269 => 
            array (
                'id' => 2883,
                'name' => 'Chitipa',
                'country_id' => 131,
            ),
            270 => 
            array (
                'id' => 2884,
                'name' => 'Dedza',
                'country_id' => 131,
            ),
            271 => 
            array (
                'id' => 2885,
                'name' => 'Dowa',
                'country_id' => 131,
            ),
            272 => 
            array (
                'id' => 2886,
                'name' => 'Karonga',
                'country_id' => 131,
            ),
            273 => 
            array (
                'id' => 2887,
                'name' => 'Kasungu',
                'country_id' => 131,
            ),
            274 => 
            array (
                'id' => 2888,
                'name' => 'Lilongwe',
                'country_id' => 131,
            ),
            275 => 
            array (
                'id' => 2889,
                'name' => 'Likoma',
                'country_id' => 131,
            ),
            276 => 
            array (
                'id' => 2890,
                'name' => 'Mchinji',
                'country_id' => 131,
            ),
            277 => 
            array (
                'id' => 2891,
                'name' => 'Mangochi',
                'country_id' => 131,
            ),
            278 => 
            array (
                'id' => 2892,
                'name' => 'Machinga',
                'country_id' => 131,
            ),
            279 => 
            array (
                'id' => 2893,
                'name' => 'Mulanje',
                'country_id' => 131,
            ),
            280 => 
            array (
                'id' => 2894,
                'name' => 'Mwanza',
                'country_id' => 131,
            ),
            281 => 
            array (
                'id' => 2895,
                'name' => 'Mzimba',
                'country_id' => 131,
            ),
            282 => 
            array (
                'id' => 2896,
                'name' => 'Northern',
                'country_id' => 131,
            ),
            283 => 
            array (
                'id' => 2897,
                'name' => 'Nkhata Bay',
                'country_id' => 131,
            ),
            284 => 
            array (
                'id' => 2898,
                'name' => 'Neno',
                'country_id' => 131,
            ),
            285 => 
            array (
                'id' => 2899,
                'name' => 'Ntchisi',
                'country_id' => 131,
            ),
            286 => 
            array (
                'id' => 2900,
                'name' => 'Nkhotakota',
                'country_id' => 131,
            ),
            287 => 
            array (
                'id' => 2901,
                'name' => 'Nsanje',
                'country_id' => 131,
            ),
            288 => 
            array (
                'id' => 2902,
                'name' => 'Ntcheu',
                'country_id' => 131,
            ),
            289 => 
            array (
                'id' => 2903,
                'name' => 'Phalombe',
                'country_id' => 131,
            ),
            290 => 
            array (
                'id' => 2904,
                'name' => 'Rumphi',
                'country_id' => 131,
            ),
            291 => 
            array (
                'id' => 2905,
                'name' => 'Southern',
                'country_id' => 131,
            ),
            292 => 
            array (
                'id' => 2906,
                'name' => 'Salima',
                'country_id' => 131,
            ),
            293 => 
            array (
                'id' => 2907,
                'name' => 'Thyolo',
                'country_id' => 131,
            ),
            294 => 
            array (
                'id' => 2908,
                'name' => 'Zomba',
                'country_id' => 131,
            ),
            295 => 
            array (
                'id' => 2909,
                'name' => 'Aguascalientes',
                'country_id' => 141,
            ),
            296 => 
            array (
                'id' => 2910,
                'name' => 'Baja California',
                'country_id' => 141,
            ),
            297 => 
            array (
                'id' => 2911,
                'name' => 'Baja California Sur',
                'country_id' => 141,
            ),
            298 => 
            array (
                'id' => 2912,
                'name' => 'Campeche',
                'country_id' => 141,
            ),
            299 => 
            array (
                'id' => 2913,
                'name' => 'Chihuahua',
                'country_id' => 141,
            ),
            300 => 
            array (
                'id' => 2914,
                'name' => 'Chiapas',
                'country_id' => 141,
            ),
            301 => 
            array (
                'id' => 2915,
                'name' => 'Coahuila',
                'country_id' => 141,
            ),
            302 => 
            array (
                'id' => 2916,
                'name' => 'Colima',
                'country_id' => 141,
            ),
            303 => 
            array (
                'id' => 2917,
                'name' => 'Distrito Federal',
                'country_id' => 141,
            ),
            304 => 
            array (
                'id' => 2918,
                'name' => 'Durango',
                'country_id' => 141,
            ),
            305 => 
            array (
                'id' => 2919,
                'name' => 'Guerrero',
                'country_id' => 141,
            ),
            306 => 
            array (
                'id' => 2920,
                'name' => 'Guanajuato',
                'country_id' => 141,
            ),
            307 => 
            array (
                'id' => 2921,
                'name' => 'Hidalgo',
                'country_id' => 141,
            ),
            308 => 
            array (
                'id' => 2922,
                'name' => 'Jalisco',
                'country_id' => 141,
            ),
            309 => 
            array (
                'id' => 2923,
                'name' => 'México',
                'country_id' => 141,
            ),
            310 => 
            array (
                'id' => 2924,
                'name' => 'Michoacán',
                'country_id' => 141,
            ),
            311 => 
            array (
                'id' => 2925,
                'name' => 'Morelos',
                'country_id' => 141,
            ),
            312 => 
            array (
                'id' => 2926,
                'name' => 'Nayarit',
                'country_id' => 141,
            ),
            313 => 
            array (
                'id' => 2927,
                'name' => 'Nuevo León',
                'country_id' => 141,
            ),
            314 => 
            array (
                'id' => 2928,
                'name' => 'Oaxaca',
                'country_id' => 141,
            ),
            315 => 
            array (
                'id' => 2929,
                'name' => 'Puebla',
                'country_id' => 141,
            ),
            316 => 
            array (
                'id' => 2930,
                'name' => 'Querétaro',
                'country_id' => 141,
            ),
            317 => 
            array (
                'id' => 2931,
                'name' => 'Quintana Roo',
                'country_id' => 141,
            ),
            318 => 
            array (
                'id' => 2932,
                'name' => 'Sinaloa',
                'country_id' => 141,
            ),
            319 => 
            array (
                'id' => 2933,
                'name' => 'San Luis Potosí',
                'country_id' => 141,
            ),
            320 => 
            array (
                'id' => 2934,
                'name' => 'Sonora',
                'country_id' => 141,
            ),
            321 => 
            array (
                'id' => 2935,
                'name' => 'Tabasco',
                'country_id' => 141,
            ),
            322 => 
            array (
                'id' => 2936,
                'name' => 'Tamaulipas',
                'country_id' => 141,
            ),
            323 => 
            array (
                'id' => 2937,
                'name' => 'Tlaxcala',
                'country_id' => 141,
            ),
            324 => 
            array (
                'id' => 2938,
                'name' => 'Veracruz',
                'country_id' => 141,
            ),
            325 => 
            array (
                'id' => 2939,
                'name' => 'Yucatán',
                'country_id' => 141,
            ),
            326 => 
            array (
                'id' => 2940,
                'name' => 'Zacatecas',
                'country_id' => 141,
            ),
            327 => 
            array (
                'id' => 2941,
                'name' => 'Johor',
                'country_id' => 132,
            ),
            328 => 
            array (
                'id' => 2942,
                'name' => 'Kedah',
                'country_id' => 132,
            ),
            329 => 
            array (
                'id' => 2943,
                'name' => 'Kelantan',
                'country_id' => 132,
            ),
            330 => 
            array (
                'id' => 2944,
                'name' => 'Melaka',
                'country_id' => 132,
            ),
            331 => 
            array (
                'id' => 2945,
                'name' => 'Negeri Sembilan',
                'country_id' => 132,
            ),
            332 => 
            array (
                'id' => 2946,
                'name' => 'Pahang',
                'country_id' => 132,
            ),
            333 => 
            array (
                'id' => 2947,
                'name' => 'Pulau Pinang',
                'country_id' => 132,
            ),
            334 => 
            array (
                'id' => 2948,
                'name' => 'Perak',
                'country_id' => 132,
            ),
            335 => 
            array (
                'id' => 2949,
                'name' => 'Perlis',
                'country_id' => 132,
            ),
            336 => 
            array (
                'id' => 2950,
                'name' => 'Selangor',
                'country_id' => 132,
            ),
            337 => 
            array (
                'id' => 2951,
                'name' => 'Terengganu',
                'country_id' => 132,
            ),
            338 => 
            array (
                'id' => 2952,
                'name' => 'Sabah',
                'country_id' => 132,
            ),
            339 => 
            array (
                'id' => 2953,
                'name' => 'Sarawak',
                'country_id' => 132,
            ),
            340 => 
            array (
                'id' => 2954,
                'name' => 'Wilayah Persekutuan Kuala Lumpur',
                'country_id' => 132,
            ),
            341 => 
            array (
                'id' => 2955,
                'name' => 'Wilayah Persekutuan Labuan',
                'country_id' => 132,
            ),
            342 => 
            array (
                'id' => 2956,
                'name' => 'Wilayah Persekutuan Putrajaya',
                'country_id' => 132,
            ),
            343 => 
            array (
                'id' => 2957,
                'name' => 'Niaosa',
                'country_id' => 149,
            ),
            344 => 
            array (
                'id' => 2958,
                'name' => 'Manica',
                'country_id' => 149,
            ),
            345 => 
            array (
                'id' => 2959,
                'name' => 'Gaza',
                'country_id' => 149,
            ),
            346 => 
            array (
                'id' => 2960,
                'name' => 'Inhambane',
                'country_id' => 149,
            ),
            347 => 
            array (
                'id' => 2961,
                'name' => 'Maputo',
                'country_id' => 149,
            ),
            348 => 
            array (
                'id' => 2962,
                'name' => 'Nampula',
                'country_id' => 149,
            ),
            349 => 
            array (
                'id' => 2963,
                'name' => 'Cabo Delgado',
                'country_id' => 149,
            ),
            350 => 
            array (
                'id' => 2964,
                'name' => 'Zambézia',
                'country_id' => 149,
            ),
            351 => 
            array (
                'id' => 2965,
                'name' => 'Sofala',
                'country_id' => 149,
            ),
            352 => 
            array (
                'id' => 2966,
                'name' => 'Tete',
                'country_id' => 149,
            ),
            353 => 
            array (
                'id' => 2967,
                'name' => 'Caprivi',
                'country_id' => 151,
            ),
            354 => 
            array (
                'id' => 2968,
                'name' => 'Erongo',
                'country_id' => 151,
            ),
            355 => 
            array (
                'id' => 2969,
                'name' => 'Hardap',
                'country_id' => 151,
            ),
            356 => 
            array (
                'id' => 2970,
                'name' => 'Karas',
                'country_id' => 151,
            ),
            357 => 
            array (
                'id' => 2971,
                'name' => 'Khomas',
                'country_id' => 151,
            ),
            358 => 
            array (
                'id' => 2972,
                'name' => 'Kunene',
                'country_id' => 151,
            ),
            359 => 
            array (
                'id' => 2973,
                'name' => 'Otjozondjupa',
                'country_id' => 151,
            ),
            360 => 
            array (
                'id' => 2974,
                'name' => 'Omaheke',
                'country_id' => 151,
            ),
            361 => 
            array (
                'id' => 2975,
                'name' => 'Okavango',
                'country_id' => 151,
            ),
            362 => 
            array (
                'id' => 2976,
                'name' => 'Oshana',
                'country_id' => 151,
            ),
            363 => 
            array (
                'id' => 2977,
                'name' => 'Omusati',
                'country_id' => 151,
            ),
            364 => 
            array (
                'id' => 2978,
                'name' => 'Oshikoto',
                'country_id' => 151,
            ),
            365 => 
            array (
                'id' => 2979,
                'name' => 'Ohangwena',
                'country_id' => 151,
            ),
            366 => 
            array (
                'id' => 2980,
                'name' => 'Agadez',
                'country_id' => 159,
            ),
            367 => 
            array (
                'id' => 2981,
                'name' => 'Diffa',
                'country_id' => 159,
            ),
            368 => 
            array (
                'id' => 2982,
                'name' => 'Dosso',
                'country_id' => 159,
            ),
            369 => 
            array (
                'id' => 2983,
                'name' => 'Maradi',
                'country_id' => 159,
            ),
            370 => 
            array (
                'id' => 2984,
                'name' => 'Tahoua',
                'country_id' => 159,
            ),
            371 => 
            array (
                'id' => 2985,
                'name' => 'Tillabéri',
                'country_id' => 159,
            ),
            372 => 
            array (
                'id' => 2986,
                'name' => 'Zinder',
                'country_id' => 159,
            ),
            373 => 
            array (
                'id' => 2987,
                'name' => 'Niamey',
                'country_id' => 159,
            ),
            374 => 
            array (
                'id' => 2988,
                'name' => 'Abia',
                'country_id' => 160,
            ),
            375 => 
            array (
                'id' => 2989,
                'name' => 'Adamawa',
                'country_id' => 160,
            ),
            376 => 
            array (
                'id' => 2990,
                'name' => 'Akwa Ibom',
                'country_id' => 160,
            ),
            377 => 
            array (
                'id' => 2991,
                'name' => 'Anambra',
                'country_id' => 160,
            ),
            378 => 
            array (
                'id' => 2992,
                'name' => 'Bauchi',
                'country_id' => 160,
            ),
            379 => 
            array (
                'id' => 2993,
                'name' => 'Benue',
                'country_id' => 160,
            ),
            380 => 
            array (
                'id' => 2994,
                'name' => 'Borno',
                'country_id' => 160,
            ),
            381 => 
            array (
                'id' => 2995,
                'name' => 'Bayelsa',
                'country_id' => 160,
            ),
            382 => 
            array (
                'id' => 2996,
                'name' => 'Cross River',
                'country_id' => 160,
            ),
            383 => 
            array (
                'id' => 2997,
                'name' => 'Delta',
                'country_id' => 160,
            ),
            384 => 
            array (
                'id' => 2998,
                'name' => 'Ebonyi',
                'country_id' => 160,
            ),
            385 => 
            array (
                'id' => 2999,
                'name' => 'Edo',
                'country_id' => 160,
            ),
            386 => 
            array (
                'id' => 3000,
                'name' => 'Ekiti',
                'country_id' => 160,
            ),
            387 => 
            array (
                'id' => 3001,
                'name' => 'Enugu',
                'country_id' => 160,
            ),
            388 => 
            array (
                'id' => 3002,
                'name' => 'Abuja Capital Territory',
                'country_id' => 160,
            ),
            389 => 
            array (
                'id' => 3003,
                'name' => 'Gombe',
                'country_id' => 160,
            ),
            390 => 
            array (
                'id' => 3004,
                'name' => 'Imo',
                'country_id' => 160,
            ),
            391 => 
            array (
                'id' => 3005,
                'name' => 'Jigawa',
                'country_id' => 160,
            ),
            392 => 
            array (
                'id' => 3006,
                'name' => 'Kaduna',
                'country_id' => 160,
            ),
            393 => 
            array (
                'id' => 3007,
                'name' => 'Kebbi',
                'country_id' => 160,
            ),
            394 => 
            array (
                'id' => 3008,
                'name' => 'Kano',
                'country_id' => 160,
            ),
            395 => 
            array (
                'id' => 3009,
                'name' => 'Kogi',
                'country_id' => 160,
            ),
            396 => 
            array (
                'id' => 3010,
                'name' => 'Katsina',
                'country_id' => 160,
            ),
            397 => 
            array (
                'id' => 3011,
                'name' => 'Kwara',
                'country_id' => 160,
            ),
            398 => 
            array (
                'id' => 3012,
                'name' => 'Lagos',
                'country_id' => 160,
            ),
            399 => 
            array (
                'id' => 3013,
                'name' => 'Nassarawa',
                'country_id' => 160,
            ),
            400 => 
            array (
                'id' => 3014,
                'name' => 'Niger',
                'country_id' => 160,
            ),
            401 => 
            array (
                'id' => 3015,
                'name' => 'Ogun',
                'country_id' => 160,
            ),
            402 => 
            array (
                'id' => 3016,
                'name' => 'Ondo',
                'country_id' => 160,
            ),
            403 => 
            array (
                'id' => 3017,
                'name' => 'Osun',
                'country_id' => 160,
            ),
            404 => 
            array (
                'id' => 3018,
                'name' => 'Oyo',
                'country_id' => 160,
            ),
            405 => 
            array (
                'id' => 3019,
                'name' => 'Plateau',
                'country_id' => 160,
            ),
            406 => 
            array (
                'id' => 3020,
                'name' => 'Rivers',
                'country_id' => 160,
            ),
            407 => 
            array (
                'id' => 3021,
                'name' => 'Sokoto',
                'country_id' => 160,
            ),
            408 => 
            array (
                'id' => 3022,
                'name' => 'Taraba',
                'country_id' => 160,
            ),
            409 => 
            array (
                'id' => 3023,
                'name' => 'Yobe',
                'country_id' => 160,
            ),
            410 => 
            array (
                'id' => 3024,
                'name' => 'Zamfara',
                'country_id' => 160,
            ),
            411 => 
            array (
                'id' => 3025,
                'name' => 'Atlántico Norte',
                'country_id' => 158,
            ),
            412 => 
            array (
                'id' => 3026,
                'name' => 'Atlántico Sur',
                'country_id' => 158,
            ),
            413 => 
            array (
                'id' => 3027,
                'name' => 'Boaco',
                'country_id' => 158,
            ),
            414 => 
            array (
                'id' => 3028,
                'name' => 'Carazo',
                'country_id' => 158,
            ),
            415 => 
            array (
                'id' => 3029,
                'name' => 'Chinandega',
                'country_id' => 158,
            ),
            416 => 
            array (
                'id' => 3030,
                'name' => 'Chontales',
                'country_id' => 158,
            ),
            417 => 
            array (
                'id' => 3031,
                'name' => 'Estelí',
                'country_id' => 158,
            ),
            418 => 
            array (
                'id' => 3032,
                'name' => 'Granada',
                'country_id' => 158,
            ),
            419 => 
            array (
                'id' => 3033,
                'name' => 'Jinotega',
                'country_id' => 158,
            ),
            420 => 
            array (
                'id' => 3034,
                'name' => 'León',
                'country_id' => 158,
            ),
            421 => 
            array (
                'id' => 3035,
                'name' => 'Madriz',
                'country_id' => 158,
            ),
            422 => 
            array (
                'id' => 3036,
                'name' => 'Managua',
                'country_id' => 158,
            ),
            423 => 
            array (
                'id' => 3037,
                'name' => 'Masaya',
                'country_id' => 158,
            ),
            424 => 
            array (
                'id' => 3038,
                'name' => 'Matagalpa',
                'country_id' => 158,
            ),
            425 => 
            array (
                'id' => 3039,
                'name' => 'Nueva Segovia',
                'country_id' => 158,
            ),
            426 => 
            array (
                'id' => 3040,
                'name' => 'Rivas',
                'country_id' => 158,
            ),
            427 => 
            array (
                'id' => 3041,
                'name' => 'Río San Juan',
                'country_id' => 158,
            ),
            428 => 
            array (
                'id' => 3042,
                'name' => 'Drenthe',
                'country_id' => 154,
            ),
            429 => 
            array (
                'id' => 3043,
                'name' => 'Flevoland',
                'country_id' => 154,
            ),
            430 => 
            array (
                'id' => 3044,
                'name' => 'Fryslân',
                'country_id' => 154,
            ),
            431 => 
            array (
                'id' => 3045,
                'name' => 'Gelderland',
                'country_id' => 154,
            ),
            432 => 
            array (
                'id' => 3046,
                'name' => 'Groningen',
                'country_id' => 154,
            ),
            433 => 
            array (
                'id' => 3047,
                'name' => 'Limburg',
                'country_id' => 154,
            ),
            434 => 
            array (
                'id' => 3048,
                'name' => 'Noord-Brabant',
                'country_id' => 154,
            ),
            435 => 
            array (
                'id' => 3049,
                'name' => 'Noord-Holland',
                'country_id' => 154,
            ),
            436 => 
            array (
                'id' => 3050,
                'name' => 'Overijssel',
                'country_id' => 154,
            ),
            437 => 
            array (
                'id' => 3051,
                'name' => 'Utrecht',
                'country_id' => 154,
            ),
            438 => 
            array (
                'id' => 3052,
                'name' => 'Zeeland',
                'country_id' => 154,
            ),
            439 => 
            array (
                'id' => 3053,
                'name' => 'Zuid-Holland',
                'country_id' => 154,
            ),
            440 => 
            array (
                'id' => 3054,
                'name' => 'Østfold',
                'country_id' => 164,
            ),
            441 => 
            array (
                'id' => 3055,
                'name' => 'Akershus',
                'country_id' => 164,
            ),
            442 => 
            array (
                'id' => 3056,
                'name' => 'Oslo',
                'country_id' => 164,
            ),
            443 => 
            array (
                'id' => 3057,
                'name' => 'Hedmark',
                'country_id' => 164,
            ),
            444 => 
            array (
                'id' => 3058,
                'name' => 'Oppland',
                'country_id' => 164,
            ),
            445 => 
            array (
                'id' => 3059,
                'name' => 'Buskerud',
                'country_id' => 164,
            ),
            446 => 
            array (
                'id' => 3060,
                'name' => 'Vestfold',
                'country_id' => 164,
            ),
            447 => 
            array (
                'id' => 3061,
                'name' => 'Telemark',
                'country_id' => 164,
            ),
            448 => 
            array (
                'id' => 3062,
                'name' => 'Aust-Agder',
                'country_id' => 164,
            ),
            449 => 
            array (
                'id' => 3063,
                'name' => 'Vest-Agder',
                'country_id' => 164,
            ),
            450 => 
            array (
                'id' => 3064,
                'name' => 'Rogaland',
                'country_id' => 164,
            ),
            451 => 
            array (
                'id' => 3065,
                'name' => 'Hordaland',
                'country_id' => 164,
            ),
            452 => 
            array (
                'id' => 3066,
                'name' => 'Sogn og Fjordane',
                'country_id' => 164,
            ),
            453 => 
            array (
                'id' => 3067,
                'name' => 'Møre og Romsdal',
                'country_id' => 164,
            ),
            454 => 
            array (
                'id' => 3068,
                'name' => 'Sør-Trøndelag',
                'country_id' => 164,
            ),
            455 => 
            array (
                'id' => 3069,
                'name' => 'Nord-Trøndelag',
                'country_id' => 164,
            ),
            456 => 
            array (
                'id' => 3070,
                'name' => 'Nordland',
                'country_id' => 164,
            ),
            457 => 
            array (
                'id' => 3071,
                'name' => 'Troms',
                'country_id' => 164,
            ),
            458 => 
            array (
                'id' => 3072,
                'name' => 'Finnmark',
                'country_id' => 164,
            ),
            459 => 
            array (
                'id' => 3073,
            'name' => '"Svalbard (Arctic Region) (See',
                'country_id' => 164,
            ),
            460 => 
            array (
                'id' => 3074,
            'name' => '"Jan Mayen (Arctic Region) (See',
                'country_id' => 164,
            ),
            461 => 
            array (
                'id' => 3075,
                'name' => 'Bagmati',
                'country_id' => 153,
            ),
            462 => 
            array (
                'id' => 3076,
                'name' => 'Bheri',
                'country_id' => 153,
            ),
            463 => 
            array (
                'id' => 3077,
                'name' => 'Dhawalagiri',
                'country_id' => 153,
            ),
            464 => 
            array (
                'id' => 3078,
                'name' => 'Gandaki',
                'country_id' => 153,
            ),
            465 => 
            array (
                'id' => 3079,
                'name' => 'Janakpur',
                'country_id' => 153,
            ),
            466 => 
            array (
                'id' => 3080,
                'name' => 'Karnali',
                'country_id' => 153,
            ),
            467 => 
            array (
                'id' => 3081,
                'name' => 'Kosi [Koshi]',
                'country_id' => 153,
            ),
            468 => 
            array (
                'id' => 3082,
                'name' => 'Lumbini',
                'country_id' => 153,
            ),
            469 => 
            array (
                'id' => 3083,
                'name' => 'Mahakali',
                'country_id' => 153,
            ),
            470 => 
            array (
                'id' => 3084,
                'name' => 'Mechi',
                'country_id' => 153,
            ),
            471 => 
            array (
                'id' => 3085,
                'name' => 'Narayani',
                'country_id' => 153,
            ),
            472 => 
            array (
                'id' => 3086,
                'name' => 'Rapti',
                'country_id' => 153,
            ),
            473 => 
            array (
                'id' => 3087,
                'name' => 'Sagarmatha',
                'country_id' => 153,
            ),
            474 => 
            array (
                'id' => 3088,
                'name' => 'Seti',
                'country_id' => 153,
            ),
            475 => 
            array (
                'id' => 3089,
                'name' => 'Aiwo',
                'country_id' => 152,
            ),
            476 => 
            array (
                'id' => 3090,
                'name' => 'Anabar',
                'country_id' => 152,
            ),
            477 => 
            array (
                'id' => 3091,
                'name' => 'Anetan',
                'country_id' => 152,
            ),
            478 => 
            array (
                'id' => 3092,
                'name' => 'Anibare',
                'country_id' => 152,
            ),
            479 => 
            array (
                'id' => 3093,
                'name' => 'Baiti',
                'country_id' => 152,
            ),
            480 => 
            array (
                'id' => 3094,
                'name' => 'Boe',
                'country_id' => 152,
            ),
            481 => 
            array (
                'id' => 3095,
                'name' => 'Buada',
                'country_id' => 152,
            ),
            482 => 
            array (
                'id' => 3096,
                'name' => 'Denigomodu',
                'country_id' => 152,
            ),
            483 => 
            array (
                'id' => 3097,
                'name' => 'Ewa',
                'country_id' => 152,
            ),
            484 => 
            array (
                'id' => 3098,
                'name' => 'Ijuw',
                'country_id' => 152,
            ),
            485 => 
            array (
                'id' => 3099,
                'name' => 'Meneng',
                'country_id' => 152,
            ),
            486 => 
            array (
                'id' => 3100,
                'name' => 'Nibok',
                'country_id' => 152,
            ),
            487 => 
            array (
                'id' => 3101,
                'name' => 'Uaboe',
                'country_id' => 152,
            ),
            488 => 
            array (
                'id' => 3102,
                'name' => 'Yaren',
                'country_id' => 152,
            ),
            489 => 
            array (
                'id' => 3103,
                'name' => 'Auckland',
                'country_id' => 157,
            ),
            490 => 
            array (
                'id' => 3104,
                'name' => 'Bay of Plenty',
                'country_id' => 157,
            ),
            491 => 
            array (
                'id' => 3105,
                'name' => 'Canterbury',
                'country_id' => 157,
            ),
            492 => 
            array (
                'id' => 3106,
                'name' => 'Chatham Islands Territory',
                'country_id' => 157,
            ),
            493 => 
            array (
                'id' => 3107,
                'name' => 'Gisborne District',
                'country_id' => 157,
            ),
            494 => 
            array (
                'id' => 3108,
                'name' => 'Hawkes\'s Bay',
                'country_id' => 157,
            ),
            495 => 
            array (
                'id' => 3109,
                'name' => 'Marlborough District',
                'country_id' => 157,
            ),
            496 => 
            array (
                'id' => 3110,
                'name' => 'Manawatu-Wanganui',
                'country_id' => 157,
            ),
            497 => 
            array (
                'id' => 3111,
                'name' => 'Nelson City',
                'country_id' => 157,
            ),
            498 => 
            array (
                'id' => 3112,
                'name' => 'Northland',
                'country_id' => 157,
            ),
            499 => 
            array (
                'id' => 3113,
                'name' => 'Otago',
                'country_id' => 157,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 3114,
                'name' => 'Southland',
                'country_id' => 157,
            ),
            1 => 
            array (
                'id' => 3115,
                'name' => 'Tasman District',
                'country_id' => 157,
            ),
            2 => 
            array (
                'id' => 3116,
                'name' => 'Taranaki',
                'country_id' => 157,
            ),
            3 => 
            array (
                'id' => 3117,
                'name' => 'Wellington',
                'country_id' => 157,
            ),
            4 => 
            array (
                'id' => 3118,
                'name' => 'Waikato',
                'country_id' => 157,
            ),
            5 => 
            array (
                'id' => 3119,
                'name' => 'West Coast',
                'country_id' => 157,
            ),
            6 => 
            array (
                'id' => 3121,
                'name' => 'Al Buraymi',
                'country_id' => 165,
            ),
            7 => 
            array (
                'id' => 3125,
                'name' => 'AI Wusta',
                'country_id' => 165,
            ),
            8 => 
            array (
                'id' => 3128,
                'name' => 'Bocas del Toro',
                'country_id' => 169,
            ),
            9 => 
            array (
                'id' => 3129,
                'name' => 'Coclé',
                'country_id' => 169,
            ),
            10 => 
            array (
                'id' => 3130,
                'name' => 'Colón',
                'country_id' => 169,
            ),
            11 => 
            array (
                'id' => 3131,
                'name' => 'Chiriquí',
                'country_id' => 169,
            ),
            12 => 
            array (
                'id' => 3132,
                'name' => 'Darién',
                'country_id' => 169,
            ),
            13 => 
            array (
                'id' => 3133,
                'name' => 'Herrera',
                'country_id' => 169,
            ),
            14 => 
            array (
                'id' => 3134,
                'name' => 'Los Santos',
                'country_id' => 169,
            ),
            15 => 
            array (
                'id' => 3135,
                'name' => 'Panamá',
                'country_id' => 169,
            ),
            16 => 
            array (
                'id' => 3136,
                'name' => 'Veraguas',
                'country_id' => 169,
            ),
            17 => 
            array (
                'id' => 3137,
                'name' => 'Emberá',
                'country_id' => 169,
            ),
            18 => 
            array (
                'id' => 3138,
                'name' => 'Kuna Yala',
                'country_id' => 169,
            ),
            19 => 
            array (
                'id' => 3139,
                'name' => 'Ngöbe-Buglé',
                'country_id' => 169,
            ),
            20 => 
            array (
                'id' => 3140,
                'name' => 'Amazonas',
                'country_id' => 172,
            ),
            21 => 
            array (
                'id' => 3141,
                'name' => 'Ancash',
                'country_id' => 172,
            ),
            22 => 
            array (
                'id' => 3142,
                'name' => 'Apurímac',
                'country_id' => 172,
            ),
            23 => 
            array (
                'id' => 3143,
                'name' => 'Arequipa',
                'country_id' => 172,
            ),
            24 => 
            array (
                'id' => 3144,
                'name' => 'Ayacucho',
                'country_id' => 172,
            ),
            25 => 
            array (
                'id' => 3145,
                'name' => 'Cajamarca',
                'country_id' => 172,
            ),
            26 => 
            array (
                'id' => 3146,
                'name' => 'El Callao',
                'country_id' => 172,
            ),
            27 => 
            array (
                'id' => 3147,
                'name' => 'Cuzco [Cusco]',
                'country_id' => 172,
            ),
            28 => 
            array (
                'id' => 3148,
                'name' => 'Huánuco',
                'country_id' => 172,
            ),
            29 => 
            array (
                'id' => 3149,
                'name' => 'Huancavelica',
                'country_id' => 172,
            ),
            30 => 
            array (
                'id' => 3150,
                'name' => 'Ica',
                'country_id' => 172,
            ),
            31 => 
            array (
                'id' => 3151,
                'name' => 'Junín',
                'country_id' => 172,
            ),
            32 => 
            array (
                'id' => 3152,
                'name' => 'La Libertad',
                'country_id' => 172,
            ),
            33 => 
            array (
                'id' => 3153,
                'name' => 'Lambayeque',
                'country_id' => 172,
            ),
            34 => 
            array (
                'id' => 3154,
                'name' => 'Lima',
                'country_id' => 172,
            ),
            35 => 
            array (
                'id' => 3155,
                'name' => 'Lima hatun llaqta',
                'country_id' => 172,
            ),
            36 => 
            array (
                'id' => 3156,
                'name' => 'Loreto',
                'country_id' => 172,
            ),
            37 => 
            array (
                'id' => 3157,
                'name' => 'Madre de Dios',
                'country_id' => 172,
            ),
            38 => 
            array (
                'id' => 3158,
                'name' => 'Moquegua',
                'country_id' => 172,
            ),
            39 => 
            array (
                'id' => 3159,
                'name' => 'Pasco',
                'country_id' => 172,
            ),
            40 => 
            array (
                'id' => 3160,
                'name' => 'Piura',
                'country_id' => 172,
            ),
            41 => 
            array (
                'id' => 3161,
                'name' => 'Puno',
                'country_id' => 172,
            ),
            42 => 
            array (
                'id' => 3162,
                'name' => 'San Martín',
                'country_id' => 172,
            ),
            43 => 
            array (
                'id' => 3163,
                'name' => 'Tacna',
                'country_id' => 172,
            ),
            44 => 
            array (
                'id' => 3164,
                'name' => 'Tumbes',
                'country_id' => 172,
            ),
            45 => 
            array (
                'id' => 3165,
                'name' => 'Ucayali',
                'country_id' => 172,
            ),
            46 => 
            array (
                'id' => 3166,
                'name' => 'Chimbu',
                'country_id' => 170,
            ),
            47 => 
            array (
                'id' => 3167,
                'name' => 'Central',
                'country_id' => 170,
            ),
            48 => 
            array (
                'id' => 3168,
                'name' => 'East New Britain',
                'country_id' => 170,
            ),
            49 => 
            array (
                'id' => 3169,
                'name' => 'Eastern Highlands',
                'country_id' => 170,
            ),
            50 => 
            array (
                'id' => 3170,
                'name' => 'Enga',
                'country_id' => 170,
            ),
            51 => 
            array (
                'id' => 3171,
                'name' => 'East Sepik',
                'country_id' => 170,
            ),
            52 => 
            array (
                'id' => 3172,
                'name' => 'Gulf',
                'country_id' => 170,
            ),
            53 => 
            array (
                'id' => 3173,
                'name' => 'Milne Bay',
                'country_id' => 170,
            ),
            54 => 
            array (
                'id' => 3174,
                'name' => 'Morobe',
                'country_id' => 170,
            ),
            55 => 
            array (
                'id' => 3175,
                'name' => 'Madang',
                'country_id' => 170,
            ),
            56 => 
            array (
                'id' => 3176,
                'name' => 'Manus',
                'country_id' => 170,
            ),
            57 => 
            array (
                'id' => 3177,
            'name' => 'National Capital District (Port Moresby)',
                'country_id' => 170,
            ),
            58 => 
            array (
                'id' => 3178,
                'name' => 'New Ireland',
                'country_id' => 170,
            ),
            59 => 
            array (
                'id' => 3179,
                'name' => 'Northern',
                'country_id' => 170,
            ),
            60 => 
            array (
                'id' => 3180,
                'name' => 'Bougainville',
                'country_id' => 170,
            ),
            61 => 
            array (
                'id' => 3181,
            'name' => 'Sandaun (West Sepik)',
                'country_id' => 170,
            ),
            62 => 
            array (
                'id' => 3182,
                'name' => 'Southern Highlands',
                'country_id' => 170,
            ),
            63 => 
            array (
                'id' => 3183,
                'name' => 'West New Britain',
                'country_id' => 170,
            ),
            64 => 
            array (
                'id' => 3184,
                'name' => 'Western Highlands',
                'country_id' => 170,
            ),
            65 => 
            array (
                'id' => 3185,
                'name' => 'Western',
                'country_id' => 170,
            ),
            66 => 
            array (
                'id' => 3186,
                'name' => 'Abra',
                'country_id' => 173,
            ),
            67 => 
            array (
                'id' => 3187,
                'name' => 'Agusan del Norte',
                'country_id' => 173,
            ),
            68 => 
            array (
                'id' => 3188,
                'name' => 'Agusan del Sur',
                'country_id' => 173,
            ),
            69 => 
            array (
                'id' => 3189,
                'name' => 'Aklan',
                'country_id' => 173,
            ),
            70 => 
            array (
                'id' => 3190,
                'name' => 'Albay',
                'country_id' => 173,
            ),
            71 => 
            array (
                'id' => 3191,
                'name' => 'Antique',
                'country_id' => 173,
            ),
            72 => 
            array (
                'id' => 3192,
                'name' => 'Apayao',
                'country_id' => 173,
            ),
            73 => 
            array (
                'id' => 3193,
                'name' => 'Aurora',
                'country_id' => 173,
            ),
            74 => 
            array (
                'id' => 3194,
                'name' => 'Bataan',
                'country_id' => 173,
            ),
            75 => 
            array (
                'id' => 3195,
                'name' => 'Basilan',
                'country_id' => 173,
            ),
            76 => 
            array (
                'id' => 3196,
                'name' => 'Benguet',
                'country_id' => 173,
            ),
            77 => 
            array (
                'id' => 3197,
                'name' => 'Biliran',
                'country_id' => 173,
            ),
            78 => 
            array (
                'id' => 3198,
                'name' => 'Bohol',
                'country_id' => 173,
            ),
            79 => 
            array (
                'id' => 3199,
                'name' => 'Batangas',
                'country_id' => 173,
            ),
            80 => 
            array (
                'id' => 3200,
                'name' => 'Batanes',
                'country_id' => 173,
            ),
            81 => 
            array (
                'id' => 3201,
                'name' => 'Bukidnon',
                'country_id' => 173,
            ),
            82 => 
            array (
                'id' => 3202,
                'name' => 'Bulacan',
                'country_id' => 173,
            ),
            83 => 
            array (
                'id' => 3203,
                'name' => 'Cagayan',
                'country_id' => 173,
            ),
            84 => 
            array (
                'id' => 3204,
                'name' => 'Camiguin',
                'country_id' => 173,
            ),
            85 => 
            array (
                'id' => 3205,
                'name' => 'Camarines Norte',
                'country_id' => 173,
            ),
            86 => 
            array (
                'id' => 3206,
                'name' => 'Capiz',
                'country_id' => 173,
            ),
            87 => 
            array (
                'id' => 3207,
                'name' => 'Camarines Sur',
                'country_id' => 173,
            ),
            88 => 
            array (
                'id' => 3208,
                'name' => 'Catanduanes',
                'country_id' => 173,
            ),
            89 => 
            array (
                'id' => 3209,
                'name' => 'Cavite',
                'country_id' => 173,
            ),
            90 => 
            array (
                'id' => 3210,
                'name' => 'Cebu',
                'country_id' => 173,
            ),
            91 => 
            array (
                'id' => 3211,
                'name' => 'Compostela Valley',
                'country_id' => 173,
            ),
            92 => 
            array (
                'id' => 3212,
                'name' => 'Davao Oriental',
                'country_id' => 173,
            ),
            93 => 
            array (
                'id' => 3213,
                'name' => 'Davao del Sur',
                'country_id' => 173,
            ),
            94 => 
            array (
                'id' => 3214,
                'name' => 'Davao del Norte',
                'country_id' => 173,
            ),
            95 => 
            array (
                'id' => 3215,
                'name' => 'Dinagat Islands',
                'country_id' => 173,
            ),
            96 => 
            array (
                'id' => 3216,
                'name' => 'Eastern Samar',
                'country_id' => 173,
            ),
            97 => 
            array (
                'id' => 3217,
                'name' => 'Guimaras',
                'country_id' => 173,
            ),
            98 => 
            array (
                'id' => 3218,
                'name' => 'Ifugao',
                'country_id' => 173,
            ),
            99 => 
            array (
                'id' => 3219,
                'name' => 'Iloilo',
                'country_id' => 173,
            ),
            100 => 
            array (
                'id' => 3220,
                'name' => 'Ilocos Norte',
                'country_id' => 173,
            ),
            101 => 
            array (
                'id' => 3221,
                'name' => 'Ilocos Sur',
                'country_id' => 173,
            ),
            102 => 
            array (
                'id' => 3222,
                'name' => 'Isabela',
                'country_id' => 173,
            ),
            103 => 
            array (
                'id' => 3223,
                'name' => 'Kalinga-Apayao',
                'country_id' => 173,
            ),
            104 => 
            array (
                'id' => 3224,
                'name' => 'Laguna',
                'country_id' => 173,
            ),
            105 => 
            array (
                'id' => 3225,
                'name' => 'Lanao del Norte',
                'country_id' => 173,
            ),
            106 => 
            array (
                'id' => 3226,
                'name' => 'Lanao del Sur',
                'country_id' => 173,
            ),
            107 => 
            array (
                'id' => 3227,
                'name' => 'Leyte',
                'country_id' => 173,
            ),
            108 => 
            array (
                'id' => 3228,
                'name' => 'La Union',
                'country_id' => 173,
            ),
            109 => 
            array (
                'id' => 3229,
                'name' => 'Maguindanao',
                'country_id' => 173,
            ),
            110 => 
            array (
                'id' => 3230,
                'name' => 'Masbate',
                'country_id' => 173,
            ),
            111 => 
            array (
                'id' => 3231,
                'name' => 'Mindoro Occidental',
                'country_id' => 173,
            ),
            112 => 
            array (
                'id' => 3232,
                'name' => 'Mindoro Oriental',
                'country_id' => 173,
            ),
            113 => 
            array (
                'id' => 3233,
                'name' => 'Mountain Province',
                'country_id' => 173,
            ),
            114 => 
            array (
                'id' => 3234,
                'name' => 'Misamis Occidental',
                'country_id' => 173,
            ),
            115 => 
            array (
                'id' => 3235,
                'name' => 'Misamis Oriental',
                'country_id' => 173,
            ),
            116 => 
            array (
                'id' => 3236,
                'name' => 'Kotabato',
                'country_id' => 173,
            ),
            117 => 
            array (
                'id' => 3237,
                'name' => 'Negros occidental',
                'country_id' => 173,
            ),
            118 => 
            array (
                'id' => 3238,
                'name' => 'Negros oriental',
                'country_id' => 173,
            ),
            119 => 
            array (
                'id' => 3239,
                'name' => 'Northern Samar',
                'country_id' => 173,
            ),
            120 => 
            array (
                'id' => 3240,
                'name' => 'Nueva Ecija',
                'country_id' => 173,
            ),
            121 => 
            array (
                'id' => 3241,
                'name' => 'Nueva Vizcaya',
                'country_id' => 173,
            ),
            122 => 
            array (
                'id' => 3242,
                'name' => 'Pampanga',
                'country_id' => 173,
            ),
            123 => 
            array (
                'id' => 3243,
                'name' => 'Pangasinan',
                'country_id' => 173,
            ),
            124 => 
            array (
                'id' => 3244,
                'name' => 'Palawan',
                'country_id' => 173,
            ),
            125 => 
            array (
                'id' => 3245,
                'name' => 'Quezon',
                'country_id' => 173,
            ),
            126 => 
            array (
                'id' => 3246,
                'name' => 'Quirino',
                'country_id' => 173,
            ),
            127 => 
            array (
                'id' => 3247,
                'name' => 'Rizal',
                'country_id' => 173,
            ),
            128 => 
            array (
                'id' => 3248,
                'name' => 'Romblon',
                'country_id' => 173,
            ),
            129 => 
            array (
                'id' => 3249,
                'name' => 'Sarangani',
                'country_id' => 173,
            ),
            130 => 
            array (
                'id' => 3250,
                'name' => 'South Cotabato',
                'country_id' => 173,
            ),
            131 => 
            array (
                'id' => 3251,
                'name' => 'Siquijor',
                'country_id' => 173,
            ),
            132 => 
            array (
                'id' => 3252,
                'name' => 'Southern Leyte',
                'country_id' => 173,
            ),
            133 => 
            array (
                'id' => 3253,
                'name' => 'Sulu',
                'country_id' => 173,
            ),
            134 => 
            array (
                'id' => 3254,
                'name' => 'Sorsogon',
                'country_id' => 173,
            ),
            135 => 
            array (
                'id' => 3255,
                'name' => 'Sultan Kudarat',
                'country_id' => 173,
            ),
            136 => 
            array (
                'id' => 3256,
                'name' => 'Surigao del Norte',
                'country_id' => 173,
            ),
            137 => 
            array (
                'id' => 3257,
                'name' => 'Surigao del Sur',
                'country_id' => 173,
            ),
            138 => 
            array (
                'id' => 3258,
                'name' => 'Tarlac',
                'country_id' => 173,
            ),
            139 => 
            array (
                'id' => 3259,
                'name' => 'Tawi-Tawi',
                'country_id' => 173,
            ),
            140 => 
            array (
                'id' => 3260,
                'name' => 'Western Samar',
                'country_id' => 173,
            ),
            141 => 
            array (
                'id' => 3261,
                'name' => 'Zamboanga del Norte',
                'country_id' => 173,
            ),
            142 => 
            array (
                'id' => 3262,
                'name' => 'Zamboanga del Sur',
                'country_id' => 173,
            ),
            143 => 
            array (
                'id' => 3263,
                'name' => 'Zambales',
                'country_id' => 173,
            ),
            144 => 
            array (
                'id' => 3264,
                'name' => 'Zamboanga Sibuguey [Zamboanga Sibugay]',
                'country_id' => 173,
            ),
            145 => 
            array (
                'id' => 3265,
                'name' => 'Balochistan',
                'country_id' => 166,
            ),
            146 => 
            array (
                'id' => 3266,
                'name' => 'Gilgit-Baltistan',
                'country_id' => 166,
            ),
            147 => 
            array (
                'id' => 3267,
                'name' => 'Islamabad',
                'country_id' => 166,
            ),
            148 => 
            array (
                'id' => 3268,
                'name' => 'Azad Kashmir',
                'country_id' => 166,
            ),
            149 => 
            array (
                'id' => 3269,
                'name' => 'Khyber Pakhtunkhwa',
                'country_id' => 166,
            ),
            150 => 
            array (
                'id' => 3270,
                'name' => 'Punjab',
                'country_id' => 166,
            ),
            151 => 
            array (
                'id' => 3271,
                'name' => 'Sindh',
                'country_id' => 166,
            ),
            152 => 
            array (
                'id' => 3272,
                'name' => 'Federally Administered Tribal Areas',
                'country_id' => 166,
            ),
            153 => 
            array (
                'id' => 3273,
                'name' => 'Dolnoslaskie',
                'country_id' => 175,
            ),
            154 => 
            array (
                'id' => 3274,
                'name' => 'Kujawsko-pomorskie',
                'country_id' => 175,
            ),
            155 => 
            array (
                'id' => 3275,
                'name' => 'Lubuskie',
                'country_id' => 175,
            ),
            156 => 
            array (
                'id' => 3276,
                'name' => 'Lódzkie',
                'country_id' => 175,
            ),
            157 => 
            array (
                'id' => 3277,
                'name' => 'Lubelskie',
                'country_id' => 175,
            ),
            158 => 
            array (
                'id' => 3278,
                'name' => 'Malopolskie',
                'country_id' => 175,
            ),
            159 => 
            array (
                'id' => 3279,
                'name' => 'Mazowieckie',
                'country_id' => 175,
            ),
            160 => 
            array (
                'id' => 3280,
                'name' => 'Opolskie',
                'country_id' => 175,
            ),
            161 => 
            array (
                'id' => 3281,
                'name' => 'Podlaskie',
                'country_id' => 175,
            ),
            162 => 
            array (
                'id' => 3282,
                'name' => 'Podkarpackie',
                'country_id' => 175,
            ),
            163 => 
            array (
                'id' => 3283,
                'name' => 'Pomorskie',
                'country_id' => 175,
            ),
            164 => 
            array (
                'id' => 3284,
                'name' => 'Swietokrzyskie',
                'country_id' => 175,
            ),
            165 => 
            array (
                'id' => 3285,
                'name' => 'Slaskie',
                'country_id' => 175,
            ),
            166 => 
            array (
                'id' => 3286,
                'name' => 'Warminsko-mazurskie',
                'country_id' => 175,
            ),
            167 => 
            array (
                'id' => 3287,
                'name' => 'Wielkopolskie',
                'country_id' => 175,
            ),
            168 => 
            array (
                'id' => 3288,
                'name' => 'Zachodniopomorskie',
                'country_id' => 175,
            ),
            169 => 
            array (
                'id' => 3289,
                'name' => 'Bethlehem',
                'country_id' => 168,
            ),
            170 => 
            array (
                'id' => 3290,
                'name' => 'Deir El Balah',
                'country_id' => 168,
            ),
            171 => 
            array (
                'id' => 3291,
                'name' => 'Gaza',
                'country_id' => 168,
            ),
            172 => 
            array (
                'id' => 3292,
                'name' => 'Hebron',
                'country_id' => 168,
            ),
            173 => 
            array (
                'id' => 3293,
                'name' => 'Jerusalem',
                'country_id' => 168,
            ),
            174 => 
            array (
                'id' => 3294,
                'name' => 'Jericho – Al Aghwar',
                'country_id' => 168,
            ),
            175 => 
            array (
                'id' => 3295,
                'name' => 'Khan Yunis',
                'country_id' => 168,
            ),
            176 => 
            array (
                'id' => 3296,
                'name' => 'Nablus',
                'country_id' => 168,
            ),
            177 => 
            array (
                'id' => 3297,
                'name' => 'North Gaza',
                'country_id' => 168,
            ),
            178 => 
            array (
                'id' => 3298,
                'name' => 'Qalqilya',
                'country_id' => 168,
            ),
            179 => 
            array (
                'id' => 3299,
                'name' => 'Ramallah',
                'country_id' => 168,
            ),
            180 => 
            array (
                'id' => 3300,
                'name' => 'Rafah',
                'country_id' => 168,
            ),
            181 => 
            array (
                'id' => 3301,
                'name' => 'Salfit',
                'country_id' => 168,
            ),
            182 => 
            array (
                'id' => 3302,
                'name' => 'Tubas',
                'country_id' => 168,
            ),
            183 => 
            array (
                'id' => 3303,
                'name' => 'Tulkarm',
                'country_id' => 168,
            ),
            184 => 
            array (
                'id' => 3304,
                'name' => 'Aveiro',
                'country_id' => 176,
            ),
            185 => 
            array (
                'id' => 3305,
                'name' => 'Beja',
                'country_id' => 176,
            ),
            186 => 
            array (
                'id' => 3306,
                'name' => 'Braga',
                'country_id' => 176,
            ),
            187 => 
            array (
                'id' => 3307,
                'name' => 'Bragança',
                'country_id' => 176,
            ),
            188 => 
            array (
                'id' => 3308,
                'name' => 'Castelo Branco',
                'country_id' => 176,
            ),
            189 => 
            array (
                'id' => 3309,
                'name' => 'Coimbra',
                'country_id' => 176,
            ),
            190 => 
            array (
                'id' => 3310,
                'name' => 'Évora',
                'country_id' => 176,
            ),
            191 => 
            array (
                'id' => 3311,
                'name' => 'Faro',
                'country_id' => 176,
            ),
            192 => 
            array (
                'id' => 3312,
                'name' => 'Guarda',
                'country_id' => 176,
            ),
            193 => 
            array (
                'id' => 3313,
                'name' => 'Leiria',
                'country_id' => 176,
            ),
            194 => 
            array (
                'id' => 3314,
                'name' => 'Lisboa',
                'country_id' => 176,
            ),
            195 => 
            array (
                'id' => 3315,
                'name' => 'Portalegre',
                'country_id' => 176,
            ),
            196 => 
            array (
                'id' => 3316,
                'name' => 'Porto',
                'country_id' => 176,
            ),
            197 => 
            array (
                'id' => 3317,
                'name' => 'Santarém',
                'country_id' => 176,
            ),
            198 => 
            array (
                'id' => 3318,
                'name' => 'Setúbal',
                'country_id' => 176,
            ),
            199 => 
            array (
                'id' => 3319,
                'name' => 'Viana do Castelo',
                'country_id' => 176,
            ),
            200 => 
            array (
                'id' => 3320,
                'name' => 'Vila Real',
                'country_id' => 176,
            ),
            201 => 
            array (
                'id' => 3321,
                'name' => 'Viseu',
                'country_id' => 176,
            ),
            202 => 
            array (
                'id' => 3322,
                'name' => 'Região Autónoma dos Açores',
                'country_id' => 176,
            ),
            203 => 
            array (
                'id' => 3323,
                'name' => 'Região Autónoma da Madeira',
                'country_id' => 176,
            ),
            204 => 
            array (
                'id' => 3324,
                'name' => 'Aimeliik    ',
                'country_id' => 167,
            ),
            205 => 
            array (
                'id' => 3325,
                'name' => 'Airai       ',
                'country_id' => 167,
            ),
            206 => 
            array (
                'id' => 3326,
                'name' => 'Angaur      ',
                'country_id' => 167,
            ),
            207 => 
            array (
                'id' => 3327,
                'name' => 'Hatobohei   ',
                'country_id' => 167,
            ),
            208 => 
            array (
                'id' => 3328,
                'name' => 'Kayangel    ',
                'country_id' => 167,
            ),
            209 => 
            array (
                'id' => 3329,
                'name' => 'Koror       ',
                'country_id' => 167,
            ),
            210 => 
            array (
                'id' => 3330,
                'name' => 'Melekeok    ',
                'country_id' => 167,
            ),
            211 => 
            array (
                'id' => 3331,
                'name' => 'Ngaraard    ',
                'country_id' => 167,
            ),
            212 => 
            array (
                'id' => 3332,
                'name' => 'Ngarchelong ',
                'country_id' => 167,
            ),
            213 => 
            array (
                'id' => 3333,
                'name' => 'Ngardmau    ',
                'country_id' => 167,
            ),
            214 => 
            array (
                'id' => 3334,
                'name' => 'Ngatpang    ',
                'country_id' => 167,
            ),
            215 => 
            array (
                'id' => 3335,
                'name' => 'Ngchesar    ',
                'country_id' => 167,
            ),
            216 => 
            array (
                'id' => 3336,
                'name' => 'Ngeremlengui',
                'country_id' => 167,
            ),
            217 => 
            array (
                'id' => 3337,
                'name' => 'Ngiwal      ',
                'country_id' => 167,
            ),
            218 => 
            array (
                'id' => 3338,
                'name' => 'Peleliu     ',
                'country_id' => 167,
            ),
            219 => 
            array (
                'id' => 3339,
                'name' => 'Sonsorol    ',
                'country_id' => 167,
            ),
            220 => 
            array (
                'id' => 3340,
                'name' => 'Concepción',
                'country_id' => 171,
            ),
            221 => 
            array (
                'id' => 3341,
                'name' => 'Alto Paraná',
                'country_id' => 171,
            ),
            222 => 
            array (
                'id' => 3342,
                'name' => 'Central',
                'country_id' => 171,
            ),
            223 => 
            array (
                'id' => 3343,
                'name' => 'Ñeembucú',
                'country_id' => 171,
            ),
            224 => 
            array (
                'id' => 3344,
                'name' => 'Amambay',
                'country_id' => 171,
            ),
            225 => 
            array (
                'id' => 3345,
                'name' => 'Canindeyú',
                'country_id' => 171,
            ),
            226 => 
            array (
                'id' => 3346,
                'name' => 'Presidente Hayes',
                'country_id' => 171,
            ),
            227 => 
            array (
                'id' => 3347,
                'name' => 'Alto Paraguay',
                'country_id' => 171,
            ),
            228 => 
            array (
                'id' => 3348,
                'name' => 'Boquerón',
                'country_id' => 171,
            ),
            229 => 
            array (
                'id' => 3349,
                'name' => 'San Pedro',
                'country_id' => 171,
            ),
            230 => 
            array (
                'id' => 3350,
                'name' => 'Cordillera',
                'country_id' => 171,
            ),
            231 => 
            array (
                'id' => 3351,
                'name' => 'Guairá',
                'country_id' => 171,
            ),
            232 => 
            array (
                'id' => 3352,
                'name' => 'Caaguazú',
                'country_id' => 171,
            ),
            233 => 
            array (
                'id' => 3353,
                'name' => 'Caazapá',
                'country_id' => 171,
            ),
            234 => 
            array (
                'id' => 3354,
                'name' => 'Itapúa',
                'country_id' => 171,
            ),
            235 => 
            array (
                'id' => 3355,
                'name' => 'Misiones',
                'country_id' => 171,
            ),
            236 => 
            array (
                'id' => 3356,
                'name' => 'Paraguarí',
                'country_id' => 171,
            ),
            237 => 
            array (
                'id' => 3357,
                'name' => 'Asunción',
                'country_id' => 171,
            ),
            238 => 
            array (
                'id' => 3359,
                'name' => 'Al Khawr wa adh Dhakhirah',
                'country_id' => 178,
            ),
            239 => 
            array (
                'id' => 3360,
                'name' => 'Ash Shamal',
                'country_id' => 178,
            ),
            240 => 
            array (
                'id' => 3364,
                'name' => 'Az¸ Z¸a‘ayin',
                'country_id' => 178,
            ),
            241 => 
            array (
                'id' => 3365,
                'name' => 'Alba',
                'country_id' => 180,
            ),
            242 => 
            array (
                'id' => 3366,
                'name' => 'Arges',
                'country_id' => 180,
            ),
            243 => 
            array (
                'id' => 3367,
                'name' => 'Arad',
                'country_id' => 180,
            ),
            244 => 
            array (
                'id' => 3368,
                'name' => 'Bucuresti',
                'country_id' => 180,
            ),
            245 => 
            array (
                'id' => 3369,
                'name' => 'Bacau',
                'country_id' => 180,
            ),
            246 => 
            array (
                'id' => 3370,
                'name' => 'Bihor',
                'country_id' => 180,
            ),
            247 => 
            array (
                'id' => 3371,
                'name' => 'Bistrita-Nasaud',
                'country_id' => 180,
            ),
            248 => 
            array (
                'id' => 3372,
                'name' => 'Braila',
                'country_id' => 180,
            ),
            249 => 
            array (
                'id' => 3373,
                'name' => 'Botosani',
                'country_id' => 180,
            ),
            250 => 
            array (
                'id' => 3374,
                'name' => 'Brasov',
                'country_id' => 180,
            ),
            251 => 
            array (
                'id' => 3375,
                'name' => 'Buzau',
                'country_id' => 180,
            ),
            252 => 
            array (
                'id' => 3376,
                'name' => 'Cluj',
                'country_id' => 180,
            ),
            253 => 
            array (
                'id' => 3377,
                'name' => 'Calarasi',
                'country_id' => 180,
            ),
            254 => 
            array (
                'id' => 3378,
                'name' => 'Caras-Severin',
                'country_id' => 180,
            ),
            255 => 
            array (
                'id' => 3379,
                'name' => 'Constarta',
                'country_id' => 180,
            ),
            256 => 
            array (
                'id' => 3380,
                'name' => 'Covasna',
                'country_id' => 180,
            ),
            257 => 
            array (
                'id' => 3381,
                'name' => 'Dâmbovita',
                'country_id' => 180,
            ),
            258 => 
            array (
                'id' => 3382,
                'name' => 'Dolj',
                'country_id' => 180,
            ),
            259 => 
            array (
                'id' => 3383,
                'name' => 'Gorj',
                'country_id' => 180,
            ),
            260 => 
            array (
                'id' => 3384,
                'name' => 'Galati',
                'country_id' => 180,
            ),
            261 => 
            array (
                'id' => 3385,
                'name' => 'Giurgiu',
                'country_id' => 180,
            ),
            262 => 
            array (
                'id' => 3386,
                'name' => 'Hunedoara',
                'country_id' => 180,
            ),
            263 => 
            array (
                'id' => 3387,
                'name' => 'Harghita',
                'country_id' => 180,
            ),
            264 => 
            array (
                'id' => 3388,
                'name' => 'Ilfov',
                'country_id' => 180,
            ),
            265 => 
            array (
                'id' => 3389,
                'name' => 'Ialomita',
                'country_id' => 180,
            ),
            266 => 
            array (
                'id' => 3390,
                'name' => 'Iasi',
                'country_id' => 180,
            ),
            267 => 
            array (
                'id' => 3391,
                'name' => 'Mehedinti',
                'country_id' => 180,
            ),
            268 => 
            array (
                'id' => 3392,
                'name' => 'Maramures',
                'country_id' => 180,
            ),
            269 => 
            array (
                'id' => 3393,
                'name' => 'Mures',
                'country_id' => 180,
            ),
            270 => 
            array (
                'id' => 3394,
                'name' => 'Neamt',
                'country_id' => 180,
            ),
            271 => 
            array (
                'id' => 3395,
                'name' => 'Olt',
                'country_id' => 180,
            ),
            272 => 
            array (
                'id' => 3396,
                'name' => 'Prahova',
                'country_id' => 180,
            ),
            273 => 
            array (
                'id' => 3397,
                'name' => 'Sibiu',
                'country_id' => 180,
            ),
            274 => 
            array (
                'id' => 3398,
                'name' => 'Salaj',
                'country_id' => 180,
            ),
            275 => 
            array (
                'id' => 3399,
                'name' => 'Satu Mare',
                'country_id' => 180,
            ),
            276 => 
            array (
                'id' => 3400,
                'name' => 'Suceava',
                'country_id' => 180,
            ),
            277 => 
            array (
                'id' => 3401,
                'name' => 'Tulcea',
                'country_id' => 180,
            ),
            278 => 
            array (
                'id' => 3402,
                'name' => 'Timis',
                'country_id' => 180,
            ),
            279 => 
            array (
                'id' => 3403,
                'name' => 'Teleorman',
                'country_id' => 180,
            ),
            280 => 
            array (
                'id' => 3404,
                'name' => 'Vâlcea',
                'country_id' => 180,
            ),
            281 => 
            array (
                'id' => 3405,
                'name' => 'Vrancea',
                'country_id' => 180,
            ),
            282 => 
            array (
                'id' => 3406,
                'name' => 'Vaslui',
                'country_id' => 180,
            ),
            283 => 
            array (
                'id' => 3407,
                'name' => 'Beograd',
                'country_id' => 193,
            ),
            284 => 
            array (
                'id' => 3408,
                'name' => 'Severnobacki okrug',
                'country_id' => 193,
            ),
            285 => 
            array (
                'id' => 3409,
                'name' => 'Srednjebanatski okrug',
                'country_id' => 193,
            ),
            286 => 
            array (
                'id' => 3410,
                'name' => 'Severnobanatski okrug',
                'country_id' => 193,
            ),
            287 => 
            array (
                'id' => 3411,
                'name' => 'Južnobanatski okrug',
                'country_id' => 193,
            ),
            288 => 
            array (
                'id' => 3412,
                'name' => 'Zapadnobacki okrug',
                'country_id' => 193,
            ),
            289 => 
            array (
                'id' => 3413,
                'name' => 'Južnobacki okrug',
                'country_id' => 193,
            ),
            290 => 
            array (
                'id' => 3414,
                'name' => 'Sremski okrug',
                'country_id' => 193,
            ),
            291 => 
            array (
                'id' => 3415,
                'name' => 'Macvanski okrug',
                'country_id' => 193,
            ),
            292 => 
            array (
                'id' => 3416,
                'name' => 'Kolubarski okrug',
                'country_id' => 193,
            ),
            293 => 
            array (
                'id' => 3417,
                'name' => 'Podunavski okrug',
                'country_id' => 193,
            ),
            294 => 
            array (
                'id' => 3418,
                'name' => 'Branicevski okrug',
                'country_id' => 193,
            ),
            295 => 
            array (
                'id' => 3419,
                'name' => 'Šumadijski okrug',
                'country_id' => 193,
            ),
            296 => 
            array (
                'id' => 3420,
                'name' => 'Pomoravski okrug',
                'country_id' => 193,
            ),
            297 => 
            array (
                'id' => 3421,
                'name' => 'Borski okrug',
                'country_id' => 193,
            ),
            298 => 
            array (
                'id' => 3422,
                'name' => 'Zajecarski okrug',
                'country_id' => 193,
            ),
            299 => 
            array (
                'id' => 3423,
                'name' => 'Zlatiborski okrug',
                'country_id' => 193,
            ),
            300 => 
            array (
                'id' => 3424,
                'name' => 'Moravicki okrug',
                'country_id' => 193,
            ),
            301 => 
            array (
                'id' => 3425,
                'name' => 'Raški okrug',
                'country_id' => 193,
            ),
            302 => 
            array (
                'id' => 3426,
                'name' => 'Rasinski okrug',
                'country_id' => 193,
            ),
            303 => 
            array (
                'id' => 3427,
                'name' => 'Nišavski okrug',
                'country_id' => 193,
            ),
            304 => 
            array (
                'id' => 3428,
                'name' => 'Toplicki okrug',
                'country_id' => 193,
            ),
            305 => 
            array (
                'id' => 3429,
                'name' => 'Pirotski okrug',
                'country_id' => 193,
            ),
            306 => 
            array (
                'id' => 3430,
                'name' => 'Jablanicki okrug',
                'country_id' => 193,
            ),
            307 => 
            array (
                'id' => 3431,
                'name' => 'Pcinjski okrug',
                'country_id' => 193,
            ),
            308 => 
            array (
                'id' => 3432,
                'name' => 'Kosovski okrug',
                'country_id' => 193,
            ),
            309 => 
            array (
                'id' => 3433,
                'name' => 'Pecki okrug',
                'country_id' => 193,
            ),
            310 => 
            array (
                'id' => 3434,
                'name' => 'Prizrenski okrug',
                'country_id' => 193,
            ),
            311 => 
            array (
                'id' => 3435,
                'name' => 'Kosovsko-Mitrovacki okrug',
                'country_id' => 193,
            ),
            312 => 
            array (
                'id' => 3436,
                'name' => 'Kosovsko-Pomoravski okrug',
                'country_id' => 193,
            ),
            313 => 
            array (
                'id' => 3437,
                'name' => 'Kosovo-Metohija',
                'country_id' => 193,
            ),
            314 => 
            array (
                'id' => 3438,
                'name' => 'Vojvodina',
                'country_id' => 193,
            ),
            315 => 
            array (
                'id' => 3439,
                'name' => '"Adygeya',
                'country_id' => 181,
            ),
            316 => 
            array (
                'id' => 3440,
                'name' => '"Altay',
                'country_id' => 181,
            ),
            317 => 
            array (
                'id' => 3441,
                'name' => 'Altayskiy kray',
                'country_id' => 181,
            ),
            318 => 
            array (
                'id' => 3442,
                'name' => 'Amurskaya oblast\'',
                'country_id' => 181,
            ),
            319 => 
            array (
                'id' => 3443,
                'name' => '"Arkhangel\'skaya oblast',
                'country_id' => 181,
            ),
            320 => 
            array (
                'id' => 3444,
                'name' => 'Astrakhanskaya oblast\'',
                'country_id' => 181,
            ),
            321 => 
            array (
                'id' => 3445,
                'name' => '"Bashkortostan',
                'country_id' => 181,
            ),
            322 => 
            array (
                'id' => 3446,
                'name' => 'Belgorodskaya oblast\'',
                'country_id' => 181,
            ),
            323 => 
            array (
                'id' => 3447,
                'name' => 'Bryanskaya oblast\'',
                'country_id' => 181,
            ),
            324 => 
            array (
                'id' => 3448,
                'name' => '"Buryatiya',
                'country_id' => 181,
            ),
            325 => 
            array (
                'id' => 3449,
                'name' => 'Chechenskaya Respublika',
                'country_id' => 181,
            ),
            326 => 
            array (
                'id' => 3450,
                'name' => 'Chelyabinskaya oblast\'',
                'country_id' => 181,
            ),
            327 => 
            array (
                'id' => 3451,
                'name' => 'Chukotskiy avtonomnyy okrug',
                'country_id' => 181,
            ),
            328 => 
            array (
                'id' => 3452,
                'name' => 'Chuvashskaya Respublika',
                'country_id' => 181,
            ),
            329 => 
            array (
                'id' => 3453,
                'name' => '"Dagestan',
                'country_id' => 181,
            ),
            330 => 
            array (
                'id' => 3454,
                'name' => 'Ingushskaya Respublika [Respublika Ingushetiya]',
                'country_id' => 181,
            ),
            331 => 
            array (
                'id' => 3455,
                'name' => 'Irkutskaya oblast\'',
                'country_id' => 181,
            ),
            332 => 
            array (
                'id' => 3456,
                'name' => 'Ivanovskaya oblast\'',
                'country_id' => 181,
            ),
            333 => 
            array (
                'id' => 3457,
                'name' => 'Kamchatskaya oblast\'',
                'country_id' => 181,
            ),
            334 => 
            array (
                'id' => 3458,
                'name' => 'Kabardino-Balkarskaya Respublika',
                'country_id' => 181,
            ),
            335 => 
            array (
                'id' => 3459,
                'name' => 'Karachayevo-Cherkesskaya Respublika',
                'country_id' => 181,
            ),
            336 => 
            array (
                'id' => 3460,
                'name' => 'Krasnodarskiy kray',
                'country_id' => 181,
            ),
            337 => 
            array (
                'id' => 3461,
                'name' => 'Kemerovskaya oblast\'',
                'country_id' => 181,
            ),
            338 => 
            array (
                'id' => 3462,
                'name' => '"Kaliningradskaya oblast',
                'country_id' => 181,
            ),
            339 => 
            array (
                'id' => 3463,
                'name' => 'Kurganskaya oblast\'',
                'country_id' => 181,
            ),
            340 => 
            array (
                'id' => 3464,
                'name' => 'Khabarovskiy kray',
                'country_id' => 181,
            ),
            341 => 
            array (
                'id' => 3465,
                'name' => 'Khanty-Mansiyskiy avtonomnyy okrug',
                'country_id' => 181,
            ),
            342 => 
            array (
                'id' => 3466,
                'name' => 'Kirovskaya oblast\'',
                'country_id' => 181,
            ),
            343 => 
            array (
                'id' => 3467,
                'name' => '"Khakasiya',
                'country_id' => 181,
            ),
            344 => 
            array (
                'id' => 3468,
                'name' => '"Kalmykiya',
                'country_id' => 181,
            ),
            345 => 
            array (
                'id' => 3469,
                'name' => 'Kaluzhskaya oblast\'',
                'country_id' => 181,
            ),
            346 => 
            array (
                'id' => 3470,
                'name' => '"Komi',
                'country_id' => 181,
            ),
            347 => 
            array (
                'id' => 3471,
                'name' => 'Kostromskaya oblast\'',
                'country_id' => 181,
            ),
            348 => 
            array (
                'id' => 3472,
                'name' => '"Kareliya',
                'country_id' => 181,
            ),
            349 => 
            array (
                'id' => 3473,
                'name' => 'Kurskaya oblast\'',
                'country_id' => 181,
            ),
            350 => 
            array (
                'id' => 3474,
                'name' => 'Krasnoyarskiy kray',
                'country_id' => 181,
            ),
            351 => 
            array (
                'id' => 3475,
                'name' => 'Leningradskaya oblast\'',
                'country_id' => 181,
            ),
            352 => 
            array (
                'id' => 3476,
                'name' => 'Lipetskaya oblast\'',
                'country_id' => 181,
            ),
            353 => 
            array (
                'id' => 3477,
                'name' => 'Magadanskaya oblast\'',
                'country_id' => 181,
            ),
            354 => 
            array (
                'id' => 3478,
                'name' => '"Mariy El',
                'country_id' => 181,
            ),
            355 => 
            array (
                'id' => 3479,
                'name' => '"Mordoviya',
                'country_id' => 181,
            ),
            356 => 
            array (
                'id' => 3480,
                'name' => 'Moskovskaya oblast\'',
                'country_id' => 181,
            ),
            357 => 
            array (
                'id' => 3481,
                'name' => 'Moskva',
                'country_id' => 181,
            ),
            358 => 
            array (
                'id' => 3482,
                'name' => 'Murmanskaya oblast\'',
                'country_id' => 181,
            ),
            359 => 
            array (
                'id' => 3483,
                'name' => 'Nenetskiy avtonomnyy okrug',
                'country_id' => 181,
            ),
            360 => 
            array (
                'id' => 3484,
                'name' => 'Novgorodskaya oblast\'',
                'country_id' => 181,
            ),
            361 => 
            array (
                'id' => 3485,
                'name' => 'Nizhegorodskaya oblast\'',
                'country_id' => 181,
            ),
            362 => 
            array (
                'id' => 3486,
                'name' => 'Novosibirskaya oblast\'',
                'country_id' => 181,
            ),
            363 => 
            array (
                'id' => 3487,
                'name' => 'Omskaya oblast\'',
                'country_id' => 181,
            ),
            364 => 
            array (
                'id' => 3488,
                'name' => 'Orenburgskaya oblast\'',
                'country_id' => 181,
            ),
            365 => 
            array (
                'id' => 3489,
                'name' => 'Orlovskaya oblast\'',
                'country_id' => 181,
            ),
            366 => 
            array (
                'id' => 3490,
                'name' => 'Perm',
                'country_id' => 181,
            ),
            367 => 
            array (
                'id' => 3491,
                'name' => 'Penzenskaya oblast\'',
                'country_id' => 181,
            ),
            368 => 
            array (
                'id' => 3492,
                'name' => 'Primorskiy kray',
                'country_id' => 181,
            ),
            369 => 
            array (
                'id' => 3493,
                'name' => 'Pskovskaya oblast\'',
                'country_id' => 181,
            ),
            370 => 
            array (
                'id' => 3494,
                'name' => 'Rostovskaya oblast\'',
                'country_id' => 181,
            ),
            371 => 
            array (
                'id' => 3495,
                'name' => 'Ryazanskaya oblast\'',
                'country_id' => 181,
            ),
            372 => 
            array (
                'id' => 3496,
                'name' => '"Sakha',
                'country_id' => 181,
            ),
            373 => 
            array (
                'id' => 3497,
                'name' => 'Sakhalinskaya oblast\'',
                'country_id' => 181,
            ),
            374 => 
            array (
                'id' => 3498,
                'name' => 'Samarskaya oblast\'',
                'country_id' => 181,
            ),
            375 => 
            array (
                'id' => 3499,
                'name' => 'Saratovskaya oblast\'',
                'country_id' => 181,
            ),
            376 => 
            array (
                'id' => 3500,
                'name' => '"Severnaya Osetiya',
                'country_id' => 181,
            ),
            377 => 
            array (
                'id' => 3501,
                'name' => 'Smolenskaya oblast\'',
                'country_id' => 181,
            ),
            378 => 
            array (
                'id' => 3502,
                'name' => 'Sankt-Peterburg',
                'country_id' => 181,
            ),
            379 => 
            array (
                'id' => 3503,
                'name' => 'Stavropol\'skiy kray',
                'country_id' => 181,
            ),
            380 => 
            array (
                'id' => 3504,
                'name' => 'Sverdlovskaya oblast\'',
                'country_id' => 181,
            ),
            381 => 
            array (
                'id' => 3505,
                'name' => '"Tatarstan',
                'country_id' => 181,
            ),
            382 => 
            array (
                'id' => 3506,
                'name' => 'Tambovskaya oblast\'',
                'country_id' => 181,
            ),
            383 => 
            array (
                'id' => 3507,
                'name' => 'Tomskaya oblast\'',
                'country_id' => 181,
            ),
            384 => 
            array (
                'id' => 3508,
                'name' => 'Tul\'skaya oblast\'',
                'country_id' => 181,
            ),
            385 => 
            array (
                'id' => 3509,
                'name' => 'Tverskaya oblast\'',
                'country_id' => 181,
            ),
            386 => 
            array (
                'id' => 3510,
                'name' => '"Tyva',
                'country_id' => 181,
            ),
            387 => 
            array (
                'id' => 3511,
                'name' => 'Tyumenskaya oblast\'',
                'country_id' => 181,
            ),
            388 => 
            array (
                'id' => 3512,
                'name' => 'Udmurtskaya Respublika',
                'country_id' => 181,
            ),
            389 => 
            array (
                'id' => 3513,
                'name' => 'Ul\'yanovskaya oblast\'',
                'country_id' => 181,
            ),
            390 => 
            array (
                'id' => 3514,
                'name' => 'Volgogradskaya oblast\'',
                'country_id' => 181,
            ),
            391 => 
            array (
                'id' => 3515,
                'name' => 'Vladimirskaya oblast\'',
                'country_id' => 181,
            ),
            392 => 
            array (
                'id' => 3516,
                'name' => 'Vologodskaya oblast\'',
                'country_id' => 181,
            ),
            393 => 
            array (
                'id' => 3517,
                'name' => 'Voronezhskaya oblast\'',
                'country_id' => 181,
            ),
            394 => 
            array (
                'id' => 3518,
                'name' => 'Yamalo-Nenetskiy avtonomnyy okrug',
                'country_id' => 181,
            ),
            395 => 
            array (
                'id' => 3519,
                'name' => 'Yaroslavskaya oblast\'',
                'country_id' => 181,
            ),
            396 => 
            array (
                'id' => 3520,
                'name' => 'Yevreyskaya avtonomnaya oblast\'',
                'country_id' => 181,
            ),
            397 => 
            array (
                'id' => 3521,
                'name' => 'Zabaykal\'skiy kray',
                'country_id' => 181,
            ),
            398 => 
            array (
                'id' => 3522,
                'name' => 'Ville de Kigali',
                'country_id' => 182,
            ),
            399 => 
            array (
                'id' => 3523,
                'name' => 'Est',
                'country_id' => 182,
            ),
            400 => 
            array (
                'id' => 3524,
                'name' => 'Nord',
                'country_id' => 182,
            ),
            401 => 
            array (
                'id' => 3525,
                'name' => 'Ouest',
                'country_id' => 182,
            ),
            402 => 
            array (
                'id' => 3526,
                'name' => 'Sud',
                'country_id' => 182,
            ),
            403 => 
            array (
                'id' => 3539,
                'name' => '\'Asir',
                'country_id' => 191,
            ),
            404 => 
            array (
                'id' => 3540,
                'name' => 'Central',
                'country_id' => 199,
            ),
            405 => 
            array (
                'id' => 3541,
                'name' => 'Choiseul',
                'country_id' => 199,
            ),
            406 => 
            array (
                'id' => 3542,
            'name' => 'Capital Territory (Honiara)',
                'country_id' => 199,
            ),
            407 => 
            array (
                'id' => 3543,
                'name' => 'Guadalcanal',
                'country_id' => 199,
            ),
            408 => 
            array (
                'id' => 3544,
                'name' => 'Isabel',
                'country_id' => 199,
            ),
            409 => 
            array (
                'id' => 3545,
                'name' => 'Makira-Ulawa',
                'country_id' => 199,
            ),
            410 => 
            array (
                'id' => 3546,
                'name' => 'Malaita',
                'country_id' => 199,
            ),
            411 => 
            array (
                'id' => 3547,
                'name' => 'Rennell and Bellona',
                'country_id' => 199,
            ),
            412 => 
            array (
                'id' => 3548,
                'name' => 'Temotu',
                'country_id' => 199,
            ),
            413 => 
            array (
                'id' => 3549,
                'name' => 'Western',
                'country_id' => 199,
            ),
            414 => 
            array (
                'id' => 3550,
                'name' => 'Anse aux Pins',
                'country_id' => 194,
            ),
            415 => 
            array (
                'id' => 3551,
                'name' => 'Anse Boileau         ',
                'country_id' => 194,
            ),
            416 => 
            array (
                'id' => 3552,
                'name' => 'Anse Étoile          ',
                'country_id' => 194,
            ),
            417 => 
            array (
                'id' => 3553,
                'name' => 'Au Cap',
                'country_id' => 194,
            ),
            418 => 
            array (
                'id' => 3554,
                'name' => 'Anse Royale          ',
                'country_id' => 194,
            ),
            419 => 
            array (
                'id' => 3555,
                'name' => 'Baie Lazare          ',
                'country_id' => 194,
            ),
            420 => 
            array (
                'id' => 3556,
                'name' => 'Baie Sainte Anne     ',
                'country_id' => 194,
            ),
            421 => 
            array (
                'id' => 3557,
                'name' => 'Beau Vallon          ',
                'country_id' => 194,
            ),
            422 => 
            array (
                'id' => 3558,
                'name' => 'Bel Air              ',
                'country_id' => 194,
            ),
            423 => 
            array (
                'id' => 3559,
                'name' => 'Bel Ombre            ',
                'country_id' => 194,
            ),
            424 => 
            array (
                'id' => 3560,
                'name' => 'Cascade              ',
                'country_id' => 194,
            ),
            425 => 
            array (
                'id' => 3561,
                'name' => 'Glacis               ',
                'country_id' => 194,
            ),
            426 => 
            array (
                'id' => 3562,
                'name' => 'Grand\'Anse Mahé',
                'country_id' => 194,
            ),
            427 => 
            array (
                'id' => 3563,
                'name' => 'Grand\'Anse Praslin',
                'country_id' => 194,
            ),
            428 => 
            array (
                'id' => 3564,
                'name' => 'La Digue             ',
                'country_id' => 194,
            ),
            429 => 
            array (
                'id' => 3565,
                'name' => 'La Rivière Anglaise  ',
                'country_id' => 194,
            ),
            430 => 
            array (
                'id' => 3566,
                'name' => 'Mont Buxton          ',
                'country_id' => 194,
            ),
            431 => 
            array (
                'id' => 3567,
                'name' => 'Mont Fleuri          ',
                'country_id' => 194,
            ),
            432 => 
            array (
                'id' => 3568,
                'name' => 'Plaisance            ',
                'country_id' => 194,
            ),
            433 => 
            array (
                'id' => 3569,
                'name' => 'Pointe La Rue        ',
                'country_id' => 194,
            ),
            434 => 
            array (
                'id' => 3570,
                'name' => 'Port Glaud           ',
                'country_id' => 194,
            ),
            435 => 
            array (
                'id' => 3571,
                'name' => 'Saint Louis          ',
                'country_id' => 194,
            ),
            436 => 
            array (
                'id' => 3572,
                'name' => 'Takamaka             ',
                'country_id' => 194,
            ),
            437 => 
            array (
                'id' => 3573,
                'name' => 'Lemamel',
                'country_id' => 194,
            ),
            438 => 
            array (
                'id' => 3574,
                'name' => 'Ros Kaiman',
                'country_id' => 194,
            ),
            439 => 
            array (
                'id' => 3575,
                'name' => 'Zalingei',
                'country_id' => 205,
            ),
            440 => 
            array (
                'id' => 3576,
                'name' => 'Sharq Darfur',
                'country_id' => 205,
            ),
            441 => 
            array (
                'id' => 3577,
                'name' => 'Shamal Darfur',
                'country_id' => 205,
            ),
            442 => 
            array (
                'id' => 3578,
                'name' => 'Janub Darfur',
                'country_id' => 205,
            ),
            443 => 
            array (
                'id' => 3579,
                'name' => 'Gharb Darfur',
                'country_id' => 205,
            ),
            444 => 
            array (
                'id' => 3580,
                'name' => 'Al Qadarif',
                'country_id' => 205,
            ),
            445 => 
            array (
                'id' => 3581,
                'name' => 'Al Jazirah',
                'country_id' => 205,
            ),
            446 => 
            array (
                'id' => 3582,
                'name' => 'Kassala',
                'country_id' => 205,
            ),
            447 => 
            array (
                'id' => 3584,
                'name' => 'Shamal Kurdufan',
                'country_id' => 205,
            ),
            448 => 
            array (
                'id' => 3585,
                'name' => 'Janub Kurdufan',
                'country_id' => 205,
            ),
            449 => 
            array (
                'id' => 3586,
                'name' => 'An Nil al Azraq',
                'country_id' => 205,
            ),
            450 => 
            array (
                'id' => 3588,
                'name' => 'An Nil',
                'country_id' => 205,
            ),
            451 => 
            array (
                'id' => 3589,
                'name' => 'An Nil al Abyad',
                'country_id' => 205,
            ),
            452 => 
            array (
                'id' => 3590,
                'name' => 'Al Bahr al Ahmar',
                'country_id' => 205,
            ),
            453 => 
            array (
                'id' => 3591,
                'name' => 'Sinnar',
                'country_id' => 205,
            ),
            454 => 
            array (
                'id' => 3592,
                'name' => 'Stockholms län',
                'country_id' => 208,
            ),
            455 => 
            array (
                'id' => 3593,
                'name' => 'Västerbottens län',
                'country_id' => 208,
            ),
            456 => 
            array (
                'id' => 3594,
                'name' => 'Norrbottens län',
                'country_id' => 208,
            ),
            457 => 
            array (
                'id' => 3595,
                'name' => 'Uppsala län',
                'country_id' => 208,
            ),
            458 => 
            array (
                'id' => 3596,
                'name' => 'Södermanlands län',
                'country_id' => 208,
            ),
            459 => 
            array (
                'id' => 3597,
                'name' => 'Östergötlands län',
                'country_id' => 208,
            ),
            460 => 
            array (
                'id' => 3598,
                'name' => 'Jönköpings län',
                'country_id' => 208,
            ),
            461 => 
            array (
                'id' => 3599,
                'name' => 'Kronoborgs län',
                'country_id' => 208,
            ),
            462 => 
            array (
                'id' => 3600,
                'name' => 'Kalmar län',
                'country_id' => 208,
            ),
            463 => 
            array (
                'id' => 3601,
                'name' => 'Gotlands län',
                'country_id' => 208,
            ),
            464 => 
            array (
                'id' => 3602,
                'name' => 'Blekinge län',
                'country_id' => 208,
            ),
            465 => 
            array (
                'id' => 3603,
                'name' => 'Skåne län',
                'country_id' => 208,
            ),
            466 => 
            array (
                'id' => 3604,
                'name' => 'Hallands län',
                'country_id' => 208,
            ),
            467 => 
            array (
                'id' => 3605,
                'name' => 'Västra Götalands län',
                'country_id' => 208,
            ),
            468 => 
            array (
                'id' => 3606,
                'name' => 'Värmlands län',
                'country_id' => 208,
            ),
            469 => 
            array (
                'id' => 3607,
                'name' => 'Örebro län',
                'country_id' => 208,
            ),
            470 => 
            array (
                'id' => 3608,
                'name' => 'Västmanlands län',
                'country_id' => 208,
            ),
            471 => 
            array (
                'id' => 3609,
                'name' => 'Dalarnes län',
                'country_id' => 208,
            ),
            472 => 
            array (
                'id' => 3610,
                'name' => 'Gävleborgs län',
                'country_id' => 208,
            ),
            473 => 
            array (
                'id' => 3611,
                'name' => 'Västernorrlands län',
                'country_id' => 208,
            ),
            474 => 
            array (
                'id' => 3612,
                'name' => 'Jämtlands län',
                'country_id' => 208,
            ),
            475 => 
            array (
                'id' => 3613,
                'name' => 'Central Singapore',
                'country_id' => 196,
            ),
            476 => 
            array (
                'id' => 3614,
                'name' => 'North East',
                'country_id' => 196,
            ),
            477 => 
            array (
                'id' => 3615,
                'name' => 'North West',
                'country_id' => 196,
            ),
            478 => 
            array (
                'id' => 3616,
                'name' => 'South East',
                'country_id' => 196,
            ),
            479 => 
            array (
                'id' => 3617,
                'name' => 'South West',
                'country_id' => 196,
            ),
            480 => 
            array (
                'id' => 3618,
                'name' => 'Ascension',
                'country_id' => 183,
            ),
            481 => 
            array (
                'id' => 3619,
                'name' => 'Saint Helena',
                'country_id' => 183,
            ),
            482 => 
            array (
                'id' => 3620,
                'name' => 'Tristan da Cunha',
                'country_id' => 183,
            ),
            483 => 
            array (
                'id' => 3621,
                'name' => 'Ajdovšcina',
                'country_id' => 198,
            ),
            484 => 
            array (
                'id' => 3622,
                'name' => 'Beltinci',
                'country_id' => 198,
            ),
            485 => 
            array (
                'id' => 3623,
                'name' => 'Bled',
                'country_id' => 198,
            ),
            486 => 
            array (
                'id' => 3624,
                'name' => 'Bohinj',
                'country_id' => 198,
            ),
            487 => 
            array (
                'id' => 3625,
                'name' => 'Borovnica',
                'country_id' => 198,
            ),
            488 => 
            array (
                'id' => 3626,
                'name' => 'Bovec',
                'country_id' => 198,
            ),
            489 => 
            array (
                'id' => 3627,
                'name' => 'Brda',
                'country_id' => 198,
            ),
            490 => 
            array (
                'id' => 3628,
                'name' => 'Brezovica',
                'country_id' => 198,
            ),
            491 => 
            array (
                'id' => 3629,
                'name' => 'Brežice',
                'country_id' => 198,
            ),
            492 => 
            array (
                'id' => 3630,
                'name' => 'Tišina',
                'country_id' => 198,
            ),
            493 => 
            array (
                'id' => 3631,
                'name' => 'Celje',
                'country_id' => 198,
            ),
            494 => 
            array (
                'id' => 3632,
                'name' => 'Cerklje na Gorenjskem',
                'country_id' => 198,
            ),
            495 => 
            array (
                'id' => 3633,
                'name' => 'Cerknica',
                'country_id' => 198,
            ),
            496 => 
            array (
                'id' => 3634,
                'name' => 'Cerkno',
                'country_id' => 198,
            ),
            497 => 
            array (
                'id' => 3635,
                'name' => 'Crenšovci',
                'country_id' => 198,
            ),
            498 => 
            array (
                'id' => 3636,
                'name' => 'Crna na Koroškem',
                'country_id' => 198,
            ),
            499 => 
            array (
                'id' => 3637,
                'name' => 'Crnomelj',
                'country_id' => 198,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 3638,
                'name' => 'Destrnik',
                'country_id' => 198,
            ),
            1 => 
            array (
                'id' => 3639,
                'name' => 'Divaca',
                'country_id' => 198,
            ),
            2 => 
            array (
                'id' => 3640,
                'name' => 'Dobrepolje',
                'country_id' => 198,
            ),
            3 => 
            array (
                'id' => 3641,
                'name' => 'Dobrova-Polhov Gradec',
                'country_id' => 198,
            ),
            4 => 
            array (
                'id' => 3642,
                'name' => 'Dol pri Ljubljani',
                'country_id' => 198,
            ),
            5 => 
            array (
                'id' => 3643,
                'name' => 'Domžale',
                'country_id' => 198,
            ),
            6 => 
            array (
                'id' => 3644,
                'name' => 'Dornava',
                'country_id' => 198,
            ),
            7 => 
            array (
                'id' => 3645,
                'name' => 'Dravograd',
                'country_id' => 198,
            ),
            8 => 
            array (
                'id' => 3646,
                'name' => 'Duplek',
                'country_id' => 198,
            ),
            9 => 
            array (
                'id' => 3647,
                'name' => 'Gorenja vas-Poljane',
                'country_id' => 198,
            ),
            10 => 
            array (
                'id' => 3648,
                'name' => 'Gorišnica',
                'country_id' => 198,
            ),
            11 => 
            array (
                'id' => 3649,
                'name' => 'Gornja Radgona',
                'country_id' => 198,
            ),
            12 => 
            array (
                'id' => 3650,
                'name' => 'Gornji Grad',
                'country_id' => 198,
            ),
            13 => 
            array (
                'id' => 3651,
                'name' => 'Gornji Petrovci',
                'country_id' => 198,
            ),
            14 => 
            array (
                'id' => 3652,
                'name' => 'Grosuplje',
                'country_id' => 198,
            ),
            15 => 
            array (
                'id' => 3653,
                'name' => 'Šalovci',
                'country_id' => 198,
            ),
            16 => 
            array (
                'id' => 3654,
                'name' => 'Hrastnik',
                'country_id' => 198,
            ),
            17 => 
            array (
                'id' => 3655,
                'name' => 'Hrpelje-Kozina',
                'country_id' => 198,
            ),
            18 => 
            array (
                'id' => 3656,
                'name' => 'Idrija',
                'country_id' => 198,
            ),
            19 => 
            array (
                'id' => 3657,
                'name' => 'Ig',
                'country_id' => 198,
            ),
            20 => 
            array (
                'id' => 3658,
                'name' => 'Ilirska Bistrica',
                'country_id' => 198,
            ),
            21 => 
            array (
                'id' => 3659,
                'name' => 'Ivancna Gorica',
                'country_id' => 198,
            ),
            22 => 
            array (
                'id' => 3660,
                'name' => 'Izola',
                'country_id' => 198,
            ),
            23 => 
            array (
                'id' => 3661,
                'name' => 'Jesenice',
                'country_id' => 198,
            ),
            24 => 
            array (
                'id' => 3662,
                'name' => 'Juršinci',
                'country_id' => 198,
            ),
            25 => 
            array (
                'id' => 3663,
                'name' => 'Kamnik',
                'country_id' => 198,
            ),
            26 => 
            array (
                'id' => 3664,
                'name' => 'Kanal',
                'country_id' => 198,
            ),
            27 => 
            array (
                'id' => 3665,
                'name' => 'Kidricevo',
                'country_id' => 198,
            ),
            28 => 
            array (
                'id' => 3666,
                'name' => 'Kobarid',
                'country_id' => 198,
            ),
            29 => 
            array (
                'id' => 3667,
                'name' => 'Kobilje',
                'country_id' => 198,
            ),
            30 => 
            array (
                'id' => 3668,
                'name' => 'Kocevje',
                'country_id' => 198,
            ),
            31 => 
            array (
                'id' => 3669,
                'name' => 'Komen',
                'country_id' => 198,
            ),
            32 => 
            array (
                'id' => 3670,
                'name' => 'Koper',
                'country_id' => 198,
            ),
            33 => 
            array (
                'id' => 3671,
                'name' => 'Kozje',
                'country_id' => 198,
            ),
            34 => 
            array (
                'id' => 3672,
                'name' => 'Kranj',
                'country_id' => 198,
            ),
            35 => 
            array (
                'id' => 3673,
                'name' => 'Kranjska Gora',
                'country_id' => 198,
            ),
            36 => 
            array (
                'id' => 3674,
                'name' => 'Krško',
                'country_id' => 198,
            ),
            37 => 
            array (
                'id' => 3675,
                'name' => 'Kungota',
                'country_id' => 198,
            ),
            38 => 
            array (
                'id' => 3676,
                'name' => 'Kuzma',
                'country_id' => 198,
            ),
            39 => 
            array (
                'id' => 3677,
                'name' => 'Laško',
                'country_id' => 198,
            ),
            40 => 
            array (
                'id' => 3678,
                'name' => 'Lenart',
                'country_id' => 198,
            ),
            41 => 
            array (
                'id' => 3679,
                'name' => 'Lendava',
                'country_id' => 198,
            ),
            42 => 
            array (
                'id' => 3680,
                'name' => 'Litija',
                'country_id' => 198,
            ),
            43 => 
            array (
                'id' => 3681,
                'name' => 'Ljubljana',
                'country_id' => 198,
            ),
            44 => 
            array (
                'id' => 3682,
                'name' => 'Ljubno',
                'country_id' => 198,
            ),
            45 => 
            array (
                'id' => 3683,
                'name' => 'Ljutomer',
                'country_id' => 198,
            ),
            46 => 
            array (
                'id' => 3684,
                'name' => 'Logatec',
                'country_id' => 198,
            ),
            47 => 
            array (
                'id' => 3685,
                'name' => 'Loška dolina',
                'country_id' => 198,
            ),
            48 => 
            array (
                'id' => 3686,
                'name' => 'Loški Potok',
                'country_id' => 198,
            ),
            49 => 
            array (
                'id' => 3687,
                'name' => 'Luce',
                'country_id' => 198,
            ),
            50 => 
            array (
                'id' => 3688,
                'name' => 'Lukovica',
                'country_id' => 198,
            ),
            51 => 
            array (
                'id' => 3689,
                'name' => 'Majšperk',
                'country_id' => 198,
            ),
            52 => 
            array (
                'id' => 3690,
                'name' => 'Maribor',
                'country_id' => 198,
            ),
            53 => 
            array (
                'id' => 3691,
                'name' => 'Medvode',
                'country_id' => 198,
            ),
            54 => 
            array (
                'id' => 3692,
                'name' => 'Mengeš',
                'country_id' => 198,
            ),
            55 => 
            array (
                'id' => 3693,
                'name' => 'Metlika',
                'country_id' => 198,
            ),
            56 => 
            array (
                'id' => 3694,
                'name' => 'Mežica',
                'country_id' => 198,
            ),
            57 => 
            array (
                'id' => 3695,
                'name' => 'Miren-Kostanjevica',
                'country_id' => 198,
            ),
            58 => 
            array (
                'id' => 3696,
                'name' => 'Mislinja',
                'country_id' => 198,
            ),
            59 => 
            array (
                'id' => 3697,
                'name' => 'Moravce',
                'country_id' => 198,
            ),
            60 => 
            array (
                'id' => 3698,
                'name' => 'Moravske Toplice',
                'country_id' => 198,
            ),
            61 => 
            array (
                'id' => 3699,
                'name' => 'Mozirje',
                'country_id' => 198,
            ),
            62 => 
            array (
                'id' => 3700,
                'name' => 'Murska Sobota',
                'country_id' => 198,
            ),
            63 => 
            array (
                'id' => 3701,
                'name' => 'Muta',
                'country_id' => 198,
            ),
            64 => 
            array (
                'id' => 3702,
                'name' => 'Naklo',
                'country_id' => 198,
            ),
            65 => 
            array (
                'id' => 3703,
                'name' => 'Nazarje',
                'country_id' => 198,
            ),
            66 => 
            array (
                'id' => 3704,
                'name' => 'Nova Gorica',
                'country_id' => 198,
            ),
            67 => 
            array (
                'id' => 3705,
                'name' => 'Novo mesto',
                'country_id' => 198,
            ),
            68 => 
            array (
                'id' => 3706,
                'name' => 'Odranci',
                'country_id' => 198,
            ),
            69 => 
            array (
                'id' => 3707,
                'name' => 'Ormož',
                'country_id' => 198,
            ),
            70 => 
            array (
                'id' => 3708,
                'name' => 'Osilnica',
                'country_id' => 198,
            ),
            71 => 
            array (
                'id' => 3709,
                'name' => 'Pesnica',
                'country_id' => 198,
            ),
            72 => 
            array (
                'id' => 3710,
                'name' => 'Piran',
                'country_id' => 198,
            ),
            73 => 
            array (
                'id' => 3711,
                'name' => 'Pivka',
                'country_id' => 198,
            ),
            74 => 
            array (
                'id' => 3712,
                'name' => 'Podcetrtek',
                'country_id' => 198,
            ),
            75 => 
            array (
                'id' => 3713,
                'name' => 'Podvelka',
                'country_id' => 198,
            ),
            76 => 
            array (
                'id' => 3714,
                'name' => 'Postojna',
                'country_id' => 198,
            ),
            77 => 
            array (
                'id' => 3715,
                'name' => 'Preddvor',
                'country_id' => 198,
            ),
            78 => 
            array (
                'id' => 3716,
                'name' => 'Ptuj',
                'country_id' => 198,
            ),
            79 => 
            array (
                'id' => 3717,
                'name' => 'Puconci',
                'country_id' => 198,
            ),
            80 => 
            array (
                'id' => 3718,
                'name' => 'Race-Fram',
                'country_id' => 198,
            ),
            81 => 
            array (
                'id' => 3719,
                'name' => 'Radece',
                'country_id' => 198,
            ),
            82 => 
            array (
                'id' => 3720,
                'name' => 'Radenci',
                'country_id' => 198,
            ),
            83 => 
            array (
                'id' => 3721,
                'name' => 'Radlje ob Dravi',
                'country_id' => 198,
            ),
            84 => 
            array (
                'id' => 3722,
                'name' => 'Radovljica',
                'country_id' => 198,
            ),
            85 => 
            array (
                'id' => 3723,
                'name' => 'Ravne na Koroškem',
                'country_id' => 198,
            ),
            86 => 
            array (
                'id' => 3724,
                'name' => 'Ribnica',
                'country_id' => 198,
            ),
            87 => 
            array (
                'id' => 3725,
                'name' => 'Rogašovci',
                'country_id' => 198,
            ),
            88 => 
            array (
                'id' => 3726,
                'name' => 'Rogaška Slatina',
                'country_id' => 198,
            ),
            89 => 
            array (
                'id' => 3727,
                'name' => 'Rogatec',
                'country_id' => 198,
            ),
            90 => 
            array (
                'id' => 3728,
                'name' => 'Ruše',
                'country_id' => 198,
            ),
            91 => 
            array (
                'id' => 3729,
                'name' => 'Semic',
                'country_id' => 198,
            ),
            92 => 
            array (
                'id' => 3730,
                'name' => 'Sevnica',
                'country_id' => 198,
            ),
            93 => 
            array (
                'id' => 3731,
                'name' => 'Sežana',
                'country_id' => 198,
            ),
            94 => 
            array (
                'id' => 3732,
                'name' => 'Slovenj Gradec',
                'country_id' => 198,
            ),
            95 => 
            array (
                'id' => 3733,
                'name' => 'Slovenska Bistrica',
                'country_id' => 198,
            ),
            96 => 
            array (
                'id' => 3734,
                'name' => 'Slovenske Konjice',
                'country_id' => 198,
            ),
            97 => 
            array (
                'id' => 3735,
                'name' => 'Starše',
                'country_id' => 198,
            ),
            98 => 
            array (
                'id' => 3736,
                'name' => 'Sveti Jurij',
                'country_id' => 198,
            ),
            99 => 
            array (
                'id' => 3737,
                'name' => 'Šencur',
                'country_id' => 198,
            ),
            100 => 
            array (
                'id' => 3738,
                'name' => 'Šentilj',
                'country_id' => 198,
            ),
            101 => 
            array (
                'id' => 3739,
                'name' => 'Šentjernej',
                'country_id' => 198,
            ),
            102 => 
            array (
                'id' => 3740,
                'name' => 'Šentjur',
                'country_id' => 198,
            ),
            103 => 
            array (
                'id' => 3741,
                'name' => 'Škocjan',
                'country_id' => 198,
            ),
            104 => 
            array (
                'id' => 3742,
                'name' => 'Škofja Loka',
                'country_id' => 198,
            ),
            105 => 
            array (
                'id' => 3743,
                'name' => 'Škofljica',
                'country_id' => 198,
            ),
            106 => 
            array (
                'id' => 3744,
                'name' => 'Šmarje pri Jelšah',
                'country_id' => 198,
            ),
            107 => 
            array (
                'id' => 3745,
                'name' => 'Šmartno ob Paki',
                'country_id' => 198,
            ),
            108 => 
            array (
                'id' => 3746,
                'name' => 'Šoštanj',
                'country_id' => 198,
            ),
            109 => 
            array (
                'id' => 3747,
                'name' => 'Štore',
                'country_id' => 198,
            ),
            110 => 
            array (
                'id' => 3748,
                'name' => 'Tolmin',
                'country_id' => 198,
            ),
            111 => 
            array (
                'id' => 3749,
                'name' => 'Trbovlje',
                'country_id' => 198,
            ),
            112 => 
            array (
                'id' => 3750,
                'name' => 'Trebnje',
                'country_id' => 198,
            ),
            113 => 
            array (
                'id' => 3751,
                'name' => 'Tržic',
                'country_id' => 198,
            ),
            114 => 
            array (
                'id' => 3752,
                'name' => 'Turnišce',
                'country_id' => 198,
            ),
            115 => 
            array (
                'id' => 3753,
                'name' => 'Velenje',
                'country_id' => 198,
            ),
            116 => 
            array (
                'id' => 3754,
                'name' => 'Velike Lašce',
                'country_id' => 198,
            ),
            117 => 
            array (
                'id' => 3755,
                'name' => 'Videm',
                'country_id' => 198,
            ),
            118 => 
            array (
                'id' => 3756,
                'name' => 'Vipava',
                'country_id' => 198,
            ),
            119 => 
            array (
                'id' => 3757,
                'name' => 'Vitanje',
                'country_id' => 198,
            ),
            120 => 
            array (
                'id' => 3758,
                'name' => 'Vodice',
                'country_id' => 198,
            ),
            121 => 
            array (
                'id' => 3759,
                'name' => 'Vojnik',
                'country_id' => 198,
            ),
            122 => 
            array (
                'id' => 3760,
                'name' => 'Vrhnika',
                'country_id' => 198,
            ),
            123 => 
            array (
                'id' => 3761,
                'name' => 'Vuzenica',
                'country_id' => 198,
            ),
            124 => 
            array (
                'id' => 3762,
                'name' => 'Zagorje ob Savi',
                'country_id' => 198,
            ),
            125 => 
            array (
                'id' => 3763,
                'name' => 'Zavrc',
                'country_id' => 198,
            ),
            126 => 
            array (
                'id' => 3764,
                'name' => 'Zrece',
                'country_id' => 198,
            ),
            127 => 
            array (
                'id' => 3765,
                'name' => 'Železniki',
                'country_id' => 198,
            ),
            128 => 
            array (
                'id' => 3766,
                'name' => 'Žiri',
                'country_id' => 198,
            ),
            129 => 
            array (
                'id' => 3767,
                'name' => 'Benedikt',
                'country_id' => 198,
            ),
            130 => 
            array (
                'id' => 3768,
                'name' => 'Bistrica ob Sotli',
                'country_id' => 198,
            ),
            131 => 
            array (
                'id' => 3769,
                'name' => 'Bloke',
                'country_id' => 198,
            ),
            132 => 
            array (
                'id' => 3770,
                'name' => 'Braslovce',
                'country_id' => 198,
            ),
            133 => 
            array (
                'id' => 3771,
                'name' => 'Cankova',
                'country_id' => 198,
            ),
            134 => 
            array (
                'id' => 3772,
                'name' => 'Cerkvenjak',
                'country_id' => 198,
            ),
            135 => 
            array (
                'id' => 3773,
                'name' => 'Dobje',
                'country_id' => 198,
            ),
            136 => 
            array (
                'id' => 3774,
                'name' => 'Dobrna',
                'country_id' => 198,
            ),
            137 => 
            array (
                'id' => 3775,
                'name' => 'Dobrovnik',
                'country_id' => 198,
            ),
            138 => 
            array (
                'id' => 3776,
                'name' => 'Dolenjske Toplice',
                'country_id' => 198,
            ),
            139 => 
            array (
                'id' => 3777,
                'name' => 'Grad',
                'country_id' => 198,
            ),
            140 => 
            array (
                'id' => 3778,
                'name' => 'Hajdina',
                'country_id' => 198,
            ),
            141 => 
            array (
                'id' => 3779,
                'name' => 'Hoce-Slivnica',
                'country_id' => 198,
            ),
            142 => 
            array (
                'id' => 3780,
                'name' => 'Hodoš',
                'country_id' => 198,
            ),
            143 => 
            array (
                'id' => 3781,
                'name' => 'Horjul',
                'country_id' => 198,
            ),
            144 => 
            array (
                'id' => 3782,
                'name' => 'Jezersko',
                'country_id' => 198,
            ),
            145 => 
            array (
                'id' => 3783,
                'name' => 'Komenda',
                'country_id' => 198,
            ),
            146 => 
            array (
                'id' => 3784,
                'name' => 'Kostel',
                'country_id' => 198,
            ),
            147 => 
            array (
                'id' => 3785,
                'name' => 'Križevci',
                'country_id' => 198,
            ),
            148 => 
            array (
                'id' => 3786,
                'name' => 'Lovrenc na Pohorju',
                'country_id' => 198,
            ),
            149 => 
            array (
                'id' => 3787,
                'name' => 'Markovci',
                'country_id' => 198,
            ),
            150 => 
            array (
                'id' => 3788,
                'name' => 'Miklavž na Dravskem polju',
                'country_id' => 198,
            ),
            151 => 
            array (
                'id' => 3789,
                'name' => 'Mirna Pec',
                'country_id' => 198,
            ),
            152 => 
            array (
                'id' => 3790,
                'name' => 'Oplotnica',
                'country_id' => 198,
            ),
            153 => 
            array (
                'id' => 3791,
                'name' => 'Podlehnik',
                'country_id' => 198,
            ),
            154 => 
            array (
                'id' => 3792,
                'name' => 'Polzela',
                'country_id' => 198,
            ),
            155 => 
            array (
                'id' => 3793,
                'name' => 'Prebold',
                'country_id' => 198,
            ),
            156 => 
            array (
                'id' => 3794,
                'name' => 'Prevalje',
                'country_id' => 198,
            ),
            157 => 
            array (
                'id' => 3795,
                'name' => 'Razkrižje',
                'country_id' => 198,
            ),
            158 => 
            array (
                'id' => 3796,
                'name' => 'Ribnica na Pohorju',
                'country_id' => 198,
            ),
            159 => 
            array (
                'id' => 3797,
                'name' => 'Selnica ob Dravi',
                'country_id' => 198,
            ),
            160 => 
            array (
                'id' => 3798,
                'name' => 'Sodražica',
                'country_id' => 198,
            ),
            161 => 
            array (
                'id' => 3799,
                'name' => 'Solcava',
                'country_id' => 198,
            ),
            162 => 
            array (
                'id' => 3800,
                'name' => 'Sveta Ana',
                'country_id' => 198,
            ),
            163 => 
            array (
                'id' => 3801,
                'name' => 'Sveti Andraž v Slovenskih goricah',
                'country_id' => 198,
            ),
            164 => 
            array (
                'id' => 3802,
                'name' => 'Šempeter-Vrtojba',
                'country_id' => 198,
            ),
            165 => 
            array (
                'id' => 3803,
                'name' => 'Tabor',
                'country_id' => 198,
            ),
            166 => 
            array (
                'id' => 3804,
                'name' => 'Trnovska vas',
                'country_id' => 198,
            ),
            167 => 
            array (
                'id' => 3805,
                'name' => 'Trzin',
                'country_id' => 198,
            ),
            168 => 
            array (
                'id' => 3806,
                'name' => 'Velika Polana',
                'country_id' => 198,
            ),
            169 => 
            array (
                'id' => 3807,
                'name' => 'Veržej',
                'country_id' => 198,
            ),
            170 => 
            array (
                'id' => 3808,
                'name' => 'Vransko',
                'country_id' => 198,
            ),
            171 => 
            array (
                'id' => 3809,
                'name' => 'Žalec',
                'country_id' => 198,
            ),
            172 => 
            array (
                'id' => 3810,
                'name' => 'Žetale',
                'country_id' => 198,
            ),
            173 => 
            array (
                'id' => 3811,
                'name' => 'Žirovnica',
                'country_id' => 198,
            ),
            174 => 
            array (
                'id' => 3812,
                'name' => 'Žužemberk',
                'country_id' => 198,
            ),
            175 => 
            array (
                'id' => 3813,
                'name' => 'Šmartno pri Litiji',
                'country_id' => 198,
            ),
            176 => 
            array (
                'id' => 3814,
                'name' => 'Apace',
                'country_id' => 198,
            ),
            177 => 
            array (
                'id' => 3815,
                'name' => 'Cirkulane',
                'country_id' => 198,
            ),
            178 => 
            array (
                'id' => 3816,
                'name' => 'Kosanjevica na Krki',
                'country_id' => 198,
            ),
            179 => 
            array (
                'id' => 3817,
                'name' => 'Makole',
                'country_id' => 198,
            ),
            180 => 
            array (
                'id' => 3818,
                'name' => 'Mokronog-Trebelno',
                'country_id' => 198,
            ),
            181 => 
            array (
                'id' => 3819,
                'name' => 'Poljcane',
                'country_id' => 198,
            ),
            182 => 
            array (
                'id' => 3820,
                'name' => 'Renèe-Vogrsko',
                'country_id' => 198,
            ),
            183 => 
            array (
                'id' => 3821,
                'name' => 'Središce ob Dravi',
                'country_id' => 198,
            ),
            184 => 
            array (
                'id' => 3822,
                'name' => 'Straža',
                'country_id' => 198,
            ),
            185 => 
            array (
                'id' => 3823,
                'name' => '"Sveta Trojica v Slovenskih',
                'country_id' => 198,
            ),
            186 => 
            array (
                'id' => 3824,
                'name' => 'Sveti Tomaž',
                'country_id' => 198,
            ),
            187 => 
            array (
                'id' => 3825,
                'name' => 'Šmarješke Toplice',
                'country_id' => 198,
            ),
            188 => 
            array (
                'id' => 3826,
                'name' => 'Gorje',
                'country_id' => 198,
            ),
            189 => 
            array (
                'id' => 3827,
                'name' => 'Log-Dragomer',
                'country_id' => 198,
            ),
            190 => 
            array (
                'id' => 3828,
                'name' => 'Recica ob Savinji',
                'country_id' => 198,
            ),
            191 => 
            array (
                'id' => 3829,
                'name' => 'Sveti Jurij v Slovenskih Goricah',
                'country_id' => 198,
            ),
            192 => 
            array (
                'id' => 3830,
                'name' => 'Šentrupert',
                'country_id' => 198,
            ),
            193 => 
            array (
                'id' => 3831,
                'name' => 'Banskobystrický kraj',
                'country_id' => 197,
            ),
            194 => 
            array (
                'id' => 3832,
                'name' => 'Bratislavský kraj',
                'country_id' => 197,
            ),
            195 => 
            array (
                'id' => 3833,
                'name' => 'Košický kraj',
                'country_id' => 197,
            ),
            196 => 
            array (
                'id' => 3834,
                'name' => 'Nitriansky kraj',
                'country_id' => 197,
            ),
            197 => 
            array (
                'id' => 3835,
                'name' => 'Prešovský kraj',
                'country_id' => 197,
            ),
            198 => 
            array (
                'id' => 3836,
                'name' => 'Trnavský kraj',
                'country_id' => 197,
            ),
            199 => 
            array (
                'id' => 3837,
                'name' => 'Trenciansky kraj',
                'country_id' => 197,
            ),
            200 => 
            array (
                'id' => 3838,
                'name' => 'Žilinský kraj',
                'country_id' => 197,
            ),
            201 => 
            array (
                'id' => 3839,
                'name' => 'Eastern',
                'country_id' => 195,
            ),
            202 => 
            array (
                'id' => 3840,
                'name' => 'Northern',
                'country_id' => 195,
            ),
            203 => 
            array (
                'id' => 3841,
                'name' => 'Southern',
                'country_id' => 195,
            ),
            204 => 
            array (
                'id' => 3842,
            'name' => 'Western Area (Freetown)',
                'country_id' => 195,
            ),
            205 => 
            array (
                'id' => 3843,
                'name' => 'Acquaviva      ',
                'country_id' => 189,
            ),
            206 => 
            array (
                'id' => 3844,
                'name' => 'Chiesanuova    ',
                'country_id' => 189,
            ),
            207 => 
            array (
                'id' => 3845,
                'name' => 'Domagnano      ',
                'country_id' => 189,
            ),
            208 => 
            array (
                'id' => 3846,
                'name' => 'Faetano        ',
                'country_id' => 189,
            ),
            209 => 
            array (
                'id' => 3847,
                'name' => 'Fiorentino     ',
                'country_id' => 189,
            ),
            210 => 
            array (
                'id' => 3848,
                'name' => 'Borgo Maggiore ',
                'country_id' => 189,
            ),
            211 => 
            array (
                'id' => 3849,
                'name' => 'San Marino     ',
                'country_id' => 189,
            ),
            212 => 
            array (
                'id' => 3850,
                'name' => 'Montegiardino  ',
                'country_id' => 189,
            ),
            213 => 
            array (
                'id' => 3851,
                'name' => 'Serravalle     ',
                'country_id' => 189,
            ),
            214 => 
            array (
                'id' => 3852,
                'name' => 'Diourbel',
                'country_id' => 192,
            ),
            215 => 
            array (
                'id' => 3853,
                'name' => 'Dakar',
                'country_id' => 192,
            ),
            216 => 
            array (
                'id' => 3854,
                'name' => 'Fatick',
                'country_id' => 192,
            ),
            217 => 
            array (
                'id' => 3855,
                'name' => 'Kaffrine',
                'country_id' => 192,
            ),
            218 => 
            array (
                'id' => 3856,
                'name' => 'Kolda',
                'country_id' => 192,
            ),
            219 => 
            array (
                'id' => 3857,
                'name' => 'Kédougou',
                'country_id' => 192,
            ),
            220 => 
            array (
                'id' => 3858,
                'name' => 'Kaolack',
                'country_id' => 192,
            ),
            221 => 
            array (
                'id' => 3859,
                'name' => 'Louga',
                'country_id' => 192,
            ),
            222 => 
            array (
                'id' => 3860,
                'name' => 'Matam',
                'country_id' => 192,
            ),
            223 => 
            array (
                'id' => 3861,
                'name' => 'Sédhiou',
                'country_id' => 192,
            ),
            224 => 
            array (
                'id' => 3862,
                'name' => 'Saint-Louis',
                'country_id' => 192,
            ),
            225 => 
            array (
                'id' => 3863,
                'name' => 'Tambacounda',
                'country_id' => 192,
            ),
            226 => 
            array (
                'id' => 3864,
                'name' => 'Thiès',
                'country_id' => 192,
            ),
            227 => 
            array (
                'id' => 3865,
                'name' => 'Ziguinchor',
                'country_id' => 192,
            ),
            228 => 
            array (
                'id' => 3884,
                'name' => 'Brokopondo',
                'country_id' => 206,
            ),
            229 => 
            array (
                'id' => 3885,
                'name' => 'Commewijne',
                'country_id' => 206,
            ),
            230 => 
            array (
                'id' => 3886,
                'name' => 'Coronie',
                'country_id' => 206,
            ),
            231 => 
            array (
                'id' => 3887,
                'name' => 'Marowijne',
                'country_id' => 206,
            ),
            232 => 
            array (
                'id' => 3888,
                'name' => 'Nickerie',
                'country_id' => 206,
            ),
            233 => 
            array (
                'id' => 3889,
                'name' => 'Paramaribo',
                'country_id' => 206,
            ),
            234 => 
            array (
                'id' => 3890,
                'name' => 'Para',
                'country_id' => 206,
            ),
            235 => 
            array (
                'id' => 3891,
                'name' => 'Saramacca',
                'country_id' => 206,
            ),
            236 => 
            array (
                'id' => 3892,
                'name' => 'Sipaliwini',
                'country_id' => 206,
            ),
            237 => 
            array (
                'id' => 3893,
                'name' => 'Wanica',
                'country_id' => 206,
            ),
            238 => 
            array (
                'id' => 3894,
                'name' => 'Northern Bahr el Ghazal',
                'country_id' => 202,
            ),
            239 => 
            array (
                'id' => 3895,
                'name' => 'Western Bahr el Ghazal',
                'country_id' => 202,
            ),
            240 => 
            array (
                'id' => 3896,
                'name' => 'Central Equatoria',
                'country_id' => 202,
            ),
            241 => 
            array (
                'id' => 3897,
                'name' => 'Eastern Equatoria',
                'country_id' => 202,
            ),
            242 => 
            array (
                'id' => 3898,
                'name' => 'Western Equatoria',
                'country_id' => 202,
            ),
            243 => 
            array (
                'id' => 3899,
                'name' => 'Jonglei',
                'country_id' => 202,
            ),
            244 => 
            array (
                'id' => 3900,
                'name' => 'Lakes',
                'country_id' => 202,
            ),
            245 => 
            array (
                'id' => 3901,
                'name' => 'Upper Nile',
                'country_id' => 202,
            ),
            246 => 
            array (
                'id' => 3902,
                'name' => 'Unity',
                'country_id' => 202,
            ),
            247 => 
            array (
                'id' => 3903,
                'name' => 'Warrap',
                'country_id' => 202,
            ),
            248 => 
            array (
                'id' => 3904,
                'name' => 'Príncipe',
                'country_id' => 190,
            ),
            249 => 
            array (
                'id' => 3905,
                'name' => 'São Tomé',
                'country_id' => 190,
            ),
            250 => 
            array (
                'id' => 3906,
                'name' => 'Ahuachapán',
                'country_id' => 66,
            ),
            251 => 
            array (
                'id' => 3907,
                'name' => 'Cabañas',
                'country_id' => 66,
            ),
            252 => 
            array (
                'id' => 3908,
                'name' => 'Chalatenango',
                'country_id' => 66,
            ),
            253 => 
            array (
                'id' => 3909,
                'name' => 'Cuscatlán',
                'country_id' => 66,
            ),
            254 => 
            array (
                'id' => 3910,
                'name' => 'La Libertad',
                'country_id' => 66,
            ),
            255 => 
            array (
                'id' => 3911,
                'name' => 'Morazán',
                'country_id' => 66,
            ),
            256 => 
            array (
                'id' => 3912,
                'name' => 'La Paz',
                'country_id' => 66,
            ),
            257 => 
            array (
                'id' => 3913,
                'name' => 'Santa Ana',
                'country_id' => 66,
            ),
            258 => 
            array (
                'id' => 3914,
                'name' => 'San Miguel',
                'country_id' => 66,
            ),
            259 => 
            array (
                'id' => 3915,
                'name' => 'Sonsonate',
                'country_id' => 66,
            ),
            260 => 
            array (
                'id' => 3916,
                'name' => 'San Salvador',
                'country_id' => 66,
            ),
            261 => 
            array (
                'id' => 3917,
                'name' => 'San Vicente',
                'country_id' => 66,
            ),
            262 => 
            array (
                'id' => 3918,
                'name' => 'La Unión',
                'country_id' => 66,
            ),
            263 => 
            array (
                'id' => 3919,
                'name' => 'Usulután',
                'country_id' => 66,
            ),
            264 => 
            array (
                'id' => 3934,
                'name' => 'Hhohho',
                'country_id' => 207,
            ),
            265 => 
            array (
                'id' => 3935,
                'name' => 'Lubombo',
                'country_id' => 207,
            ),
            266 => 
            array (
                'id' => 3936,
                'name' => 'Manzini',
                'country_id' => 207,
            ),
            267 => 
            array (
                'id' => 3937,
                'name' => 'Shiselweni',
                'country_id' => 207,
            ),
            268 => 
            array (
                'id' => 3938,
                'name' => 'Batha',
                'country_id' => 41,
            ),
            269 => 
            array (
                'id' => 3939,
                'name' => 'Ba?r al Ghazal',
                'country_id' => 41,
            ),
            270 => 
            array (
                'id' => 3940,
                'name' => 'Burku',
                'country_id' => 41,
            ),
            271 => 
            array (
                'id' => 3941,
                'name' => 'Chari-Baguirmi',
                'country_id' => 41,
            ),
            272 => 
            array (
                'id' => 3942,
                'name' => 'Innidi',
                'country_id' => 41,
            ),
            273 => 
            array (
                'id' => 3943,
                'name' => 'Guéra',
                'country_id' => 41,
            ),
            274 => 
            array (
                'id' => 3944,
                'name' => 'Hadjer Lamis',
                'country_id' => 41,
            ),
            275 => 
            array (
                'id' => 3945,
                'name' => 'Kanem',
                'country_id' => 41,
            ),
            276 => 
            array (
                'id' => 3946,
                'name' => 'Lac',
                'country_id' => 41,
            ),
            277 => 
            array (
                'id' => 3947,
                'name' => 'Logone-Occidental',
                'country_id' => 41,
            ),
            278 => 
            array (
                'id' => 3948,
                'name' => 'Logone-Oriental',
                'country_id' => 41,
            ),
            279 => 
            array (
                'id' => 3949,
                'name' => 'Mandoul',
                'country_id' => 41,
            ),
            280 => 
            array (
                'id' => 3950,
                'name' => 'Moyen-Chari',
                'country_id' => 41,
            ),
            281 => 
            array (
                'id' => 3951,
                'name' => 'Mayo-Kebbi-Est',
                'country_id' => 41,
            ),
            282 => 
            array (
                'id' => 3952,
                'name' => 'Mayo-Kebbi-Ouest',
                'country_id' => 41,
            ),
            283 => 
            array (
                'id' => 3953,
                'name' => 'Ville de Ndjamena',
                'country_id' => 41,
            ),
            284 => 
            array (
                'id' => 3954,
                'name' => 'Ouaddaï',
                'country_id' => 41,
            ),
            285 => 
            array (
                'id' => 3955,
                'name' => 'Salamat',
                'country_id' => 41,
            ),
            286 => 
            array (
                'id' => 3956,
                'name' => 'Sila',
                'country_id' => 41,
            ),
            287 => 
            array (
                'id' => 3957,
                'name' => 'Tandjilé',
                'country_id' => 41,
            ),
            288 => 
            array (
                'id' => 3958,
                'name' => 'Tibasti',
                'country_id' => 41,
            ),
            289 => 
            array (
                'id' => 3959,
                'name' => 'Wadi Fira',
                'country_id' => 41,
            ),
            290 => 
            array (
                'id' => 3960,
                'name' => 'Centre',
                'country_id' => 215,
            ),
            291 => 
            array (
                'id' => 3961,
                'name' => 'Kara',
                'country_id' => 215,
            ),
            292 => 
            array (
                'id' => 3962,
            'name' => 'Maritime (Région)',
                'country_id' => 215,
            ),
            293 => 
            array (
                'id' => 3963,
                'name' => 'Plateaux',
                'country_id' => 215,
            ),
            294 => 
            array (
                'id' => 3964,
                'name' => 'Savannes',
                'country_id' => 215,
            ),
            295 => 
            array (
                'id' => 3965,
                'name' => 'Krung Thep Maha Nakhon [Bangkok]',
                'country_id' => 214,
            ),
            296 => 
            array (
                'id' => 3966,
                'name' => 'Samut Prakan',
                'country_id' => 214,
            ),
            297 => 
            array (
                'id' => 3967,
                'name' => 'Nonthaburi',
                'country_id' => 214,
            ),
            298 => 
            array (
                'id' => 3968,
                'name' => 'Pathum Thani',
                'country_id' => 214,
            ),
            299 => 
            array (
                'id' => 3969,
                'name' => 'Phra Nakhon Si Ayutthaya',
                'country_id' => 214,
            ),
            300 => 
            array (
                'id' => 3970,
                'name' => 'Ang Thong',
                'country_id' => 214,
            ),
            301 => 
            array (
                'id' => 3971,
                'name' => 'Lop Buri',
                'country_id' => 214,
            ),
            302 => 
            array (
                'id' => 3972,
                'name' => 'Sing Buri',
                'country_id' => 214,
            ),
            303 => 
            array (
                'id' => 3973,
                'name' => 'Chai Nat',
                'country_id' => 214,
            ),
            304 => 
            array (
                'id' => 3974,
                'name' => 'Saraburi',
                'country_id' => 214,
            ),
            305 => 
            array (
                'id' => 3975,
                'name' => 'Chon Buri',
                'country_id' => 214,
            ),
            306 => 
            array (
                'id' => 3976,
                'name' => 'Rayong',
                'country_id' => 214,
            ),
            307 => 
            array (
                'id' => 3977,
                'name' => 'Chanthaburi',
                'country_id' => 214,
            ),
            308 => 
            array (
                'id' => 3978,
                'name' => 'Trat',
                'country_id' => 214,
            ),
            309 => 
            array (
                'id' => 3979,
                'name' => 'Chachoengsao',
                'country_id' => 214,
            ),
            310 => 
            array (
                'id' => 3980,
                'name' => 'Prachin Buri',
                'country_id' => 214,
            ),
            311 => 
            array (
                'id' => 3981,
                'name' => 'Nakhon Nayok',
                'country_id' => 214,
            ),
            312 => 
            array (
                'id' => 3982,
                'name' => 'Sa Kaeo',
                'country_id' => 214,
            ),
            313 => 
            array (
                'id' => 3983,
                'name' => 'Nakhon Ratchasima',
                'country_id' => 214,
            ),
            314 => 
            array (
                'id' => 3984,
                'name' => 'Buri Ram',
                'country_id' => 214,
            ),
            315 => 
            array (
                'id' => 3985,
                'name' => 'Surin',
                'country_id' => 214,
            ),
            316 => 
            array (
                'id' => 3986,
                'name' => 'Si Sa Ket',
                'country_id' => 214,
            ),
            317 => 
            array (
                'id' => 3987,
                'name' => 'Ubon Ratchathani',
                'country_id' => 214,
            ),
            318 => 
            array (
                'id' => 3988,
                'name' => 'Yasothon',
                'country_id' => 214,
            ),
            319 => 
            array (
                'id' => 3989,
                'name' => 'Chaiyaphum',
                'country_id' => 214,
            ),
            320 => 
            array (
                'id' => 3990,
                'name' => 'Amnat Charoen',
                'country_id' => 214,
            ),
            321 => 
            array (
                'id' => 3991,
                'name' => 'Bueng Kan',
                'country_id' => 214,
            ),
            322 => 
            array (
                'id' => 3992,
                'name' => 'Nong Bua Lam Phu',
                'country_id' => 214,
            ),
            323 => 
            array (
                'id' => 3993,
                'name' => 'Khon Kaen',
                'country_id' => 214,
            ),
            324 => 
            array (
                'id' => 3994,
                'name' => 'Udon Thani',
                'country_id' => 214,
            ),
            325 => 
            array (
                'id' => 3995,
                'name' => 'Loei',
                'country_id' => 214,
            ),
            326 => 
            array (
                'id' => 3996,
                'name' => 'Nong Khai',
                'country_id' => 214,
            ),
            327 => 
            array (
                'id' => 3997,
                'name' => 'Maha Sarakham',
                'country_id' => 214,
            ),
            328 => 
            array (
                'id' => 3998,
                'name' => 'Roi Et',
                'country_id' => 214,
            ),
            329 => 
            array (
                'id' => 3999,
                'name' => 'Kalasin',
                'country_id' => 214,
            ),
            330 => 
            array (
                'id' => 4000,
                'name' => 'Sakon Nakhon',
                'country_id' => 214,
            ),
            331 => 
            array (
                'id' => 4001,
                'name' => 'Nakhon Phanom',
                'country_id' => 214,
            ),
            332 => 
            array (
                'id' => 4002,
                'name' => 'Mukdahan',
                'country_id' => 214,
            ),
            333 => 
            array (
                'id' => 4003,
                'name' => 'Chiang Mai',
                'country_id' => 214,
            ),
            334 => 
            array (
                'id' => 4004,
                'name' => 'Lamphun',
                'country_id' => 214,
            ),
            335 => 
            array (
                'id' => 4005,
                'name' => 'Lampang',
                'country_id' => 214,
            ),
            336 => 
            array (
                'id' => 4006,
                'name' => 'Uttaradit',
                'country_id' => 214,
            ),
            337 => 
            array (
                'id' => 4007,
                'name' => 'Phrae',
                'country_id' => 214,
            ),
            338 => 
            array (
                'id' => 4008,
                'name' => 'Nan',
                'country_id' => 214,
            ),
            339 => 
            array (
                'id' => 4009,
                'name' => 'Phayao',
                'country_id' => 214,
            ),
            340 => 
            array (
                'id' => 4010,
                'name' => 'Chiang Rai',
                'country_id' => 214,
            ),
            341 => 
            array (
                'id' => 4011,
                'name' => 'Mae Hong Son',
                'country_id' => 214,
            ),
            342 => 
            array (
                'id' => 4012,
                'name' => 'Nakhon Sawan',
                'country_id' => 214,
            ),
            343 => 
            array (
                'id' => 4013,
                'name' => 'Uthai Thani',
                'country_id' => 214,
            ),
            344 => 
            array (
                'id' => 4014,
                'name' => 'Kamphaeng Phet',
                'country_id' => 214,
            ),
            345 => 
            array (
                'id' => 4015,
                'name' => 'Tak',
                'country_id' => 214,
            ),
            346 => 
            array (
                'id' => 4016,
                'name' => 'Sukhothai',
                'country_id' => 214,
            ),
            347 => 
            array (
                'id' => 4017,
                'name' => 'Phitsanulok',
                'country_id' => 214,
            ),
            348 => 
            array (
                'id' => 4018,
                'name' => 'Phichit',
                'country_id' => 214,
            ),
            349 => 
            array (
                'id' => 4019,
                'name' => 'Phetchabun',
                'country_id' => 214,
            ),
            350 => 
            array (
                'id' => 4020,
                'name' => 'Ratchaburi',
                'country_id' => 214,
            ),
            351 => 
            array (
                'id' => 4021,
                'name' => 'Kanchanaburi',
                'country_id' => 214,
            ),
            352 => 
            array (
                'id' => 4022,
                'name' => 'Suphan Buri',
                'country_id' => 214,
            ),
            353 => 
            array (
                'id' => 4023,
                'name' => 'Nakhon Pathom',
                'country_id' => 214,
            ),
            354 => 
            array (
                'id' => 4024,
                'name' => 'Samut Sakhon',
                'country_id' => 214,
            ),
            355 => 
            array (
                'id' => 4025,
                'name' => 'Samut Songkhram',
                'country_id' => 214,
            ),
            356 => 
            array (
                'id' => 4026,
                'name' => 'Phetchaburi',
                'country_id' => 214,
            ),
            357 => 
            array (
                'id' => 4027,
                'name' => 'Prachuap Khiri Khan',
                'country_id' => 214,
            ),
            358 => 
            array (
                'id' => 4028,
                'name' => 'Nakhon Si Thammarat',
                'country_id' => 214,
            ),
            359 => 
            array (
                'id' => 4029,
                'name' => 'Krabi',
                'country_id' => 214,
            ),
            360 => 
            array (
                'id' => 4030,
                'name' => 'Phangnga',
                'country_id' => 214,
            ),
            361 => 
            array (
                'id' => 4031,
                'name' => 'Phuket',
                'country_id' => 214,
            ),
            362 => 
            array (
                'id' => 4032,
                'name' => 'Surat Thani',
                'country_id' => 214,
            ),
            363 => 
            array (
                'id' => 4033,
                'name' => 'Ranong',
                'country_id' => 214,
            ),
            364 => 
            array (
                'id' => 4034,
                'name' => 'Chumphon',
                'country_id' => 214,
            ),
            365 => 
            array (
                'id' => 4035,
                'name' => 'Songkhla',
                'country_id' => 214,
            ),
            366 => 
            array (
                'id' => 4036,
                'name' => 'Satun',
                'country_id' => 214,
            ),
            367 => 
            array (
                'id' => 4037,
                'name' => 'Trang',
                'country_id' => 214,
            ),
            368 => 
            array (
                'id' => 4038,
                'name' => 'Phatthalung',
                'country_id' => 214,
            ),
            369 => 
            array (
                'id' => 4039,
                'name' => 'Pattani',
                'country_id' => 214,
            ),
            370 => 
            array (
                'id' => 4040,
                'name' => 'Yala',
                'country_id' => 214,
            ),
            371 => 
            array (
                'id' => 4041,
                'name' => 'Narathiwat',
                'country_id' => 214,
            ),
            372 => 
            array (
                'id' => 4042,
                'name' => 'Phatthaya',
                'country_id' => 214,
            ),
            373 => 
            array (
                'id' => 4043,
                'name' => 'Kuhistoni Badakhshon',
                'country_id' => 212,
            ),
            374 => 
            array (
                'id' => 4044,
                'name' => 'Khatlon',
                'country_id' => 212,
            ),
            375 => 
            array (
                'id' => 4045,
                'name' => 'Sughd',
                'country_id' => 212,
            ),
            376 => 
            array (
                'id' => 4046,
                'name' => 'Aileu',
                'country_id' => 63,
            ),
            377 => 
            array (
                'id' => 4047,
                'name' => 'Ainaro',
                'country_id' => 63,
            ),
            378 => 
            array (
                'id' => 4048,
                'name' => 'Baucau',
                'country_id' => 63,
            ),
            379 => 
            array (
                'id' => 4049,
                'name' => 'Bobonaro',
                'country_id' => 63,
            ),
            380 => 
            array (
                'id' => 4050,
                'name' => 'Cova Lima',
                'country_id' => 63,
            ),
            381 => 
            array (
                'id' => 4051,
                'name' => 'Díli',
                'country_id' => 63,
            ),
            382 => 
            array (
                'id' => 4052,
                'name' => 'Ermera',
                'country_id' => 63,
            ),
            383 => 
            array (
                'id' => 4053,
                'name' => 'Lautem',
                'country_id' => 63,
            ),
            384 => 
            array (
                'id' => 4054,
                'name' => 'Liquiça',
                'country_id' => 63,
            ),
            385 => 
            array (
                'id' => 4055,
                'name' => 'Manufahi',
                'country_id' => 63,
            ),
            386 => 
            array (
                'id' => 4056,
                'name' => 'Manatuto',
                'country_id' => 63,
            ),
            387 => 
            array (
                'id' => 4057,
                'name' => 'Oecussi',
                'country_id' => 63,
            ),
            388 => 
            array (
                'id' => 4058,
                'name' => 'Viqueque',
                'country_id' => 63,
            ),
            389 => 
            array (
                'id' => 4059,
                'name' => 'Ahal',
                'country_id' => 221,
            ),
            390 => 
            array (
                'id' => 4060,
                'name' => 'Balkan',
                'country_id' => 221,
            ),
            391 => 
            array (
                'id' => 4061,
                'name' => 'Dasoguz',
                'country_id' => 221,
            ),
            392 => 
            array (
                'id' => 4062,
                'name' => 'Lebap',
                'country_id' => 221,
            ),
            393 => 
            array (
                'id' => 4063,
                'name' => 'Mary',
                'country_id' => 221,
            ),
            394 => 
            array (
                'id' => 4064,
                'name' => 'Asgabat',
                'country_id' => 221,
            ),
            395 => 
            array (
                'id' => 4065,
                'name' => 'Tunis',
                'country_id' => 219,
            ),
            396 => 
            array (
                'id' => 4066,
                'name' => 'Ariana',
                'country_id' => 219,
            ),
            397 => 
            array (
                'id' => 4067,
                'name' => 'Ben Arous',
                'country_id' => 219,
            ),
            398 => 
            array (
                'id' => 4068,
                'name' => 'La Manouba',
                'country_id' => 219,
            ),
            399 => 
            array (
                'id' => 4069,
                'name' => 'Nabeul',
                'country_id' => 219,
            ),
            400 => 
            array (
                'id' => 4070,
                'name' => 'Zaghouan',
                'country_id' => 219,
            ),
            401 => 
            array (
                'id' => 4071,
                'name' => 'Bizerte',
                'country_id' => 219,
            ),
            402 => 
            array (
                'id' => 4072,
                'name' => 'Béja',
                'country_id' => 219,
            ),
            403 => 
            array (
                'id' => 4073,
                'name' => 'Jendouba',
                'country_id' => 219,
            ),
            404 => 
            array (
                'id' => 4074,
                'name' => 'Le Kef',
                'country_id' => 219,
            ),
            405 => 
            array (
                'id' => 4075,
                'name' => 'Siliana',
                'country_id' => 219,
            ),
            406 => 
            array (
                'id' => 4076,
                'name' => 'Kairouan',
                'country_id' => 219,
            ),
            407 => 
            array (
                'id' => 4077,
                'name' => 'Kasserine',
                'country_id' => 219,
            ),
            408 => 
            array (
                'id' => 4078,
                'name' => 'Sidi Bouzid',
                'country_id' => 219,
            ),
            409 => 
            array (
                'id' => 4079,
                'name' => 'Sousse',
                'country_id' => 219,
            ),
            410 => 
            array (
                'id' => 4080,
                'name' => 'Monastir',
                'country_id' => 219,
            ),
            411 => 
            array (
                'id' => 4081,
                'name' => 'Mahdia',
                'country_id' => 219,
            ),
            412 => 
            array (
                'id' => 4082,
                'name' => 'Sfax',
                'country_id' => 219,
            ),
            413 => 
            array (
                'id' => 4083,
                'name' => 'Gafsa',
                'country_id' => 219,
            ),
            414 => 
            array (
                'id' => 4084,
                'name' => 'Tozeur',
                'country_id' => 219,
            ),
            415 => 
            array (
                'id' => 4085,
                'name' => 'Kebili',
                'country_id' => 219,
            ),
            416 => 
            array (
                'id' => 4086,
                'name' => 'Gabès',
                'country_id' => 219,
            ),
            417 => 
            array (
                'id' => 4087,
                'name' => 'Medenine',
                'country_id' => 219,
            ),
            418 => 
            array (
                'id' => 4088,
                'name' => 'Tataouine',
                'country_id' => 219,
            ),
            419 => 
            array (
                'id' => 4089,
                'name' => '\'Eua',
                'country_id' => 217,
            ),
            420 => 
            array (
                'id' => 4090,
                'name' => 'Ha\'apai',
                'country_id' => 217,
            ),
            421 => 
            array (
                'id' => 4091,
                'name' => 'Niuas',
                'country_id' => 217,
            ),
            422 => 
            array (
                'id' => 4092,
                'name' => 'Tongatapu',
                'country_id' => 217,
            ),
            423 => 
            array (
                'id' => 4093,
                'name' => 'Vava\'u',
                'country_id' => 217,
            ),
            424 => 
            array (
                'id' => 4094,
                'name' => 'Adana',
                'country_id' => 220,
            ),
            425 => 
            array (
                'id' => 4095,
                'name' => 'Adiyaman',
                'country_id' => 220,
            ),
            426 => 
            array (
                'id' => 4096,
                'name' => 'Afyonkarahisar',
                'country_id' => 220,
            ),
            427 => 
            array (
                'id' => 4097,
                'name' => 'Agri',
                'country_id' => 220,
            ),
            428 => 
            array (
                'id' => 4098,
                'name' => 'Amasya',
                'country_id' => 220,
            ),
            429 => 
            array (
                'id' => 4099,
                'name' => 'Ankara',
                'country_id' => 220,
            ),
            430 => 
            array (
                'id' => 4100,
                'name' => 'Antalya',
                'country_id' => 220,
            ),
            431 => 
            array (
                'id' => 4101,
                'name' => 'Artvin',
                'country_id' => 220,
            ),
            432 => 
            array (
                'id' => 4102,
                'name' => 'Aydin',
                'country_id' => 220,
            ),
            433 => 
            array (
                'id' => 4103,
                'name' => 'Balikesir',
                'country_id' => 220,
            ),
            434 => 
            array (
                'id' => 4104,
                'name' => 'Bilecik',
                'country_id' => 220,
            ),
            435 => 
            array (
                'id' => 4105,
                'name' => 'Bingöl',
                'country_id' => 220,
            ),
            436 => 
            array (
                'id' => 4106,
                'name' => 'Bitlis',
                'country_id' => 220,
            ),
            437 => 
            array (
                'id' => 4107,
                'name' => 'Bolu',
                'country_id' => 220,
            ),
            438 => 
            array (
                'id' => 4108,
                'name' => 'Burdur',
                'country_id' => 220,
            ),
            439 => 
            array (
                'id' => 4109,
                'name' => 'Bursa',
                'country_id' => 220,
            ),
            440 => 
            array (
                'id' => 4110,
                'name' => 'Canakkale',
                'country_id' => 220,
            ),
            441 => 
            array (
                'id' => 4111,
                'name' => 'Çankiri',
                'country_id' => 220,
            ),
            442 => 
            array (
                'id' => 4112,
                'name' => 'Corum',
                'country_id' => 220,
            ),
            443 => 
            array (
                'id' => 4113,
                'name' => 'Denizli',
                'country_id' => 220,
            ),
            444 => 
            array (
                'id' => 4114,
                'name' => 'Diyarbakir',
                'country_id' => 220,
            ),
            445 => 
            array (
                'id' => 4115,
                'name' => 'Edirne',
                'country_id' => 220,
            ),
            446 => 
            array (
                'id' => 4116,
                'name' => 'Elazig',
                'country_id' => 220,
            ),
            447 => 
            array (
                'id' => 4117,
                'name' => 'Erzincan',
                'country_id' => 220,
            ),
            448 => 
            array (
                'id' => 4118,
                'name' => 'Erzurum',
                'country_id' => 220,
            ),
            449 => 
            array (
                'id' => 4119,
                'name' => 'Eskisehir',
                'country_id' => 220,
            ),
            450 => 
            array (
                'id' => 4120,
                'name' => 'Gaziantep',
                'country_id' => 220,
            ),
            451 => 
            array (
                'id' => 4121,
                'name' => 'Giresun',
                'country_id' => 220,
            ),
            452 => 
            array (
                'id' => 4122,
                'name' => 'Gümüshane',
                'country_id' => 220,
            ),
            453 => 
            array (
                'id' => 4123,
                'name' => 'Hakkari',
                'country_id' => 220,
            ),
            454 => 
            array (
                'id' => 4124,
                'name' => 'Hatay',
                'country_id' => 220,
            ),
            455 => 
            array (
                'id' => 4125,
                'name' => 'Isparta',
                'country_id' => 220,
            ),
            456 => 
            array (
                'id' => 4126,
                'name' => 'Mersin',
                'country_id' => 220,
            ),
            457 => 
            array (
                'id' => 4127,
                'name' => 'Istanbul',
                'country_id' => 220,
            ),
            458 => 
            array (
                'id' => 4128,
                'name' => 'Izmir',
                'country_id' => 220,
            ),
            459 => 
            array (
                'id' => 4129,
                'name' => 'Kars',
                'country_id' => 220,
            ),
            460 => 
            array (
                'id' => 4130,
                'name' => 'Kastamonu',
                'country_id' => 220,
            ),
            461 => 
            array (
                'id' => 4131,
                'name' => 'Kayseri',
                'country_id' => 220,
            ),
            462 => 
            array (
                'id' => 4132,
                'name' => 'Kirklareli',
                'country_id' => 220,
            ),
            463 => 
            array (
                'id' => 4133,
                'name' => 'Kirsehir',
                'country_id' => 220,
            ),
            464 => 
            array (
                'id' => 4134,
                'name' => 'Kocaeli',
                'country_id' => 220,
            ),
            465 => 
            array (
                'id' => 4135,
                'name' => 'Konya',
                'country_id' => 220,
            ),
            466 => 
            array (
                'id' => 4136,
                'name' => 'Kütahya',
                'country_id' => 220,
            ),
            467 => 
            array (
                'id' => 4137,
                'name' => 'Malatya',
                'country_id' => 220,
            ),
            468 => 
            array (
                'id' => 4138,
                'name' => 'Manisa',
                'country_id' => 220,
            ),
            469 => 
            array (
                'id' => 4139,
                'name' => 'Kahramanmaras',
                'country_id' => 220,
            ),
            470 => 
            array (
                'id' => 4140,
                'name' => 'Mardin',
                'country_id' => 220,
            ),
            471 => 
            array (
                'id' => 4141,
                'name' => 'Mugla',
                'country_id' => 220,
            ),
            472 => 
            array (
                'id' => 4142,
                'name' => 'Mus',
                'country_id' => 220,
            ),
            473 => 
            array (
                'id' => 4143,
                'name' => 'Nevsehir',
                'country_id' => 220,
            ),
            474 => 
            array (
                'id' => 4144,
                'name' => 'Nigde',
                'country_id' => 220,
            ),
            475 => 
            array (
                'id' => 4145,
                'name' => 'Ordu',
                'country_id' => 220,
            ),
            476 => 
            array (
                'id' => 4146,
                'name' => 'Rize',
                'country_id' => 220,
            ),
            477 => 
            array (
                'id' => 4147,
                'name' => 'Sakarya',
                'country_id' => 220,
            ),
            478 => 
            array (
                'id' => 4148,
                'name' => 'Samsun',
                'country_id' => 220,
            ),
            479 => 
            array (
                'id' => 4149,
                'name' => 'Siirt',
                'country_id' => 220,
            ),
            480 => 
            array (
                'id' => 4150,
                'name' => 'Sinop',
                'country_id' => 220,
            ),
            481 => 
            array (
                'id' => 4151,
                'name' => 'Sivas',
                'country_id' => 220,
            ),
            482 => 
            array (
                'id' => 4152,
                'name' => 'Tekirdag',
                'country_id' => 220,
            ),
            483 => 
            array (
                'id' => 4153,
                'name' => 'Tokat',
                'country_id' => 220,
            ),
            484 => 
            array (
                'id' => 4154,
                'name' => 'Trabzon',
                'country_id' => 220,
            ),
            485 => 
            array (
                'id' => 4155,
                'name' => 'Tunceli',
                'country_id' => 220,
            ),
            486 => 
            array (
                'id' => 4156,
                'name' => 'Sanliurfa',
                'country_id' => 220,
            ),
            487 => 
            array (
                'id' => 4157,
                'name' => 'Usak',
                'country_id' => 220,
            ),
            488 => 
            array (
                'id' => 4158,
                'name' => 'Van',
                'country_id' => 220,
            ),
            489 => 
            array (
                'id' => 4159,
                'name' => 'Yozgat',
                'country_id' => 220,
            ),
            490 => 
            array (
                'id' => 4160,
                'name' => 'Zonguldak',
                'country_id' => 220,
            ),
            491 => 
            array (
                'id' => 4161,
                'name' => 'Aksaray',
                'country_id' => 220,
            ),
            492 => 
            array (
                'id' => 4162,
                'name' => 'Bayburt',
                'country_id' => 220,
            ),
            493 => 
            array (
                'id' => 4163,
                'name' => 'Karaman',
                'country_id' => 220,
            ),
            494 => 
            array (
                'id' => 4164,
                'name' => 'Kirikkale',
                'country_id' => 220,
            ),
            495 => 
            array (
                'id' => 4165,
                'name' => 'Batman',
                'country_id' => 220,
            ),
            496 => 
            array (
                'id' => 4166,
                'name' => 'Sirnak',
                'country_id' => 220,
            ),
            497 => 
            array (
                'id' => 4167,
                'name' => 'Bartin',
                'country_id' => 220,
            ),
            498 => 
            array (
                'id' => 4168,
                'name' => 'Ardahan',
                'country_id' => 220,
            ),
            499 => 
            array (
                'id' => 4169,
                'name' => 'Igdir',
                'country_id' => 220,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 4170,
                'name' => 'Yalova',
                'country_id' => 220,
            ),
            1 => 
            array (
                'id' => 4171,
                'name' => 'Karabuk',
                'country_id' => 220,
            ),
            2 => 
            array (
                'id' => 4172,
                'name' => 'Kilis',
                'country_id' => 220,
            ),
            3 => 
            array (
                'id' => 4173,
                'name' => 'Osmaniye',
                'country_id' => 220,
            ),
            4 => 
            array (
                'id' => 4174,
                'name' => 'Düzce',
                'country_id' => 220,
            ),
            5 => 
            array (
                'id' => 4175,
                'name' => 'Arima',
                'country_id' => 218,
            ),
            6 => 
            array (
                'id' => 4176,
                'name' => 'Chaguanas',
                'country_id' => 218,
            ),
            7 => 
            array (
                'id' => 4177,
                'name' => 'Couva-Tabaquite-Talparo',
                'country_id' => 218,
            ),
            8 => 
            array (
                'id' => 4178,
                'name' => 'Diego Martin',
                'country_id' => 218,
            ),
            9 => 
            array (
                'id' => 4179,
                'name' => 'Eastern Tobago',
                'country_id' => 218,
            ),
            10 => 
            array (
                'id' => 4180,
                'name' => 'Penal-Debe',
                'country_id' => 218,
            ),
            11 => 
            array (
                'id' => 4181,
                'name' => 'Port of Spain',
                'country_id' => 218,
            ),
            12 => 
            array (
                'id' => 4182,
                'name' => 'Princes Town',
                'country_id' => 218,
            ),
            13 => 
            array (
                'id' => 4183,
                'name' => 'Point Fortin',
                'country_id' => 218,
            ),
            14 => 
            array (
                'id' => 4184,
                'name' => 'Rio Claro-Mayaro',
                'country_id' => 218,
            ),
            15 => 
            array (
                'id' => 4185,
                'name' => 'San Fernando',
                'country_id' => 218,
            ),
            16 => 
            array (
                'id' => 4186,
                'name' => 'Sangre Grande',
                'country_id' => 218,
            ),
            17 => 
            array (
                'id' => 4187,
                'name' => 'Siparia',
                'country_id' => 218,
            ),
            18 => 
            array (
                'id' => 4188,
                'name' => 'San Juan-Laventille',
                'country_id' => 218,
            ),
            19 => 
            array (
                'id' => 4189,
                'name' => 'Tunapuna-Piarco',
                'country_id' => 218,
            ),
            20 => 
            array (
                'id' => 4190,
                'name' => 'Western Tobago',
                'country_id' => 218,
            ),
            21 => 
            array (
                'id' => 4191,
                'name' => 'Funafuti',
                'country_id' => 223,
            ),
            22 => 
            array (
                'id' => 4192,
                'name' => 'Niutao',
                'country_id' => 223,
            ),
            23 => 
            array (
                'id' => 4193,
                'name' => 'Nui',
                'country_id' => 223,
            ),
            24 => 
            array (
                'id' => 4194,
                'name' => 'Nukufetau',
                'country_id' => 223,
            ),
            25 => 
            array (
                'id' => 4195,
                'name' => 'Nukulaelae',
                'country_id' => 223,
            ),
            26 => 
            array (
                'id' => 4196,
                'name' => 'Nanumea',
                'country_id' => 223,
            ),
            27 => 
            array (
                'id' => 4197,
                'name' => 'Nanumanga',
                'country_id' => 223,
            ),
            28 => 
            array (
                'id' => 4198,
                'name' => 'Vaitupu',
                'country_id' => 223,
            ),
            29 => 
            array (
                'id' => 4199,
                'name' => 'Changhua',
                'country_id' => 211,
            ),
            30 => 
            array (
                'id' => 4200,
                'name' => 'Chiayi',
                'country_id' => 211,
            ),
            31 => 
            array (
                'id' => 4201,
                'name' => 'Hsinchu',
                'country_id' => 211,
            ),
            32 => 
            array (
                'id' => 4202,
                'name' => 'Hualien',
                'country_id' => 211,
            ),
            33 => 
            array (
                'id' => 4203,
                'name' => 'Ilan',
                'country_id' => 211,
            ),
            34 => 
            array (
                'id' => 4204,
                'name' => 'Keelung',
                'country_id' => 211,
            ),
            35 => 
            array (
                'id' => 4205,
                'name' => 'Kaohsiung',
                'country_id' => 211,
            ),
            36 => 
            array (
                'id' => 4206,
                'name' => 'Miaoli',
                'country_id' => 211,
            ),
            37 => 
            array (
                'id' => 4207,
                'name' => 'Nantou',
                'country_id' => 211,
            ),
            38 => 
            array (
                'id' => 4208,
                'name' => 'Penghu',
                'country_id' => 211,
            ),
            39 => 
            array (
                'id' => 4209,
                'name' => 'Pingtung',
                'country_id' => 211,
            ),
            40 => 
            array (
                'id' => 4210,
                'name' => 'Taoyuan',
                'country_id' => 211,
            ),
            41 => 
            array (
                'id' => 4211,
                'name' => 'Tainan',
                'country_id' => 211,
            ),
            42 => 
            array (
                'id' => 4212,
                'name' => 'Taipei',
                'country_id' => 211,
            ),
            43 => 
            array (
                'id' => 4213,
                'name' => 'Taitung',
                'country_id' => 211,
            ),
            44 => 
            array (
                'id' => 4214,
                'name' => 'Taichung',
                'country_id' => 211,
            ),
            45 => 
            array (
                'id' => 4215,
                'name' => 'Yunlin',
                'country_id' => 211,
            ),
            46 => 
            array (
                'id' => 4216,
                'name' => 'Arusha',
                'country_id' => 213,
            ),
            47 => 
            array (
                'id' => 4217,
                'name' => 'Dar es Salaam',
                'country_id' => 213,
            ),
            48 => 
            array (
                'id' => 4218,
                'name' => 'Dodoma',
                'country_id' => 213,
            ),
            49 => 
            array (
                'id' => 4219,
                'name' => 'Iringa',
                'country_id' => 213,
            ),
            50 => 
            array (
                'id' => 4220,
                'name' => 'Kagera',
                'country_id' => 213,
            ),
            51 => 
            array (
                'id' => 4221,
                'name' => 'Kaskazini Pemba',
                'country_id' => 213,
            ),
            52 => 
            array (
                'id' => 4222,
                'name' => 'Kaskazini Unguja',
                'country_id' => 213,
            ),
            53 => 
            array (
                'id' => 4223,
                'name' => 'Kigoma',
                'country_id' => 213,
            ),
            54 => 
            array (
                'id' => 4224,
                'name' => 'Kilimanjaro',
                'country_id' => 213,
            ),
            55 => 
            array (
                'id' => 4225,
                'name' => 'Kusini Pemba',
                'country_id' => 213,
            ),
            56 => 
            array (
                'id' => 4226,
                'name' => 'Kusini Unguja',
                'country_id' => 213,
            ),
            57 => 
            array (
                'id' => 4227,
                'name' => 'Lindi',
                'country_id' => 213,
            ),
            58 => 
            array (
                'id' => 4228,
                'name' => 'Mara',
                'country_id' => 213,
            ),
            59 => 
            array (
                'id' => 4229,
                'name' => 'Mbeya',
                'country_id' => 213,
            ),
            60 => 
            array (
                'id' => 4230,
                'name' => 'Mjini Magharibi',
                'country_id' => 213,
            ),
            61 => 
            array (
                'id' => 4231,
                'name' => 'Morogoro',
                'country_id' => 213,
            ),
            62 => 
            array (
                'id' => 4232,
                'name' => 'Mtwara',
                'country_id' => 213,
            ),
            63 => 
            array (
                'id' => 4233,
                'name' => 'Mwanza',
                'country_id' => 213,
            ),
            64 => 
            array (
                'id' => 4234,
                'name' => 'Pwani',
                'country_id' => 213,
            ),
            65 => 
            array (
                'id' => 4235,
                'name' => 'Rukwa',
                'country_id' => 213,
            ),
            66 => 
            array (
                'id' => 4236,
                'name' => 'Ruvuma',
                'country_id' => 213,
            ),
            67 => 
            array (
                'id' => 4237,
                'name' => 'Shinyanga',
                'country_id' => 213,
            ),
            68 => 
            array (
                'id' => 4238,
                'name' => 'Singida',
                'country_id' => 213,
            ),
            69 => 
            array (
                'id' => 4239,
                'name' => 'Tabora',
                'country_id' => 213,
            ),
            70 => 
            array (
                'id' => 4240,
                'name' => 'Tanga',
                'country_id' => 213,
            ),
            71 => 
            array (
                'id' => 4241,
                'name' => 'Manyara',
                'country_id' => 213,
            ),
            72 => 
            array (
                'id' => 4242,
                'name' => 'Vinnytska oblast',
                'country_id' => 225,
            ),
            73 => 
            array (
                'id' => 4243,
                'name' => 'Volynska oblast',
                'country_id' => 225,
            ),
            74 => 
            array (
                'id' => 4244,
                'name' => 'Luhanska oblast',
                'country_id' => 225,
            ),
            75 => 
            array (
                'id' => 4245,
                'name' => 'Dnipropetrovska oblast',
                'country_id' => 225,
            ),
            76 => 
            array (
                'id' => 4246,
                'name' => 'Donetska oblast',
                'country_id' => 225,
            ),
            77 => 
            array (
                'id' => 4247,
                'name' => 'Zhytomyrska oblast',
                'country_id' => 225,
            ),
            78 => 
            array (
                'id' => 4248,
                'name' => 'Zakarpatska oblast',
                'country_id' => 225,
            ),
            79 => 
            array (
                'id' => 4249,
                'name' => 'Zaporizka oblast',
                'country_id' => 225,
            ),
            80 => 
            array (
                'id' => 4250,
                'name' => 'Ivano-Frankivska oblast',
                'country_id' => 225,
            ),
            81 => 
            array (
                'id' => 4251,
                'name' => 'Kyiv',
                'country_id' => 225,
            ),
            82 => 
            array (
                'id' => 4252,
                'name' => 'Kyivska oblast',
                'country_id' => 225,
            ),
            83 => 
            array (
                'id' => 4253,
                'name' => 'Kirovohradska oblast',
                'country_id' => 225,
            ),
            84 => 
            array (
                'id' => 4254,
                'name' => 'Sevastopol',
                'country_id' => 225,
            ),
            85 => 
            array (
                'id' => 4255,
                'name' => 'Respublika Krym',
                'country_id' => 225,
            ),
            86 => 
            array (
                'id' => 4256,
                'name' => 'Lvivska oblast',
                'country_id' => 225,
            ),
            87 => 
            array (
                'id' => 4257,
                'name' => 'Mykolaivska oblast',
                'country_id' => 225,
            ),
            88 => 
            array (
                'id' => 4258,
                'name' => 'Odeska oblast',
                'country_id' => 225,
            ),
            89 => 
            array (
                'id' => 4259,
                'name' => 'Poltavska oblast',
                'country_id' => 225,
            ),
            90 => 
            array (
                'id' => 4260,
                'name' => 'Rivnenska oblast',
                'country_id' => 225,
            ),
            91 => 
            array (
                'id' => 4261,
                'name' => 'Sumska oblast',
                'country_id' => 225,
            ),
            92 => 
            array (
                'id' => 4262,
                'name' => 'Ternopilska oblast',
                'country_id' => 225,
            ),
            93 => 
            array (
                'id' => 4263,
                'name' => 'Kharkivska oblast',
                'country_id' => 225,
            ),
            94 => 
            array (
                'id' => 4264,
                'name' => 'Khersonska oblast',
                'country_id' => 225,
            ),
            95 => 
            array (
                'id' => 4265,
                'name' => 'Khmelnytska oblast',
                'country_id' => 225,
            ),
            96 => 
            array (
                'id' => 4266,
                'name' => 'Cherkaska oblast',
                'country_id' => 225,
            ),
            97 => 
            array (
                'id' => 4267,
                'name' => 'Chernihivska oblast',
                'country_id' => 225,
            ),
            98 => 
            array (
                'id' => 4268,
                'name' => 'Chernivetska oblast',
                'country_id' => 225,
            ),
            99 => 
            array (
                'id' => 4269,
                'name' => 'Kalangala',
                'country_id' => 224,
            ),
            100 => 
            array (
                'id' => 4270,
                'name' => 'Kampala',
                'country_id' => 224,
            ),
            101 => 
            array (
                'id' => 4271,
                'name' => 'Kiboga',
                'country_id' => 224,
            ),
            102 => 
            array (
                'id' => 4272,
                'name' => 'Luwero',
                'country_id' => 224,
            ),
            103 => 
            array (
                'id' => 4273,
                'name' => 'Masaka',
                'country_id' => 224,
            ),
            104 => 
            array (
                'id' => 4274,
                'name' => 'Mpigi',
                'country_id' => 224,
            ),
            105 => 
            array (
                'id' => 4275,
                'name' => 'Mubende',
                'country_id' => 224,
            ),
            106 => 
            array (
                'id' => 4276,
                'name' => 'Mukono',
                'country_id' => 224,
            ),
            107 => 
            array (
                'id' => 4277,
                'name' => 'Nakasongola',
                'country_id' => 224,
            ),
            108 => 
            array (
                'id' => 4278,
                'name' => 'Rakai',
                'country_id' => 224,
            ),
            109 => 
            array (
                'id' => 4279,
                'name' => 'Sembabule',
                'country_id' => 224,
            ),
            110 => 
            array (
                'id' => 4280,
                'name' => 'Kayunga',
                'country_id' => 224,
            ),
            111 => 
            array (
                'id' => 4281,
                'name' => 'Wakiso',
                'country_id' => 224,
            ),
            112 => 
            array (
                'id' => 4282,
                'name' => 'Mityana',
                'country_id' => 224,
            ),
            113 => 
            array (
                'id' => 4283,
                'name' => 'Nakaseke',
                'country_id' => 224,
            ),
            114 => 
            array (
                'id' => 4284,
                'name' => 'Lyantonde',
                'country_id' => 224,
            ),
            115 => 
            array (
                'id' => 4285,
                'name' => 'Bugiri',
                'country_id' => 224,
            ),
            116 => 
            array (
                'id' => 4286,
                'name' => 'Busia',
                'country_id' => 224,
            ),
            117 => 
            array (
                'id' => 4287,
                'name' => 'Iganga',
                'country_id' => 224,
            ),
            118 => 
            array (
                'id' => 4288,
                'name' => 'Jinja',
                'country_id' => 224,
            ),
            119 => 
            array (
                'id' => 4289,
                'name' => 'Kamuli',
                'country_id' => 224,
            ),
            120 => 
            array (
                'id' => 4290,
                'name' => 'Kapchorwa',
                'country_id' => 224,
            ),
            121 => 
            array (
                'id' => 4291,
                'name' => 'Katakwi',
                'country_id' => 224,
            ),
            122 => 
            array (
                'id' => 4292,
                'name' => 'Kumi',
                'country_id' => 224,
            ),
            123 => 
            array (
                'id' => 4293,
                'name' => 'Mbale',
                'country_id' => 224,
            ),
            124 => 
            array (
                'id' => 4294,
                'name' => 'Pallisa',
                'country_id' => 224,
            ),
            125 => 
            array (
                'id' => 4295,
                'name' => 'Soroti',
                'country_id' => 224,
            ),
            126 => 
            array (
                'id' => 4296,
                'name' => 'Tororo',
                'country_id' => 224,
            ),
            127 => 
            array (
                'id' => 4297,
                'name' => 'Kaberamaido',
                'country_id' => 224,
            ),
            128 => 
            array (
                'id' => 4298,
                'name' => 'Sironko',
                'country_id' => 224,
            ),
            129 => 
            array (
                'id' => 4299,
                'name' => 'Amuria',
                'country_id' => 224,
            ),
            130 => 
            array (
                'id' => 4300,
                'name' => 'Budaka',
                'country_id' => 224,
            ),
            131 => 
            array (
                'id' => 4301,
                'name' => 'Bukwa',
                'country_id' => 224,
            ),
            132 => 
            array (
                'id' => 4302,
                'name' => 'Butaleja',
                'country_id' => 224,
            ),
            133 => 
            array (
                'id' => 4303,
                'name' => 'Kaliro',
                'country_id' => 224,
            ),
            134 => 
            array (
                'id' => 4304,
                'name' => 'Manafwa',
                'country_id' => 224,
            ),
            135 => 
            array (
                'id' => 4305,
                'name' => 'Namutumba',
                'country_id' => 224,
            ),
            136 => 
            array (
                'id' => 4306,
                'name' => 'Bududa',
                'country_id' => 224,
            ),
            137 => 
            array (
                'id' => 4307,
                'name' => 'Bukedea',
                'country_id' => 224,
            ),
            138 => 
            array (
                'id' => 4308,
                'name' => 'Mayuge',
                'country_id' => 224,
            ),
            139 => 
            array (
                'id' => 4309,
                'name' => 'Adjumani',
                'country_id' => 224,
            ),
            140 => 
            array (
                'id' => 4310,
                'name' => 'Apac',
                'country_id' => 224,
            ),
            141 => 
            array (
                'id' => 4311,
                'name' => 'Arua',
                'country_id' => 224,
            ),
            142 => 
            array (
                'id' => 4312,
                'name' => 'Gulu',
                'country_id' => 224,
            ),
            143 => 
            array (
                'id' => 4313,
                'name' => 'Kitgum',
                'country_id' => 224,
            ),
            144 => 
            array (
                'id' => 4314,
                'name' => 'Kotido',
                'country_id' => 224,
            ),
            145 => 
            array (
                'id' => 4315,
                'name' => 'Lira',
                'country_id' => 224,
            ),
            146 => 
            array (
                'id' => 4316,
                'name' => 'Moroto',
                'country_id' => 224,
            ),
            147 => 
            array (
                'id' => 4317,
                'name' => 'Moyo',
                'country_id' => 224,
            ),
            148 => 
            array (
                'id' => 4318,
                'name' => 'Nebbi',
                'country_id' => 224,
            ),
            149 => 
            array (
                'id' => 4319,
                'name' => 'Nakapiripirit',
                'country_id' => 224,
            ),
            150 => 
            array (
                'id' => 4320,
                'name' => 'Pader',
                'country_id' => 224,
            ),
            151 => 
            array (
                'id' => 4321,
                'name' => 'Yumbe',
                'country_id' => 224,
            ),
            152 => 
            array (
                'id' => 4322,
                'name' => 'Amolatar',
                'country_id' => 224,
            ),
            153 => 
            array (
                'id' => 4323,
                'name' => 'Kaabong',
                'country_id' => 224,
            ),
            154 => 
            array (
                'id' => 4324,
                'name' => 'Koboko',
                'country_id' => 224,
            ),
            155 => 
            array (
                'id' => 4325,
                'name' => 'Abim',
                'country_id' => 224,
            ),
            156 => 
            array (
                'id' => 4326,
                'name' => 'Dokolo',
                'country_id' => 224,
            ),
            157 => 
            array (
                'id' => 4327,
                'name' => 'Amuru',
                'country_id' => 224,
            ),
            158 => 
            array (
                'id' => 4328,
                'name' => 'Maracha',
                'country_id' => 224,
            ),
            159 => 
            array (
                'id' => 4329,
                'name' => 'Oyam',
                'country_id' => 224,
            ),
            160 => 
            array (
                'id' => 4330,
                'name' => 'Bundibugyo',
                'country_id' => 224,
            ),
            161 => 
            array (
                'id' => 4331,
                'name' => 'Bushenyi',
                'country_id' => 224,
            ),
            162 => 
            array (
                'id' => 4332,
                'name' => 'Hoima',
                'country_id' => 224,
            ),
            163 => 
            array (
                'id' => 4333,
                'name' => 'Kabale',
                'country_id' => 224,
            ),
            164 => 
            array (
                'id' => 4334,
                'name' => 'Kabarole',
                'country_id' => 224,
            ),
            165 => 
            array (
                'id' => 4335,
                'name' => 'Kasese',
                'country_id' => 224,
            ),
            166 => 
            array (
                'id' => 4336,
                'name' => 'Kibaale',
                'country_id' => 224,
            ),
            167 => 
            array (
                'id' => 4337,
                'name' => 'Kisoro',
                'country_id' => 224,
            ),
            168 => 
            array (
                'id' => 4338,
                'name' => 'Masindi',
                'country_id' => 224,
            ),
            169 => 
            array (
                'id' => 4339,
                'name' => 'Mbarara',
                'country_id' => 224,
            ),
            170 => 
            array (
                'id' => 4340,
                'name' => 'Ntungamo',
                'country_id' => 224,
            ),
            171 => 
            array (
                'id' => 4341,
                'name' => 'Rukungiri',
                'country_id' => 224,
            ),
            172 => 
            array (
                'id' => 4342,
                'name' => 'Kamwenge',
                'country_id' => 224,
            ),
            173 => 
            array (
                'id' => 4343,
                'name' => 'Kanungu',
                'country_id' => 224,
            ),
            174 => 
            array (
                'id' => 4344,
                'name' => 'Kyenjojo',
                'country_id' => 224,
            ),
            175 => 
            array (
                'id' => 4345,
                'name' => 'Ibanda',
                'country_id' => 224,
            ),
            176 => 
            array (
                'id' => 4346,
                'name' => 'Isingiro',
                'country_id' => 224,
            ),
            177 => 
            array (
                'id' => 4347,
                'name' => 'Kiruhura',
                'country_id' => 224,
            ),
            178 => 
            array (
                'id' => 4348,
                'name' => 'Buliisa',
                'country_id' => 224,
            ),
            179 => 
            array (
                'id' => 4349,
                'name' => 'Alaska',
                'country_id' => 228,
            ),
            180 => 
            array (
                'id' => 4350,
                'name' => 'Alabama',
                'country_id' => 228,
            ),
            181 => 
            array (
                'id' => 4351,
                'name' => 'Arkansas',
                'country_id' => 228,
            ),
            182 => 
            array (
                'id' => 4352,
            'name' => 'American Samoa (see also separate entry under AS)',
                'country_id' => 228,
            ),
            183 => 
            array (
                'id' => 4353,
                'name' => 'Arizona',
                'country_id' => 228,
            ),
            184 => 
            array (
                'id' => 4354,
                'name' => 'California',
                'country_id' => 228,
            ),
            185 => 
            array (
                'id' => 4355,
                'name' => 'Colorado',
                'country_id' => 228,
            ),
            186 => 
            array (
                'id' => 4356,
                'name' => 'Connecticut',
                'country_id' => 228,
            ),
            187 => 
            array (
                'id' => 4357,
                'name' => 'District of Columbia',
                'country_id' => 228,
            ),
            188 => 
            array (
                'id' => 4358,
                'name' => 'Delaware',
                'country_id' => 228,
            ),
            189 => 
            array (
                'id' => 4359,
                'name' => 'Florida',
                'country_id' => 228,
            ),
            190 => 
            array (
                'id' => 4360,
                'name' => 'Georgia',
                'country_id' => 228,
            ),
            191 => 
            array (
                'id' => 4361,
            'name' => 'Guam (see also separate entry under GU)',
                'country_id' => 228,
            ),
            192 => 
            array (
                'id' => 4362,
                'name' => 'Hawaii',
                'country_id' => 228,
            ),
            193 => 
            array (
                'id' => 4363,
                'name' => 'Iowa',
                'country_id' => 228,
            ),
            194 => 
            array (
                'id' => 4364,
                'name' => 'Idaho',
                'country_id' => 228,
            ),
            195 => 
            array (
                'id' => 4365,
                'name' => 'Illinois',
                'country_id' => 228,
            ),
            196 => 
            array (
                'id' => 4366,
                'name' => 'Indiana',
                'country_id' => 228,
            ),
            197 => 
            array (
                'id' => 4367,
                'name' => 'Kansas',
                'country_id' => 228,
            ),
            198 => 
            array (
                'id' => 4368,
                'name' => 'Kentucky',
                'country_id' => 228,
            ),
            199 => 
            array (
                'id' => 4369,
                'name' => 'Louisiana',
                'country_id' => 228,
            ),
            200 => 
            array (
                'id' => 4370,
                'name' => 'Massachusetts',
                'country_id' => 228,
            ),
            201 => 
            array (
                'id' => 4371,
                'name' => 'Maryland',
                'country_id' => 228,
            ),
            202 => 
            array (
                'id' => 4372,
                'name' => 'Maine',
                'country_id' => 228,
            ),
            203 => 
            array (
                'id' => 4373,
                'name' => 'Michigan',
                'country_id' => 228,
            ),
            204 => 
            array (
                'id' => 4374,
                'name' => 'Minnesota',
                'country_id' => 228,
            ),
            205 => 
            array (
                'id' => 4375,
                'name' => 'Missouri',
                'country_id' => 228,
            ),
            206 => 
            array (
                'id' => 4376,
            'name' => 'Northern Mariana Islands (see also separate entry MP)',
                'country_id' => 228,
            ),
            207 => 
            array (
                'id' => 4377,
                'name' => 'Mississippi',
                'country_id' => 228,
            ),
            208 => 
            array (
                'id' => 4378,
                'name' => 'Montana',
                'country_id' => 228,
            ),
            209 => 
            array (
                'id' => 4379,
                'name' => 'North Carolina',
                'country_id' => 228,
            ),
            210 => 
            array (
                'id' => 4380,
                'name' => 'North Dakota',
                'country_id' => 228,
            ),
            211 => 
            array (
                'id' => 4381,
                'name' => 'Nebraska',
                'country_id' => 228,
            ),
            212 => 
            array (
                'id' => 4382,
                'name' => 'New Hampshire',
                'country_id' => 228,
            ),
            213 => 
            array (
                'id' => 4383,
                'name' => 'New Jersey',
                'country_id' => 228,
            ),
            214 => 
            array (
                'id' => 4384,
                'name' => 'New Mexico',
                'country_id' => 228,
            ),
            215 => 
            array (
                'id' => 4385,
                'name' => 'Nevada',
                'country_id' => 228,
            ),
            216 => 
            array (
                'id' => 4386,
                'name' => 'New York',
                'country_id' => 228,
            ),
            217 => 
            array (
                'id' => 4387,
                'name' => 'Ohio',
                'country_id' => 228,
            ),
            218 => 
            array (
                'id' => 4388,
                'name' => 'Oklahoma',
                'country_id' => 228,
            ),
            219 => 
            array (
                'id' => 4389,
                'name' => 'Oregon',
                'country_id' => 228,
            ),
            220 => 
            array (
                'id' => 4390,
                'name' => 'Pennsylvania',
                'country_id' => 228,
            ),
            221 => 
            array (
                'id' => 4391,
            'name' => 'Puerto Rico (see also separate entry under PR)',
                'country_id' => 228,
            ),
            222 => 
            array (
                'id' => 4392,
                'name' => 'Rhode Island',
                'country_id' => 228,
            ),
            223 => 
            array (
                'id' => 4393,
                'name' => 'South Carolina',
                'country_id' => 228,
            ),
            224 => 
            array (
                'id' => 4394,
                'name' => 'South Dakota',
                'country_id' => 228,
            ),
            225 => 
            array (
                'id' => 4395,
                'name' => 'Tennessee',
                'country_id' => 228,
            ),
            226 => 
            array (
                'id' => 4396,
                'name' => 'Texas',
                'country_id' => 228,
            ),
            227 => 
            array (
                'id' => 4397,
            'name' => 'U.S. Minor Outlying Islands (cf. separate entry UM)',
                'country_id' => 228,
            ),
            228 => 
            array (
                'id' => 4398,
                'name' => 'Utah',
                'country_id' => 228,
            ),
            229 => 
            array (
                'id' => 4399,
                'name' => 'Virginia',
                'country_id' => 228,
            ),
            230 => 
            array (
                'id' => 4400,
            'name' => 'Virgin Islands of the U.S. (see also separate entry VI)',
                'country_id' => 228,
            ),
            231 => 
            array (
                'id' => 4401,
                'name' => 'Vermont',
                'country_id' => 228,
            ),
            232 => 
            array (
                'id' => 4402,
                'name' => 'Washington',
                'country_id' => 228,
            ),
            233 => 
            array (
                'id' => 4403,
                'name' => 'Wisconsin',
                'country_id' => 228,
            ),
            234 => 
            array (
                'id' => 4404,
                'name' => 'West Virginia',
                'country_id' => 228,
            ),
            235 => 
            array (
                'id' => 4405,
                'name' => 'Wyoming',
                'country_id' => 228,
            ),
            236 => 
            array (
                'id' => 4406,
                'name' => 'Artigas',
                'country_id' => 229,
            ),
            237 => 
            array (
                'id' => 4407,
                'name' => 'Canelones',
                'country_id' => 229,
            ),
            238 => 
            array (
                'id' => 4408,
                'name' => 'Cerro Largo',
                'country_id' => 229,
            ),
            239 => 
            array (
                'id' => 4409,
                'name' => 'Colonia',
                'country_id' => 229,
            ),
            240 => 
            array (
                'id' => 4410,
                'name' => 'Durazno',
                'country_id' => 229,
            ),
            241 => 
            array (
                'id' => 4411,
                'name' => 'Florida',
                'country_id' => 229,
            ),
            242 => 
            array (
                'id' => 4412,
                'name' => 'Flores',
                'country_id' => 229,
            ),
            243 => 
            array (
                'id' => 4413,
                'name' => 'Lavalleja',
                'country_id' => 229,
            ),
            244 => 
            array (
                'id' => 4414,
                'name' => 'Maldonado',
                'country_id' => 229,
            ),
            245 => 
            array (
                'id' => 4415,
                'name' => 'Montevideo',
                'country_id' => 229,
            ),
            246 => 
            array (
                'id' => 4416,
                'name' => 'Paysandú',
                'country_id' => 229,
            ),
            247 => 
            array (
                'id' => 4417,
                'name' => 'Río Negro',
                'country_id' => 229,
            ),
            248 => 
            array (
                'id' => 4418,
                'name' => 'Rocha',
                'country_id' => 229,
            ),
            249 => 
            array (
                'id' => 4419,
                'name' => 'Rivera',
                'country_id' => 229,
            ),
            250 => 
            array (
                'id' => 4420,
                'name' => 'Salto',
                'country_id' => 229,
            ),
            251 => 
            array (
                'id' => 4421,
                'name' => 'San José',
                'country_id' => 229,
            ),
            252 => 
            array (
                'id' => 4422,
                'name' => 'Soriano',
                'country_id' => 229,
            ),
            253 => 
            array (
                'id' => 4423,
                'name' => 'Tacuarembó',
                'country_id' => 229,
            ),
            254 => 
            array (
                'id' => 4424,
                'name' => 'Treinta y Tres',
                'country_id' => 229,
            ),
            255 => 
            array (
                'id' => 4425,
                'name' => 'Andijon',
                'country_id' => 231,
            ),
            256 => 
            array (
                'id' => 4426,
                'name' => 'Bukhoro',
                'country_id' => 231,
            ),
            257 => 
            array (
                'id' => 4427,
                'name' => 'Farg‘ona',
                'country_id' => 231,
            ),
            258 => 
            array (
                'id' => 4428,
                'name' => 'Jizzax',
                'country_id' => 231,
            ),
            259 => 
            array (
                'id' => 4429,
                'name' => 'Khorazm',
                'country_id' => 231,
            ),
            260 => 
            array (
                'id' => 4430,
                'name' => 'Namangan',
                'country_id' => 231,
            ),
            261 => 
            array (
                'id' => 4431,
                'name' => 'Nawoiy',
                'country_id' => 231,
            ),
            262 => 
            array (
                'id' => 4432,
                'name' => 'Qashqadaryo',
                'country_id' => 231,
            ),
            263 => 
            array (
                'id' => 4433,
                'name' => 'Qoraqalpog‘iston Respublikasi',
                'country_id' => 231,
            ),
            264 => 
            array (
                'id' => 4434,
                'name' => 'Samarqand',
                'country_id' => 231,
            ),
            265 => 
            array (
                'id' => 4435,
                'name' => 'Sirdaryo',
                'country_id' => 231,
            ),
            266 => 
            array (
                'id' => 4436,
                'name' => 'Surkhondaryo',
                'country_id' => 231,
            ),
            267 => 
            array (
                'id' => 4437,
                'name' => 'Toshkent',
                'country_id' => 231,
            ),
            268 => 
            array (
                'id' => 4438,
                'name' => 'Xorazm',
                'country_id' => 231,
            ),
            269 => 
            array (
                'id' => 4439,
                'name' => 'Charlotte     ',
                'country_id' => 187,
            ),
            270 => 
            array (
                'id' => 4440,
                'name' => 'Saint Andrew  ',
                'country_id' => 187,
            ),
            271 => 
            array (
                'id' => 4441,
                'name' => 'Saint David   ',
                'country_id' => 187,
            ),
            272 => 
            array (
                'id' => 4442,
                'name' => 'Saint George  ',
                'country_id' => 187,
            ),
            273 => 
            array (
                'id' => 4443,
                'name' => 'Saint Patrick ',
                'country_id' => 187,
            ),
            274 => 
            array (
                'id' => 4444,
                'name' => 'Grenadines    ',
                'country_id' => 187,
            ),
            275 => 
            array (
                'id' => 4445,
                'name' => 'Distrito Capital',
                'country_id' => 233,
            ),
            276 => 
            array (
                'id' => 4446,
                'name' => 'Anzoátegui',
                'country_id' => 233,
            ),
            277 => 
            array (
                'id' => 4447,
                'name' => 'Apure',
                'country_id' => 233,
            ),
            278 => 
            array (
                'id' => 4448,
                'name' => 'Aragua',
                'country_id' => 233,
            ),
            279 => 
            array (
                'id' => 4449,
                'name' => 'Barinas',
                'country_id' => 233,
            ),
            280 => 
            array (
                'id' => 4450,
                'name' => 'Bolívar',
                'country_id' => 233,
            ),
            281 => 
            array (
                'id' => 4451,
                'name' => 'Carabobo',
                'country_id' => 233,
            ),
            282 => 
            array (
                'id' => 4452,
                'name' => 'Cojedes',
                'country_id' => 233,
            ),
            283 => 
            array (
                'id' => 4453,
                'name' => 'Falcón',
                'country_id' => 233,
            ),
            284 => 
            array (
                'id' => 4454,
                'name' => 'Guárico',
                'country_id' => 233,
            ),
            285 => 
            array (
                'id' => 4455,
                'name' => 'Lara',
                'country_id' => 233,
            ),
            286 => 
            array (
                'id' => 4456,
                'name' => 'Mérida',
                'country_id' => 233,
            ),
            287 => 
            array (
                'id' => 4457,
                'name' => 'Miranda',
                'country_id' => 233,
            ),
            288 => 
            array (
                'id' => 4458,
                'name' => 'Monagas',
                'country_id' => 233,
            ),
            289 => 
            array (
                'id' => 4459,
                'name' => 'Nueva Esparta',
                'country_id' => 233,
            ),
            290 => 
            array (
                'id' => 4460,
                'name' => 'Portuguesa',
                'country_id' => 233,
            ),
            291 => 
            array (
                'id' => 4461,
                'name' => 'Sucre',
                'country_id' => 233,
            ),
            292 => 
            array (
                'id' => 4462,
                'name' => 'Táchira',
                'country_id' => 233,
            ),
            293 => 
            array (
                'id' => 4463,
                'name' => 'Trujillo',
                'country_id' => 233,
            ),
            294 => 
            array (
                'id' => 4464,
                'name' => 'Yaracuy',
                'country_id' => 233,
            ),
            295 => 
            array (
                'id' => 4465,
                'name' => 'Zulia',
                'country_id' => 233,
            ),
            296 => 
            array (
                'id' => 4466,
                'name' => 'Dependencias Federales',
                'country_id' => 233,
            ),
            297 => 
            array (
                'id' => 4467,
                'name' => 'vargas',
                'country_id' => 233,
            ),
            298 => 
            array (
                'id' => 4468,
                'name' => 'Delta Amacuro',
                'country_id' => 233,
            ),
            299 => 
            array (
                'id' => 4469,
                'name' => 'Amazonas',
                'country_id' => 233,
            ),
            300 => 
            array (
                'id' => 4470,
                'name' => 'Lai Chau',
                'country_id' => 234,
            ),
            301 => 
            array (
                'id' => 4471,
                'name' => 'Lao Cai',
                'country_id' => 234,
            ),
            302 => 
            array (
                'id' => 4472,
                'name' => 'Ha Giang',
                'country_id' => 234,
            ),
            303 => 
            array (
                'id' => 4473,
                'name' => 'Cao Bang',
                'country_id' => 234,
            ),
            304 => 
            array (
                'id' => 4474,
                'name' => 'Son La',
                'country_id' => 234,
            ),
            305 => 
            array (
                'id' => 4475,
                'name' => 'Yen Bai',
                'country_id' => 234,
            ),
            306 => 
            array (
                'id' => 4476,
                'name' => 'Tuyen Quang',
                'country_id' => 234,
            ),
            307 => 
            array (
                'id' => 4477,
                'name' => 'Lang Son',
                'country_id' => 234,
            ),
            308 => 
            array (
                'id' => 4478,
                'name' => 'Quang Ninh',
                'country_id' => 234,
            ),
            309 => 
            array (
                'id' => 4479,
                'name' => 'Hoa Binh',
                'country_id' => 234,
            ),
            310 => 
            array (
                'id' => 4480,
                'name' => 'Ha Tay',
                'country_id' => 234,
            ),
            311 => 
            array (
                'id' => 4481,
                'name' => 'Ninh Binh',
                'country_id' => 234,
            ),
            312 => 
            array (
                'id' => 4482,
                'name' => 'Thai Binh',
                'country_id' => 234,
            ),
            313 => 
            array (
                'id' => 4483,
                'name' => 'Thanh Hoa',
                'country_id' => 234,
            ),
            314 => 
            array (
                'id' => 4484,
                'name' => 'Nghe An',
                'country_id' => 234,
            ),
            315 => 
            array (
                'id' => 4485,
                'name' => 'Ha Tinh',
                'country_id' => 234,
            ),
            316 => 
            array (
                'id' => 4486,
                'name' => 'Quang Binh',
                'country_id' => 234,
            ),
            317 => 
            array (
                'id' => 4487,
                'name' => 'Quang Tri',
                'country_id' => 234,
            ),
            318 => 
            array (
                'id' => 4488,
                'name' => 'Thua Thien-Hue',
                'country_id' => 234,
            ),
            319 => 
            array (
                'id' => 4489,
                'name' => 'Quang Nam',
                'country_id' => 234,
            ),
            320 => 
            array (
                'id' => 4490,
                'name' => 'Kon Tum',
                'country_id' => 234,
            ),
            321 => 
            array (
                'id' => 4491,
                'name' => 'Quang Ngai',
                'country_id' => 234,
            ),
            322 => 
            array (
                'id' => 4492,
                'name' => 'Gia Lai',
                'country_id' => 234,
            ),
            323 => 
            array (
                'id' => 4493,
                'name' => 'Binh Dinh',
                'country_id' => 234,
            ),
            324 => 
            array (
                'id' => 4494,
                'name' => 'Phu Yen',
                'country_id' => 234,
            ),
            325 => 
            array (
                'id' => 4495,
                'name' => 'Dak Lak',
                'country_id' => 234,
            ),
            326 => 
            array (
                'id' => 4496,
                'name' => 'Khanh Hoa',
                'country_id' => 234,
            ),
            327 => 
            array (
                'id' => 4497,
                'name' => 'Lam Dong',
                'country_id' => 234,
            ),
            328 => 
            array (
                'id' => 4498,
                'name' => 'Ninh Thuan',
                'country_id' => 234,
            ),
            329 => 
            array (
                'id' => 4499,
                'name' => 'Tay Ninh',
                'country_id' => 234,
            ),
            330 => 
            array (
                'id' => 4500,
                'name' => 'Dong Nai',
                'country_id' => 234,
            ),
            331 => 
            array (
                'id' => 4501,
                'name' => 'Binh Thuan',
                'country_id' => 234,
            ),
            332 => 
            array (
                'id' => 4502,
                'name' => 'Long An',
                'country_id' => 234,
            ),
            333 => 
            array (
                'id' => 4503,
                'name' => 'Ba Ria-Vung Tau',
                'country_id' => 234,
            ),
            334 => 
            array (
                'id' => 4504,
                'name' => 'An Giang',
                'country_id' => 234,
            ),
            335 => 
            array (
                'id' => 4505,
                'name' => 'Dong Thap',
                'country_id' => 234,
            ),
            336 => 
            array (
                'id' => 4506,
                'name' => 'Tien Giang',
                'country_id' => 234,
            ),
            337 => 
            array (
                'id' => 4507,
                'name' => 'Kien Giang',
                'country_id' => 234,
            ),
            338 => 
            array (
                'id' => 4508,
                'name' => 'Vinh Long',
                'country_id' => 234,
            ),
            339 => 
            array (
                'id' => 4509,
                'name' => 'Ben Tre',
                'country_id' => 234,
            ),
            340 => 
            array (
                'id' => 4510,
                'name' => 'Tra Vinh',
                'country_id' => 234,
            ),
            341 => 
            array (
                'id' => 4511,
                'name' => 'Soc Trang',
                'country_id' => 234,
            ),
            342 => 
            array (
                'id' => 4512,
                'name' => 'Bac Kan',
                'country_id' => 234,
            ),
            343 => 
            array (
                'id' => 4513,
                'name' => 'Bac Giang',
                'country_id' => 234,
            ),
            344 => 
            array (
                'id' => 4514,
                'name' => 'Bac Lieu',
                'country_id' => 234,
            ),
            345 => 
            array (
                'id' => 4515,
                'name' => 'Bac Ninh',
                'country_id' => 234,
            ),
            346 => 
            array (
                'id' => 4516,
                'name' => 'Binh Duong',
                'country_id' => 234,
            ),
            347 => 
            array (
                'id' => 4517,
                'name' => 'Binh Phuoc',
                'country_id' => 234,
            ),
            348 => 
            array (
                'id' => 4518,
                'name' => 'Ca Mau',
                'country_id' => 234,
            ),
            349 => 
            array (
                'id' => 4519,
                'name' => 'Hai Duong',
                'country_id' => 234,
            ),
            350 => 
            array (
                'id' => 4520,
                'name' => 'Ha Nam',
                'country_id' => 234,
            ),
            351 => 
            array (
                'id' => 4521,
                'name' => 'Hung Yen',
                'country_id' => 234,
            ),
            352 => 
            array (
                'id' => 4522,
                'name' => 'Nam Dinh',
                'country_id' => 234,
            ),
            353 => 
            array (
                'id' => 4523,
                'name' => 'Phu Tho',
                'country_id' => 234,
            ),
            354 => 
            array (
                'id' => 4524,
                'name' => 'Thai Nguyen',
                'country_id' => 234,
            ),
            355 => 
            array (
                'id' => 4525,
                'name' => 'Vinh Phuc',
                'country_id' => 234,
            ),
            356 => 
            array (
                'id' => 4526,
                'name' => 'Dien Bien',
                'country_id' => 234,
            ),
            357 => 
            array (
                'id' => 4527,
                'name' => 'Dak Nong',
                'country_id' => 234,
            ),
            358 => 
            array (
                'id' => 4528,
                'name' => 'Hau Giang',
                'country_id' => 234,
            ),
            359 => 
            array (
                'id' => 4529,
                'name' => 'Can Tho',
                'country_id' => 234,
            ),
            360 => 
            array (
                'id' => 4530,
                'name' => '"Da Nang',
                'country_id' => 234,
            ),
            361 => 
            array (
                'id' => 4531,
                'name' => 'Ha Noi',
                'country_id' => 234,
            ),
            362 => 
            array (
                'id' => 4532,
                'name' => 'Hai Phong',
                'country_id' => 234,
            ),
            363 => 
            array (
                'id' => 4533,
                'name' => 'Ho Chi Minh',
                'country_id' => 234,
            ),
            364 => 
            array (
                'id' => 4534,
                'name' => 'Malampa',
                'country_id' => 232,
            ),
            365 => 
            array (
                'id' => 4535,
                'name' => 'Pénama',
                'country_id' => 232,
            ),
            366 => 
            array (
                'id' => 4536,
                'name' => 'Sanma',
                'country_id' => 232,
            ),
            367 => 
            array (
                'id' => 4537,
                'name' => 'Shéfa',
                'country_id' => 232,
            ),
            368 => 
            array (
                'id' => 4538,
                'name' => 'Taféa',
                'country_id' => 232,
            ),
            369 => 
            array (
                'id' => 4539,
                'name' => 'Torba',
                'country_id' => 232,
            ),
            370 => 
            array (
                'id' => 4540,
                'name' => 'A\'ana',
                'country_id' => 188,
            ),
            371 => 
            array (
                'id' => 4541,
                'name' => 'Aiga-i-le-Tai',
                'country_id' => 188,
            ),
            372 => 
            array (
                'id' => 4542,
                'name' => 'Atua',
                'country_id' => 188,
            ),
            373 => 
            array (
                'id' => 4543,
                'name' => 'Fa\'asaleleaga',
                'country_id' => 188,
            ),
            374 => 
            array (
                'id' => 4544,
                'name' => 'Gaga\'emauga',
                'country_id' => 188,
            ),
            375 => 
            array (
                'id' => 4545,
                'name' => 'Gagaifomauga',
                'country_id' => 188,
            ),
            376 => 
            array (
                'id' => 4546,
                'name' => 'Palauli',
                'country_id' => 188,
            ),
            377 => 
            array (
                'id' => 4547,
                'name' => 'Satupa \'itea',
                'country_id' => 188,
            ),
            378 => 
            array (
                'id' => 4548,
                'name' => 'Tuamasaga',
                'country_id' => 188,
            ),
            379 => 
            array (
                'id' => 4549,
                'name' => 'Va\'a-o-Fonoti',
                'country_id' => 188,
            ),
            380 => 
            array (
                'id' => 4550,
                'name' => 'Vaisigano',
                'country_id' => 188,
            ),
            381 => 
            array (
                'id' => 4566,
                'name' => 'Raymah',
                'country_id' => 236,
            ),
            382 => 
            array (
                'id' => 4571,
                'name' => 'Eastern Cape',
                'country_id' => 201,
            ),
            383 => 
            array (
                'id' => 4572,
                'name' => 'Free State',
                'country_id' => 201,
            ),
            384 => 
            array (
                'id' => 4573,
                'name' => 'Gauteng',
                'country_id' => 201,
            ),
            385 => 
            array (
                'id' => 4574,
                'name' => 'Limpopo',
                'country_id' => 201,
            ),
            386 => 
            array (
                'id' => 4575,
                'name' => 'Mpumalanga',
                'country_id' => 201,
            ),
            387 => 
            array (
                'id' => 4576,
                'name' => 'Northern Cape',
                'country_id' => 201,
            ),
            388 => 
            array (
                'id' => 4577,
                'name' => 'Kwazulu-Natal',
                'country_id' => 201,
            ),
            389 => 
            array (
                'id' => 4578,
                'name' => 'North-West',
                'country_id' => 201,
            ),
            390 => 
            array (
                'id' => 4579,
                'name' => 'Western Cape',
                'country_id' => 201,
            ),
            391 => 
            array (
                'id' => 4580,
                'name' => 'Western',
                'country_id' => 237,
            ),
            392 => 
            array (
                'id' => 4581,
                'name' => 'Central',
                'country_id' => 237,
            ),
            393 => 
            array (
                'id' => 4582,
                'name' => 'Eastern',
                'country_id' => 237,
            ),
            394 => 
            array (
                'id' => 4583,
                'name' => 'Luapula',
                'country_id' => 237,
            ),
            395 => 
            array (
                'id' => 4584,
                'name' => 'Northern',
                'country_id' => 237,
            ),
            396 => 
            array (
                'id' => 4585,
                'name' => 'North-Western',
                'country_id' => 237,
            ),
            397 => 
            array (
                'id' => 4586,
                'name' => 'Southern',
                'country_id' => 237,
            ),
            398 => 
            array (
                'id' => 4587,
                'name' => 'Copperbelt',
                'country_id' => 237,
            ),
            399 => 
            array (
                'id' => 4588,
                'name' => 'Lusaka',
                'country_id' => 237,
            ),
            400 => 
            array (
                'id' => 4589,
                'name' => 'Bulawayo',
                'country_id' => 238,
            ),
            401 => 
            array (
                'id' => 4590,
                'name' => 'Harare',
                'country_id' => 238,
            ),
            402 => 
            array (
                'id' => 4591,
                'name' => 'Manicaland',
                'country_id' => 238,
            ),
            403 => 
            array (
                'id' => 4592,
                'name' => 'Mashonaland Central',
                'country_id' => 238,
            ),
            404 => 
            array (
                'id' => 4593,
                'name' => 'Mashonaland East',
                'country_id' => 238,
            ),
            405 => 
            array (
                'id' => 4594,
                'name' => 'Midlands',
                'country_id' => 238,
            ),
            406 => 
            array (
                'id' => 4595,
                'name' => 'Matabeleland North',
                'country_id' => 238,
            ),
            407 => 
            array (
                'id' => 4596,
                'name' => 'Matabeleland South',
                'country_id' => 238,
            ),
            408 => 
            array (
                'id' => 4597,
                'name' => 'Masvingo',
                'country_id' => 238,
            ),
            409 => 
            array (
                'id' => 4598,
                'name' => 'Mashonaland West',
                'country_id' => 238,
            ),
            410 => 
            array (
                'id' => 4599,
                'name' => 'Tripoli',
                'country_id' => 124,
            ),
            411 => 
            array (
                'id' => 4600,
                'name' => 'Jeddah',
                'country_id' => 191,
            ),
            412 => 
            array (
                'id' => 4601,
                'name' => 'Carcasse',
                'country_id' => 59,
            ),
            413 => 
            array (
                'id' => 4602,
                'name' => 'Downtown',
                'country_id' => 59,
            ),
            414 => 
            array (
                'id' => 4603,
                'name' => 'East Point Settlement',
                'country_id' => 59,
            ),
            415 => 
            array (
                'id' => 4604,
                'name' => 'Minni Minni',
                'country_id' => 59,
            ),
            416 => 
            array (
                'id' => 4605,
                'name' => 'Anguilla',
                'country_id' => 7,
            ),
            417 => 
            array (
                'id' => 4606,
                'name' => 'Blowing Point',
                'country_id' => 7,
            ),
            418 => 
            array (
                'id' => 4607,
                'name' => 'Crocus Hill',
                'country_id' => 7,
            ),
            419 => 
            array (
                'id' => 4608,
                'name' => 'Meads Bay Beach',
                'country_id' => 7,
            ),
            420 => 
            array (
                'id' => 4609,
                'name' => 'Road Bay',
                'country_id' => 7,
            ),
            421 => 
            array (
                'id' => 4610,
                'name' => 'Sombrero',
                'country_id' => 7,
            ),
            422 => 
            array (
                'id' => 4611,
                'name' => 'The Forest',
                'country_id' => 7,
            ),
            423 => 
            array (
                'id' => 4612,
                'name' => 'The Road',
                'country_id' => 7,
            ),
            424 => 
            array (
                'id' => 4613,
                'name' => 'The Valley',
                'country_id' => 7,
            ),
            425 => 
            array (
                'id' => 4614,
                'name' => 'Wall Blake',
                'country_id' => 7,
            ),
            426 => 
            array (
                'id' => 4615,
                'name' => 'Aboa',
                'country_id' => 8,
            ),
            427 => 
            array (
                'id' => 4616,
                'name' => 'Amundsen-Scott',
                'country_id' => 8,
            ),
            428 => 
            array (
                'id' => 4617,
                'name' => 'Arctowski',
                'country_id' => 8,
            ),
            429 => 
            array (
                'id' => 4618,
                'name' => 'Artigas',
                'country_id' => 8,
            ),
            430 => 
            array (
                'id' => 4619,
                'name' => 'Arturo Prat',
                'country_id' => 8,
            ),
            431 => 
            array (
                'id' => 4620,
                'name' => 'Belgrano II',
                'country_id' => 8,
            ),
            432 => 
            array (
                'id' => 4621,
                'name' => 'Bellingshausen',
                'country_id' => 8,
            ),
            433 => 
            array (
                'id' => 4622,
                'name' => 'Casey Station',
                'country_id' => 8,
            ),
            434 => 
            array (
                'id' => 4623,
                'name' => 'Comandante Ferraz',
                'country_id' => 8,
            ),
            435 => 
            array (
                'id' => 4624,
                'name' => 'Concordia',
                'country_id' => 8,
            ),
            436 => 
            array (
                'id' => 4625,
                'name' => 'Davis Station',
                'country_id' => 8,
            ),
            437 => 
            array (
                'id' => 4626,
                'name' => 'Dome Fuji',
                'country_id' => 8,
            ),
            438 => 
            array (
                'id' => 4627,
                'name' => 'Druzhnaya 4',
                'country_id' => 8,
            ),
            439 => 
            array (
                'id' => 4628,
                'name' => 'Dumont d\'Urville Station',
                'country_id' => 8,
            ),
            440 => 
            array (
                'id' => 4629,
                'name' => 'Escudero',
                'country_id' => 8,
            ),
            441 => 
            array (
                'id' => 4630,
                'name' => 'Esperanza',
                'country_id' => 8,
            ),
            442 => 
            array (
                'id' => 4631,
                'name' => 'Gabriel de Castilla',
                'country_id' => 8,
            ),
            443 => 
            array (
                'id' => 4632,
                'name' => 'General Bernardo O\'higgins',
                'country_id' => 8,
            ),
            444 => 
            array (
                'id' => 4633,
                'name' => 'Great Wall',
                'country_id' => 8,
            ),
            445 => 
            array (
                'id' => 4634,
                'name' => 'Halley',
                'country_id' => 8,
            ),
            446 => 
            array (
                'id' => 4635,
                'name' => 'Jang Bogo Station',
                'country_id' => 8,
            ),
            447 => 
            array (
                'id' => 4636,
                'name' => 'Juan Carlos Primero',
                'country_id' => 8,
            ),
            448 => 
            array (
                'id' => 4637,
                'name' => 'Jubany',
                'country_id' => 8,
            ),
            449 => 
            array (
                'id' => 4638,
                'name' => 'King Sejong',
                'country_id' => 8,
            ),
            450 => 
            array (
                'id' => 4639,
                'name' => 'Kohnen',
                'country_id' => 8,
            ),
            451 => 
            array (
                'id' => 4640,
                'name' => 'Law Base',
                'country_id' => 8,
            ),
            452 => 
            array (
                'id' => 4641,
                'name' => 'Maitri',
                'country_id' => 8,
            ),
            453 => 
            array (
                'id' => 4642,
                'name' => 'Marambio',
                'country_id' => 8,
            ),
            454 => 
            array (
                'id' => 4643,
                'name' => 'Mario Zucchelli',
                'country_id' => 8,
            ),
            455 => 
            array (
                'id' => 4644,
                'name' => 'Mawson Station',
                'country_id' => 8,
            ),
            456 => 
            array (
                'id' => 4645,
                'name' => 'McMurdo',
                'country_id' => 8,
            ),
            457 => 
            array (
                'id' => 4646,
                'name' => 'Mirny',
                'country_id' => 8,
            ),
            458 => 
            array (
                'id' => 4647,
                'name' => 'Neumayer',
                'country_id' => 8,
            ),
            459 => 
            array (
                'id' => 4648,
                'name' => 'Novolazarevskaya',
                'country_id' => 8,
            ),
            460 => 
            array (
                'id' => 4649,
                'name' => 'Orcadas',
                'country_id' => 8,
            ),
            461 => 
            array (
                'id' => 4650,
                'name' => 'Palmer',
                'country_id' => 8,
            ),
            462 => 
            array (
                'id' => 4651,
                'name' => 'Port Foster',
                'country_id' => 8,
            ),
            463 => 
            array (
                'id' => 4652,
                'name' => 'Progress',
                'country_id' => 8,
            ),
            464 => 
            array (
                'id' => 4653,
                'name' => 'Rothera',
                'country_id' => 8,
            ),
            465 => 
            array (
                'id' => 4654,
                'name' => 'San Martin',
                'country_id' => 8,
            ),
            466 => 
            array (
                'id' => 4655,
                'name' => 'SANAE IV',
                'country_id' => 8,
            ),
            467 => 
            array (
                'id' => 4656,
                'name' => 'Scott Base',
                'country_id' => 8,
            ),
            468 => 
            array (
                'id' => 4657,
                'name' => 'Signy',
                'country_id' => 8,
            ),
            469 => 
            array (
                'id' => 4658,
                'name' => 'Syowa',
                'country_id' => 8,
            ),
            470 => 
            array (
                'id' => 4659,
                'name' => 'Teniente R. Marsh',
                'country_id' => 8,
            ),
            471 => 
            array (
                'id' => 4660,
                'name' => 'Tor',
                'country_id' => 8,
            ),
            472 => 
            array (
                'id' => 4661,
                'name' => 'Troll',
                'country_id' => 8,
            ),
            473 => 
            array (
                'id' => 4662,
                'name' => 'Vernadsky',
                'country_id' => 8,
            ),
            474 => 
            array (
                'id' => 4663,
                'name' => 'Vostok',
                'country_id' => 8,
            ),
            475 => 
            array (
                'id' => 4664,
                'name' => 'Wasa',
                'country_id' => 8,
            ),
            476 => 
            array (
                'id' => 4665,
                'name' => 'Zhongshan',
                'country_id' => 8,
            ),
            477 => 
            array (
                'id' => 4666,
                'name' => 'Apia',
                'country_id' => 4,
            ),
            478 => 
            array (
                'id' => 4667,
                'name' => 'Fitiuta',
                'country_id' => 4,
            ),
            479 => 
            array (
                'id' => 4668,
                'name' => 'Ofu',
                'country_id' => 4,
            ),
            480 => 
            array (
                'id' => 4669,
                'name' => 'Pago Pago',
                'country_id' => 4,
            ),
            481 => 
            array (
                'id' => 4670,
                'name' => 'Tau',
                'country_id' => 4,
            ),
            482 => 
            array (
                'id' => 4671,
                'name' => 'Aruba',
                'country_id' => 12,
            ),
            483 => 
            array (
                'id' => 4672,
                'name' => 'Barcadera',
                'country_id' => 12,
            ),
            484 => 
            array (
                'id' => 4673,
                'name' => 'Bushiribana',
                'country_id' => 12,
            ),
            485 => 
            array (
                'id' => 4674,
                'name' => 'Druif',
                'country_id' => 12,
            ),
            486 => 
            array (
                'id' => 4675,
                'name' => 'Noord',
                'country_id' => 12,
            ),
            487 => 
            array (
                'id' => 4676,
                'name' => 'Oranjestad',
                'country_id' => 12,
            ),
            488 => 
            array (
                'id' => 4677,
                'name' => 'Palm Beach',
                'country_id' => 12,
            ),
            489 => 
            array (
                'id' => 4678,
                'name' => 'Paradera',
                'country_id' => 12,
            ),
            490 => 
            array (
                'id' => 4679,
                'name' => 'Santa Cruz',
                'country_id' => 12,
            ),
            491 => 
            array (
                'id' => 4680,
                'name' => 'Sint Nicolaas',
                'country_id' => 12,
            ),
            492 => 
            array (
                'id' => 4681,
                'name' => 'Devonshire',
                'country_id' => 24,
            ),
            493 => 
            array (
                'id' => 4682,
                'name' => 'Freeport',
                'country_id' => 24,
            ),
            494 => 
            array (
                'id' => 4683,
                'name' => 'Hamilton',
                'country_id' => 24,
            ),
            495 => 
            array (
                'id' => 4684,
                'name' => 'Harbour Island',
                'country_id' => 24,
            ),
            496 => 
            array (
                'id' => 4685,
                'name' => 'Kings Wharf',
                'country_id' => 24,
            ),
            497 => 
            array (
                'id' => 4686,
                'name' => 'Paget Parish',
                'country_id' => 24,
            ),
            498 => 
            array (
                'id' => 4687,
                'name' => 'Pembroke',
                'country_id' => 24,
            ),
            499 => 
            array (
                'id' => 4688,
                'name' => 'Saint George',
                'country_id' => 24,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 4689,
                'name' => 'Sandys',
                'country_id' => 24,
            ),
            1 => 
            array (
                'id' => 4690,
                'name' => 'Southampton',
                'country_id' => 24,
            ),
            2 => 
            array (
                'id' => 4691,
                'name' => 'Warwick',
                'country_id' => 24,
            ),
            3 => 
            array (
                'id' => 4692,
                'name' => 'Cocos Islands',
                'country_id' => 45,
            ),
            4 => 
            array (
                'id' => 4693,
                'name' => 'Aitutaki',
                'country_id' => 50,
            ),
            5 => 
            array (
                'id' => 4694,
                'name' => 'Arutunga',
                'country_id' => 50,
            ),
            6 => 
            array (
                'id' => 4695,
                'name' => 'Atiu',
                'country_id' => 50,
            ),
            7 => 
            array (
                'id' => 4696,
                'name' => 'Avarua',
                'country_id' => 50,
            ),
            8 => 
            array (
                'id' => 4697,
                'name' => 'Mangaia',
                'country_id' => 50,
            ),
            9 => 
            array (
                'id' => 4698,
                'name' => 'Manihiki Island',
                'country_id' => 50,
            ),
            10 => 
            array (
                'id' => 4699,
                'name' => 'Mauke Island',
                'country_id' => 50,
            ),
            11 => 
            array (
                'id' => 4700,
                'name' => 'Mitiaro Island',
                'country_id' => 50,
            ),
            12 => 
            array (
                'id' => 4701,
                'name' => 'Penrhyn Island',
                'country_id' => 50,
            ),
            13 => 
            array (
                'id' => 4702,
                'name' => 'Puka Puka Island',
                'country_id' => 50,
            ),
            14 => 
            array (
                'id' => 4703,
                'name' => 'Rarotonga',
                'country_id' => 50,
            ),
            15 => 
            array (
                'id' => 4704,
                'name' => 'Bullenbaai',
                'country_id' => 55,
            ),
            16 => 
            array (
                'id' => 4705,
                'name' => 'Caracas Baai',
                'country_id' => 55,
            ),
            17 => 
            array (
                'id' => 4706,
                'name' => 'Curacao',
                'country_id' => 55,
            ),
            18 => 
            array (
                'id' => 4707,
                'name' => 'Dominguito',
                'country_id' => 55,
            ),
            19 => 
            array (
                'id' => 4708,
                'name' => 'Emmastad',
                'country_id' => 55,
            ),
            20 => 
            array (
                'id' => 4709,
                'name' => 'Fuikbaai',
                'country_id' => 55,
            ),
            21 => 
            array (
                'id' => 4710,
                'name' => 'Grote Berg',
                'country_id' => 55,
            ),
            22 => 
            array (
                'id' => 4711,
                'name' => 'Otrobanda',
                'country_id' => 55,
            ),
            23 => 
            array (
                'id' => 4712,
                'name' => 'Punda',
                'country_id' => 55,
            ),
            24 => 
            array (
                'id' => 4713,
                'name' => 'Santa Rosa',
                'country_id' => 55,
            ),
            25 => 
            array (
                'id' => 4714,
                'name' => 'Sint Michielsbaai',
                'country_id' => 55,
            ),
            26 => 
            array (
                'id' => 4715,
                'name' => 'Suffisant Dorp',
                'country_id' => 55,
            ),
            27 => 
            array (
                'id' => 4716,
                'name' => 'Willemstad',
                'country_id' => 55,
            ),
            28 => 
            array (
                'id' => 4717,
                'name' => 'Zeelandia',
                'country_id' => 55,
            ),
            29 => 
            array (
                'id' => 4718,
                'name' => 'Christmas Island',
                'country_id' => 44,
            ),
            30 => 
            array (
                'id' => 4719,
                'name' => 'Flying Fish Cove',
                'country_id' => 44,
            ),
            31 => 
            array (
                'id' => 4720,
                'name' => 'Fox Bay',
                'country_id' => 71,
            ),
            32 => 
            array (
                'id' => 4721,
                'name' => 'Mount Pleasant',
                'country_id' => 71,
            ),
            33 => 
            array (
                'id' => 4722,
                'name' => 'Port Stanley',
                'country_id' => 71,
            ),
            34 => 
            array (
                'id' => 4723,
                'name' => 'Port William',
                'country_id' => 71,
            ),
            35 => 
            array (
                'id' => 4724,
                'name' => 'Eidi',
                'country_id' => 72,
            ),
            36 => 
            array (
                'id' => 4725,
                'name' => 'Faroo Islands-Vagar Apt',
                'country_id' => 72,
            ),
            37 => 
            array (
                'id' => 4726,
                'name' => 'Fuglafjordur',
                'country_id' => 72,
            ),
            38 => 
            array (
                'id' => 4727,
                'name' => 'Fuglefjord = Fuglafirdi',
                'country_id' => 72,
            ),
            39 => 
            array (
                'id' => 4728,
                'name' => 'Glyvrar',
                'country_id' => 72,
            ),
            40 => 
            array (
                'id' => 4729,
                'name' => 'Hoyvik',
                'country_id' => 72,
            ),
            41 => 
            array (
                'id' => 4730,
                'name' => 'Husevig',
                'country_id' => 72,
            ),
            42 => 
            array (
                'id' => 4731,
                'name' => 'Klaksvik',
                'country_id' => 72,
            ),
            43 => 
            array (
                'id' => 4732,
                'name' => 'Kollafjordur',
                'country_id' => 72,
            ),
            44 => 
            array (
                'id' => 4733,
                'name' => 'Lopra',
                'country_id' => 72,
            ),
            45 => 
            array (
                'id' => 4734,
                'name' => 'Nolsoy',
                'country_id' => 72,
            ),
            46 => 
            array (
                'id' => 4735,
                'name' => 'Noryskali',
                'country_id' => 72,
            ),
            47 => 
            array (
                'id' => 4736,
                'name' => 'Runavik',
                'country_id' => 72,
            ),
            48 => 
            array (
                'id' => 4737,
                'name' => 'Sjovar havn',
                'country_id' => 72,
            ),
            49 => 
            array (
                'id' => 4738,
                'name' => 'Skali',
                'country_id' => 72,
            ),
            50 => 
            array (
                'id' => 4739,
                'name' => 'Solmundefjord',
                'country_id' => 72,
            ),
            51 => 
            array (
                'id' => 4740,
                'name' => 'Sorvagur',
                'country_id' => 72,
            ),
            52 => 
            array (
                'id' => 4741,
                'name' => 'Strendur',
                'country_id' => 72,
            ),
            53 => 
            array (
                'id' => 4742,
                'name' => 'Streymnes',
                'country_id' => 72,
            ),
            54 => 
            array (
                'id' => 4743,
                'name' => 'Thorshavn',
                'country_id' => 72,
            ),
            55 => 
            array (
                'id' => 4744,
                'name' => 'Toftir',
                'country_id' => 72,
            ),
            56 => 
            array (
                'id' => 4745,
                'name' => 'Torshavn',
                'country_id' => 72,
            ),
            57 => 
            array (
                'id' => 4746,
                'name' => 'Tvoroyri',
                'country_id' => 72,
            ),
            58 => 
            array (
                'id' => 4747,
                'name' => 'Vadair',
                'country_id' => 72,
            ),
            59 => 
            array (
                'id' => 4748,
                'name' => 'Vagur',
                'country_id' => 72,
            ),
            60 => 
            array (
                'id' => 4749,
                'name' => 'Vagur',
                'country_id' => 72,
            ),
            61 => 
            array (
                'id' => 4750,
                'name' => 'Velbastadur',
                'country_id' => 72,
            ),
            62 => 
            array (
                'id' => 4751,
                'name' => 'Vestmanhavn',
                'country_id' => 72,
            ),
            63 => 
            array (
                'id' => 4752,
                'name' => 'Cayenne',
                'country_id' => 76,
            ),
            64 => 
            array (
                'id' => 4753,
                'name' => 'Degrad des Cannes',
                'country_id' => 76,
            ),
            65 => 
            array (
                'id' => 4754,
                'name' => 'Guadeloupe',
                'country_id' => 76,
            ),
            66 => 
            array (
                'id' => 4755,
                'name' => 'Kourou',
                'country_id' => 76,
            ),
            67 => 
            array (
                'id' => 4756,
                'name' => 'Le Larivot',
                'country_id' => 76,
            ),
            68 => 
            array (
                'id' => 4757,
                'name' => 'Macouria',
                'country_id' => 76,
            ),
            69 => 
            array (
                'id' => 4758,
                'name' => 'Maripasoula',
                'country_id' => 76,
            ),
            70 => 
            array (
                'id' => 4759,
                'name' => 'Matoury',
                'country_id' => 76,
            ),
            71 => 
            array (
                'id' => 4760,
                'name' => 'Regina',
                'country_id' => 76,
            ),
            72 => 
            array (
                'id' => 4761,
                'name' => 'Remire',
                'country_id' => 76,
            ),
            73 => 
            array (
                'id' => 4762,
                'name' => 'Saint Georges de lOyapock',
                'country_id' => 76,
            ),
            74 => 
            array (
                'id' => 4763,
                'name' => 'Saint Laurent du Maroni',
                'country_id' => 76,
            ),
            75 => 
            array (
                'id' => 4764,
                'name' => 'Saul',
                'country_id' => 76,
            ),
            76 => 
            array (
                'id' => 4765,
                'name' => 'Sinnamary',
                'country_id' => 76,
            ),
            77 => 
            array (
                'id' => 4766,
                'name' => 'Alderney',
                'country_id' => 90,
            ),
            78 => 
            array (
                'id' => 4767,
                'name' => 'Guernsey',
                'country_id' => 90,
            ),
            79 => 
            array (
                'id' => 4768,
                'name' => 'Saint Peter Port',
                'country_id' => 90,
            ),
            80 => 
            array (
                'id' => 4769,
                'name' => 'Saint Sampson',
                'country_id' => 90,
            ),
            81 => 
            array (
                'id' => 4770,
                'name' => 'Alderney',
                'country_id' => 90,
            ),
            82 => 
            array (
                'id' => 4771,
                'name' => 'Guernsey',
                'country_id' => 90,
            ),
            83 => 
            array (
                'id' => 4772,
                'name' => 'Saint Peter Port',
                'country_id' => 90,
            ),
            84 => 
            array (
                'id' => 4773,
                'name' => 'Saint Sampson',
                'country_id' => 90,
            ),
            85 => 
            array (
                'id' => 4774,
                'name' => 'Gibraltar',
                'country_id' => 83,
            ),
            86 => 
            array (
                'id' => 4775,
                'name' => 'Waterport',
                'country_id' => 83,
            ),
            87 => 
            array (
                'id' => 4776,
                'name' => 'Baie-Mahault',
                'country_id' => 87,
            ),
            88 => 
            array (
                'id' => 4777,
                'name' => 'Basse-Terre',
                'country_id' => 87,
            ),
            89 => 
            array (
                'id' => 4778,
                'name' => 'Capesterre-Belle-Eau',
                'country_id' => 87,
            ),
            90 => 
            array (
                'id' => 4779,
                'name' => 'Folle-Anse',
                'country_id' => 87,
            ),
            91 => 
            array (
                'id' => 4780,
                'name' => 'Gourbeyre',
                'country_id' => 87,
            ),
            92 => 
            array (
                'id' => 4781,
                'name' => 'Grand-Bourg',
                'country_id' => 87,
            ),
            93 => 
            array (
                'id' => 4782,
                'name' => 'Jarry',
                'country_id' => 87,
            ),
            94 => 
            array (
                'id' => 4783,
                'name' => 'La Desirade',
                'country_id' => 87,
            ),
            95 => 
            array (
                'id' => 4784,
                'name' => 'Lamentin',
                'country_id' => 87,
            ),
            96 => 
            array (
                'id' => 4785,
                'name' => 'Le Gosier',
                'country_id' => 87,
            ),
            97 => 
            array (
                'id' => 4786,
                'name' => 'Le Moule',
                'country_id' => 87,
            ),
            98 => 
            array (
                'id' => 4787,
                'name' => 'Les Abymes',
                'country_id' => 87,
            ),
            99 => 
            array (
                'id' => 4788,
                'name' => 'Marie-Galante',
                'country_id' => 87,
            ),
            100 => 
            array (
                'id' => 4789,
                'name' => 'Marigot',
                'country_id' => 87,
            ),
            101 => 
            array (
                'id' => 4790,
                'name' => 'Petit-Canal',
                'country_id' => 87,
            ),
            102 => 
            array (
                'id' => 4791,
                'name' => 'Pointe-a-Pitre',
                'country_id' => 87,
            ),
            103 => 
            array (
                'id' => 4792,
                'name' => 'Pointe-a-Pitre',
                'country_id' => 87,
            ),
            104 => 
            array (
                'id' => 4793,
                'name' => 'Pointe-Noire',
                'country_id' => 87,
            ),
            105 => 
            array (
                'id' => 4794,
                'name' => 'Port-Louis',
                'country_id' => 87,
            ),
            106 => 
            array (
                'id' => 4795,
                'name' => 'Saint Barthelemy',
                'country_id' => 87,
            ),
            107 => 
            array (
                'id' => 4796,
                'name' => 'Saint Martin Apt',
                'country_id' => 87,
            ),
            108 => 
            array (
                'id' => 4797,
                'name' => 'Saint-Claude',
                'country_id' => 87,
            ),
            109 => 
            array (
                'id' => 4798,
                'name' => 'Sainte Anne',
                'country_id' => 87,
            ),
            110 => 
            array (
                'id' => 4799,
                'name' => 'Sainte Rose',
                'country_id' => 87,
            ),
            111 => 
            array (
                'id' => 4800,
                'name' => 'Saint-Francois',
                'country_id' => 87,
            ),
            112 => 
            array (
                'id' => 4801,
                'name' => 'Terre-de-Bas',
                'country_id' => 87,
            ),
            113 => 
            array (
                'id' => 4802,
                'name' => 'Terre-de-Haut',
                'country_id' => 87,
            ),
            114 => 
            array (
                'id' => 4803,
                'name' => 'Trois-Rivieres',
                'country_id' => 87,
            ),
            115 => 
            array (
                'id' => 4804,
                'name' => 'Agana',
                'country_id' => 88,
            ),
            116 => 
            array (
                'id' => 4805,
            'name' => 'Apra (Agana)',
                'country_id' => 88,
            ),
            117 => 
            array (
                'id' => 4806,
                'name' => 'Barrigada',
                'country_id' => 88,
            ),
            118 => 
            array (
                'id' => 4807,
                'name' => 'Dededo',
                'country_id' => 88,
            ),
            119 => 
            array (
                'id' => 4808,
                'name' => 'Guam',
                'country_id' => 88,
            ),
            120 => 
            array (
                'id' => 4809,
                'name' => 'Hagatna',
                'country_id' => 88,
            ),
            121 => 
            array (
                'id' => 4810,
                'name' => 'Inarajan',
                'country_id' => 88,
            ),
            122 => 
            array (
                'id' => 4811,
                'name' => 'Mongmong-Toto-Maite',
                'country_id' => 88,
            ),
            123 => 
            array (
                'id' => 4812,
                'name' => 'Piti',
                'country_id' => 88,
            ),
            124 => 
            array (
                'id' => 4813,
                'name' => 'Santa Rita',
                'country_id' => 88,
            ),
            125 => 
            array (
                'id' => 4814,
                'name' => 'Sinajana',
                'country_id' => 88,
            ),
            126 => 
            array (
                'id' => 4815,
                'name' => 'Tamuning',
                'country_id' => 88,
            ),
            127 => 
            array (
                'id' => 4816,
                'name' => 'Tumon',
                'country_id' => 88,
            ),
            128 => 
            array (
                'id' => 4817,
                'name' => 'Yigo',
                'country_id' => 88,
            ),
            129 => 
            array (
                'id' => 4818,
                'name' => 'Alderney',
                'country_id' => 90,
            ),
            130 => 
            array (
                'id' => 4819,
                'name' => 'Guernsey',
                'country_id' => 90,
            ),
            131 => 
            array (
                'id' => 4820,
                'name' => 'Saint Peter Port',
                'country_id' => 90,
            ),
            132 => 
            array (
                'id' => 4821,
                'name' => 'Saint Sampson',
                'country_id' => 90,
            ),
            133 => 
            array (
                'id' => 4822,
                'name' => 'Aberdeen',
                'country_id' => 97,
            ),
            134 => 
            array (
                'id' => 4823,
                'name' => 'Ap Lei Chau',
                'country_id' => 97,
            ),
            135 => 
            array (
                'id' => 4824,
                'name' => 'Causeway Bay',
                'country_id' => 97,
            ),
            136 => 
            array (
                'id' => 4825,
                'name' => 'Cha Kwo Ling',
                'country_id' => 97,
            ),
            137 => 
            array (
                'id' => 4826,
                'name' => 'Chai Wan',
                'country_id' => 97,
            ),
            138 => 
            array (
                'id' => 4827,
                'name' => 'Cheung Sha Wan',
                'country_id' => 97,
            ),
            139 => 
            array (
                'id' => 4828,
                'name' => 'Fanling',
                'country_id' => 97,
            ),
            140 => 
            array (
                'id' => 4829,
                'name' => 'Fo Tan',
                'country_id' => 97,
            ),
            141 => 
            array (
                'id' => 4830,
                'name' => 'Ha Kwai Chung',
                'country_id' => 97,
            ),
            142 => 
            array (
                'id' => 4831,
                'name' => 'Hang Hau Town',
                'country_id' => 97,
            ),
            143 => 
            array (
                'id' => 4832,
                'name' => 'Happy Valley',
                'country_id' => 97,
            ),
            144 => 
            array (
                'id' => 4833,
                'name' => 'Hok Tsui Wan',
                'country_id' => 97,
            ),
            145 => 
            array (
                'id' => 4834,
                'name' => 'Hong Kong',
                'country_id' => 97,
            ),
            146 => 
            array (
                'id' => 4835,
                'name' => 'Hong Kong Central',
                'country_id' => 97,
            ),
            147 => 
            array (
                'id' => 4836,
                'name' => 'Hung Hom',
                'country_id' => 97,
            ),
            148 => 
            array (
                'id' => 4837,
                'name' => 'Kennedy Town',
                'country_id' => 97,
            ),
            149 => 
            array (
                'id' => 4838,
                'name' => 'Kowloon',
                'country_id' => 97,
            ),
            150 => 
            array (
                'id' => 4839,
                'name' => 'Kwai Chung',
                'country_id' => 97,
            ),
            151 => 
            array (
                'id' => 4840,
                'name' => 'Kwun Tong',
                'country_id' => 97,
            ),
            152 => 
            array (
                'id' => 4841,
                'name' => 'Lai Chi Kok',
                'country_id' => 97,
            ),
            153 => 
            array (
                'id' => 4842,
                'name' => 'Lam Tei',
                'country_id' => 97,
            ),
            154 => 
            array (
                'id' => 4843,
                'name' => 'Lamma Island',
                'country_id' => 97,
            ),
            155 => 
            array (
                'id' => 4844,
                'name' => 'Lei Yue Mun',
                'country_id' => 97,
            ),
            156 => 
            array (
                'id' => 4845,
                'name' => 'Lok Ma Chau',
                'country_id' => 97,
            ),
            157 => 
            array (
                'id' => 4846,
                'name' => 'Ma Liu Shui',
                'country_id' => 97,
            ),
            158 => 
            array (
                'id' => 4847,
                'name' => 'Ma On Shan Tsuen',
                'country_id' => 97,
            ),
            159 => 
            array (
                'id' => 4848,
                'name' => 'Ma Wan',
                'country_id' => 97,
            ),
            160 => 
            array (
                'id' => 4849,
                'name' => 'Mid Levels',
                'country_id' => 97,
            ),
            161 => 
            array (
                'id' => 4850,
                'name' => 'Mong Kok',
                'country_id' => 97,
            ),
            162 => 
            array (
                'id' => 4851,
                'name' => 'Ngau Chi Wan',
                'country_id' => 97,
            ),
            163 => 
            array (
                'id' => 4852,
                'name' => 'North Point',
                'country_id' => 97,
            ),
            164 => 
            array (
                'id' => 4853,
                'name' => 'Pok Fu Lam',
                'country_id' => 97,
            ),
            165 => 
            array (
                'id' => 4854,
                'name' => 'Quarry Bay',
                'country_id' => 97,
            ),
            166 => 
            array (
                'id' => 4855,
                'name' => 'Repulse Bay',
                'country_id' => 97,
            ),
            167 => 
            array (
                'id' => 4856,
                'name' => 'Sai Kung',
                'country_id' => 97,
            ),
            168 => 
            array (
                'id' => 4857,
                'name' => 'San Po Kong',
                'country_id' => 97,
            ),
            169 => 
            array (
                'id' => 4858,
                'name' => 'Sha Tau Kok',
                'country_id' => 97,
            ),
            170 => 
            array (
                'id' => 4859,
                'name' => 'Sha Tin',
                'country_id' => 97,
            ),
            171 => 
            array (
                'id' => 4860,
                'name' => 'Sham Shui Po',
                'country_id' => 97,
            ),
            172 => 
            array (
                'id' => 4861,
                'name' => 'Sham Tseng',
                'country_id' => 97,
            ),
            173 => 
            array (
                'id' => 4862,
                'name' => 'Shek Kong',
                'country_id' => 97,
            ),
            174 => 
            array (
                'id' => 4863,
                'name' => 'Shek Tong Tsui',
                'country_id' => 97,
            ),
            175 => 
            array (
                'id' => 4864,
                'name' => 'Sheung Shui',
                'country_id' => 97,
            ),
            176 => 
            array (
                'id' => 4865,
                'name' => 'Sheung Wan',
                'country_id' => 97,
            ),
            177 => 
            array (
                'id' => 4866,
                'name' => 'Tai Hang',
                'country_id' => 97,
            ),
            178 => 
            array (
                'id' => 4867,
                'name' => 'Tai Kok Tsui',
                'country_id' => 97,
            ),
            179 => 
            array (
                'id' => 4868,
                'name' => 'Tai Lang Shui',
                'country_id' => 97,
            ),
            180 => 
            array (
                'id' => 4869,
                'name' => 'Tai Mong Tsai',
                'country_id' => 97,
            ),
            181 => 
            array (
                'id' => 4870,
                'name' => 'Tai No',
                'country_id' => 97,
            ),
            182 => 
            array (
                'id' => 4871,
                'name' => 'Tai Po',
                'country_id' => 97,
            ),
            183 => 
            array (
                'id' => 4872,
                'name' => 'Tai Tam',
                'country_id' => 97,
            ),
            184 => 
            array (
                'id' => 4873,
                'name' => 'Tai Wai',
                'country_id' => 97,
            ),
            185 => 
            array (
                'id' => 4874,
                'name' => 'Tin Shui Wai',
                'country_id' => 97,
            ),
            186 => 
            array (
                'id' => 4875,
                'name' => 'Ting Kau',
                'country_id' => 97,
            ),
            187 => 
            array (
                'id' => 4876,
                'name' => 'To Kwa Wan',
                'country_id' => 97,
            ),
            188 => 
            array (
                'id' => 4877,
                'name' => 'Tolo Harbour',
                'country_id' => 97,
            ),
            189 => 
            array (
                'id' => 4878,
                'name' => 'Tseung Kwan O',
                'country_id' => 97,
            ),
            190 => 
            array (
                'id' => 4879,
                'name' => 'Tsim Sha Tsui',
                'country_id' => 97,
            ),
            191 => 
            array (
                'id' => 4880,
                'name' => 'Tsing Yi',
                'country_id' => 97,
            ),
            192 => 
            array (
                'id' => 4881,
                'name' => 'Tsuen Wan',
                'country_id' => 97,
            ),
            193 => 
            array (
                'id' => 4882,
                'name' => 'Tuen Mun',
                'country_id' => 97,
            ),
            194 => 
            array (
                'id' => 4883,
                'name' => 'Victoria',
                'country_id' => 97,
            ),
            195 => 
            array (
                'id' => 4884,
                'name' => 'Wah Fu',
                'country_id' => 97,
            ),
            196 => 
            array (
                'id' => 4885,
                'name' => 'Wan Chai',
                'country_id' => 97,
            ),
            197 => 
            array (
                'id' => 4886,
                'name' => 'Wan Tsai',
                'country_id' => 97,
            ),
            198 => 
            array (
                'id' => 4887,
                'name' => 'Wong Chuk Hang',
                'country_id' => 97,
            ),
            199 => 
            array (
                'id' => 4888,
                'name' => 'Wong Tai Sin',
                'country_id' => 97,
            ),
            200 => 
            array (
                'id' => 4889,
                'name' => 'Yau Ma Tei',
                'country_id' => 97,
            ),
            201 => 
            array (
                'id' => 4890,
                'name' => 'Yau Tong',
                'country_id' => 97,
            ),
            202 => 
            array (
                'id' => 4891,
                'name' => 'Yuen Long',
                'country_id' => 97,
            ),
            203 => 
            array (
                'id' => 4892,
                'name' => 'Bride',
                'country_id' => 105,
            ),
            204 => 
            array (
                'id' => 4893,
                'name' => 'Castletown',
                'country_id' => 105,
            ),
            205 => 
            array (
                'id' => 4894,
                'name' => 'Douglas',
                'country_id' => 105,
            ),
            206 => 
            array (
                'id' => 4895,
                'name' => 'Jurby',
                'country_id' => 105,
            ),
            207 => 
            array (
                'id' => 4896,
                'name' => 'Kirkmichale',
                'country_id' => 105,
            ),
            208 => 
            array (
                'id' => 4897,
                'name' => 'Maughold',
                'country_id' => 105,
            ),
            209 => 
            array (
                'id' => 4898,
                'name' => 'Onchan',
                'country_id' => 105,
            ),
            210 => 
            array (
                'id' => 4899,
                'name' => 'Peel',
                'country_id' => 105,
            ),
            211 => 
            array (
                'id' => 4900,
                'name' => 'Port Saint Mary',
                'country_id' => 105,
            ),
            212 => 
            array (
                'id' => 4901,
                'name' => 'Ramsey',
                'country_id' => 105,
            ),
            213 => 
            array (
                'id' => 4902,
                'name' => 'Ronaldsway Apt',
                'country_id' => 105,
            ),
            214 => 
            array (
                'id' => 4903,
                'name' => 'Santon',
                'country_id' => 105,
            ),
            215 => 
            array (
                'id' => 4904,
                'name' => 'La Fosse',
                'country_id' => 110,
            ),
            216 => 
            array (
                'id' => 4905,
                'name' => 'Saint Aubin',
                'country_id' => 110,
            ),
            217 => 
            array (
                'id' => 4906,
                'name' => 'Saint Clement',
                'country_id' => 110,
            ),
            218 => 
            array (
                'id' => 4907,
                'name' => 'Saint Helier',
                'country_id' => 110,
            ),
            219 => 
            array (
                'id' => 4908,
                'name' => 'Saint Johns',
                'country_id' => 110,
            ),
            220 => 
            array (
                'id' => 4909,
                'name' => 'Saint Peter',
                'country_id' => 110,
            ),
            221 => 
            array (
                'id' => 4910,
                'name' => 'Saint Saviour',
                'country_id' => 110,
            ),
            222 => 
            array (
                'id' => 4911,
                'name' => 'States Apt',
                'country_id' => 110,
            ),
            223 => 
            array (
                'id' => 4912,
                'name' => 'Trinity',
                'country_id' => 110,
            ),
            224 => 
            array (
                'id' => 4913,
                'name' => 'Cayman Brac',
                'country_id' => 39,
            ),
            225 => 
            array (
                'id' => 4914,
                'name' => 'Georgetown',
                'country_id' => 39,
            ),
            226 => 
            array (
                'id' => 4915,
                'name' => 'Grand Cayman',
                'country_id' => 39,
            ),
            227 => 
            array (
                'id' => 4916,
                'name' => 'Little Cayman',
                'country_id' => 39,
            ),
            228 => 
            array (
                'id' => 4917,
                'name' => 'Anse-Marcel',
                'country_id' => 240,
            ),
            229 => 
            array (
                'id' => 4918,
                'name' => 'Baie Orientale',
                'country_id' => 240,
            ),
            230 => 
            array (
                'id' => 4919,
                'name' => 'Galisbay',
                'country_id' => 240,
            ),
            231 => 
            array (
                'id' => 4920,
                'name' => 'Grand-Case',
                'country_id' => 240,
            ),
            232 => 
            array (
                'id' => 4921,
                'name' => 'Grandes Cayes',
                'country_id' => 240,
            ),
            233 => 
            array (
                'id' => 4922,
                'name' => 'LEsperance Apt',
                'country_id' => 240,
            ),
            234 => 
            array (
                'id' => 4923,
                'name' => 'Marigot',
                'country_id' => 240,
            ),
            235 => 
            array (
                'id' => 4924,
                'name' => 'Macau',
                'country_id' => 128,
            ),
            236 => 
            array (
                'id' => 4925,
                'name' => 'Rota',
                'country_id' => 163,
            ),
            237 => 
            array (
                'id' => 4926,
                'name' => 'Saipan',
                'country_id' => 163,
            ),
            238 => 
            array (
                'id' => 4927,
                'name' => 'Tinian',
                'country_id' => 163,
            ),
            239 => 
            array (
                'id' => 4928,
                'name' => 'Bellefonaine',
                'country_id' => 137,
            ),
            240 => 
            array (
                'id' => 4929,
                'name' => 'Ducos',
                'country_id' => 137,
            ),
            241 => 
            array (
                'id' => 4930,
                'name' => 'Fonds-Saint-Denis',
                'country_id' => 137,
            ),
            242 => 
            array (
                'id' => 4931,
                'name' => 'Fort-de-France',
                'country_id' => 137,
            ),
            243 => 
            array (
                'id' => 4932,
                'name' => 'Gros-Morne',
                'country_id' => 137,
            ),
            244 => 
            array (
                'id' => 4933,
                'name' => 'La Trinite',
                'country_id' => 137,
            ),
            245 => 
            array (
                'id' => 4934,
                'name' => 'Le Carbet',
                'country_id' => 137,
            ),
            246 => 
            array (
                'id' => 4935,
                'name' => 'Le Diamant',
                'country_id' => 137,
            ),
            247 => 
            array (
                'id' => 4936,
                'name' => 'Le Francois',
                'country_id' => 137,
            ),
            248 => 
            array (
                'id' => 4937,
                'name' => 'Le Lamentin',
                'country_id' => 137,
            ),
            249 => 
            array (
                'id' => 4938,
                'name' => 'Le Lorrain',
                'country_id' => 137,
            ),
            250 => 
            array (
                'id' => 4939,
                'name' => 'Le Marin',
                'country_id' => 137,
            ),
            251 => 
            array (
                'id' => 4940,
                'name' => 'Le Morne Rouge',
                'country_id' => 137,
            ),
            252 => 
            array (
                'id' => 4941,
                'name' => 'Le Robert',
                'country_id' => 137,
            ),
            253 => 
            array (
                'id' => 4942,
                'name' => 'Les Trois-Ilets',
                'country_id' => 137,
            ),
            254 => 
            array (
                'id' => 4943,
                'name' => 'Macouba',
                'country_id' => 137,
            ),
            255 => 
            array (
                'id' => 4944,
                'name' => 'Pointe Des Carrieres',
                'country_id' => 137,
            ),
            256 => 
            array (
                'id' => 4945,
                'name' => 'Riviere-Pilote',
                'country_id' => 137,
            ),
            257 => 
            array (
                'id' => 4946,
                'name' => 'Saint Pierre',
                'country_id' => 137,
            ),
            258 => 
            array (
                'id' => 4947,
                'name' => 'Sainte-Anne',
                'country_id' => 137,
            ),
            259 => 
            array (
                'id' => 4948,
                'name' => 'Sainte-Marie',
                'country_id' => 137,
            ),
            260 => 
            array (
                'id' => 4949,
                'name' => 'Saint-Joseph',
                'country_id' => 137,
            ),
            261 => 
            array (
                'id' => 4950,
                'name' => 'Scholcher',
                'country_id' => 137,
            ),
            262 => 
            array (
                'id' => 4951,
                'name' => 'Brades',
                'country_id' => 147,
            ),
            263 => 
            array (
                'id' => 4952,
                'name' => 'Little Bay',
                'country_id' => 147,
            ),
            264 => 
            array (
                'id' => 4953,
                'name' => 'Montserrat',
                'country_id' => 147,
            ),
            265 => 
            array (
                'id' => 4954,
                'name' => 'Plymouth',
                'country_id' => 147,
            ),
            266 => 
            array (
                'id' => 4955,
                'name' => 'Baie de Prony',
                'country_id' => 156,
            ),
            267 => 
            array (
                'id' => 4956,
                'name' => 'Baie Ugue',
                'country_id' => 156,
            ),
            268 => 
            array (
                'id' => 4957,
                'name' => 'Belep Island',
                'country_id' => 156,
            ),
            269 => 
            array (
                'id' => 4958,
                'name' => 'Bourail',
                'country_id' => 156,
            ),
            270 => 
            array (
                'id' => 4959,
                'name' => 'Doniambo/Noumea',
                'country_id' => 156,
            ),
            271 => 
            array (
                'id' => 4960,
                'name' => 'Ducos',
                'country_id' => 156,
            ),
            272 => 
            array (
                'id' => 4961,
                'name' => 'Dumbea',
                'country_id' => 156,
            ),
            273 => 
            array (
                'id' => 4962,
                'name' => 'Hienghene',
                'country_id' => 156,
            ),
            274 => 
            array (
                'id' => 4963,
                'name' => 'Houailou',
                'country_id' => 156,
            ),
            275 => 
            array (
                'id' => 4964,
                'name' => 'Ile des Pins',
                'country_id' => 156,
            ),
            276 => 
            array (
                'id' => 4965,
                'name' => 'Ile Quen',
                'country_id' => 156,
            ),
            277 => 
            array (
                'id' => 4966,
                'name' => 'Kone',
                'country_id' => 156,
            ),
            278 => 
            array (
                'id' => 4967,
                'name' => 'Kouaoua',
                'country_id' => 156,
            ),
            279 => 
            array (
                'id' => 4968,
                'name' => 'Koumac',
                'country_id' => 156,
            ),
            280 => 
            array (
                'id' => 4969,
                'name' => 'Lifou',
                'country_id' => 156,
            ),
            281 => 
            array (
                'id' => 4970,
                'name' => 'Mare',
                'country_id' => 156,
            ),
            282 => 
            array (
                'id' => 4971,
                'name' => 'Mont-Dore',
                'country_id' => 156,
            ),
            283 => 
            array (
                'id' => 4972,
                'name' => 'Nakety',
                'country_id' => 156,
            ),
            284 => 
            array (
                'id' => 4973,
                'name' => 'Nepoui',
                'country_id' => 156,
            ),
            285 => 
            array (
                'id' => 4974,
                'name' => 'Noumea',
                'country_id' => 156,
            ),
            286 => 
            array (
                'id' => 4975,
                'name' => 'Noumea Magenta Airport',
                'country_id' => 156,
            ),
            287 => 
            array (
                'id' => 4976,
                'name' => 'Ouvea',
                'country_id' => 156,
            ),
            288 => 
            array (
                'id' => 4977,
                'name' => 'Poro',
                'country_id' => 156,
            ),
            289 => 
            array (
                'id' => 4978,
                'name' => 'Pouembout',
                'country_id' => 156,
            ),
            290 => 
            array (
                'id' => 4979,
                'name' => 'Prony',
                'country_id' => 156,
            ),
            291 => 
            array (
                'id' => 4980,
                'name' => 'Station de Mueo',
                'country_id' => 156,
            ),
            292 => 
            array (
                'id' => 4981,
                'name' => 'Teoudie',
                'country_id' => 156,
            ),
            293 => 
            array (
                'id' => 4982,
                'name' => 'Thio',
                'country_id' => 156,
            ),
            294 => 
            array (
                'id' => 4983,
                'name' => 'Tiga',
                'country_id' => 156,
            ),
            295 => 
            array (
                'id' => 4984,
                'name' => 'Tontouta',
                'country_id' => 156,
            ),
            296 => 
            array (
                'id' => 4985,
                'name' => 'Touho',
                'country_id' => 156,
            ),
            297 => 
            array (
                'id' => 4986,
                'name' => 'Vavouto',
                'country_id' => 156,
            ),
            298 => 
            array (
                'id' => 4987,
                'name' => 'Wala',
                'country_id' => 156,
            ),
            299 => 
            array (
                'id' => 4988,
                'name' => 'Norfolk Island',
                'country_id' => 162,
            ),
            300 => 
            array (
                'id' => 4989,
                'name' => 'Alofi',
                'country_id' => 161,
            ),
            301 => 
            array (
                'id' => 4990,
                'name' => 'Niue Island',
                'country_id' => 161,
            ),
            302 => 
            array (
                'id' => 4991,
                'name' => 'Ahe',
                'country_id' => 77,
            ),
            303 => 
            array (
                'id' => 4992,
                'name' => 'Ahurei',
                'country_id' => 77,
            ),
            304 => 
            array (
                'id' => 4993,
                'name' => 'Anaa',
                'country_id' => 77,
            ),
            305 => 
            array (
                'id' => 4994,
                'name' => 'Apataki',
                'country_id' => 77,
            ),
            306 => 
            array (
                'id' => 4995,
                'name' => 'Arue',
                'country_id' => 77,
            ),
            307 => 
            array (
                'id' => 4996,
                'name' => 'Arutua',
                'country_id' => 77,
            ),
            308 => 
            array (
                'id' => 4997,
                'name' => 'Atoll Hereheretue',
                'country_id' => 77,
            ),
            309 => 
            array (
                'id' => 4998,
                'name' => 'Atoll Pukapuka',
                'country_id' => 77,
            ),
            310 => 
            array (
                'id' => 4999,
                'name' => 'Atuona',
                'country_id' => 77,
            ),
            311 => 
            array (
                'id' => 5000,
                'name' => 'Auti',
                'country_id' => 77,
            ),
            312 => 
            array (
                'id' => 5001,
                'name' => 'Bora-Bora',
                'country_id' => 77,
            ),
            313 => 
            array (
                'id' => 5002,
                'name' => 'Faaa',
                'country_id' => 77,
            ),
            314 => 
            array (
                'id' => 5003,
                'name' => 'Faaite',
                'country_id' => 77,
            ),
            315 => 
            array (
                'id' => 5004,
                'name' => 'Fakahina',
                'country_id' => 77,
            ),
            316 => 
            array (
                'id' => 5005,
                'name' => 'Fakarava',
                'country_id' => 77,
            ),
            317 => 
            array (
                'id' => 5006,
                'name' => 'Fangatau',
                'country_id' => 77,
            ),
            318 => 
            array (
                'id' => 5007,
                'name' => 'Fatu Hiva',
                'country_id' => 77,
            ),
            319 => 
            array (
                'id' => 5008,
                'name' => 'Hao Is',
                'country_id' => 77,
            ),
            320 => 
            array (
                'id' => 5009,
                'name' => 'Hatiheu',
                'country_id' => 77,
            ),
            321 => 
            array (
                'id' => 5010,
                'name' => 'Hikueru',
                'country_id' => 77,
            ),
            322 => 
            array (
                'id' => 5011,
                'name' => 'Hiva Oa',
                'country_id' => 77,
            ),
            323 => 
            array (
                'id' => 5012,
                'name' => 'Huahine',
                'country_id' => 77,
            ),
            324 => 
            array (
                'id' => 5013,
                'name' => 'Ikitake',
                'country_id' => 77,
            ),
            325 => 
            array (
                'id' => 5014,
                'name' => 'Iles Gambier',
                'country_id' => 77,
            ),
            326 => 
            array (
                'id' => 5015,
                'name' => 'Kaukura',
                'country_id' => 77,
            ),
            327 => 
            array (
                'id' => 5016,
                'name' => 'Makemo',
                'country_id' => 77,
            ),
            328 => 
            array (
                'id' => 5017,
                'name' => 'Manihi',
                'country_id' => 77,
            ),
            329 => 
            array (
                'id' => 5018,
                'name' => 'Maroe',
                'country_id' => 77,
            ),
            330 => 
            array (
                'id' => 5019,
                'name' => 'Mataiva',
                'country_id' => 77,
            ),
            331 => 
            array (
                'id' => 5020,
                'name' => 'Maupiti',
                'country_id' => 77,
            ),
            332 => 
            array (
                'id' => 5021,
                'name' => 'Moerai',
                'country_id' => 77,
            ),
            333 => 
            array (
                'id' => 5022,
                'name' => 'Moorea',
                'country_id' => 77,
            ),
            334 => 
            array (
                'id' => 5023,
                'name' => 'Napuka Island',
                'country_id' => 77,
            ),
            335 => 
            array (
                'id' => 5024,
                'name' => 'Nuku Hiva',
                'country_id' => 77,
            ),
            336 => 
            array (
                'id' => 5025,
                'name' => 'Nukutavake',
                'country_id' => 77,
            ),
            337 => 
            array (
                'id' => 5026,
                'name' => 'Otepa',
                'country_id' => 77,
            ),
            338 => 
            array (
                'id' => 5027,
                'name' => 'Papara',
                'country_id' => 77,
            ),
            339 => 
            array (
                'id' => 5028,
                'name' => 'Papeete',
                'country_id' => 77,
            ),
            340 => 
            array (
                'id' => 5029,
                'name' => 'Pukarua',
                'country_id' => 77,
            ),
            341 => 
            array (
                'id' => 5030,
                'name' => 'Punaauia',
                'country_id' => 77,
            ),
            342 => 
            array (
                'id' => 5031,
                'name' => 'Raiatea',
                'country_id' => 77,
            ),
            343 => 
            array (
                'id' => 5032,
                'name' => 'Raivavae',
                'country_id' => 77,
            ),
            344 => 
            array (
                'id' => 5033,
                'name' => 'Rangiroa',
                'country_id' => 77,
            ),
            345 => 
            array (
                'id' => 5034,
                'name' => 'Reao',
                'country_id' => 77,
            ),
            346 => 
            array (
                'id' => 5035,
                'name' => 'Rurutu',
                'country_id' => 77,
            ),
            347 => 
            array (
                'id' => 5036,
                'name' => 'Taiohae',
                'country_id' => 77,
            ),
            348 => 
            array (
                'id' => 5037,
                'name' => 'Takapoto',
                'country_id' => 77,
            ),
            349 => 
            array (
                'id' => 5038,
                'name' => 'Takaroa',
                'country_id' => 77,
            ),
            350 => 
            array (
                'id' => 5039,
                'name' => 'Takume',
                'country_id' => 77,
            ),
            351 => 
            array (
                'id' => 5040,
                'name' => 'Tatakoto',
                'country_id' => 77,
            ),
            352 => 
            array (
                'id' => 5041,
                'name' => 'Tetiaroa Is',
                'country_id' => 77,
            ),
            353 => 
            array (
                'id' => 5042,
                'name' => 'Tikehau',
                'country_id' => 77,
            ),
            354 => 
            array (
                'id' => 5043,
                'name' => 'Tubuai',
                'country_id' => 77,
            ),
            355 => 
            array (
                'id' => 5044,
                'name' => 'Tupana',
                'country_id' => 77,
            ),
            356 => 
            array (
                'id' => 5045,
                'name' => 'Tureia',
                'country_id' => 77,
            ),
            357 => 
            array (
                'id' => 5046,
                'name' => 'Ua Huka',
                'country_id' => 77,
            ),
            358 => 
            array (
                'id' => 5047,
                'name' => 'Ua Pou',
                'country_id' => 77,
            ),
            359 => 
            array (
                'id' => 5048,
                'name' => 'Uturoa',
                'country_id' => 77,
            ),
            360 => 
            array (
                'id' => 5049,
                'name' => 'Vahitahi',
                'country_id' => 77,
            ),
            361 => 
            array (
                'id' => 5050,
                'name' => 'Vaitape',
                'country_id' => 77,
            ),
            362 => 
            array (
                'id' => 5051,
                'name' => 'Miquelon',
                'country_id' => 186,
            ),
            363 => 
            array (
                'id' => 5052,
                'name' => 'Saint Pierre',
                'country_id' => 186,
            ),
            364 => 
            array (
                'id' => 5053,
                'name' => 'Pitcairn Is',
                'country_id' => 174,
            ),
            365 => 
            array (
                'id' => 5054,
                'name' => 'Adjuntas',
                'country_id' => 177,
            ),
            366 => 
            array (
                'id' => 5055,
                'name' => 'Aguada',
                'country_id' => 177,
            ),
            367 => 
            array (
                'id' => 5056,
                'name' => 'Aguadilla',
                'country_id' => 177,
            ),
            368 => 
            array (
                'id' => 5057,
                'name' => 'Aguas Buenas',
                'country_id' => 177,
            ),
            369 => 
            array (
                'id' => 5058,
                'name' => 'Aguirre',
                'country_id' => 177,
            ),
            370 => 
            array (
                'id' => 5059,
                'name' => 'Aibonito',
                'country_id' => 177,
            ),
            371 => 
            array (
                'id' => 5060,
                'name' => 'Anasco',
                'country_id' => 177,
            ),
            372 => 
            array (
                'id' => 5061,
                'name' => 'Arecibo',
                'country_id' => 177,
            ),
            373 => 
            array (
                'id' => 5062,
                'name' => 'Arroyo',
                'country_id' => 177,
            ),
            374 => 
            array (
                'id' => 5063,
                'name' => 'Barceloneta',
                'country_id' => 177,
            ),
            375 => 
            array (
                'id' => 5064,
                'name' => 'Barranquitas',
                'country_id' => 177,
            ),
            376 => 
            array (
                'id' => 5065,
                'name' => 'Bayamon',
                'country_id' => 177,
            ),
            377 => 
            array (
                'id' => 5066,
                'name' => 'Boqueron',
                'country_id' => 177,
            ),
            378 => 
            array (
                'id' => 5067,
                'name' => 'Cabo Rojo',
                'country_id' => 177,
            ),
            379 => 
            array (
                'id' => 5068,
                'name' => 'Caguas',
                'country_id' => 177,
            ),
            380 => 
            array (
                'id' => 5069,
                'name' => 'Camuy',
                'country_id' => 177,
            ),
            381 => 
            array (
                'id' => 5070,
                'name' => 'Canovanas',
                'country_id' => 177,
            ),
            382 => 
            array (
                'id' => 5071,
                'name' => 'Caparra Hills',
                'country_id' => 177,
            ),
            383 => 
            array (
                'id' => 5072,
                'name' => 'Carolina',
                'country_id' => 177,
            ),
            384 => 
            array (
                'id' => 5073,
                'name' => 'Catano',
                'country_id' => 177,
            ),
            385 => 
            array (
                'id' => 5074,
                'name' => 'Cayey',
                'country_id' => 177,
            ),
            386 => 
            array (
                'id' => 5075,
                'name' => 'Ceiba',
                'country_id' => 177,
            ),
            387 => 
            array (
                'id' => 5076,
                'name' => 'Ciales',
                'country_id' => 177,
            ),
            388 => 
            array (
                'id' => 5077,
                'name' => 'Cidra',
                'country_id' => 177,
            ),
            389 => 
            array (
                'id' => 5078,
                'name' => 'Coamo',
                'country_id' => 177,
            ),
            390 => 
            array (
                'id' => 5079,
                'name' => 'Comerio',
                'country_id' => 177,
            ),
            391 => 
            array (
                'id' => 5080,
                'name' => 'Corozal',
                'country_id' => 177,
            ),
            392 => 
            array (
                'id' => 5081,
                'name' => 'Coto Laurel',
                'country_id' => 177,
            ),
            393 => 
            array (
                'id' => 5082,
                'name' => 'Culebra',
                'country_id' => 177,
            ),
            394 => 
            array (
                'id' => 5083,
                'name' => 'Cupey',
                'country_id' => 177,
            ),
            395 => 
            array (
                'id' => 5084,
                'name' => 'Dorado',
                'country_id' => 177,
            ),
            396 => 
            array (
                'id' => 5085,
                'name' => 'Fajardo',
                'country_id' => 177,
            ),
            397 => 
            array (
                'id' => 5086,
                'name' => 'Florida',
                'country_id' => 177,
            ),
            398 => 
            array (
                'id' => 5087,
                'name' => 'Fort Buchanan',
                'country_id' => 177,
            ),
            399 => 
            array (
                'id' => 5088,
                'name' => 'Guabano',
                'country_id' => 177,
            ),
            400 => 
            array (
                'id' => 5089,
                'name' => 'Guanica',
                'country_id' => 177,
            ),
            401 => 
            array (
                'id' => 5090,
                'name' => 'Guayama',
                'country_id' => 177,
            ),
            402 => 
            array (
                'id' => 5091,
                'name' => 'Guayanilla',
                'country_id' => 177,
            ),
            403 => 
            array (
                'id' => 5092,
                'name' => 'Guaynabo',
                'country_id' => 177,
            ),
            404 => 
            array (
                'id' => 5093,
                'name' => 'Gurabo',
                'country_id' => 177,
            ),
            405 => 
            array (
                'id' => 5094,
                'name' => 'Hatillo',
                'country_id' => 177,
            ),
            406 => 
            array (
                'id' => 5095,
                'name' => 'Hato Rey',
                'country_id' => 177,
            ),
            407 => 
            array (
                'id' => 5096,
                'name' => 'Hato Tejas',
                'country_id' => 177,
            ),
            408 => 
            array (
                'id' => 5097,
                'name' => 'Hormigueros',
                'country_id' => 177,
            ),
            409 => 
            array (
                'id' => 5098,
                'name' => 'Humacao',
                'country_id' => 177,
            ),
            410 => 
            array (
                'id' => 5099,
                'name' => 'Isabela',
                'country_id' => 177,
            ),
            411 => 
            array (
                'id' => 5100,
                'name' => 'Jayuya',
                'country_id' => 177,
            ),
            412 => 
            array (
                'id' => 5101,
                'name' => 'Jicome',
                'country_id' => 177,
            ),
            413 => 
            array (
                'id' => 5102,
                'name' => 'Juana Diaz',
                'country_id' => 177,
            ),
            414 => 
            array (
                'id' => 5103,
                'name' => 'Juncos',
                'country_id' => 177,
            ),
            415 => 
            array (
                'id' => 5104,
                'name' => 'Lajas',
                'country_id' => 177,
            ),
            416 => 
            array (
                'id' => 5105,
                'name' => 'Lares',
                'country_id' => 177,
            ),
            417 => 
            array (
                'id' => 5106,
            'name' => 'Las Mareas (Guayama)',
                'country_id' => 177,
            ),
            418 => 
            array (
                'id' => 5107,
                'name' => 'Las Marias',
                'country_id' => 177,
            ),
            419 => 
            array (
                'id' => 5108,
                'name' => 'Las Piedras',
                'country_id' => 177,
            ),
            420 => 
            array (
                'id' => 5109,
                'name' => 'Las Pinas',
                'country_id' => 177,
            ),
            421 => 
            array (
                'id' => 5110,
                'name' => 'Loiza',
                'country_id' => 177,
            ),
            422 => 
            array (
                'id' => 5111,
                'name' => 'Luquillo',
                'country_id' => 177,
            ),
            423 => 
            array (
                'id' => 5112,
                'name' => 'Manati',
                'country_id' => 177,
            ),
            424 => 
            array (
                'id' => 5113,
                'name' => 'Maricao',
                'country_id' => 177,
            ),
            425 => 
            array (
                'id' => 5114,
                'name' => 'Maunabo',
                'country_id' => 177,
            ),
            426 => 
            array (
                'id' => 5115,
                'name' => 'Mayaguez',
                'country_id' => 177,
            ),
            427 => 
            array (
                'id' => 5116,
                'name' => 'Miramar',
                'country_id' => 177,
            ),
            428 => 
            array (
                'id' => 5117,
                'name' => 'Moca',
                'country_id' => 177,
            ),
            429 => 
            array (
                'id' => 5118,
                'name' => 'Morovis',
                'country_id' => 177,
            ),
            430 => 
            array (
                'id' => 5119,
                'name' => 'Naguabo',
                'country_id' => 177,
            ),
            431 => 
            array (
                'id' => 5120,
                'name' => 'Naranjito',
                'country_id' => 177,
            ),
            432 => 
            array (
                'id' => 5121,
                'name' => 'Noxell',
                'country_id' => 177,
            ),
            433 => 
            array (
                'id' => 5122,
                'name' => 'Orocovis',
                'country_id' => 177,
            ),
            434 => 
            array (
                'id' => 5123,
                'name' => 'Patillas',
                'country_id' => 177,
            ),
            435 => 
            array (
                'id' => 5124,
                'name' => 'Penuelas',
                'country_id' => 177,
            ),
            436 => 
            array (
                'id' => 5125,
                'name' => 'Ponce',
                'country_id' => 177,
            ),
            437 => 
            array (
                'id' => 5126,
                'name' => 'Puerta de Tierra',
                'country_id' => 177,
            ),
            438 => 
            array (
                'id' => 5127,
                'name' => 'Puerto de Jobos',
                'country_id' => 177,
            ),
            439 => 
            array (
                'id' => 5128,
                'name' => 'Puerto Nuevo',
                'country_id' => 177,
            ),
            440 => 
            array (
                'id' => 5129,
                'name' => 'Puerto Yabucoa',
                'country_id' => 177,
            ),
            441 => 
            array (
                'id' => 5130,
                'name' => 'Quebradillas',
                'country_id' => 177,
            ),
            442 => 
            array (
                'id' => 5131,
                'name' => 'Rincon',
                'country_id' => 177,
            ),
            443 => 
            array (
                'id' => 5132,
                'name' => 'Rio Grande',
                'country_id' => 177,
            ),
            444 => 
            array (
                'id' => 5133,
                'name' => 'Rio Piedras',
                'country_id' => 177,
            ),
            445 => 
            array (
                'id' => 5134,
                'name' => 'Roosevelt Roads',
                'country_id' => 177,
            ),
            446 => 
            array (
                'id' => 5135,
                'name' => 'Sabana Grande',
                'country_id' => 177,
            ),
            447 => 
            array (
                'id' => 5136,
                'name' => 'Sabana Seca',
                'country_id' => 177,
            ),
            448 => 
            array (
                'id' => 5137,
                'name' => 'Salinas',
                'country_id' => 177,
            ),
            449 => 
            array (
                'id' => 5138,
                'name' => 'San German',
                'country_id' => 177,
            ),
            450 => 
            array (
                'id' => 5139,
                'name' => 'San Juan',
                'country_id' => 177,
            ),
            451 => 
            array (
                'id' => 5140,
                'name' => 'San Lorenzo',
                'country_id' => 177,
            ),
            452 => 
            array (
                'id' => 5141,
                'name' => 'San Sebastian',
                'country_id' => 177,
            ),
            453 => 
            array (
                'id' => 5142,
                'name' => 'Santa Isabel',
                'country_id' => 177,
            ),
            454 => 
            array (
                'id' => 5143,
                'name' => 'Toa Alta',
                'country_id' => 177,
            ),
            455 => 
            array (
                'id' => 5144,
                'name' => 'Toa Baja',
                'country_id' => 177,
            ),
            456 => 
            array (
                'id' => 5145,
                'name' => 'Tottillas',
                'country_id' => 177,
            ),
            457 => 
            array (
                'id' => 5146,
                'name' => 'Trujillo Alto',
                'country_id' => 177,
            ),
            458 => 
            array (
                'id' => 5147,
                'name' => 'Trujillo Bajo',
                'country_id' => 177,
            ),
            459 => 
            array (
                'id' => 5148,
                'name' => 'Utuado',
                'country_id' => 177,
            ),
            460 => 
            array (
                'id' => 5149,
                'name' => 'Vega Alta',
                'country_id' => 177,
            ),
            461 => 
            array (
                'id' => 5150,
                'name' => 'Vega Baja',
                'country_id' => 177,
            ),
            462 => 
            array (
                'id' => 5151,
                'name' => 'Vieques',
                'country_id' => 177,
            ),
            463 => 
            array (
                'id' => 5152,
                'name' => 'Villalba',
                'country_id' => 177,
            ),
            464 => 
            array (
                'id' => 5153,
                'name' => 'Yabucoa',
                'country_id' => 177,
            ),
            465 => 
            array (
                'id' => 5154,
                'name' => 'Yauco',
                'country_id' => 177,
            ),
            466 => 
            array (
                'id' => 5155,
                'name' => 'Bras Panon',
                'country_id' => 179,
            ),
            467 => 
            array (
                'id' => 5156,
                'name' => 'Etang Sale les Hauts',
                'country_id' => 179,
            ),
            468 => 
            array (
                'id' => 5157,
                'name' => 'La Saline les Hauts',
                'country_id' => 179,
            ),
            469 => 
            array (
                'id' => 5158,
                'name' => 'Le Port',
                'country_id' => 179,
            ),
            470 => 
            array (
                'id' => 5159,
                'name' => 'Le Tampon',
                'country_id' => 179,
            ),
            471 => 
            array (
                'id' => 5160,
                'name' => 'Les Avirons',
                'country_id' => 179,
            ),
            472 => 
            array (
                'id' => 5161,
                'name' => 'LEtang-Sale',
                'country_id' => 179,
            ),
            473 => 
            array (
                'id' => 5162,
                'name' => 'Plaine des Cafres',
                'country_id' => 179,
            ),
            474 => 
            array (
                'id' => 5163,
                'name' => 'Plaine des Palmistes',
                'country_id' => 179,
            ),
            475 => 
            array (
                'id' => 5164,
                'name' => 'Pointe des Galets',
                'country_id' => 179,
            ),
            476 => 
            array (
                'id' => 5165,
                'name' => 'Port de Pointe des Galets',
                'country_id' => 179,
            ),
            477 => 
            array (
                'id' => 5166,
                'name' => 'Possession',
                'country_id' => 179,
            ),
            478 => 
            array (
                'id' => 5167,
                'name' => 'Reunion',
                'country_id' => 179,
            ),
            479 => 
            array (
                'id' => 5168,
                'name' => 'Saint Benoit',
                'country_id' => 179,
            ),
            480 => 
            array (
                'id' => 5169,
                'name' => 'Saint Leu',
                'country_id' => 179,
            ),
            481 => 
            array (
                'id' => 5170,
                'name' => 'Saint Pierre de la Reunion',
                'country_id' => 179,
            ),
            482 => 
            array (
                'id' => 5171,
                'name' => 'Saint-Andre',
                'country_id' => 179,
            ),
            483 => 
            array (
                'id' => 5172,
                'name' => 'Saint-Benoit',
                'country_id' => 179,
            ),
            484 => 
            array (
                'id' => 5173,
                'name' => 'Saint-Denis de la Reunion Apt',
                'country_id' => 179,
            ),
            485 => 
            array (
                'id' => 5174,
                'name' => 'Sainte Anne',
                'country_id' => 179,
            ),
            486 => 
            array (
                'id' => 5175,
                'name' => 'Sainte Clotilde',
                'country_id' => 179,
            ),
            487 => 
            array (
                'id' => 5176,
                'name' => 'Sainte Marie',
                'country_id' => 179,
            ),
            488 => 
            array (
                'id' => 5177,
                'name' => 'Sainte Suzanne',
                'country_id' => 179,
            ),
            489 => 
            array (
                'id' => 5178,
                'name' => 'Saint-Gilles-les Bains',
                'country_id' => 179,
            ),
            490 => 
            array (
                'id' => 5179,
                'name' => 'Saint-Gilles-Les Hauts',
                'country_id' => 179,
            ),
            491 => 
            array (
                'id' => 5180,
                'name' => 'Saint-Joseph',
                'country_id' => 179,
            ),
            492 => 
            array (
                'id' => 5181,
                'name' => 'Saint-Paul',
                'country_id' => 179,
            ),
            493 => 
            array (
                'id' => 5182,
                'name' => 'Saint-Philippe',
                'country_id' => 179,
            ),
            494 => 
            array (
                'id' => 5183,
                'name' => 'Sat Louis',
                'country_id' => 179,
            ),
            495 => 
            array (
                'id' => 5184,
                'name' => 'Trois Bassins',
                'country_id' => 179,
            ),
            496 => 
            array (
                'id' => 5185,
                'name' => 'Cockburn Harbour',
                'country_id' => 222,
            ),
            497 => 
            array (
                'id' => 5186,
                'name' => 'Grand Turk Island',
                'country_id' => 222,
            ),
            498 => 
            array (
                'id' => 5187,
                'name' => 'Middle Caicos',
                'country_id' => 222,
            ),
            499 => 
            array (
                'id' => 5188,
                'name' => 'North Caicos',
                'country_id' => 222,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 5189,
                'name' => 'Pine Cay',
                'country_id' => 222,
            ),
            1 => 
            array (
                'id' => 5190,
                'name' => 'Providenciales',
                'country_id' => 222,
            ),
            2 => 
            array (
                'id' => 5191,
                'name' => 'Salt Cay',
                'country_id' => 222,
            ),
            3 => 
            array (
                'id' => 5192,
                'name' => 'South Caicos',
                'country_id' => 222,
            ),
            4 => 
            array (
                'id' => 5193,
                'name' => 'Atafu',
                'country_id' => 216,
            ),
            5 => 
            array (
                'id' => 5194,
                'name' => 'Fakaofo',
                'country_id' => 216,
            ),
            6 => 
            array (
                'id' => 5195,
                'name' => 'Nukunonu',
                'country_id' => 216,
            ),
            7 => 
            array (
                'id' => 5196,
                'name' => 'Aberaeron',
                'country_id' => 227,
            ),
            8 => 
            array (
                'id' => 5197,
                'name' => 'Aberdare',
                'country_id' => 227,
            ),
            9 => 
            array (
                'id' => 5198,
                'name' => 'Aberdeen',
                'country_id' => 227,
            ),
            10 => 
            array (
                'id' => 5199,
                'name' => 'Aberfeldy',
                'country_id' => 227,
            ),
            11 => 
            array (
                'id' => 5200,
                'name' => 'Abergavenny',
                'country_id' => 227,
            ),
            12 => 
            array (
                'id' => 5201,
                'name' => 'Abergele',
                'country_id' => 227,
            ),
            13 => 
            array (
                'id' => 5202,
                'name' => 'Abertillery',
                'country_id' => 227,
            ),
            14 => 
            array (
                'id' => 5203,
                'name' => 'Aberystwyth',
                'country_id' => 227,
            ),
            15 => 
            array (
                'id' => 5204,
                'name' => 'Abingdon',
                'country_id' => 227,
            ),
            16 => 
            array (
                'id' => 5205,
                'name' => 'Accrington',
                'country_id' => 227,
            ),
            17 => 
            array (
                'id' => 5206,
                'name' => 'Adlington',
                'country_id' => 227,
            ),
            18 => 
            array (
                'id' => 5207,
                'name' => 'Airdrie',
                'country_id' => 227,
            ),
            19 => 
            array (
                'id' => 5208,
                'name' => 'Alcester',
                'country_id' => 227,
            ),
            20 => 
            array (
                'id' => 5209,
                'name' => 'Aldeburgh',
                'country_id' => 227,
            ),
            21 => 
            array (
                'id' => 5210,
                'name' => 'Aldershot',
                'country_id' => 227,
            ),
            22 => 
            array (
                'id' => 5211,
                'name' => 'Aldridge',
                'country_id' => 227,
            ),
            23 => 
            array (
                'id' => 5212,
                'name' => 'Alford',
                'country_id' => 227,
            ),
            24 => 
            array (
                'id' => 5213,
                'name' => 'Alfreton',
                'country_id' => 227,
            ),
            25 => 
            array (
                'id' => 5214,
                'name' => 'Alloa',
                'country_id' => 227,
            ),
            26 => 
            array (
                'id' => 5215,
                'name' => 'Alnwick',
                'country_id' => 227,
            ),
            27 => 
            array (
                'id' => 5216,
                'name' => 'Alsager',
                'country_id' => 227,
            ),
            28 => 
            array (
                'id' => 5217,
                'name' => 'Alston',
                'country_id' => 227,
            ),
            29 => 
            array (
                'id' => 5218,
                'name' => 'Amesbury',
                'country_id' => 227,
            ),
            30 => 
            array (
                'id' => 5219,
                'name' => 'Amlwch',
                'country_id' => 227,
            ),
            31 => 
            array (
                'id' => 5220,
                'name' => 'Ammanford',
                'country_id' => 227,
            ),
            32 => 
            array (
                'id' => 5221,
                'name' => 'Ampthill',
                'country_id' => 227,
            ),
            33 => 
            array (
                'id' => 5222,
                'name' => 'Andover',
                'country_id' => 227,
            ),
            34 => 
            array (
                'id' => 5223,
                'name' => 'Annan',
                'country_id' => 227,
            ),
            35 => 
            array (
                'id' => 5224,
                'name' => 'Antrim',
                'country_id' => 227,
            ),
            36 => 
            array (
                'id' => 5225,
                'name' => 'Appleby in Westmorland',
                'country_id' => 227,
            ),
            37 => 
            array (
                'id' => 5226,
                'name' => 'Arbroath',
                'country_id' => 227,
            ),
            38 => 
            array (
                'id' => 5227,
                'name' => 'Armagh',
                'country_id' => 227,
            ),
            39 => 
            array (
                'id' => 5228,
                'name' => 'Arundel',
                'country_id' => 227,
            ),
            40 => 
            array (
                'id' => 5229,
                'name' => 'Ashbourne',
                'country_id' => 227,
            ),
            41 => 
            array (
                'id' => 5230,
                'name' => 'Ashburton',
                'country_id' => 227,
            ),
            42 => 
            array (
                'id' => 5231,
                'name' => 'Ashby de la Zouch',
                'country_id' => 227,
            ),
            43 => 
            array (
                'id' => 5232,
                'name' => 'Ashford',
                'country_id' => 227,
            ),
            44 => 
            array (
                'id' => 5233,
                'name' => 'Ashington',
                'country_id' => 227,
            ),
            45 => 
            array (
                'id' => 5234,
                'name' => 'Ashton in Makerfield',
                'country_id' => 227,
            ),
            46 => 
            array (
                'id' => 5235,
                'name' => 'Atherstone',
                'country_id' => 227,
            ),
            47 => 
            array (
                'id' => 5236,
                'name' => 'Auchtermuchty',
                'country_id' => 227,
            ),
            48 => 
            array (
                'id' => 5237,
                'name' => 'Axminster',
                'country_id' => 227,
            ),
            49 => 
            array (
                'id' => 5238,
                'name' => 'Aylesbury',
                'country_id' => 227,
            ),
            50 => 
            array (
                'id' => 5239,
                'name' => 'Aylsham',
                'country_id' => 227,
            ),
            51 => 
            array (
                'id' => 5240,
                'name' => 'Ayr',
                'country_id' => 227,
            ),
            52 => 
            array (
                'id' => 5241,
                'name' => 'Bacup',
                'country_id' => 227,
            ),
            53 => 
            array (
                'id' => 5242,
                'name' => 'Bakewell',
                'country_id' => 227,
            ),
            54 => 
            array (
                'id' => 5243,
                'name' => 'Bala',
                'country_id' => 227,
            ),
            55 => 
            array (
                'id' => 5244,
                'name' => 'Ballater',
                'country_id' => 227,
            ),
            56 => 
            array (
                'id' => 5245,
                'name' => 'Ballycastle',
                'country_id' => 227,
            ),
            57 => 
            array (
                'id' => 5246,
                'name' => 'Ballyclare',
                'country_id' => 227,
            ),
            58 => 
            array (
                'id' => 5247,
                'name' => 'Ballymena',
                'country_id' => 227,
            ),
            59 => 
            array (
                'id' => 5248,
                'name' => 'Ballymoney',
                'country_id' => 227,
            ),
            60 => 
            array (
                'id' => 5249,
                'name' => 'Ballynahinch',
                'country_id' => 227,
            ),
            61 => 
            array (
                'id' => 5250,
                'name' => 'Banbridge',
                'country_id' => 227,
            ),
            62 => 
            array (
                'id' => 5251,
                'name' => 'Banbury',
                'country_id' => 227,
            ),
            63 => 
            array (
                'id' => 5252,
                'name' => 'Banchory',
                'country_id' => 227,
            ),
            64 => 
            array (
                'id' => 5253,
                'name' => 'Banff',
                'country_id' => 227,
            ),
            65 => 
            array (
                'id' => 5254,
                'name' => 'Bangor',
                'country_id' => 227,
            ),
            66 => 
            array (
                'id' => 5255,
                'name' => 'Barmouth',
                'country_id' => 227,
            ),
            67 => 
            array (
                'id' => 5256,
                'name' => 'Barnard Castle',
                'country_id' => 227,
            ),
            68 => 
            array (
                'id' => 5257,
                'name' => 'Barnet',
                'country_id' => 227,
            ),
            69 => 
            array (
                'id' => 5258,
                'name' => 'Barnoldswick',
                'country_id' => 227,
            ),
            70 => 
            array (
                'id' => 5259,
                'name' => 'Barnsley',
                'country_id' => 227,
            ),
            71 => 
            array (
                'id' => 5260,
                'name' => 'Barnstaple',
                'country_id' => 227,
            ),
            72 => 
            array (
                'id' => 5261,
                'name' => 'Barrhead',
                'country_id' => 227,
            ),
            73 => 
            array (
                'id' => 5262,
                'name' => 'Barrow in Furness',
                'country_id' => 227,
            ),
            74 => 
            array (
                'id' => 5263,
                'name' => 'Barry',
                'country_id' => 227,
            ),
            75 => 
            array (
                'id' => 5264,
                'name' => 'Barton upon Humber',
                'country_id' => 227,
            ),
            76 => 
            array (
                'id' => 5265,
                'name' => 'Basildon',
                'country_id' => 227,
            ),
            77 => 
            array (
                'id' => 5266,
                'name' => 'Basingstoke',
                'country_id' => 227,
            ),
            78 => 
            array (
                'id' => 5267,
                'name' => 'Bath',
                'country_id' => 227,
            ),
            79 => 
            array (
                'id' => 5268,
                'name' => 'Bathgate',
                'country_id' => 227,
            ),
            80 => 
            array (
                'id' => 5269,
                'name' => 'Batley',
                'country_id' => 227,
            ),
            81 => 
            array (
                'id' => 5270,
                'name' => 'Battle',
                'country_id' => 227,
            ),
            82 => 
            array (
                'id' => 5271,
                'name' => 'Bawtry',
                'country_id' => 227,
            ),
            83 => 
            array (
                'id' => 5272,
                'name' => 'Beaconsfield',
                'country_id' => 227,
            ),
            84 => 
            array (
                'id' => 5273,
                'name' => 'Bearsden',
                'country_id' => 227,
            ),
            85 => 
            array (
                'id' => 5274,
                'name' => 'Beaumaris',
                'country_id' => 227,
            ),
            86 => 
            array (
                'id' => 5275,
                'name' => 'Bebington',
                'country_id' => 227,
            ),
            87 => 
            array (
                'id' => 5276,
                'name' => 'Beccles',
                'country_id' => 227,
            ),
            88 => 
            array (
                'id' => 5277,
                'name' => 'Bedale',
                'country_id' => 227,
            ),
            89 => 
            array (
                'id' => 5278,
                'name' => 'Bedford',
                'country_id' => 227,
            ),
            90 => 
            array (
                'id' => 5279,
                'name' => 'Bedlington',
                'country_id' => 227,
            ),
            91 => 
            array (
                'id' => 5280,
                'name' => 'Bedworth',
                'country_id' => 227,
            ),
            92 => 
            array (
                'id' => 5281,
                'name' => 'Beeston',
                'country_id' => 227,
            ),
            93 => 
            array (
                'id' => 5282,
                'name' => 'Bellshill',
                'country_id' => 227,
            ),
            94 => 
            array (
                'id' => 5283,
                'name' => 'Belper',
                'country_id' => 227,
            ),
            95 => 
            array (
                'id' => 5284,
                'name' => 'Berkhamsted',
                'country_id' => 227,
            ),
            96 => 
            array (
                'id' => 5285,
                'name' => 'Berwick upon Tweed',
                'country_id' => 227,
            ),
            97 => 
            array (
                'id' => 5286,
                'name' => 'Betws y Coed',
                'country_id' => 227,
            ),
            98 => 
            array (
                'id' => 5287,
                'name' => 'Beverley',
                'country_id' => 227,
            ),
            99 => 
            array (
                'id' => 5288,
                'name' => 'Bewdley',
                'country_id' => 227,
            ),
            100 => 
            array (
                'id' => 5289,
                'name' => 'Bexhill on Sea',
                'country_id' => 227,
            ),
            101 => 
            array (
                'id' => 5290,
                'name' => 'Bicester',
                'country_id' => 227,
            ),
            102 => 
            array (
                'id' => 5291,
                'name' => 'Biddulph',
                'country_id' => 227,
            ),
            103 => 
            array (
                'id' => 5292,
                'name' => 'Bideford',
                'country_id' => 227,
            ),
            104 => 
            array (
                'id' => 5293,
                'name' => 'Biggar',
                'country_id' => 227,
            ),
            105 => 
            array (
                'id' => 5294,
                'name' => 'Biggleswade',
                'country_id' => 227,
            ),
            106 => 
            array (
                'id' => 5295,
                'name' => 'Billericay',
                'country_id' => 227,
            ),
            107 => 
            array (
                'id' => 5296,
                'name' => 'Bilston',
                'country_id' => 227,
            ),
            108 => 
            array (
                'id' => 5297,
                'name' => 'Bingham',
                'country_id' => 227,
            ),
            109 => 
            array (
                'id' => 5298,
                'name' => 'Birkenhead',
                'country_id' => 227,
            ),
            110 => 
            array (
                'id' => 5299,
                'name' => 'Birmingham',
                'country_id' => 227,
            ),
            111 => 
            array (
                'id' => 5300,
                'name' => 'Bishop Auckland',
                'country_id' => 227,
            ),
            112 => 
            array (
                'id' => 5301,
                'name' => 'Blackburn',
                'country_id' => 227,
            ),
            113 => 
            array (
                'id' => 5302,
                'name' => 'Blackheath',
                'country_id' => 227,
            ),
            114 => 
            array (
                'id' => 5303,
                'name' => 'Blackpool',
                'country_id' => 227,
            ),
            115 => 
            array (
                'id' => 5304,
                'name' => 'Blaenau Ffestiniog',
                'country_id' => 227,
            ),
            116 => 
            array (
                'id' => 5305,
                'name' => 'Blandford Forum',
                'country_id' => 227,
            ),
            117 => 
            array (
                'id' => 5306,
                'name' => 'Bletchley',
                'country_id' => 227,
            ),
            118 => 
            array (
                'id' => 5307,
                'name' => 'Bloxwich',
                'country_id' => 227,
            ),
            119 => 
            array (
                'id' => 5308,
                'name' => 'Blyth',
                'country_id' => 227,
            ),
            120 => 
            array (
                'id' => 5309,
                'name' => 'Bodmin',
                'country_id' => 227,
            ),
            121 => 
            array (
                'id' => 5310,
                'name' => 'Bognor Regis',
                'country_id' => 227,
            ),
            122 => 
            array (
                'id' => 5311,
                'name' => 'Bollington',
                'country_id' => 227,
            ),
            123 => 
            array (
                'id' => 5312,
                'name' => 'Bolsover',
                'country_id' => 227,
            ),
            124 => 
            array (
                'id' => 5313,
                'name' => 'Bolton',
                'country_id' => 227,
            ),
            125 => 
            array (
                'id' => 5314,
                'name' => 'Bootle',
                'country_id' => 227,
            ),
            126 => 
            array (
                'id' => 5315,
                'name' => 'Borehamwood',
                'country_id' => 227,
            ),
            127 => 
            array (
                'id' => 5316,
                'name' => 'Boston',
                'country_id' => 227,
            ),
            128 => 
            array (
                'id' => 5317,
                'name' => 'Bourne',
                'country_id' => 227,
            ),
            129 => 
            array (
                'id' => 5318,
                'name' => 'Bournemouth',
                'country_id' => 227,
            ),
            130 => 
            array (
                'id' => 5319,
                'name' => 'Brackley',
                'country_id' => 227,
            ),
            131 => 
            array (
                'id' => 5320,
                'name' => 'Bracknell',
                'country_id' => 227,
            ),
            132 => 
            array (
                'id' => 5321,
                'name' => 'Bradford',
                'country_id' => 227,
            ),
            133 => 
            array (
                'id' => 5322,
                'name' => 'Bradford on Avon',
                'country_id' => 227,
            ),
            134 => 
            array (
                'id' => 5323,
                'name' => 'Brading',
                'country_id' => 227,
            ),
            135 => 
            array (
                'id' => 5324,
                'name' => 'Bradley Stoke',
                'country_id' => 227,
            ),
            136 => 
            array (
                'id' => 5325,
                'name' => 'Bradninch',
                'country_id' => 227,
            ),
            137 => 
            array (
                'id' => 5326,
                'name' => 'Braintree',
                'country_id' => 227,
            ),
            138 => 
            array (
                'id' => 5327,
                'name' => 'Brechin',
                'country_id' => 227,
            ),
            139 => 
            array (
                'id' => 5328,
                'name' => 'Brecon',
                'country_id' => 227,
            ),
            140 => 
            array (
                'id' => 5329,
                'name' => 'Brentwood',
                'country_id' => 227,
            ),
            141 => 
            array (
                'id' => 5330,
                'name' => 'Bridge of Allan',
                'country_id' => 227,
            ),
            142 => 
            array (
                'id' => 5331,
                'name' => 'Bridgend',
                'country_id' => 227,
            ),
            143 => 
            array (
                'id' => 5332,
                'name' => 'Bridgnorth',
                'country_id' => 227,
            ),
            144 => 
            array (
                'id' => 5333,
                'name' => 'Bridgwater',
                'country_id' => 227,
            ),
            145 => 
            array (
                'id' => 5334,
                'name' => 'Bridlington',
                'country_id' => 227,
            ),
            146 => 
            array (
                'id' => 5335,
                'name' => 'Bridport',
                'country_id' => 227,
            ),
            147 => 
            array (
                'id' => 5336,
                'name' => 'Brigg',
                'country_id' => 227,
            ),
            148 => 
            array (
                'id' => 5337,
                'name' => 'Brighouse',
                'country_id' => 227,
            ),
            149 => 
            array (
                'id' => 5338,
                'name' => 'Brightlingsea',
                'country_id' => 227,
            ),
            150 => 
            array (
                'id' => 5339,
                'name' => 'Brighton',
                'country_id' => 227,
            ),
            151 => 
            array (
                'id' => 5340,
                'name' => 'Bristol',
                'country_id' => 227,
            ),
            152 => 
            array (
                'id' => 5341,
                'name' => 'Brixham',
                'country_id' => 227,
            ),
            153 => 
            array (
                'id' => 5342,
                'name' => 'Broadstairs',
                'country_id' => 227,
            ),
            154 => 
            array (
                'id' => 5343,
                'name' => 'Bromsgrove',
                'country_id' => 227,
            ),
            155 => 
            array (
                'id' => 5344,
                'name' => 'Bromyard',
                'country_id' => 227,
            ),
            156 => 
            array (
                'id' => 5345,
                'name' => 'Brynmawr',
                'country_id' => 227,
            ),
            157 => 
            array (
                'id' => 5346,
                'name' => 'Buckfastleigh',
                'country_id' => 227,
            ),
            158 => 
            array (
                'id' => 5347,
                'name' => 'Buckie',
                'country_id' => 227,
            ),
            159 => 
            array (
                'id' => 5348,
                'name' => 'Buckingham',
                'country_id' => 227,
            ),
            160 => 
            array (
                'id' => 5349,
                'name' => 'Buckley',
                'country_id' => 227,
            ),
            161 => 
            array (
                'id' => 5350,
                'name' => 'Bude',
                'country_id' => 227,
            ),
            162 => 
            array (
                'id' => 5351,
                'name' => 'Budleigh Salterton',
                'country_id' => 227,
            ),
            163 => 
            array (
                'id' => 5352,
                'name' => 'Builth Wells',
                'country_id' => 227,
            ),
            164 => 
            array (
                'id' => 5353,
                'name' => 'Bungay',
                'country_id' => 227,
            ),
            165 => 
            array (
                'id' => 5354,
                'name' => 'Buntingford',
                'country_id' => 227,
            ),
            166 => 
            array (
                'id' => 5355,
                'name' => 'Burford',
                'country_id' => 227,
            ),
            167 => 
            array (
                'id' => 5356,
                'name' => 'Burgess Hill',
                'country_id' => 227,
            ),
            168 => 
            array (
                'id' => 5357,
                'name' => 'Burnham on Crouch',
                'country_id' => 227,
            ),
            169 => 
            array (
                'id' => 5358,
                'name' => 'Burnham on Sea',
                'country_id' => 227,
            ),
            170 => 
            array (
                'id' => 5359,
                'name' => 'Burnley',
                'country_id' => 227,
            ),
            171 => 
            array (
                'id' => 5360,
                'name' => 'Burntisland',
                'country_id' => 227,
            ),
            172 => 
            array (
                'id' => 5361,
                'name' => 'Burntwood',
                'country_id' => 227,
            ),
            173 => 
            array (
                'id' => 5362,
                'name' => 'Burry Port',
                'country_id' => 227,
            ),
            174 => 
            array (
                'id' => 5363,
                'name' => 'Burton Latimer',
                'country_id' => 227,
            ),
            175 => 
            array (
                'id' => 5364,
                'name' => 'Bury',
                'country_id' => 227,
            ),
            176 => 
            array (
                'id' => 5365,
                'name' => 'Bushmills',
                'country_id' => 227,
            ),
            177 => 
            array (
                'id' => 5366,
                'name' => 'Buxton',
                'country_id' => 227,
            ),
            178 => 
            array (
                'id' => 5367,
                'name' => 'Caernarfon',
                'country_id' => 227,
            ),
            179 => 
            array (
                'id' => 5368,
                'name' => 'Caerphilly',
                'country_id' => 227,
            ),
            180 => 
            array (
                'id' => 5369,
                'name' => 'Caistor',
                'country_id' => 227,
            ),
            181 => 
            array (
                'id' => 5370,
                'name' => 'Caldicot',
                'country_id' => 227,
            ),
            182 => 
            array (
                'id' => 5371,
                'name' => 'Callander',
                'country_id' => 227,
            ),
            183 => 
            array (
                'id' => 5372,
                'name' => 'Calne',
                'country_id' => 227,
            ),
            184 => 
            array (
                'id' => 5373,
                'name' => 'Camberley',
                'country_id' => 227,
            ),
            185 => 
            array (
                'id' => 5374,
                'name' => 'Camborne',
                'country_id' => 227,
            ),
            186 => 
            array (
                'id' => 5375,
                'name' => 'Cambridge',
                'country_id' => 227,
            ),
            187 => 
            array (
                'id' => 5376,
                'name' => 'Camelford',
                'country_id' => 227,
            ),
            188 => 
            array (
                'id' => 5377,
                'name' => 'Campbeltown',
                'country_id' => 227,
            ),
            189 => 
            array (
                'id' => 5378,
                'name' => 'Cannock',
                'country_id' => 227,
            ),
            190 => 
            array (
                'id' => 5379,
                'name' => 'Canterbury',
                'country_id' => 227,
            ),
            191 => 
            array (
                'id' => 5380,
                'name' => 'Cardiff',
                'country_id' => 227,
            ),
            192 => 
            array (
                'id' => 5381,
                'name' => 'Cardigan',
                'country_id' => 227,
            ),
            193 => 
            array (
                'id' => 5382,
                'name' => 'Carlisle',
                'country_id' => 227,
            ),
            194 => 
            array (
                'id' => 5383,
                'name' => 'Carluke',
                'country_id' => 227,
            ),
            195 => 
            array (
                'id' => 5384,
                'name' => 'Carmarthen',
                'country_id' => 227,
            ),
            196 => 
            array (
                'id' => 5385,
                'name' => 'Carnforth',
                'country_id' => 227,
            ),
            197 => 
            array (
                'id' => 5386,
                'name' => 'Carnoustie',
                'country_id' => 227,
            ),
            198 => 
            array (
                'id' => 5387,
                'name' => 'Carrickfergus',
                'country_id' => 227,
            ),
            199 => 
            array (
                'id' => 5388,
                'name' => 'Carterton',
                'country_id' => 227,
            ),
            200 => 
            array (
                'id' => 5389,
                'name' => 'Castle Douglas',
                'country_id' => 227,
            ),
            201 => 
            array (
                'id' => 5390,
                'name' => 'Castlederg',
                'country_id' => 227,
            ),
            202 => 
            array (
                'id' => 5391,
                'name' => 'Castleford',
                'country_id' => 227,
            ),
            203 => 
            array (
                'id' => 5392,
                'name' => 'Castlewellan',
                'country_id' => 227,
            ),
            204 => 
            array (
                'id' => 5393,
                'name' => 'Chard',
                'country_id' => 227,
            ),
            205 => 
            array (
                'id' => 5394,
                'name' => 'Charlbury',
                'country_id' => 227,
            ),
            206 => 
            array (
                'id' => 5395,
                'name' => 'Chatham',
                'country_id' => 227,
            ),
            207 => 
            array (
                'id' => 5396,
                'name' => 'Chatteris',
                'country_id' => 227,
            ),
            208 => 
            array (
                'id' => 5397,
                'name' => 'Chelmsford',
                'country_id' => 227,
            ),
            209 => 
            array (
                'id' => 5398,
                'name' => 'Cheltenham',
                'country_id' => 227,
            ),
            210 => 
            array (
                'id' => 5399,
                'name' => 'Chepstow',
                'country_id' => 227,
            ),
            211 => 
            array (
                'id' => 5400,
                'name' => 'Chesham',
                'country_id' => 227,
            ),
            212 => 
            array (
                'id' => 5401,
                'name' => 'Cheshunt',
                'country_id' => 227,
            ),
            213 => 
            array (
                'id' => 5402,
                'name' => 'Chester',
                'country_id' => 227,
            ),
            214 => 
            array (
                'id' => 5403,
                'name' => 'Chester le Street',
                'country_id' => 227,
            ),
            215 => 
            array (
                'id' => 5404,
                'name' => 'Chesterfield',
                'country_id' => 227,
            ),
            216 => 
            array (
                'id' => 5405,
                'name' => 'Chichester',
                'country_id' => 227,
            ),
            217 => 
            array (
                'id' => 5406,
                'name' => 'Chippenham',
                'country_id' => 227,
            ),
            218 => 
            array (
                'id' => 5407,
                'name' => 'Chipping Campden',
                'country_id' => 227,
            ),
            219 => 
            array (
                'id' => 5408,
                'name' => 'Chipping Norton',
                'country_id' => 227,
            ),
            220 => 
            array (
                'id' => 5409,
                'name' => 'Chipping Sodbury',
                'country_id' => 227,
            ),
            221 => 
            array (
                'id' => 5410,
                'name' => 'Chorley',
                'country_id' => 227,
            ),
            222 => 
            array (
                'id' => 5411,
                'name' => 'Christchurch',
                'country_id' => 227,
            ),
            223 => 
            array (
                'id' => 5412,
                'name' => 'Church Stretton',
                'country_id' => 227,
            ),
            224 => 
            array (
                'id' => 5413,
                'name' => 'Cinderford',
                'country_id' => 227,
            ),
            225 => 
            array (
                'id' => 5414,
                'name' => 'Cirencester',
                'country_id' => 227,
            ),
            226 => 
            array (
                'id' => 5415,
                'name' => 'Clacton on Sea',
                'country_id' => 227,
            ),
            227 => 
            array (
                'id' => 5416,
                'name' => 'Cleckheaton',
                'country_id' => 227,
            ),
            228 => 
            array (
                'id' => 5417,
                'name' => 'Cleethorpes',
                'country_id' => 227,
            ),
            229 => 
            array (
                'id' => 5418,
                'name' => 'Clevedon',
                'country_id' => 227,
            ),
            230 => 
            array (
                'id' => 5419,
                'name' => 'Clitheroe',
                'country_id' => 227,
            ),
            231 => 
            array (
                'id' => 5420,
                'name' => 'Clogher',
                'country_id' => 227,
            ),
            232 => 
            array (
                'id' => 5421,
                'name' => 'Clydebank',
                'country_id' => 227,
            ),
            233 => 
            array (
                'id' => 5422,
                'name' => 'Coalisland',
                'country_id' => 227,
            ),
            234 => 
            array (
                'id' => 5423,
                'name' => 'Coalville',
                'country_id' => 227,
            ),
            235 => 
            array (
                'id' => 5424,
                'name' => 'Coatbridge',
                'country_id' => 227,
            ),
            236 => 
            array (
                'id' => 5425,
                'name' => 'Cockermouth',
                'country_id' => 227,
            ),
            237 => 
            array (
                'id' => 5426,
                'name' => 'Coggeshall',
                'country_id' => 227,
            ),
            238 => 
            array (
                'id' => 5427,
                'name' => 'Colchester',
                'country_id' => 227,
            ),
            239 => 
            array (
                'id' => 5428,
                'name' => 'Coldstream',
                'country_id' => 227,
            ),
            240 => 
            array (
                'id' => 5429,
                'name' => 'Coleraine',
                'country_id' => 227,
            ),
            241 => 
            array (
                'id' => 5430,
                'name' => 'Coleshill',
                'country_id' => 227,
            ),
            242 => 
            array (
                'id' => 5431,
                'name' => 'Colne',
                'country_id' => 227,
            ),
            243 => 
            array (
                'id' => 5432,
                'name' => 'Colwyn Bay',
                'country_id' => 227,
            ),
            244 => 
            array (
                'id' => 5433,
                'name' => 'Comber',
                'country_id' => 227,
            ),
            245 => 
            array (
                'id' => 5434,
                'name' => 'Congleton',
                'country_id' => 227,
            ),
            246 => 
            array (
                'id' => 5435,
                'name' => 'Conwy',
                'country_id' => 227,
            ),
            247 => 
            array (
                'id' => 5436,
                'name' => 'Cookstown',
                'country_id' => 227,
            ),
            248 => 
            array (
                'id' => 5437,
                'name' => 'Corbridge',
                'country_id' => 227,
            ),
            249 => 
            array (
                'id' => 5438,
                'name' => 'Corby',
                'country_id' => 227,
            ),
            250 => 
            array (
                'id' => 5439,
                'name' => 'Coventry',
                'country_id' => 227,
            ),
            251 => 
            array (
                'id' => 5440,
                'name' => 'Cowbridge',
                'country_id' => 227,
            ),
            252 => 
            array (
                'id' => 5441,
                'name' => 'Cowdenbeath',
                'country_id' => 227,
            ),
            253 => 
            array (
                'id' => 5442,
                'name' => 'Cowes',
                'country_id' => 227,
            ),
            254 => 
            array (
                'id' => 5443,
                'name' => 'Craigavon',
                'country_id' => 227,
            ),
            255 => 
            array (
                'id' => 5444,
                'name' => 'Cramlington',
                'country_id' => 227,
            ),
            256 => 
            array (
                'id' => 5445,
                'name' => 'Crawley',
                'country_id' => 227,
            ),
            257 => 
            array (
                'id' => 5446,
                'name' => 'Crayford',
                'country_id' => 227,
            ),
            258 => 
            array (
                'id' => 5447,
                'name' => 'Crediton',
                'country_id' => 227,
            ),
            259 => 
            array (
                'id' => 5448,
                'name' => 'Crewe',
                'country_id' => 227,
            ),
            260 => 
            array (
                'id' => 5449,
                'name' => 'Crewkerne',
                'country_id' => 227,
            ),
            261 => 
            array (
                'id' => 5450,
                'name' => 'Criccieth',
                'country_id' => 227,
            ),
            262 => 
            array (
                'id' => 5451,
                'name' => 'Crickhowell',
                'country_id' => 227,
            ),
            263 => 
            array (
                'id' => 5452,
                'name' => 'Crieff',
                'country_id' => 227,
            ),
            264 => 
            array (
                'id' => 5453,
                'name' => 'Cromarty',
                'country_id' => 227,
            ),
            265 => 
            array (
                'id' => 5454,
                'name' => 'Cromer',
                'country_id' => 227,
            ),
            266 => 
            array (
                'id' => 5455,
                'name' => 'Crowborough',
                'country_id' => 227,
            ),
            267 => 
            array (
                'id' => 5456,
                'name' => 'Crowthorne',
                'country_id' => 227,
            ),
            268 => 
            array (
                'id' => 5457,
                'name' => 'Crumlin',
                'country_id' => 227,
            ),
            269 => 
            array (
                'id' => 5458,
                'name' => 'Cuckfield',
                'country_id' => 227,
            ),
            270 => 
            array (
                'id' => 5459,
                'name' => 'Cullen',
                'country_id' => 227,
            ),
            271 => 
            array (
                'id' => 5460,
                'name' => 'Cullompton',
                'country_id' => 227,
            ),
            272 => 
            array (
                'id' => 5461,
                'name' => 'Cumbernauld',
                'country_id' => 227,
            ),
            273 => 
            array (
                'id' => 5462,
                'name' => 'Cupar',
                'country_id' => 227,
            ),
            274 => 
            array (
                'id' => 5463,
                'name' => 'Cwmbran',
                'country_id' => 227,
            ),
            275 => 
            array (
                'id' => 5464,
                'name' => 'Dalbeattie',
                'country_id' => 227,
            ),
            276 => 
            array (
                'id' => 5465,
                'name' => 'Dalkeith',
                'country_id' => 227,
            ),
            277 => 
            array (
                'id' => 5466,
                'name' => 'Darlington',
                'country_id' => 227,
            ),
            278 => 
            array (
                'id' => 5467,
                'name' => 'Dartford',
                'country_id' => 227,
            ),
            279 => 
            array (
                'id' => 5468,
                'name' => 'Dartmouth',
                'country_id' => 227,
            ),
            280 => 
            array (
                'id' => 5469,
                'name' => 'Darwen',
                'country_id' => 227,
            ),
            281 => 
            array (
                'id' => 5470,
                'name' => 'Daventry',
                'country_id' => 227,
            ),
            282 => 
            array (
                'id' => 5471,
                'name' => 'Dawlish',
                'country_id' => 227,
            ),
            283 => 
            array (
                'id' => 5472,
                'name' => 'Deal',
                'country_id' => 227,
            ),
            284 => 
            array (
                'id' => 5473,
                'name' => 'Denbigh',
                'country_id' => 227,
            ),
            285 => 
            array (
                'id' => 5474,
                'name' => 'Denton',
                'country_id' => 227,
            ),
            286 => 
            array (
                'id' => 5475,
                'name' => 'Derby',
                'country_id' => 227,
            ),
            287 => 
            array (
                'id' => 5476,
                'name' => 'Dereham',
                'country_id' => 227,
            ),
            288 => 
            array (
                'id' => 5477,
                'name' => 'Devizes',
                'country_id' => 227,
            ),
            289 => 
            array (
                'id' => 5478,
                'name' => 'Dewsbury',
                'country_id' => 227,
            ),
            290 => 
            array (
                'id' => 5479,
                'name' => 'Didcot',
                'country_id' => 227,
            ),
            291 => 
            array (
                'id' => 5480,
                'name' => 'Dingwall',
                'country_id' => 227,
            ),
            292 => 
            array (
                'id' => 5481,
                'name' => 'Dinnington',
                'country_id' => 227,
            ),
            293 => 
            array (
                'id' => 5482,
                'name' => 'Diss',
                'country_id' => 227,
            ),
            294 => 
            array (
                'id' => 5483,
                'name' => 'Dolgellau',
                'country_id' => 227,
            ),
            295 => 
            array (
                'id' => 5484,
                'name' => 'Donaghadee',
                'country_id' => 227,
            ),
            296 => 
            array (
                'id' => 5485,
                'name' => 'Doncaster',
                'country_id' => 227,
            ),
            297 => 
            array (
                'id' => 5486,
                'name' => 'Dorchester',
                'country_id' => 227,
            ),
            298 => 
            array (
                'id' => 5487,
                'name' => 'Dorking',
                'country_id' => 227,
            ),
            299 => 
            array (
                'id' => 5488,
                'name' => 'Dornoch',
                'country_id' => 227,
            ),
            300 => 
            array (
                'id' => 5489,
                'name' => 'Dover',
                'country_id' => 227,
            ),
            301 => 
            array (
                'id' => 5490,
                'name' => 'Downham Market',
                'country_id' => 227,
            ),
            302 => 
            array (
                'id' => 5491,
                'name' => 'Downpatrick',
                'country_id' => 227,
            ),
            303 => 
            array (
                'id' => 5492,
                'name' => 'Driffield',
                'country_id' => 227,
            ),
            304 => 
            array (
                'id' => 5493,
                'name' => 'Dronfield',
                'country_id' => 227,
            ),
            305 => 
            array (
                'id' => 5494,
                'name' => 'Droylsden',
                'country_id' => 227,
            ),
            306 => 
            array (
                'id' => 5495,
                'name' => 'Dudley',
                'country_id' => 227,
            ),
            307 => 
            array (
                'id' => 5496,
                'name' => 'Dufftown',
                'country_id' => 227,
            ),
            308 => 
            array (
                'id' => 5497,
                'name' => 'Dukinfield',
                'country_id' => 227,
            ),
            309 => 
            array (
                'id' => 5498,
                'name' => 'Dumbarton',
                'country_id' => 227,
            ),
            310 => 
            array (
                'id' => 5499,
                'name' => 'Dumfries',
                'country_id' => 227,
            ),
            311 => 
            array (
                'id' => 5500,
                'name' => 'Dunbar',
                'country_id' => 227,
            ),
            312 => 
            array (
                'id' => 5501,
                'name' => 'Dunblane',
                'country_id' => 227,
            ),
            313 => 
            array (
                'id' => 5502,
                'name' => 'Dundee',
                'country_id' => 227,
            ),
            314 => 
            array (
                'id' => 5503,
                'name' => 'Dunfermline',
                'country_id' => 227,
            ),
            315 => 
            array (
                'id' => 5504,
                'name' => 'Dungannon',
                'country_id' => 227,
            ),
            316 => 
            array (
                'id' => 5505,
                'name' => 'Dunoon',
                'country_id' => 227,
            ),
            317 => 
            array (
                'id' => 5506,
                'name' => 'Duns',
                'country_id' => 227,
            ),
            318 => 
            array (
                'id' => 5507,
                'name' => 'Dunstable',
                'country_id' => 227,
            ),
            319 => 
            array (
                'id' => 5508,
                'name' => 'Durham',
                'country_id' => 227,
            ),
            320 => 
            array (
                'id' => 5509,
                'name' => 'Dursley',
                'country_id' => 227,
            ),
            321 => 
            array (
                'id' => 5510,
                'name' => 'Easingwold',
                'country_id' => 227,
            ),
            322 => 
            array (
                'id' => 5511,
                'name' => 'East Grinstead',
                'country_id' => 227,
            ),
            323 => 
            array (
                'id' => 5512,
                'name' => 'East Kilbride',
                'country_id' => 227,
            ),
            324 => 
            array (
                'id' => 5513,
                'name' => 'Eastbourne',
                'country_id' => 227,
            ),
            325 => 
            array (
                'id' => 5514,
                'name' => 'Eastleigh',
                'country_id' => 227,
            ),
            326 => 
            array (
                'id' => 5515,
                'name' => 'Eastwood',
                'country_id' => 227,
            ),
            327 => 
            array (
                'id' => 5516,
                'name' => 'Ebbw Vale',
                'country_id' => 227,
            ),
            328 => 
            array (
                'id' => 5517,
                'name' => 'Edenbridge',
                'country_id' => 227,
            ),
            329 => 
            array (
                'id' => 5518,
                'name' => 'Edinburgh',
                'country_id' => 227,
            ),
            330 => 
            array (
                'id' => 5519,
                'name' => 'Egham',
                'country_id' => 227,
            ),
            331 => 
            array (
                'id' => 5520,
                'name' => 'Elgin',
                'country_id' => 227,
            ),
            332 => 
            array (
                'id' => 5521,
                'name' => 'Ellesmere',
                'country_id' => 227,
            ),
            333 => 
            array (
                'id' => 5522,
                'name' => 'Ellesmere Port',
                'country_id' => 227,
            ),
            334 => 
            array (
                'id' => 5523,
                'name' => 'Ely',
                'country_id' => 227,
            ),
            335 => 
            array (
                'id' => 5524,
                'name' => 'Enniskillen',
                'country_id' => 227,
            ),
            336 => 
            array (
                'id' => 5525,
                'name' => 'Epping',
                'country_id' => 227,
            ),
            337 => 
            array (
                'id' => 5526,
                'name' => 'Epsom',
                'country_id' => 227,
            ),
            338 => 
            array (
                'id' => 5527,
                'name' => 'Erith',
                'country_id' => 227,
            ),
            339 => 
            array (
                'id' => 5528,
                'name' => 'Esher',
                'country_id' => 227,
            ),
            340 => 
            array (
                'id' => 5529,
                'name' => 'Evesham',
                'country_id' => 227,
            ),
            341 => 
            array (
                'id' => 5530,
                'name' => 'Exeter',
                'country_id' => 227,
            ),
            342 => 
            array (
                'id' => 5531,
                'name' => 'Exmouth',
                'country_id' => 227,
            ),
            343 => 
            array (
                'id' => 5532,
                'name' => 'Eye',
                'country_id' => 227,
            ),
            344 => 
            array (
                'id' => 5533,
                'name' => 'Eyemouth',
                'country_id' => 227,
            ),
            345 => 
            array (
                'id' => 5534,
                'name' => 'Failsworth',
                'country_id' => 227,
            ),
            346 => 
            array (
                'id' => 5535,
                'name' => 'Fairford',
                'country_id' => 227,
            ),
            347 => 
            array (
                'id' => 5536,
                'name' => 'Fakenham',
                'country_id' => 227,
            ),
            348 => 
            array (
                'id' => 5537,
                'name' => 'Falkirk',
                'country_id' => 227,
            ),
            349 => 
            array (
                'id' => 5538,
                'name' => 'Falkland',
                'country_id' => 227,
            ),
            350 => 
            array (
                'id' => 5539,
                'name' => 'Falmouth',
                'country_id' => 227,
            ),
            351 => 
            array (
                'id' => 5540,
                'name' => 'Fareham',
                'country_id' => 227,
            ),
            352 => 
            array (
                'id' => 5541,
                'name' => 'Faringdon',
                'country_id' => 227,
            ),
            353 => 
            array (
                'id' => 5542,
                'name' => 'Farnborough',
                'country_id' => 227,
            ),
            354 => 
            array (
                'id' => 5543,
                'name' => 'Farnham',
                'country_id' => 227,
            ),
            355 => 
            array (
                'id' => 5544,
                'name' => 'Farnworth',
                'country_id' => 227,
            ),
            356 => 
            array (
                'id' => 5545,
                'name' => 'Faversham',
                'country_id' => 227,
            ),
            357 => 
            array (
                'id' => 5546,
                'name' => 'Felixstowe',
                'country_id' => 227,
            ),
            358 => 
            array (
                'id' => 5547,
                'name' => 'Ferndown',
                'country_id' => 227,
            ),
            359 => 
            array (
                'id' => 5548,
                'name' => 'Filey',
                'country_id' => 227,
            ),
            360 => 
            array (
                'id' => 5549,
                'name' => 'Fintona',
                'country_id' => 227,
            ),
            361 => 
            array (
                'id' => 5550,
                'name' => 'Fishguard',
                'country_id' => 227,
            ),
            362 => 
            array (
                'id' => 5551,
                'name' => 'Fivemiletown',
                'country_id' => 227,
            ),
            363 => 
            array (
                'id' => 5552,
                'name' => 'Fleet',
                'country_id' => 227,
            ),
            364 => 
            array (
                'id' => 5553,
                'name' => 'Fleetwood',
                'country_id' => 227,
            ),
            365 => 
            array (
                'id' => 5554,
                'name' => 'Flint',
                'country_id' => 227,
            ),
            366 => 
            array (
                'id' => 5555,
                'name' => 'Flitwick',
                'country_id' => 227,
            ),
            367 => 
            array (
                'id' => 5556,
                'name' => 'Folkestone',
                'country_id' => 227,
            ),
            368 => 
            array (
                'id' => 5557,
                'name' => 'Fordingbridge',
                'country_id' => 227,
            ),
            369 => 
            array (
                'id' => 5558,
                'name' => 'Forfar',
                'country_id' => 227,
            ),
            370 => 
            array (
                'id' => 5559,
                'name' => 'Forres',
                'country_id' => 227,
            ),
            371 => 
            array (
                'id' => 5560,
                'name' => 'Fort William',
                'country_id' => 227,
            ),
            372 => 
            array (
                'id' => 5561,
                'name' => 'Fowey',
                'country_id' => 227,
            ),
            373 => 
            array (
                'id' => 5562,
                'name' => 'Framlingham',
                'country_id' => 227,
            ),
            374 => 
            array (
                'id' => 5563,
                'name' => 'Fraserburgh',
                'country_id' => 227,
            ),
            375 => 
            array (
                'id' => 5564,
                'name' => 'Frodsham',
                'country_id' => 227,
            ),
            376 => 
            array (
                'id' => 5565,
                'name' => 'Frome',
                'country_id' => 227,
            ),
            377 => 
            array (
                'id' => 5566,
                'name' => 'Gainsborough',
                'country_id' => 227,
            ),
            378 => 
            array (
                'id' => 5567,
                'name' => 'Galashiels',
                'country_id' => 227,
            ),
            379 => 
            array (
                'id' => 5568,
                'name' => 'Gateshead',
                'country_id' => 227,
            ),
            380 => 
            array (
                'id' => 5569,
                'name' => 'Gillingham',
                'country_id' => 227,
            ),
            381 => 
            array (
                'id' => 5570,
                'name' => 'Glasgow',
                'country_id' => 227,
            ),
            382 => 
            array (
                'id' => 5571,
                'name' => 'Glastonbury',
                'country_id' => 227,
            ),
            383 => 
            array (
                'id' => 5572,
                'name' => 'Glossop',
                'country_id' => 227,
            ),
            384 => 
            array (
                'id' => 5573,
                'name' => 'Gloucester',
                'country_id' => 227,
            ),
            385 => 
            array (
                'id' => 5574,
                'name' => 'Godalming',
                'country_id' => 227,
            ),
            386 => 
            array (
                'id' => 5575,
                'name' => 'Godmanchester',
                'country_id' => 227,
            ),
            387 => 
            array (
                'id' => 5576,
                'name' => 'Goole',
                'country_id' => 227,
            ),
            388 => 
            array (
                'id' => 5577,
                'name' => 'Gorseinon',
                'country_id' => 227,
            ),
            389 => 
            array (
                'id' => 5578,
                'name' => 'Gosport',
                'country_id' => 227,
            ),
            390 => 
            array (
                'id' => 5579,
                'name' => 'Gourock',
                'country_id' => 227,
            ),
            391 => 
            array (
                'id' => 5580,
                'name' => 'Grange over Sands',
                'country_id' => 227,
            ),
            392 => 
            array (
                'id' => 5581,
                'name' => 'Grangemouth',
                'country_id' => 227,
            ),
            393 => 
            array (
                'id' => 5582,
                'name' => 'Grantham',
                'country_id' => 227,
            ),
            394 => 
            array (
                'id' => 5583,
                'name' => 'Grantown on Spey',
                'country_id' => 227,
            ),
            395 => 
            array (
                'id' => 5584,
                'name' => 'Gravesend',
                'country_id' => 227,
            ),
            396 => 
            array (
                'id' => 5585,
                'name' => 'Grays',
                'country_id' => 227,
            ),
            397 => 
            array (
                'id' => 5586,
                'name' => 'Great Yarmouth',
                'country_id' => 227,
            ),
            398 => 
            array (
                'id' => 5587,
                'name' => 'Greenock',
                'country_id' => 227,
            ),
            399 => 
            array (
                'id' => 5588,
                'name' => 'Grimsby',
                'country_id' => 227,
            ),
            400 => 
            array (
                'id' => 5589,
                'name' => 'Guildford',
                'country_id' => 227,
            ),
            401 => 
            array (
                'id' => 5590,
                'name' => 'Haddington',
                'country_id' => 227,
            ),
            402 => 
            array (
                'id' => 5591,
                'name' => 'Hadleigh',
                'country_id' => 227,
            ),
            403 => 
            array (
                'id' => 5592,
                'name' => 'Hailsham',
                'country_id' => 227,
            ),
            404 => 
            array (
                'id' => 5593,
                'name' => 'Halesowen',
                'country_id' => 227,
            ),
            405 => 
            array (
                'id' => 5594,
                'name' => 'Halesworth',
                'country_id' => 227,
            ),
            406 => 
            array (
                'id' => 5595,
                'name' => 'Halifax',
                'country_id' => 227,
            ),
            407 => 
            array (
                'id' => 5596,
                'name' => 'Halstead',
                'country_id' => 227,
            ),
            408 => 
            array (
                'id' => 5597,
                'name' => 'Haltwhistle',
                'country_id' => 227,
            ),
            409 => 
            array (
                'id' => 5598,
                'name' => 'Hamilton',
                'country_id' => 227,
            ),
            410 => 
            array (
                'id' => 5599,
                'name' => 'Harlow',
                'country_id' => 227,
            ),
            411 => 
            array (
                'id' => 5600,
                'name' => 'Harpenden',
                'country_id' => 227,
            ),
            412 => 
            array (
                'id' => 5601,
                'name' => 'Harrogate',
                'country_id' => 227,
            ),
            413 => 
            array (
                'id' => 5602,
                'name' => 'Hartlepool',
                'country_id' => 227,
            ),
            414 => 
            array (
                'id' => 5603,
                'name' => 'Harwich',
                'country_id' => 227,
            ),
            415 => 
            array (
                'id' => 5604,
                'name' => 'Haslemere',
                'country_id' => 227,
            ),
            416 => 
            array (
                'id' => 5605,
                'name' => 'Hastings',
                'country_id' => 227,
            ),
            417 => 
            array (
                'id' => 5606,
                'name' => 'Hatfield',
                'country_id' => 227,
            ),
            418 => 
            array (
                'id' => 5607,
                'name' => 'Havant',
                'country_id' => 227,
            ),
            419 => 
            array (
                'id' => 5608,
                'name' => 'Haverfordwest',
                'country_id' => 227,
            ),
            420 => 
            array (
                'id' => 5609,
                'name' => 'Haverhill',
                'country_id' => 227,
            ),
            421 => 
            array (
                'id' => 5610,
                'name' => 'Hawarden',
                'country_id' => 227,
            ),
            422 => 
            array (
                'id' => 5611,
                'name' => 'Hawick',
                'country_id' => 227,
            ),
            423 => 
            array (
                'id' => 5612,
                'name' => 'Hay on Wye',
                'country_id' => 227,
            ),
            424 => 
            array (
                'id' => 5613,
                'name' => 'Hayle',
                'country_id' => 227,
            ),
            425 => 
            array (
                'id' => 5614,
                'name' => 'Haywards Heath',
                'country_id' => 227,
            ),
            426 => 
            array (
                'id' => 5615,
                'name' => 'Heanor',
                'country_id' => 227,
            ),
            427 => 
            array (
                'id' => 5616,
                'name' => 'Heathfield',
                'country_id' => 227,
            ),
            428 => 
            array (
                'id' => 5617,
                'name' => 'Hebden Bridge',
                'country_id' => 227,
            ),
            429 => 
            array (
                'id' => 5618,
                'name' => 'Helensburgh',
                'country_id' => 227,
            ),
            430 => 
            array (
                'id' => 5619,
                'name' => 'Helston',
                'country_id' => 227,
            ),
            431 => 
            array (
                'id' => 5620,
                'name' => 'Hemel Hempstead',
                'country_id' => 227,
            ),
            432 => 
            array (
                'id' => 5621,
                'name' => 'Henley on Thames',
                'country_id' => 227,
            ),
            433 => 
            array (
                'id' => 5622,
                'name' => 'Hereford',
                'country_id' => 227,
            ),
            434 => 
            array (
                'id' => 5623,
                'name' => 'Herne Bay',
                'country_id' => 227,
            ),
            435 => 
            array (
                'id' => 5624,
                'name' => 'Hertford',
                'country_id' => 227,
            ),
            436 => 
            array (
                'id' => 5625,
                'name' => 'Hessle',
                'country_id' => 227,
            ),
            437 => 
            array (
                'id' => 5626,
                'name' => 'Heswall',
                'country_id' => 227,
            ),
            438 => 
            array (
                'id' => 5627,
                'name' => 'Hexham',
                'country_id' => 227,
            ),
            439 => 
            array (
                'id' => 5628,
                'name' => 'High Wycombe',
                'country_id' => 227,
            ),
            440 => 
            array (
                'id' => 5629,
                'name' => 'Higham Ferrers',
                'country_id' => 227,
            ),
            441 => 
            array (
                'id' => 5630,
                'name' => 'Highworth',
                'country_id' => 227,
            ),
            442 => 
            array (
                'id' => 5631,
                'name' => 'Hinckley',
                'country_id' => 227,
            ),
            443 => 
            array (
                'id' => 5632,
                'name' => 'Hitchin',
                'country_id' => 227,
            ),
            444 => 
            array (
                'id' => 5633,
                'name' => 'Hoddesdon',
                'country_id' => 227,
            ),
            445 => 
            array (
                'id' => 5634,
                'name' => 'Holmfirth',
                'country_id' => 227,
            ),
            446 => 
            array (
                'id' => 5635,
                'name' => 'Holsworthy',
                'country_id' => 227,
            ),
            447 => 
            array (
                'id' => 5636,
                'name' => 'Holyhead',
                'country_id' => 227,
            ),
            448 => 
            array (
                'id' => 5637,
                'name' => 'Holywell',
                'country_id' => 227,
            ),
            449 => 
            array (
                'id' => 5638,
                'name' => 'Honiton',
                'country_id' => 227,
            ),
            450 => 
            array (
                'id' => 5639,
                'name' => 'Horley',
                'country_id' => 227,
            ),
            451 => 
            array (
                'id' => 5640,
                'name' => 'Horncastle',
                'country_id' => 227,
            ),
            452 => 
            array (
                'id' => 5641,
                'name' => 'Hornsea',
                'country_id' => 227,
            ),
            453 => 
            array (
                'id' => 5642,
                'name' => 'Horsham',
                'country_id' => 227,
            ),
            454 => 
            array (
                'id' => 5643,
                'name' => 'Horwich',
                'country_id' => 227,
            ),
            455 => 
            array (
                'id' => 5644,
                'name' => 'Houghton le Spring',
                'country_id' => 227,
            ),
            456 => 
            array (
                'id' => 5645,
                'name' => 'Hove',
                'country_id' => 227,
            ),
            457 => 
            array (
                'id' => 5646,
                'name' => 'Howden',
                'country_id' => 227,
            ),
            458 => 
            array (
                'id' => 5647,
                'name' => 'Hoylake',
                'country_id' => 227,
            ),
            459 => 
            array (
                'id' => 5648,
                'name' => 'Hucknall',
                'country_id' => 227,
            ),
            460 => 
            array (
                'id' => 5649,
                'name' => 'Huddersfield',
                'country_id' => 227,
            ),
            461 => 
            array (
                'id' => 5650,
                'name' => 'Hungerford',
                'country_id' => 227,
            ),
            462 => 
            array (
                'id' => 5651,
                'name' => 'Hunstanton',
                'country_id' => 227,
            ),
            463 => 
            array (
                'id' => 5652,
                'name' => 'Huntingdon',
                'country_id' => 227,
            ),
            464 => 
            array (
                'id' => 5653,
                'name' => 'Huntly',
                'country_id' => 227,
            ),
            465 => 
            array (
                'id' => 5654,
                'name' => 'Hyde',
                'country_id' => 227,
            ),
            466 => 
            array (
                'id' => 5655,
                'name' => 'Hythe',
                'country_id' => 227,
            ),
            467 => 
            array (
                'id' => 5656,
                'name' => 'Ilford',
                'country_id' => 227,
            ),
            468 => 
            array (
                'id' => 5657,
                'name' => 'Ilfracombe',
                'country_id' => 227,
            ),
            469 => 
            array (
                'id' => 5658,
                'name' => 'Ilkeston',
                'country_id' => 227,
            ),
            470 => 
            array (
                'id' => 5659,
                'name' => 'Ilkley',
                'country_id' => 227,
            ),
            471 => 
            array (
                'id' => 5660,
                'name' => 'Ilminster',
                'country_id' => 227,
            ),
            472 => 
            array (
                'id' => 5661,
                'name' => 'Innerleithen',
                'country_id' => 227,
            ),
            473 => 
            array (
                'id' => 5662,
                'name' => 'Inveraray',
                'country_id' => 227,
            ),
            474 => 
            array (
                'id' => 5663,
                'name' => 'Inverkeithing',
                'country_id' => 227,
            ),
            475 => 
            array (
                'id' => 5664,
                'name' => 'Inverness',
                'country_id' => 227,
            ),
            476 => 
            array (
                'id' => 5665,
                'name' => 'Inverurie',
                'country_id' => 227,
            ),
            477 => 
            array (
                'id' => 5666,
                'name' => 'Ipswich',
                'country_id' => 227,
            ),
            478 => 
            array (
                'id' => 5667,
                'name' => 'Irthlingborough',
                'country_id' => 227,
            ),
            479 => 
            array (
                'id' => 5668,
                'name' => 'Irvine',
                'country_id' => 227,
            ),
            480 => 
            array (
                'id' => 5669,
                'name' => 'Ivybridge',
                'country_id' => 227,
            ),
            481 => 
            array (
                'id' => 5670,
                'name' => 'Jarrow',
                'country_id' => 227,
            ),
            482 => 
            array (
                'id' => 5671,
                'name' => 'Jedburgh',
                'country_id' => 227,
            ),
            483 => 
            array (
                'id' => 5672,
                'name' => 'Johnstone',
                'country_id' => 227,
            ),
            484 => 
            array (
                'id' => 5673,
                'name' => 'Keighley',
                'country_id' => 227,
            ),
            485 => 
            array (
                'id' => 5674,
                'name' => 'Keith',
                'country_id' => 227,
            ),
            486 => 
            array (
                'id' => 5675,
                'name' => 'Kelso',
                'country_id' => 227,
            ),
            487 => 
            array (
                'id' => 5676,
                'name' => 'Kempston',
                'country_id' => 227,
            ),
            488 => 
            array (
                'id' => 5677,
                'name' => 'Kendal',
                'country_id' => 227,
            ),
            489 => 
            array (
                'id' => 5678,
                'name' => 'Kenilworth',
                'country_id' => 227,
            ),
            490 => 
            array (
                'id' => 5679,
                'name' => 'Kesgrave',
                'country_id' => 227,
            ),
            491 => 
            array (
                'id' => 5680,
                'name' => 'Keswick',
                'country_id' => 227,
            ),
            492 => 
            array (
                'id' => 5681,
                'name' => 'Kettering',
                'country_id' => 227,
            ),
            493 => 
            array (
                'id' => 5682,
                'name' => 'Keynsham',
                'country_id' => 227,
            ),
            494 => 
            array (
                'id' => 5683,
                'name' => 'Kidderminster',
                'country_id' => 227,
            ),
            495 => 
            array (
                'id' => 5684,
                'name' => 'Kilbarchan',
                'country_id' => 227,
            ),
            496 => 
            array (
                'id' => 5685,
                'name' => 'Kilkeel',
                'country_id' => 227,
            ),
            497 => 
            array (
                'id' => 5686,
                'name' => 'Killyleagh',
                'country_id' => 227,
            ),
            498 => 
            array (
                'id' => 5687,
                'name' => 'Kilmarnock',
                'country_id' => 227,
            ),
            499 => 
            array (
                'id' => 5688,
                'name' => 'Kilwinning',
                'country_id' => 227,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 5689,
                'name' => 'Kinghorn',
                'country_id' => 227,
            ),
            1 => 
            array (
                'id' => 5690,
                'name' => 'Kingsbridge',
                'country_id' => 227,
            ),
            2 => 
            array (
                'id' => 5691,
                'name' => 'Kington',
                'country_id' => 227,
            ),
            3 => 
            array (
                'id' => 5692,
                'name' => 'Kingussie',
                'country_id' => 227,
            ),
            4 => 
            array (
                'id' => 5693,
                'name' => 'Kinross',
                'country_id' => 227,
            ),
            5 => 
            array (
                'id' => 5694,
                'name' => 'Kintore',
                'country_id' => 227,
            ),
            6 => 
            array (
                'id' => 5695,
                'name' => 'Kirkby',
                'country_id' => 227,
            ),
            7 => 
            array (
                'id' => 5696,
                'name' => 'Kirkby Lonsdale',
                'country_id' => 227,
            ),
            8 => 
            array (
                'id' => 5697,
                'name' => 'Kirkcaldy',
                'country_id' => 227,
            ),
            9 => 
            array (
                'id' => 5698,
                'name' => 'Kirkcudbright',
                'country_id' => 227,
            ),
            10 => 
            array (
                'id' => 5699,
                'name' => 'Kirkham',
                'country_id' => 227,
            ),
            11 => 
            array (
                'id' => 5700,
                'name' => 'Kirkwall',
                'country_id' => 227,
            ),
            12 => 
            array (
                'id' => 5701,
                'name' => 'Kirriemuir',
                'country_id' => 227,
            ),
            13 => 
            array (
                'id' => 5702,
                'name' => 'Knaresborough',
                'country_id' => 227,
            ),
            14 => 
            array (
                'id' => 5703,
                'name' => 'Knighton',
                'country_id' => 227,
            ),
            15 => 
            array (
                'id' => 5704,
                'name' => 'Knutsford',
                'country_id' => 227,
            ),
            16 => 
            array (
                'id' => 5705,
                'name' => 'Ladybank',
                'country_id' => 227,
            ),
            17 => 
            array (
                'id' => 5706,
                'name' => 'Lampeter',
                'country_id' => 227,
            ),
            18 => 
            array (
                'id' => 5707,
                'name' => 'Lanark',
                'country_id' => 227,
            ),
            19 => 
            array (
                'id' => 5708,
                'name' => 'Lancaster',
                'country_id' => 227,
            ),
            20 => 
            array (
                'id' => 5709,
                'name' => 'Langholm',
                'country_id' => 227,
            ),
            21 => 
            array (
                'id' => 5710,
                'name' => 'Largs',
                'country_id' => 227,
            ),
            22 => 
            array (
                'id' => 5711,
                'name' => 'Larne',
                'country_id' => 227,
            ),
            23 => 
            array (
                'id' => 5712,
                'name' => 'Laugharne',
                'country_id' => 227,
            ),
            24 => 
            array (
                'id' => 5713,
                'name' => 'Launceston',
                'country_id' => 227,
            ),
            25 => 
            array (
                'id' => 5714,
                'name' => 'Laurencekirk',
                'country_id' => 227,
            ),
            26 => 
            array (
                'id' => 5715,
                'name' => 'Leamington Spa',
                'country_id' => 227,
            ),
            27 => 
            array (
                'id' => 5716,
                'name' => 'Leatherhead',
                'country_id' => 227,
            ),
            28 => 
            array (
                'id' => 5717,
                'name' => 'Ledbury',
                'country_id' => 227,
            ),
            29 => 
            array (
                'id' => 5718,
                'name' => 'Leeds',
                'country_id' => 227,
            ),
            30 => 
            array (
                'id' => 5719,
                'name' => 'Leek',
                'country_id' => 227,
            ),
            31 => 
            array (
                'id' => 5720,
                'name' => 'Leicester',
                'country_id' => 227,
            ),
            32 => 
            array (
                'id' => 5721,
                'name' => 'Leighton Buzzard',
                'country_id' => 227,
            ),
            33 => 
            array (
                'id' => 5722,
                'name' => 'Leiston',
                'country_id' => 227,
            ),
            34 => 
            array (
                'id' => 5723,
                'name' => 'Leominster',
                'country_id' => 227,
            ),
            35 => 
            array (
                'id' => 5724,
                'name' => 'Lerwick',
                'country_id' => 227,
            ),
            36 => 
            array (
                'id' => 5725,
                'name' => 'Letchworth',
                'country_id' => 227,
            ),
            37 => 
            array (
                'id' => 5726,
                'name' => 'Leven',
                'country_id' => 227,
            ),
            38 => 
            array (
                'id' => 5727,
                'name' => 'Lewes',
                'country_id' => 227,
            ),
            39 => 
            array (
                'id' => 5728,
                'name' => 'Leyland',
                'country_id' => 227,
            ),
            40 => 
            array (
                'id' => 5729,
                'name' => 'Lichfield',
                'country_id' => 227,
            ),
            41 => 
            array (
                'id' => 5730,
                'name' => 'Limavady',
                'country_id' => 227,
            ),
            42 => 
            array (
                'id' => 5731,
                'name' => 'Lincoln',
                'country_id' => 227,
            ),
            43 => 
            array (
                'id' => 5732,
                'name' => 'Linlithgow',
                'country_id' => 227,
            ),
            44 => 
            array (
                'id' => 5733,
                'name' => 'Lisburn',
                'country_id' => 227,
            ),
            45 => 
            array (
                'id' => 5734,
                'name' => 'Liskeard',
                'country_id' => 227,
            ),
            46 => 
            array (
                'id' => 5735,
                'name' => 'Lisnaskea',
                'country_id' => 227,
            ),
            47 => 
            array (
                'id' => 5736,
                'name' => 'Littlehampton',
                'country_id' => 227,
            ),
            48 => 
            array (
                'id' => 5737,
                'name' => 'Liverpool',
                'country_id' => 227,
            ),
            49 => 
            array (
                'id' => 5738,
                'name' => 'Llandeilo',
                'country_id' => 227,
            ),
            50 => 
            array (
                'id' => 5739,
                'name' => 'Llandovery',
                'country_id' => 227,
            ),
            51 => 
            array (
                'id' => 5740,
                'name' => 'Llandrindod Wells',
                'country_id' => 227,
            ),
            52 => 
            array (
                'id' => 5741,
                'name' => 'Llandudno',
                'country_id' => 227,
            ),
            53 => 
            array (
                'id' => 5742,
                'name' => 'Llanelli',
                'country_id' => 227,
            ),
            54 => 
            array (
                'id' => 5743,
                'name' => 'Llanfyllin',
                'country_id' => 227,
            ),
            55 => 
            array (
                'id' => 5744,
                'name' => 'Llangollen',
                'country_id' => 227,
            ),
            56 => 
            array (
                'id' => 5745,
                'name' => 'Llanidloes',
                'country_id' => 227,
            ),
            57 => 
            array (
                'id' => 5746,
                'name' => 'Llanrwst',
                'country_id' => 227,
            ),
            58 => 
            array (
                'id' => 5747,
                'name' => 'Llantrisant',
                'country_id' => 227,
            ),
            59 => 
            array (
                'id' => 5748,
                'name' => 'Llantwit Major',
                'country_id' => 227,
            ),
            60 => 
            array (
                'id' => 5749,
                'name' => 'Llanwrtyd Wells',
                'country_id' => 227,
            ),
            61 => 
            array (
                'id' => 5750,
                'name' => 'Loanhead',
                'country_id' => 227,
            ),
            62 => 
            array (
                'id' => 5751,
                'name' => 'Lochgilphead',
                'country_id' => 227,
            ),
            63 => 
            array (
                'id' => 5752,
                'name' => 'Lockerbie',
                'country_id' => 227,
            ),
            64 => 
            array (
                'id' => 5753,
                'name' => 'Londonderry',
                'country_id' => 227,
            ),
            65 => 
            array (
                'id' => 5754,
                'name' => 'Long Eaton',
                'country_id' => 227,
            ),
            66 => 
            array (
                'id' => 5755,
                'name' => 'Longridge',
                'country_id' => 227,
            ),
            67 => 
            array (
                'id' => 5756,
                'name' => 'Looe',
                'country_id' => 227,
            ),
            68 => 
            array (
                'id' => 5757,
                'name' => 'Lossiemouth',
                'country_id' => 227,
            ),
            69 => 
            array (
                'id' => 5758,
                'name' => 'Lostwithiel',
                'country_id' => 227,
            ),
            70 => 
            array (
                'id' => 5759,
                'name' => 'Loughborough',
                'country_id' => 227,
            ),
            71 => 
            array (
                'id' => 5760,
                'name' => 'Loughton',
                'country_id' => 227,
            ),
            72 => 
            array (
                'id' => 5761,
                'name' => 'Louth',
                'country_id' => 227,
            ),
            73 => 
            array (
                'id' => 5762,
                'name' => 'Lowestoft',
                'country_id' => 227,
            ),
            74 => 
            array (
                'id' => 5763,
                'name' => 'Ludlow',
                'country_id' => 227,
            ),
            75 => 
            array (
                'id' => 5764,
                'name' => 'Lurgan',
                'country_id' => 227,
            ),
            76 => 
            array (
                'id' => 5765,
                'name' => 'Luton',
                'country_id' => 227,
            ),
            77 => 
            array (
                'id' => 5766,
                'name' => 'Lutterworth',
                'country_id' => 227,
            ),
            78 => 
            array (
                'id' => 5767,
                'name' => 'Lydd',
                'country_id' => 227,
            ),
            79 => 
            array (
                'id' => 5768,
                'name' => 'Lydney',
                'country_id' => 227,
            ),
            80 => 
            array (
                'id' => 5769,
                'name' => 'Lyme Regis',
                'country_id' => 227,
            ),
            81 => 
            array (
                'id' => 5770,
                'name' => 'Lymington',
                'country_id' => 227,
            ),
            82 => 
            array (
                'id' => 5771,
                'name' => 'Lynton',
                'country_id' => 227,
            ),
            83 => 
            array (
                'id' => 5772,
                'name' => 'Mablethorpe',
                'country_id' => 227,
            ),
            84 => 
            array (
                'id' => 5773,
                'name' => 'Macclesfield',
                'country_id' => 227,
            ),
            85 => 
            array (
                'id' => 5774,
                'name' => 'Machynlleth',
                'country_id' => 227,
            ),
            86 => 
            array (
                'id' => 5775,
                'name' => 'Maesteg',
                'country_id' => 227,
            ),
            87 => 
            array (
                'id' => 5776,
                'name' => 'Magherafelt',
                'country_id' => 227,
            ),
            88 => 
            array (
                'id' => 5777,
                'name' => 'Maidenhead',
                'country_id' => 227,
            ),
            89 => 
            array (
                'id' => 5778,
                'name' => 'Maidstone',
                'country_id' => 227,
            ),
            90 => 
            array (
                'id' => 5779,
                'name' => 'Maldon',
                'country_id' => 227,
            ),
            91 => 
            array (
                'id' => 5780,
                'name' => 'Malmesbury',
                'country_id' => 227,
            ),
            92 => 
            array (
                'id' => 5781,
                'name' => 'Malton',
                'country_id' => 227,
            ),
            93 => 
            array (
                'id' => 5782,
                'name' => 'Malvern',
                'country_id' => 227,
            ),
            94 => 
            array (
                'id' => 5783,
                'name' => 'Manchester',
                'country_id' => 227,
            ),
            95 => 
            array (
                'id' => 5784,
                'name' => 'Manningtree',
                'country_id' => 227,
            ),
            96 => 
            array (
                'id' => 5785,
                'name' => 'Mansfield',
                'country_id' => 227,
            ),
            97 => 
            array (
                'id' => 5786,
                'name' => 'March',
                'country_id' => 227,
            ),
            98 => 
            array (
                'id' => 5787,
                'name' => 'Margate',
                'country_id' => 227,
            ),
            99 => 
            array (
                'id' => 5788,
                'name' => 'Market Deeping',
                'country_id' => 227,
            ),
            100 => 
            array (
                'id' => 5789,
                'name' => 'Market Drayton',
                'country_id' => 227,
            ),
            101 => 
            array (
                'id' => 5790,
                'name' => 'Market Harborough',
                'country_id' => 227,
            ),
            102 => 
            array (
                'id' => 5791,
                'name' => 'Market Rasen',
                'country_id' => 227,
            ),
            103 => 
            array (
                'id' => 5792,
                'name' => 'Market Weighton',
                'country_id' => 227,
            ),
            104 => 
            array (
                'id' => 5793,
                'name' => 'Markethill',
                'country_id' => 227,
            ),
            105 => 
            array (
                'id' => 5794,
                'name' => 'Markinch',
                'country_id' => 227,
            ),
            106 => 
            array (
                'id' => 5795,
                'name' => 'Marlborough',
                'country_id' => 227,
            ),
            107 => 
            array (
                'id' => 5796,
                'name' => 'Marlow',
                'country_id' => 227,
            ),
            108 => 
            array (
                'id' => 5797,
                'name' => 'Maryport',
                'country_id' => 227,
            ),
            109 => 
            array (
                'id' => 5798,
                'name' => 'Matlock',
                'country_id' => 227,
            ),
            110 => 
            array (
                'id' => 5799,
                'name' => 'Maybole',
                'country_id' => 227,
            ),
            111 => 
            array (
                'id' => 5800,
                'name' => 'Melksham',
                'country_id' => 227,
            ),
            112 => 
            array (
                'id' => 5801,
                'name' => 'Melrose',
                'country_id' => 227,
            ),
            113 => 
            array (
                'id' => 5802,
                'name' => 'Melton Mowbray',
                'country_id' => 227,
            ),
            114 => 
            array (
                'id' => 5803,
                'name' => 'Merthyr Tydfil',
                'country_id' => 227,
            ),
            115 => 
            array (
                'id' => 5804,
                'name' => 'Mexborough',
                'country_id' => 227,
            ),
            116 => 
            array (
                'id' => 5805,
                'name' => 'Middleham',
                'country_id' => 227,
            ),
            117 => 
            array (
                'id' => 5806,
                'name' => 'Middlesbrough',
                'country_id' => 227,
            ),
            118 => 
            array (
                'id' => 5807,
                'name' => 'Middlewich',
                'country_id' => 227,
            ),
            119 => 
            array (
                'id' => 5808,
                'name' => 'Midhurst',
                'country_id' => 227,
            ),
            120 => 
            array (
                'id' => 5809,
                'name' => 'Midsomer Norton',
                'country_id' => 227,
            ),
            121 => 
            array (
                'id' => 5810,
                'name' => 'Milford Haven',
                'country_id' => 227,
            ),
            122 => 
            array (
                'id' => 5811,
                'name' => 'Milngavie',
                'country_id' => 227,
            ),
            123 => 
            array (
                'id' => 5812,
                'name' => 'Milton Keynes',
                'country_id' => 227,
            ),
            124 => 
            array (
                'id' => 5813,
                'name' => 'Minehead',
                'country_id' => 227,
            ),
            125 => 
            array (
                'id' => 5814,
                'name' => 'Moffat',
                'country_id' => 227,
            ),
            126 => 
            array (
                'id' => 5815,
                'name' => 'Mold',
                'country_id' => 227,
            ),
            127 => 
            array (
                'id' => 5816,
                'name' => 'Monifieth',
                'country_id' => 227,
            ),
            128 => 
            array (
                'id' => 5817,
                'name' => 'Monmouth',
                'country_id' => 227,
            ),
            129 => 
            array (
                'id' => 5818,
                'name' => 'Montgomery',
                'country_id' => 227,
            ),
            130 => 
            array (
                'id' => 5819,
                'name' => 'Montrose',
                'country_id' => 227,
            ),
            131 => 
            array (
                'id' => 5820,
                'name' => 'Morecambe',
                'country_id' => 227,
            ),
            132 => 
            array (
                'id' => 5821,
                'name' => 'Moreton in Marsh',
                'country_id' => 227,
            ),
            133 => 
            array (
                'id' => 5822,
                'name' => 'Moretonhampstead',
                'country_id' => 227,
            ),
            134 => 
            array (
                'id' => 5823,
                'name' => 'Morley',
                'country_id' => 227,
            ),
            135 => 
            array (
                'id' => 5824,
                'name' => 'Morpeth',
                'country_id' => 227,
            ),
            136 => 
            array (
                'id' => 5825,
                'name' => 'Motherwell',
                'country_id' => 227,
            ),
            137 => 
            array (
                'id' => 5826,
                'name' => 'Musselburgh',
                'country_id' => 227,
            ),
            138 => 
            array (
                'id' => 5827,
                'name' => 'Nailsea',
                'country_id' => 227,
            ),
            139 => 
            array (
                'id' => 5828,
                'name' => 'Nailsworth',
                'country_id' => 227,
            ),
            140 => 
            array (
                'id' => 5829,
                'name' => 'Nairn',
                'country_id' => 227,
            ),
            141 => 
            array (
                'id' => 5830,
                'name' => 'Nantwich',
                'country_id' => 227,
            ),
            142 => 
            array (
                'id' => 5831,
                'name' => 'Narberth',
                'country_id' => 227,
            ),
            143 => 
            array (
                'id' => 5832,
                'name' => 'Neath',
                'country_id' => 227,
            ),
            144 => 
            array (
                'id' => 5833,
                'name' => 'Needham Market',
                'country_id' => 227,
            ),
            145 => 
            array (
                'id' => 5834,
                'name' => 'Neston',
                'country_id' => 227,
            ),
            146 => 
            array (
                'id' => 5835,
                'name' => 'New Mills',
                'country_id' => 227,
            ),
            147 => 
            array (
                'id' => 5836,
                'name' => 'New Milton',
                'country_id' => 227,
            ),
            148 => 
            array (
                'id' => 5837,
                'name' => 'Newbury',
                'country_id' => 227,
            ),
            149 => 
            array (
                'id' => 5838,
                'name' => 'Newcastle',
                'country_id' => 227,
            ),
            150 => 
            array (
                'id' => 5839,
                'name' => 'Newcastle Emlyn',
                'country_id' => 227,
            ),
            151 => 
            array (
                'id' => 5840,
                'name' => 'Newcastle upon Tyne',
                'country_id' => 227,
            ),
            152 => 
            array (
                'id' => 5841,
                'name' => 'Newent',
                'country_id' => 227,
            ),
            153 => 
            array (
                'id' => 5842,
                'name' => 'Newhaven',
                'country_id' => 227,
            ),
            154 => 
            array (
                'id' => 5843,
                'name' => 'Newmarket',
                'country_id' => 227,
            ),
            155 => 
            array (
                'id' => 5844,
                'name' => 'Newport',
                'country_id' => 227,
            ),
            156 => 
            array (
                'id' => 5845,
                'name' => 'Newport Pagnell',
                'country_id' => 227,
            ),
            157 => 
            array (
                'id' => 5846,
                'name' => 'Newport on Tay',
                'country_id' => 227,
            ),
            158 => 
            array (
                'id' => 5847,
                'name' => 'Newquay',
                'country_id' => 227,
            ),
            159 => 
            array (
                'id' => 5848,
                'name' => 'Newry',
                'country_id' => 227,
            ),
            160 => 
            array (
                'id' => 5849,
                'name' => 'Newton Abbot',
                'country_id' => 227,
            ),
            161 => 
            array (
                'id' => 5850,
                'name' => 'Newton Aycliffe',
                'country_id' => 227,
            ),
            162 => 
            array (
                'id' => 5851,
                'name' => 'Newton Stewart',
                'country_id' => 227,
            ),
            163 => 
            array (
                'id' => 5852,
                'name' => 'Newton le Willows',
                'country_id' => 227,
            ),
            164 => 
            array (
                'id' => 5853,
                'name' => 'Newtown',
                'country_id' => 227,
            ),
            165 => 
            array (
                'id' => 5854,
                'name' => 'Newtownabbey',
                'country_id' => 227,
            ),
            166 => 
            array (
                'id' => 5855,
                'name' => 'Newtownards',
                'country_id' => 227,
            ),
            167 => 
            array (
                'id' => 5856,
                'name' => 'Normanton',
                'country_id' => 227,
            ),
            168 => 
            array (
                'id' => 5857,
                'name' => 'North Berwick',
                'country_id' => 227,
            ),
            169 => 
            array (
                'id' => 5858,
                'name' => 'North Walsham',
                'country_id' => 227,
            ),
            170 => 
            array (
                'id' => 5859,
                'name' => 'Northallerton',
                'country_id' => 227,
            ),
            171 => 
            array (
                'id' => 5860,
                'name' => 'Northampton',
                'country_id' => 227,
            ),
            172 => 
            array (
                'id' => 5861,
                'name' => 'Northwich',
                'country_id' => 227,
            ),
            173 => 
            array (
                'id' => 5862,
                'name' => 'Norwich',
                'country_id' => 227,
            ),
            174 => 
            array (
                'id' => 5863,
                'name' => 'Nottingham',
                'country_id' => 227,
            ),
            175 => 
            array (
                'id' => 5864,
                'name' => 'Nuneaton',
                'country_id' => 227,
            ),
            176 => 
            array (
                'id' => 5865,
                'name' => 'Oakham',
                'country_id' => 227,
            ),
            177 => 
            array (
                'id' => 5866,
                'name' => 'Oban',
                'country_id' => 227,
            ),
            178 => 
            array (
                'id' => 5867,
                'name' => 'Okehampton',
                'country_id' => 227,
            ),
            179 => 
            array (
                'id' => 5868,
                'name' => 'Oldbury',
                'country_id' => 227,
            ),
            180 => 
            array (
                'id' => 5869,
                'name' => 'Oldham',
                'country_id' => 227,
            ),
            181 => 
            array (
                'id' => 5870,
                'name' => 'Oldmeldrum',
                'country_id' => 227,
            ),
            182 => 
            array (
                'id' => 5871,
                'name' => 'Olney',
                'country_id' => 227,
            ),
            183 => 
            array (
                'id' => 5872,
                'name' => 'Omagh',
                'country_id' => 227,
            ),
            184 => 
            array (
                'id' => 5873,
                'name' => 'Ormskirk',
                'country_id' => 227,
            ),
            185 => 
            array (
                'id' => 5874,
                'name' => 'Orpington',
                'country_id' => 227,
            ),
            186 => 
            array (
                'id' => 5875,
                'name' => 'Ossett',
                'country_id' => 227,
            ),
            187 => 
            array (
                'id' => 5876,
                'name' => 'Oswestry',
                'country_id' => 227,
            ),
            188 => 
            array (
                'id' => 5877,
                'name' => 'Otley',
                'country_id' => 227,
            ),
            189 => 
            array (
                'id' => 5878,
                'name' => 'Oundle',
                'country_id' => 227,
            ),
            190 => 
            array (
                'id' => 5879,
                'name' => 'Oxford',
                'country_id' => 227,
            ),
            191 => 
            array (
                'id' => 5880,
                'name' => 'Padstow',
                'country_id' => 227,
            ),
            192 => 
            array (
                'id' => 5881,
                'name' => 'Paignton',
                'country_id' => 227,
            ),
            193 => 
            array (
                'id' => 5882,
                'name' => 'Painswick',
                'country_id' => 227,
            ),
            194 => 
            array (
                'id' => 5883,
                'name' => 'Paisley',
                'country_id' => 227,
            ),
            195 => 
            array (
                'id' => 5884,
                'name' => 'Peebles',
                'country_id' => 227,
            ),
            196 => 
            array (
                'id' => 5885,
                'name' => 'Pembroke',
                'country_id' => 227,
            ),
            197 => 
            array (
                'id' => 5886,
                'name' => 'Penarth',
                'country_id' => 227,
            ),
            198 => 
            array (
                'id' => 5887,
                'name' => 'Penicuik',
                'country_id' => 227,
            ),
            199 => 
            array (
                'id' => 5888,
                'name' => 'Penistone',
                'country_id' => 227,
            ),
            200 => 
            array (
                'id' => 5889,
                'name' => 'Penmaenmawr',
                'country_id' => 227,
            ),
            201 => 
            array (
                'id' => 5890,
                'name' => 'Penrith',
                'country_id' => 227,
            ),
            202 => 
            array (
                'id' => 5891,
                'name' => 'Penryn',
                'country_id' => 227,
            ),
            203 => 
            array (
                'id' => 5892,
                'name' => 'Penzance',
                'country_id' => 227,
            ),
            204 => 
            array (
                'id' => 5893,
                'name' => 'Pershore',
                'country_id' => 227,
            ),
            205 => 
            array (
                'id' => 5894,
                'name' => 'Perth',
                'country_id' => 227,
            ),
            206 => 
            array (
                'id' => 5895,
                'name' => 'Peterborough',
                'country_id' => 227,
            ),
            207 => 
            array (
                'id' => 5896,
                'name' => 'Peterhead',
                'country_id' => 227,
            ),
            208 => 
            array (
                'id' => 5897,
                'name' => 'Peterlee',
                'country_id' => 227,
            ),
            209 => 
            array (
                'id' => 5898,
                'name' => 'Petersfield',
                'country_id' => 227,
            ),
            210 => 
            array (
                'id' => 5899,
                'name' => 'Petworth',
                'country_id' => 227,
            ),
            211 => 
            array (
                'id' => 5900,
                'name' => 'Pickering',
                'country_id' => 227,
            ),
            212 => 
            array (
                'id' => 5901,
                'name' => 'Pitlochry',
                'country_id' => 227,
            ),
            213 => 
            array (
                'id' => 5902,
                'name' => 'Pittenweem',
                'country_id' => 227,
            ),
            214 => 
            array (
                'id' => 5903,
                'name' => 'Plymouth',
                'country_id' => 227,
            ),
            215 => 
            array (
                'id' => 5904,
                'name' => 'Pocklington',
                'country_id' => 227,
            ),
            216 => 
            array (
                'id' => 5905,
                'name' => 'Polegate',
                'country_id' => 227,
            ),
            217 => 
            array (
                'id' => 5906,
                'name' => 'Pontefract',
                'country_id' => 227,
            ),
            218 => 
            array (
                'id' => 5907,
                'name' => 'Pontypridd',
                'country_id' => 227,
            ),
            219 => 
            array (
                'id' => 5908,
                'name' => 'Poole',
                'country_id' => 227,
            ),
            220 => 
            array (
                'id' => 5909,
                'name' => 'Port Talbot',
                'country_id' => 227,
            ),
            221 => 
            array (
                'id' => 5910,
                'name' => 'Portadown',
                'country_id' => 227,
            ),
            222 => 
            array (
                'id' => 5911,
                'name' => 'Portaferry',
                'country_id' => 227,
            ),
            223 => 
            array (
                'id' => 5912,
                'name' => 'Porth',
                'country_id' => 227,
            ),
            224 => 
            array (
                'id' => 5913,
                'name' => 'Porthcawl',
                'country_id' => 227,
            ),
            225 => 
            array (
                'id' => 5914,
                'name' => 'Porthmadog',
                'country_id' => 227,
            ),
            226 => 
            array (
                'id' => 5915,
                'name' => 'Portishead',
                'country_id' => 227,
            ),
            227 => 
            array (
                'id' => 5916,
                'name' => 'Portrush',
                'country_id' => 227,
            ),
            228 => 
            array (
                'id' => 5917,
                'name' => 'Portsmouth',
                'country_id' => 227,
            ),
            229 => 
            array (
                'id' => 5918,
                'name' => 'Portstewart',
                'country_id' => 227,
            ),
            230 => 
            array (
                'id' => 5919,
                'name' => 'Potters Bar',
                'country_id' => 227,
            ),
            231 => 
            array (
                'id' => 5920,
                'name' => 'Potton',
                'country_id' => 227,
            ),
            232 => 
            array (
                'id' => 5921,
                'name' => 'Poulton le Fylde',
                'country_id' => 227,
            ),
            233 => 
            array (
                'id' => 5922,
                'name' => 'Prescot',
                'country_id' => 227,
            ),
            234 => 
            array (
                'id' => 5923,
                'name' => 'Prestatyn',
                'country_id' => 227,
            ),
            235 => 
            array (
                'id' => 5924,
                'name' => 'Presteigne',
                'country_id' => 227,
            ),
            236 => 
            array (
                'id' => 5925,
                'name' => 'Preston',
                'country_id' => 227,
            ),
            237 => 
            array (
                'id' => 5926,
                'name' => 'Prestwick',
                'country_id' => 227,
            ),
            238 => 
            array (
                'id' => 5927,
                'name' => 'Princes Risborough',
                'country_id' => 227,
            ),
            239 => 
            array (
                'id' => 5928,
                'name' => 'Prudhoe',
                'country_id' => 227,
            ),
            240 => 
            array (
                'id' => 5929,
                'name' => 'Pudsey',
                'country_id' => 227,
            ),
            241 => 
            array (
                'id' => 5930,
                'name' => 'Pwllheli',
                'country_id' => 227,
            ),
            242 => 
            array (
                'id' => 5931,
                'name' => 'Ramsgate',
                'country_id' => 227,
            ),
            243 => 
            array (
                'id' => 5932,
                'name' => 'Randalstown',
                'country_id' => 227,
            ),
            244 => 
            array (
                'id' => 5933,
                'name' => 'Rayleigh',
                'country_id' => 227,
            ),
            245 => 
            array (
                'id' => 5934,
                'name' => 'Reading',
                'country_id' => 227,
            ),
            246 => 
            array (
                'id' => 5935,
                'name' => 'Redcar',
                'country_id' => 227,
            ),
            247 => 
            array (
                'id' => 5936,
                'name' => 'Redditch',
                'country_id' => 227,
            ),
            248 => 
            array (
                'id' => 5937,
                'name' => 'Redhill',
                'country_id' => 227,
            ),
            249 => 
            array (
                'id' => 5938,
                'name' => 'Redruth',
                'country_id' => 227,
            ),
            250 => 
            array (
                'id' => 5939,
                'name' => 'Reigate',
                'country_id' => 227,
            ),
            251 => 
            array (
                'id' => 5940,
                'name' => 'Retford',
                'country_id' => 227,
            ),
            252 => 
            array (
                'id' => 5941,
                'name' => 'Rhayader',
                'country_id' => 227,
            ),
            253 => 
            array (
                'id' => 5942,
                'name' => 'Rhuddlan',
                'country_id' => 227,
            ),
            254 => 
            array (
                'id' => 5943,
                'name' => 'Rhyl',
                'country_id' => 227,
            ),
            255 => 
            array (
                'id' => 5944,
                'name' => 'Richmond',
                'country_id' => 227,
            ),
            256 => 
            array (
                'id' => 5945,
                'name' => 'Rickmansworth',
                'country_id' => 227,
            ),
            257 => 
            array (
                'id' => 5946,
                'name' => 'Ringwood',
                'country_id' => 227,
            ),
            258 => 
            array (
                'id' => 5947,
                'name' => 'Ripley',
                'country_id' => 227,
            ),
            259 => 
            array (
                'id' => 5948,
                'name' => 'Ripon',
                'country_id' => 227,
            ),
            260 => 
            array (
                'id' => 5949,
                'name' => 'Rochdale',
                'country_id' => 227,
            ),
            261 => 
            array (
                'id' => 5950,
                'name' => 'Rochester',
                'country_id' => 227,
            ),
            262 => 
            array (
                'id' => 5951,
                'name' => 'Rochford',
                'country_id' => 227,
            ),
            263 => 
            array (
                'id' => 5952,
                'name' => 'Romford',
                'country_id' => 227,
            ),
            264 => 
            array (
                'id' => 5953,
                'name' => 'Romsey',
                'country_id' => 227,
            ),
            265 => 
            array (
                'id' => 5954,
                'name' => 'Ross on Wye',
                'country_id' => 227,
            ),
            266 => 
            array (
                'id' => 5955,
                'name' => 'Rostrevor',
                'country_id' => 227,
            ),
            267 => 
            array (
                'id' => 5956,
                'name' => 'Rothbury',
                'country_id' => 227,
            ),
            268 => 
            array (
                'id' => 5957,
                'name' => 'Rotherham',
                'country_id' => 227,
            ),
            269 => 
            array (
                'id' => 5958,
                'name' => 'Rothesay',
                'country_id' => 227,
            ),
            270 => 
            array (
                'id' => 5959,
                'name' => 'Rowley Regis',
                'country_id' => 227,
            ),
            271 => 
            array (
                'id' => 5960,
                'name' => 'Royston',
                'country_id' => 227,
            ),
            272 => 
            array (
                'id' => 5961,
                'name' => 'Rugby',
                'country_id' => 227,
            ),
            273 => 
            array (
                'id' => 5962,
                'name' => 'Rugeley',
                'country_id' => 227,
            ),
            274 => 
            array (
                'id' => 5963,
                'name' => 'Runcorn',
                'country_id' => 227,
            ),
            275 => 
            array (
                'id' => 5964,
                'name' => 'Rushden',
                'country_id' => 227,
            ),
            276 => 
            array (
                'id' => 5965,
                'name' => 'Rutherglen',
                'country_id' => 227,
            ),
            277 => 
            array (
                'id' => 5966,
                'name' => 'Ruthin',
                'country_id' => 227,
            ),
            278 => 
            array (
                'id' => 5967,
                'name' => 'Ryde',
                'country_id' => 227,
            ),
            279 => 
            array (
                'id' => 5968,
                'name' => 'Rye',
                'country_id' => 227,
            ),
            280 => 
            array (
                'id' => 5969,
                'name' => 'Saffron Walden',
                'country_id' => 227,
            ),
            281 => 
            array (
                'id' => 5970,
                'name' => 'Saintfield',
                'country_id' => 227,
            ),
            282 => 
            array (
                'id' => 5971,
                'name' => 'Salcombe',
                'country_id' => 227,
            ),
            283 => 
            array (
                'id' => 5972,
                'name' => 'Sale',
                'country_id' => 227,
            ),
            284 => 
            array (
                'id' => 5973,
                'name' => 'Salford',
                'country_id' => 227,
            ),
            285 => 
            array (
                'id' => 5974,
                'name' => 'Salisbury',
                'country_id' => 227,
            ),
            286 => 
            array (
                'id' => 5975,
                'name' => 'Saltash',
                'country_id' => 227,
            ),
            287 => 
            array (
                'id' => 5976,
                'name' => 'Saltcoats',
                'country_id' => 227,
            ),
            288 => 
            array (
                'id' => 5977,
                'name' => 'Sandbach',
                'country_id' => 227,
            ),
            289 => 
            array (
                'id' => 5978,
                'name' => 'Sandhurst',
                'country_id' => 227,
            ),
            290 => 
            array (
                'id' => 5979,
                'name' => 'Sandown',
                'country_id' => 227,
            ),
            291 => 
            array (
                'id' => 5980,
                'name' => 'Sandwich',
                'country_id' => 227,
            ),
            292 => 
            array (
                'id' => 5981,
                'name' => 'Sandy',
                'country_id' => 227,
            ),
            293 => 
            array (
                'id' => 5982,
                'name' => 'Sawbridgeworth',
                'country_id' => 227,
            ),
            294 => 
            array (
                'id' => 5983,
                'name' => 'Saxmundham',
                'country_id' => 227,
            ),
            295 => 
            array (
                'id' => 5984,
                'name' => 'Scarborough',
                'country_id' => 227,
            ),
            296 => 
            array (
                'id' => 5985,
                'name' => 'Scunthorpe',
                'country_id' => 227,
            ),
            297 => 
            array (
                'id' => 5986,
                'name' => 'Seaford',
                'country_id' => 227,
            ),
            298 => 
            array (
                'id' => 5987,
                'name' => 'Seaton',
                'country_id' => 227,
            ),
            299 => 
            array (
                'id' => 5988,
                'name' => 'Sedgefield',
                'country_id' => 227,
            ),
            300 => 
            array (
                'id' => 5989,
                'name' => 'Selby',
                'country_id' => 227,
            ),
            301 => 
            array (
                'id' => 5990,
                'name' => 'Selkirk',
                'country_id' => 227,
            ),
            302 => 
            array (
                'id' => 5991,
                'name' => 'Selsey',
                'country_id' => 227,
            ),
            303 => 
            array (
                'id' => 5992,
                'name' => 'Settle',
                'country_id' => 227,
            ),
            304 => 
            array (
                'id' => 5993,
                'name' => 'Sevenoaks',
                'country_id' => 227,
            ),
            305 => 
            array (
                'id' => 5994,
                'name' => 'Shaftesbury',
                'country_id' => 227,
            ),
            306 => 
            array (
                'id' => 5995,
                'name' => 'Shanklin',
                'country_id' => 227,
            ),
            307 => 
            array (
                'id' => 5996,
                'name' => 'Sheerness',
                'country_id' => 227,
            ),
            308 => 
            array (
                'id' => 5997,
                'name' => 'Sheffield',
                'country_id' => 227,
            ),
            309 => 
            array (
                'id' => 5998,
                'name' => 'Shepshed',
                'country_id' => 227,
            ),
            310 => 
            array (
                'id' => 5999,
                'name' => 'Shepton Mallet',
                'country_id' => 227,
            ),
            311 => 
            array (
                'id' => 6000,
                'name' => 'Sherborne',
                'country_id' => 227,
            ),
            312 => 
            array (
                'id' => 6001,
                'name' => 'Sheringham',
                'country_id' => 227,
            ),
            313 => 
            array (
                'id' => 6002,
                'name' => 'Shildon',
                'country_id' => 227,
            ),
            314 => 
            array (
                'id' => 6003,
                'name' => 'Shipston on Stour',
                'country_id' => 227,
            ),
            315 => 
            array (
                'id' => 6004,
                'name' => 'Shoreham by Sea',
                'country_id' => 227,
            ),
            316 => 
            array (
                'id' => 6005,
                'name' => 'Shrewsbury',
                'country_id' => 227,
            ),
            317 => 
            array (
                'id' => 6006,
                'name' => 'Sidmouth',
                'country_id' => 227,
            ),
            318 => 
            array (
                'id' => 6007,
                'name' => 'Sittingbourne',
                'country_id' => 227,
            ),
            319 => 
            array (
                'id' => 6008,
                'name' => 'Skegness',
                'country_id' => 227,
            ),
            320 => 
            array (
                'id' => 6009,
                'name' => 'Skelmersdale',
                'country_id' => 227,
            ),
            321 => 
            array (
                'id' => 6010,
                'name' => 'Skipton',
                'country_id' => 227,
            ),
            322 => 
            array (
                'id' => 6011,
                'name' => 'Sleaford',
                'country_id' => 227,
            ),
            323 => 
            array (
                'id' => 6012,
                'name' => 'Slough',
                'country_id' => 227,
            ),
            324 => 
            array (
                'id' => 6013,
                'name' => 'Smethwick',
                'country_id' => 227,
            ),
            325 => 
            array (
                'id' => 6014,
                'name' => 'Soham',
                'country_id' => 227,
            ),
            326 => 
            array (
                'id' => 6015,
                'name' => 'Solihull',
                'country_id' => 227,
            ),
            327 => 
            array (
                'id' => 6016,
                'name' => 'Somerton',
                'country_id' => 227,
            ),
            328 => 
            array (
                'id' => 6017,
                'name' => 'South Molton',
                'country_id' => 227,
            ),
            329 => 
            array (
                'id' => 6018,
                'name' => 'South Shields',
                'country_id' => 227,
            ),
            330 => 
            array (
                'id' => 6019,
                'name' => 'South Woodham Ferrers',
                'country_id' => 227,
            ),
            331 => 
            array (
                'id' => 6020,
                'name' => 'Southam',
                'country_id' => 227,
            ),
            332 => 
            array (
                'id' => 6021,
                'name' => 'Southampton',
                'country_id' => 227,
            ),
            333 => 
            array (
                'id' => 6022,
                'name' => 'Southborough',
                'country_id' => 227,
            ),
            334 => 
            array (
                'id' => 6023,
                'name' => 'Southend on Sea',
                'country_id' => 227,
            ),
            335 => 
            array (
                'id' => 6024,
                'name' => 'Southport',
                'country_id' => 227,
            ),
            336 => 
            array (
                'id' => 6025,
                'name' => 'Southsea',
                'country_id' => 227,
            ),
            337 => 
            array (
                'id' => 6026,
                'name' => 'Southwell',
                'country_id' => 227,
            ),
            338 => 
            array (
                'id' => 6027,
                'name' => 'Southwold',
                'country_id' => 227,
            ),
            339 => 
            array (
                'id' => 6028,
                'name' => 'Spalding',
                'country_id' => 227,
            ),
            340 => 
            array (
                'id' => 6029,
                'name' => 'Spennymoor',
                'country_id' => 227,
            ),
            341 => 
            array (
                'id' => 6030,
                'name' => 'Spilsby',
                'country_id' => 227,
            ),
            342 => 
            array (
                'id' => 6031,
                'name' => 'Stafford',
                'country_id' => 227,
            ),
            343 => 
            array (
                'id' => 6032,
                'name' => 'Staines',
                'country_id' => 227,
            ),
            344 => 
            array (
                'id' => 6033,
                'name' => 'Stamford',
                'country_id' => 227,
            ),
            345 => 
            array (
                'id' => 6034,
                'name' => 'Stanley',
                'country_id' => 227,
            ),
            346 => 
            array (
                'id' => 6035,
                'name' => 'Staveley',
                'country_id' => 227,
            ),
            347 => 
            array (
                'id' => 6036,
                'name' => 'Stevenage',
                'country_id' => 227,
            ),
            348 => 
            array (
                'id' => 6037,
                'name' => 'Stirling',
                'country_id' => 227,
            ),
            349 => 
            array (
                'id' => 6038,
                'name' => 'Stockport',
                'country_id' => 227,
            ),
            350 => 
            array (
                'id' => 6039,
                'name' => 'Stockton on Tees',
                'country_id' => 227,
            ),
            351 => 
            array (
                'id' => 6040,
                'name' => 'Stoke on Trent',
                'country_id' => 227,
            ),
            352 => 
            array (
                'id' => 6041,
                'name' => 'Stone',
                'country_id' => 227,
            ),
            353 => 
            array (
                'id' => 6042,
                'name' => 'Stowmarket',
                'country_id' => 227,
            ),
            354 => 
            array (
                'id' => 6043,
                'name' => 'Strabane',
                'country_id' => 227,
            ),
            355 => 
            array (
                'id' => 6044,
                'name' => 'Stranraer',
                'country_id' => 227,
            ),
            356 => 
            array (
                'id' => 6045,
                'name' => 'Stratford upon Avon',
                'country_id' => 227,
            ),
            357 => 
            array (
                'id' => 6046,
                'name' => 'Strood',
                'country_id' => 227,
            ),
            358 => 
            array (
                'id' => 6047,
                'name' => 'Stroud',
                'country_id' => 227,
            ),
            359 => 
            array (
                'id' => 6048,
                'name' => 'Sudbury',
                'country_id' => 227,
            ),
            360 => 
            array (
                'id' => 6049,
                'name' => 'Sunderland',
                'country_id' => 227,
            ),
            361 => 
            array (
                'id' => 6050,
                'name' => 'Sutton Coldfield',
                'country_id' => 227,
            ),
            362 => 
            array (
                'id' => 6051,
                'name' => 'Sutton in Ashfield',
                'country_id' => 227,
            ),
            363 => 
            array (
                'id' => 6052,
                'name' => 'Swadlincote',
                'country_id' => 227,
            ),
            364 => 
            array (
                'id' => 6053,
                'name' => 'Swanage',
                'country_id' => 227,
            ),
            365 => 
            array (
                'id' => 6054,
                'name' => 'Swanley',
                'country_id' => 227,
            ),
            366 => 
            array (
                'id' => 6055,
                'name' => 'Swansea',
                'country_id' => 227,
            ),
            367 => 
            array (
                'id' => 6056,
                'name' => 'Swindon',
                'country_id' => 227,
            ),
            368 => 
            array (
                'id' => 6057,
                'name' => 'Tadcaster',
                'country_id' => 227,
            ),
            369 => 
            array (
                'id' => 6058,
                'name' => 'Tadley',
                'country_id' => 227,
            ),
            370 => 
            array (
                'id' => 6059,
                'name' => 'Tain',
                'country_id' => 227,
            ),
            371 => 
            array (
                'id' => 6060,
                'name' => 'Talgarth',
                'country_id' => 227,
            ),
            372 => 
            array (
                'id' => 6061,
                'name' => 'Tamworth',
                'country_id' => 227,
            ),
            373 => 
            array (
                'id' => 6062,
                'name' => 'Taunton',
                'country_id' => 227,
            ),
            374 => 
            array (
                'id' => 6063,
                'name' => 'Tavistock',
                'country_id' => 227,
            ),
            375 => 
            array (
                'id' => 6064,
                'name' => 'Teignmouth',
                'country_id' => 227,
            ),
            376 => 
            array (
                'id' => 6065,
                'name' => 'Telford',
                'country_id' => 227,
            ),
            377 => 
            array (
                'id' => 6066,
                'name' => 'Tenby',
                'country_id' => 227,
            ),
            378 => 
            array (
                'id' => 6067,
                'name' => 'Tenterden',
                'country_id' => 227,
            ),
            379 => 
            array (
                'id' => 6068,
                'name' => 'Tetbury',
                'country_id' => 227,
            ),
            380 => 
            array (
                'id' => 6069,
                'name' => 'Tewkesbury',
                'country_id' => 227,
            ),
            381 => 
            array (
                'id' => 6070,
                'name' => 'Thame',
                'country_id' => 227,
            ),
            382 => 
            array (
                'id' => 6071,
                'name' => 'Thatcham',
                'country_id' => 227,
            ),
            383 => 
            array (
                'id' => 6072,
                'name' => 'Thaxted',
                'country_id' => 227,
            ),
            384 => 
            array (
                'id' => 6073,
                'name' => 'Thetford',
                'country_id' => 227,
            ),
            385 => 
            array (
                'id' => 6074,
                'name' => 'Thirsk',
                'country_id' => 227,
            ),
            386 => 
            array (
                'id' => 6075,
                'name' => 'Thornbury',
                'country_id' => 227,
            ),
            387 => 
            array (
                'id' => 6076,
                'name' => 'Thrapston',
                'country_id' => 227,
            ),
            388 => 
            array (
                'id' => 6077,
                'name' => 'Thurso',
                'country_id' => 227,
            ),
            389 => 
            array (
                'id' => 6078,
                'name' => 'Tilbury',
                'country_id' => 227,
            ),
            390 => 
            array (
                'id' => 6079,
                'name' => 'Tillicoultry',
                'country_id' => 227,
            ),
            391 => 
            array (
                'id' => 6080,
                'name' => 'Tipton',
                'country_id' => 227,
            ),
            392 => 
            array (
                'id' => 6081,
                'name' => 'Tiverton',
                'country_id' => 227,
            ),
            393 => 
            array (
                'id' => 6082,
                'name' => 'Tobermory',
                'country_id' => 227,
            ),
            394 => 
            array (
                'id' => 6083,
                'name' => 'Todmorden',
                'country_id' => 227,
            ),
            395 => 
            array (
                'id' => 6084,
                'name' => 'Tonbridge',
                'country_id' => 227,
            ),
            396 => 
            array (
                'id' => 6085,
                'name' => 'Torpoint',
                'country_id' => 227,
            ),
            397 => 
            array (
                'id' => 6086,
                'name' => 'Torquay',
                'country_id' => 227,
            ),
            398 => 
            array (
                'id' => 6087,
                'name' => 'Totnes',
                'country_id' => 227,
            ),
            399 => 
            array (
                'id' => 6088,
                'name' => 'Totton',
                'country_id' => 227,
            ),
            400 => 
            array (
                'id' => 6089,
                'name' => 'Towcester',
                'country_id' => 227,
            ),
            401 => 
            array (
                'id' => 6090,
                'name' => 'Tredegar',
                'country_id' => 227,
            ),
            402 => 
            array (
                'id' => 6091,
                'name' => 'Tregaron',
                'country_id' => 227,
            ),
            403 => 
            array (
                'id' => 6092,
                'name' => 'Tring',
                'country_id' => 227,
            ),
            404 => 
            array (
                'id' => 6093,
                'name' => 'Troon',
                'country_id' => 227,
            ),
            405 => 
            array (
                'id' => 6094,
                'name' => 'Trowbridge',
                'country_id' => 227,
            ),
            406 => 
            array (
                'id' => 6095,
                'name' => 'Truro',
                'country_id' => 227,
            ),
            407 => 
            array (
                'id' => 6096,
                'name' => 'Tunbridge Wells',
                'country_id' => 227,
            ),
            408 => 
            array (
                'id' => 6097,
                'name' => 'Tywyn',
                'country_id' => 227,
            ),
            409 => 
            array (
                'id' => 6098,
                'name' => 'Uckfield',
                'country_id' => 227,
            ),
            410 => 
            array (
                'id' => 6099,
                'name' => 'Ulverston',
                'country_id' => 227,
            ),
            411 => 
            array (
                'id' => 6100,
                'name' => 'Uppingham',
                'country_id' => 227,
            ),
            412 => 
            array (
                'id' => 6101,
                'name' => 'Usk',
                'country_id' => 227,
            ),
            413 => 
            array (
                'id' => 6102,
                'name' => 'Uttoxeter',
                'country_id' => 227,
            ),
            414 => 
            array (
                'id' => 6103,
                'name' => 'Ventnor',
                'country_id' => 227,
            ),
            415 => 
            array (
                'id' => 6104,
                'name' => 'Verwood',
                'country_id' => 227,
            ),
            416 => 
            array (
                'id' => 6105,
                'name' => 'Wadebridge',
                'country_id' => 227,
            ),
            417 => 
            array (
                'id' => 6106,
                'name' => 'Wadhurst',
                'country_id' => 227,
            ),
            418 => 
            array (
                'id' => 6107,
                'name' => 'Wakefield',
                'country_id' => 227,
            ),
            419 => 
            array (
                'id' => 6108,
                'name' => 'Wallasey',
                'country_id' => 227,
            ),
            420 => 
            array (
                'id' => 6109,
                'name' => 'Wallingford',
                'country_id' => 227,
            ),
            421 => 
            array (
                'id' => 6110,
                'name' => 'Walsall',
                'country_id' => 227,
            ),
            422 => 
            array (
                'id' => 6111,
                'name' => 'Waltham Abbey',
                'country_id' => 227,
            ),
            423 => 
            array (
                'id' => 6112,
                'name' => 'Waltham Cross',
                'country_id' => 227,
            ),
            424 => 
            array (
                'id' => 6113,
                'name' => 'Walton on Thames',
                'country_id' => 227,
            ),
            425 => 
            array (
                'id' => 6114,
                'name' => 'Walton on the Naze',
                'country_id' => 227,
            ),
            426 => 
            array (
                'id' => 6115,
                'name' => 'Wantage',
                'country_id' => 227,
            ),
            427 => 
            array (
                'id' => 6116,
                'name' => 'Ware',
                'country_id' => 227,
            ),
            428 => 
            array (
                'id' => 6117,
                'name' => 'Wareham',
                'country_id' => 227,
            ),
            429 => 
            array (
                'id' => 6118,
                'name' => 'Warminster',
                'country_id' => 227,
            ),
            430 => 
            array (
                'id' => 6119,
                'name' => 'Warrenpoint',
                'country_id' => 227,
            ),
            431 => 
            array (
                'id' => 6120,
                'name' => 'Warrington',
                'country_id' => 227,
            ),
            432 => 
            array (
                'id' => 6121,
                'name' => 'Warwick',
                'country_id' => 227,
            ),
            433 => 
            array (
                'id' => 6122,
                'name' => 'Washington',
                'country_id' => 227,
            ),
            434 => 
            array (
                'id' => 6123,
                'name' => 'Watford',
                'country_id' => 227,
            ),
            435 => 
            array (
                'id' => 6124,
                'name' => 'Wednesbury',
                'country_id' => 227,
            ),
            436 => 
            array (
                'id' => 6125,
                'name' => 'Wednesfield',
                'country_id' => 227,
            ),
            437 => 
            array (
                'id' => 6126,
                'name' => 'Wellingborough',
                'country_id' => 227,
            ),
            438 => 
            array (
                'id' => 6127,
                'name' => 'Wellington',
                'country_id' => 227,
            ),
            439 => 
            array (
                'id' => 6128,
                'name' => 'Wells',
                'country_id' => 227,
            ),
            440 => 
            array (
                'id' => 6129,
                'name' => 'Wells next the Sea',
                'country_id' => 227,
            ),
            441 => 
            array (
                'id' => 6130,
                'name' => 'Welshpool',
                'country_id' => 227,
            ),
            442 => 
            array (
                'id' => 6131,
                'name' => 'Welwyn Garden City',
                'country_id' => 227,
            ),
            443 => 
            array (
                'id' => 6132,
                'name' => 'Wem',
                'country_id' => 227,
            ),
            444 => 
            array (
                'id' => 6133,
                'name' => 'Wendover',
                'country_id' => 227,
            ),
            445 => 
            array (
                'id' => 6134,
                'name' => 'West Bromwich',
                'country_id' => 227,
            ),
            446 => 
            array (
                'id' => 6135,
                'name' => 'Westbury',
                'country_id' => 227,
            ),
            447 => 
            array (
                'id' => 6136,
                'name' => 'Westerham',
                'country_id' => 227,
            ),
            448 => 
            array (
                'id' => 6137,
                'name' => 'Westhoughton',
                'country_id' => 227,
            ),
            449 => 
            array (
                'id' => 6138,
                'name' => 'Weston super Mare',
                'country_id' => 227,
            ),
            450 => 
            array (
                'id' => 6139,
                'name' => 'Wetherby',
                'country_id' => 227,
            ),
            451 => 
            array (
                'id' => 6140,
                'name' => 'Weybridge',
                'country_id' => 227,
            ),
            452 => 
            array (
                'id' => 6141,
                'name' => 'Weymouth',
                'country_id' => 227,
            ),
            453 => 
            array (
                'id' => 6142,
                'name' => 'Whaley Bridge',
                'country_id' => 227,
            ),
            454 => 
            array (
                'id' => 6143,
                'name' => 'Whitby',
                'country_id' => 227,
            ),
            455 => 
            array (
                'id' => 6144,
                'name' => 'Whitchurch',
                'country_id' => 227,
            ),
            456 => 
            array (
                'id' => 6145,
                'name' => 'Whitehaven',
                'country_id' => 227,
            ),
            457 => 
            array (
                'id' => 6146,
                'name' => 'Whitley Bay',
                'country_id' => 227,
            ),
            458 => 
            array (
                'id' => 6147,
                'name' => 'Whitnash',
                'country_id' => 227,
            ),
            459 => 
            array (
                'id' => 6148,
                'name' => 'Whitstable',
                'country_id' => 227,
            ),
            460 => 
            array (
                'id' => 6149,
                'name' => 'Whitworth',
                'country_id' => 227,
            ),
            461 => 
            array (
                'id' => 6150,
                'name' => 'Wick',
                'country_id' => 227,
            ),
            462 => 
            array (
                'id' => 6151,
                'name' => 'Wickford',
                'country_id' => 227,
            ),
            463 => 
            array (
                'id' => 6152,
                'name' => 'Widnes',
                'country_id' => 227,
            ),
            464 => 
            array (
                'id' => 6153,
                'name' => 'Wigan',
                'country_id' => 227,
            ),
            465 => 
            array (
                'id' => 6154,
                'name' => 'Wigston',
                'country_id' => 227,
            ),
            466 => 
            array (
                'id' => 6155,
                'name' => 'Wigtown',
                'country_id' => 227,
            ),
            467 => 
            array (
                'id' => 6156,
                'name' => 'Willenhall',
                'country_id' => 227,
            ),
            468 => 
            array (
                'id' => 6157,
                'name' => 'Wincanton',
                'country_id' => 227,
            ),
            469 => 
            array (
                'id' => 6158,
                'name' => 'Winchester',
                'country_id' => 227,
            ),
            470 => 
            array (
                'id' => 6159,
                'name' => 'Windermere',
                'country_id' => 227,
            ),
            471 => 
            array (
                'id' => 6160,
                'name' => 'Winsford',
                'country_id' => 227,
            ),
            472 => 
            array (
                'id' => 6161,
                'name' => 'Winslow',
                'country_id' => 227,
            ),
            473 => 
            array (
                'id' => 6162,
                'name' => 'Wisbech',
                'country_id' => 227,
            ),
            474 => 
            array (
                'id' => 6163,
                'name' => 'Witham',
                'country_id' => 227,
            ),
            475 => 
            array (
                'id' => 6164,
                'name' => 'Withernsea',
                'country_id' => 227,
            ),
            476 => 
            array (
                'id' => 6165,
                'name' => 'Witney',
                'country_id' => 227,
            ),
            477 => 
            array (
                'id' => 6166,
                'name' => 'Woburn',
                'country_id' => 227,
            ),
            478 => 
            array (
                'id' => 6167,
                'name' => 'Woking',
                'country_id' => 227,
            ),
            479 => 
            array (
                'id' => 6168,
                'name' => 'Wokingham',
                'country_id' => 227,
            ),
            480 => 
            array (
                'id' => 6169,
                'name' => 'Wolverhampton',
                'country_id' => 227,
            ),
            481 => 
            array (
                'id' => 6170,
                'name' => 'Wombwell',
                'country_id' => 227,
            ),
            482 => 
            array (
                'id' => 6171,
                'name' => 'Woodbridge',
                'country_id' => 227,
            ),
            483 => 
            array (
                'id' => 6172,
                'name' => 'Woodstock',
                'country_id' => 227,
            ),
            484 => 
            array (
                'id' => 6173,
                'name' => 'Wootton Bassett',
                'country_id' => 227,
            ),
            485 => 
            array (
                'id' => 6174,
                'name' => 'Worcester',
                'country_id' => 227,
            ),
            486 => 
            array (
                'id' => 6175,
                'name' => 'Workington',
                'country_id' => 227,
            ),
            487 => 
            array (
                'id' => 6176,
                'name' => 'Worksop',
                'country_id' => 227,
            ),
            488 => 
            array (
                'id' => 6177,
                'name' => 'Worthing',
                'country_id' => 227,
            ),
            489 => 
            array (
                'id' => 6178,
                'name' => 'Wotton under Edge',
                'country_id' => 227,
            ),
            490 => 
            array (
                'id' => 6179,
                'name' => 'Wrexham',
                'country_id' => 227,
            ),
            491 => 
            array (
                'id' => 6180,
                'name' => 'Wymondham',
                'country_id' => 227,
            ),
            492 => 
            array (
                'id' => 6181,
                'name' => 'Yarm',
                'country_id' => 227,
            ),
            493 => 
            array (
                'id' => 6182,
                'name' => 'Yarmouth',
                'country_id' => 227,
            ),
            494 => 
            array (
                'id' => 6183,
                'name' => 'Yate',
                'country_id' => 227,
            ),
            495 => 
            array (
                'id' => 6184,
                'name' => 'Yateley',
                'country_id' => 227,
            ),
            496 => 
            array (
                'id' => 6185,
                'name' => 'Yeadon',
                'country_id' => 227,
            ),
            497 => 
            array (
                'id' => 6186,
                'name' => 'Yeovil',
                'country_id' => 227,
            ),
            498 => 
            array (
                'id' => 6187,
                'name' => 'York',
                'country_id' => 227,
            ),
            499 => 
            array (
                'id' => 6188,
                'name' => 'Vatican City',
                'country_id' => 95,
            ),
        ));
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 6189,
                'name' => 'Anegada',
                'country_id' => 30,
            ),
            1 => 
            array (
                'id' => 6190,
                'name' => 'Beef Island',
                'country_id' => 30,
            ),
            2 => 
            array (
                'id' => 6191,
                'name' => 'Jost Van Dyke',
                'country_id' => 30,
            ),
            3 => 
            array (
                'id' => 6192,
                'name' => 'N. Sound/Virgin Gorda',
                'country_id' => 30,
            ),
            4 => 
            array (
                'id' => 6193,
                'name' => 'Norman Island',
                'country_id' => 30,
            ),
            5 => 
            array (
                'id' => 6194,
                'name' => 'Port Purcell',
                'country_id' => 30,
            ),
            6 => 
            array (
                'id' => 6195,
                'name' => 'Road Town',
                'country_id' => 30,
            ),
            7 => 
            array (
                'id' => 6196,
                'name' => 'Sopers Hole',
                'country_id' => 30,
            ),
            8 => 
            array (
                'id' => 6197,
                'name' => 'Spanish Town',
                'country_id' => 30,
            ),
            9 => 
            array (
                'id' => 6198,
                'name' => 'Tortola',
                'country_id' => 30,
            ),
            10 => 
            array (
                'id' => 6199,
                'name' => 'Virgin Gorda',
                'country_id' => 30,
            ),
            11 => 
            array (
                'id' => 6200,
                'name' => 'Altona',
                'country_id' => 230,
            ),
            12 => 
            array (
                'id' => 6201,
                'name' => 'Anguilla',
                'country_id' => 230,
            ),
            13 => 
            array (
                'id' => 6202,
                'name' => 'Charlotte Amalie',
                'country_id' => 230,
            ),
            14 => 
            array (
                'id' => 6203,
                'name' => 'Christiansted',
                'country_id' => 230,
            ),
            15 => 
            array (
                'id' => 6204,
                'name' => 'Cruz Bay',
                'country_id' => 230,
            ),
            16 => 
            array (
                'id' => 6205,
                'name' => 'Enighed Pond',
                'country_id' => 230,
            ),
            17 => 
            array (
                'id' => 6206,
                'name' => 'Frederiksted',
                'country_id' => 230,
            ),
            18 => 
            array (
                'id' => 6207,
                'name' => 'Hovic',
                'country_id' => 230,
            ),
            19 => 
            array (
                'id' => 6208,
                'name' => 'Kingshill',
                'country_id' => 230,
            ),
            20 => 
            array (
                'id' => 6209,
                'name' => 'Limetree Bay',
                'country_id' => 230,
            ),
            21 => 
            array (
                'id' => 6210,
                'name' => 'Port Alucroix',
                'country_id' => 230,
            ),
            22 => 
            array (
                'id' => 6211,
                'name' => 'Saint Croix',
                'country_id' => 230,
            ),
            23 => 
            array (
                'id' => 6212,
                'name' => 'Saint John',
                'country_id' => 230,
            ),
            24 => 
            array (
                'id' => 6213,
                'name' => 'Saint Thomas',
                'country_id' => 230,
            ),
            25 => 
            array (
                'id' => 6214,
                'name' => 'Futuna Island Apt',
                'country_id' => 235,
            ),
            26 => 
            array (
                'id' => 6215,
                'name' => 'Leava',
                'country_id' => 235,
            ),
            27 => 
            array (
                'id' => 6216,
                'name' => 'Matautu',
                'country_id' => 235,
            ),
            28 => 
            array (
                'id' => 6217,
                'name' => 'Sigave',
                'country_id' => 235,
            ),
            29 => 
            array (
                'id' => 6218,
                'name' => 'Wallis Island Apt',
                'country_id' => 235,
            ),
            30 => 
            array (
                'id' => 6219,
                'name' => 'Bandele',
                'country_id' => 140,
            ),
            31 => 
            array (
                'id' => 6220,
                'name' => 'Bandrele',
                'country_id' => 140,
            ),
            32 => 
            array (
                'id' => 6221,
                'name' => 'Chiconi',
                'country_id' => 140,
            ),
            33 => 
            array (
                'id' => 6222,
                'name' => 'Chirongui',
                'country_id' => 140,
            ),
            34 => 
            array (
                'id' => 6223,
                'name' => 'Dzaoudzi',
                'country_id' => 140,
            ),
            35 => 
            array (
                'id' => 6224,
                'name' => 'Koungou',
                'country_id' => 140,
            ),
            36 => 
            array (
                'id' => 6225,
                'name' => 'Longoni',
                'country_id' => 140,
            ),
            37 => 
            array (
                'id' => 6226,
                'name' => 'Mamoudzou',
                'country_id' => 140,
            ),
            38 => 
            array (
                'id' => 6227,
                'name' => 'Mtsamboro',
                'country_id' => 140,
            ),
            39 => 
            array (
                'id' => 6228,
                'name' => 'Passamainty',
                'country_id' => 140,
            ),
            40 => 
            array (
                'id' => 6229,
                'name' => 'Sada',
                'country_id' => 140,
            ),
            41 => 
            array (
                'id' => 6230,
                'name' => 'Aruba',
                'country_id' => 155,
            ),
            42 => 
            array (
                'id' => 6231,
                'name' => 'Bonaire',
                'country_id' => 155,
            ),
            43 => 
            array (
                'id' => 6232,
                'name' => 'Curaçao',
                'country_id' => 155,
            ),
            44 => 
            array (
                'id' => 6233,
                'name' => 'Saba',
                'country_id' => 155,
            ),
            45 => 
            array (
                'id' => 6234,
                'name' => 'Sint Eustatius',
                'country_id' => 155,
            ),
            46 => 
            array (
                'id' => 6235,
                'name' => 'Sint Maarten',
                'country_id' => 155,
            ),
            47 => 
            array (
                'id' => 6236,
                'name' => 'Gagra',
                'country_id' => 239,
            ),
            48 => 
            array (
                'id' => 6237,
                'name' => 'Gali',
                'country_id' => 239,
            ),
            49 => 
            array (
                'id' => 6238,
                'name' => 'Gudauta',
                'country_id' => 239,
            ),
            50 => 
            array (
                'id' => 6239,
                'name' => 'New Athos',
                'country_id' => 239,
            ),
            51 => 
            array (
                'id' => 6240,
                'name' => 'Ochamchire',
                'country_id' => 239,
            ),
            52 => 
            array (
                'id' => 6241,
                'name' => 'Pitsunda',
                'country_id' => 239,
            ),
            53 => 
            array (
                'id' => 6242,
                'name' => 'Tkvarcheli',
                'country_id' => 239,
            ),
        ));
        
        
    }
}