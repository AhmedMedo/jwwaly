<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Beneficiary
 *
 * @property int $id
 * @property int $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $email
 * @property int|null $country_id
 * @property int|null $city_id
 * @property string|null $address
 * @property int|null $relation_id
 * @property int $is_active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Relation|null $relation
 * @method static \Illuminate\Database\Eloquent\Builder|Beneficiary newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Beneficiary newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Beneficiary query()
 * @method static \Illuminate\Database\Eloquent\Builder|Beneficiary whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Beneficiary whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Beneficiary whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Beneficiary whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Beneficiary whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Beneficiary whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Beneficiary whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Beneficiary whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Beneficiary whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Beneficiary wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Beneficiary whereRelationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Beneficiary whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Beneficiary whereUserId($value)
 */
	class Beneficiary extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\City
 *
 * @property int $id
 * @property string $name
 * @property int $country_id
 * @property int $is_active
 * @method static \Illuminate\Database\Eloquent\Builder|City newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|City newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|City query()
 * @method static \Illuminate\Database\Eloquent\Builder|City whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereName($value)
 * @mixin \Eloquent
 */
	class City extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Country
 *
 * @property int $id
 * @property string $name
 * @property int $code
 * @property string $iso_code
 * @property int $is_active
 * @method static \Illuminate\Database\Eloquent\Builder|Country newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Country newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Country query()
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereIsoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereName($value)
 * @mixin \Eloquent
 */
	class Country extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\FamilyMember
 *
 * @property int $id
 * @property int $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $email
 * @property int|null $country_id
 * @property int|null $city_id
 * @property string|null $address
 * @property int|null $relation
 * @property int $is_active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyMember newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyMember newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyMember query()
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyMember whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyMember whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyMember whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyMember whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyMember whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyMember whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyMember whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyMember whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyMember whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyMember wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyMember whereRelation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyMember whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyMember whereUserId($value)
 * @mixin \Eloquent
 * @property int|null $relation_id
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyMember whereRelationId($value)
 */
	class FamilyMember extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Package
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $cards
 * @property float|null $storage
 * @property string|null $unit
 * @property float|null $price
 * @property int|null $is_active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Package newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Package newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Package query()
 * @method static \Illuminate\Database\Eloquent\Builder|Package whereCards($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Package whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Package whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Package whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Package whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Package wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Package whereStorage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Package whereUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Package whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Package extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PaymentCard
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $card_number
 * @property string|null $card_holder_name
 * @property string|null $card_type
 * @property string|null $expiration_month
 * @property string|null $expiration_year
 * @property string|null $cvc
 * @property int|null $is_active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCard newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCard query()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCard whereCardHolderName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCard whereCardNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCard whereCardType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCard whereCvc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCard whereExpirationMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCard whereExpirationYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCard whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCard whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentCard whereUserId($value)
 * @mixin \Eloquent
 */
	class PaymentCard extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Relation
 *
 * @property int $id
 * @property string $title
 * @property int $is_active
 * @method static \Illuminate\Database\Eloquent\Builder|Relation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Relation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Relation query()
 * @method static \Illuminate\Database\Eloquent\Builder|Relation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Relation whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Relation whereTitle($value)
 * @mixin \Eloquent
 */
	class Relation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Subscription
 *
 * @property int $id
 * @property int $user_id
 * @property int $package_id
 * @property string|null $token
 * @property string|null $subscription_at
 * @property string|null $next_renewal_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription query()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereNextRenewalDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereSubscriptionAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereUserId($value)
 * @mixin \Eloquent
 */
	class Subscription extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Upload
 *
 * @property int $id
 * @property string|null $path
 * @property string|null $type
 * @property float|null $size
 * @property string|null $unit
 * @property int|null $is_active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Upload newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Upload newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Upload query()
 * @method static \Illuminate\Database\Eloquent\Builder|Upload whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Upload whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Upload whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Upload wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Upload whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Upload whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Upload whereUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Upload whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|Upload whereUserId($value)
 */
	class Upload extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $email
 * @property int|null $country_id
 * @property int|null $city_id
 * @property string|null $address
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string $status
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $total_ecard_sent
 * @property string|null $total_storage_used
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTotalEcardSent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTotalStorageUsed($value)
 */
	class User extends \Eloquent {}
}

